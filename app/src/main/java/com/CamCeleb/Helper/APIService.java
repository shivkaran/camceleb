package com.CamCeleb.Helper;

import com.CamCeleb.POJOClass.Celeb_Home_Request.Gson_Response_Celeb_Request_list;
import com.CamCeleb.POJOClass.Celeb_dashoad.Gson_response_celeb_dashbord;
import com.CamCeleb.POJOClass.Celeb_profile_details.Gson_Response_Celeb_profile_details;
import com.CamCeleb.POJOClass.Celebdetails.GsonResponse_celebDetails;
import com.CamCeleb.POJOClass.ChecksumGenrate.GsonResponse_checksume;
import com.CamCeleb.POJOClass.Comment.Comment_send.Gson_response_Comment_send;
import com.CamCeleb.POJOClass.Comment.Gson_response_comment;
import com.CamCeleb.POJOClass.DashboardList;
import com.CamCeleb.POJOClass.Favourite.Gson_Response_Favourite_data;
import com.CamCeleb.POJOClass.Login_model.Gson_response_login_data;
import com.CamCeleb.POJOClass.Notification.Gson_response_notification;
import com.CamCeleb.POJOClass.OTPVerification.Gson_Response_OTP_Resend;
import com.CamCeleb.POJOClass.RequestList.Gson_response_requestlist;
import com.CamCeleb.POJOClass.RequestVideo.Gson_Response_Requestvideo;
import com.CamCeleb.POJOClass.Request_Details.Gson_Response_Requestdetail;
import com.CamCeleb.POJOClass.Result;
import com.CamCeleb.POJOClass.SearchData.Gson_Response_search_data;


import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;


public interface APIService {

    @FormUrlEncoded
    @POST("user/gettoken")
    Call<Result> createUser(
            @Field("device_id") String tokenid,
            @Field("device_type") String tokentype);

    @FormUrlEncoded
    @POST("user/login")
    Call<Gson_response_login_data> UserLogin(
            @Field("email") String emailid,
            @Field("password") String passworddata,
            @Field("user_type") String usertype
    );


    @FormUrlEncoded
    @POST("user/forgotpassword")
    Call<Gson_Response_OTP_Resend> Forgotpassword(
            @Field("email") String emailid
    );


    @FormUrlEncoded
    @POST("user/resendemail")
    Call<Gson_Response_OTP_Resend> ResendOtp(
            @Field("email") String emailid,
            @Field("user_type") String user_type
    );

    @FormUrlEncoded
    @POST("user/changepassword")
    Call<Result> Changepassword(
            @Field("old_password") String old_password,
            @Field("new_password") String new_password
    );

    @FormUrlEncoded
    @POST("user/celebritylist")
    Call<Result> UserDashbord(
            @Field("celeb_id") String celeb_id,
            @Field("start") String start,
            @Field("limit") String limit
    );

    @Headers({"Content-Type: application/json"})
    @GET("user/masterdata")
    Call<Result> getMasterData();

    @Headers({"Content-Type: application/json"})
    @GET("user/logout")
    Call<Result> Logout();

   // @Headers({"Content-Type: application/json"})
    //@FormUrlEncoded
    @Multipart
    @POST("user/register")
    Call<Gson_response_login_data> RegisterUser(
            @Part("user_type") RequestBody user_type,
            @Part("full_name") RequestBody full_names,
            @Part("email") RequestBody emails,
            @Part("password") RequestBody passwords,
            @Part("phone_number") RequestBody phone_number,
            @Part("booking_price") RequestBody booking_price,
            @Part("social_platform_type") RequestBody social_platform_type,
            @Part("social_page_name") RequestBody social_page_name,
            @Part("followers_count") RequestBody followers_count,
            @Part("categories") RequestBody categories,
            @Part("about_me") RequestBody about_me,
            @Part MultipartBody.Part profile_pic);

    @FormUrlEncoded
    @POST("user/sociallogin")
    Call<Gson_response_login_data>SocialLogin(
            @Field("social_type") String social_type,
            @Field("is_provided_by_user") String is_provided_by_user,
            @Field("email") String email,
            @Field("social_id") String social_id,
            @Field("full_name") String full_name,
            @Field("phone_number") String Phonenumber
    );
    @FormUrlEncoded
    @POST("request/requestlist")
    Call<Result>CelebDash(
            @Field("start") String start,
            @Field("limit") String limit
    );




    /*@Multipart
    @POST("user/editprofile")
    Call<Result> EditProfile(
            @Part("user_type") RequestBody user_type,
            @Part("full_name") RequestBody full_names,
            @Part("email") RequestBody emails,
            @Part("password") RequestBody passwords,
            @Part("phone_number") RequestBody phone_number,
            @Part("booking_price") RequestBody booking_price,
            @Part("social_platform_type") RequestBody social_platform_type,
            @Part("social_page_name") RequestBody social_page_name,
            @Part("followers_count") RequestBody followers_count,
            @Part("categories") RequestBody categories,
            @Part("about_me") RequestBody about_me,
            @Part("intro_video") RequestBody intro_video,
            @Part("video_thumb") RequestBody video_thumb,
            @Part("is_online") RequestBody is_online,
            @Part("bank_name") RequestBody bank_name,
            @Part("account_number") RequestBody account_number,
            @Part("ifsc_code") RequestBody ifsc_code,
            @Part("branch_name") RequestBody branch_name,
            @Part MultipartBody.Part profile_pic);*/

        @Multipart
        @POST()
        Call<Result> VideoAccepct(
                @Part("request_id") RequestBody request_id,
                @Part("action")RequestBody action,
                @Part MultipartBody.Part video_thumb,
                @Part MultipartBody.Part video);

    @FormUrlEncoded
    @POST("user/celebritydetail")
    Call<GsonResponse_celebDetails> Celebritydetail(
            @Field("celeb_id") String celeb_id);


    @FormUrlEncoded
    @POST("user/verify")
    Call<Gson_Response_OTP_Resend> VerificationOtp(
            @Field("email") String emailid,
            @Field("otp") String str_otp
    );

    @FormUrlEncoded
    @POST("request/commentlist")
    Call<Gson_response_comment> CommentList(
            @Field("start") String start,
            @Field("limit") String limit,
            @Field("video_id") String video_id
    );

    @FormUrlEncoded
    @POST("request/addcomment")
    Call<Gson_response_Comment_send> CommentSend(
            @Field("video_id")String video_id,
            @Field("comment")String comment);


    @FormUrlEncoded
    @POST("request/likeunlike")
    Call<Result> LikeUnlike(
            @Field("video_id")String video_id);


    @FormUrlEncoded
    @POST("user/addremovefavourite")
    Call<Result> addremovefavourite(
            @Field("celeb_id") String str_video_id);


    @Multipart
    @POST("user/requestvideo")
    Call<Gson_Response_Requestvideo> requestvideo(
                                @Part("celeb_id") RequestBody celeb_id,
                                @Part("friend_name") RequestBody friend_name,
                                @Part("phone_number") RequestBody phone_number,
                                @Part("email") RequestBody email,
                                @Part("instructions") RequestBody instructions,
                                @Part("booking_for") RequestBody booking_for,
                                @Part("is_private") RequestBody is_private,
                                @Part MultipartBody.Part requestFile);
    @FormUrlEncoded
    @POST("user/requestvideo")
    Call<Gson_Response_Requestvideo> requestvideowithout_image(
            @Field("celeb_id")String celeb_id,
            @Field("friend_name")String friend_name,
            @Field("phone_number")String phone_number,
            @Field("email")String email,
            @Field("instructions")String instructions,
            @Field("booking_for")String booking_for,
            @Field("is_private")String is_private);


    @FormUrlEncoded
    @POST("user/notificationlist")
    Call<Gson_response_notification> NotificationList(
            @Field("start") String start,
            @Field("limit") String limit);


    @FormUrlEncoded
    @POST("user/search")
    Call<Gson_Response_search_data> SearchData(
            @Field("search_text") String search_text,
            @Field("start") String start,
            @Field("limit") String limit);



        @POST("celebrity/getprofiledetails")
    Call<Gson_Response_Favourite_data> FavouriteList();

    @FormUrlEncoded
    @POST("request/requestlist")
    Call<Gson_response_requestlist> RequestList(
            @Field("start") String start,
            @Field("limit") String limit);

    @FormUrlEncoded
    @POST("request/cancelrequest")
    Call<Result> CancelRequest(
            @Field("request_id") String request_id);


    @Multipart
    @POST("user/editprofile")
    Call<Gson_response_login_data> UpdateProfile_image(
            @Part("full_name") RequestBody full_name,
            @Part("phone_number") RequestBody phone_number,
            @Part MultipartBody.Part requestFile);

    @FormUrlEncoded
    @POST("user/editprofile")
    Call<Gson_response_login_data> UpdateProfile(
            @Field("full_name") String full_name,
            @Field("phone_number") String phone_number);

//    Celeb Api call

    @FormUrlEncoded
    @POST("celebrity/dashboard")
    Call<Gson_response_celeb_dashbord> CelebDashBord(
            @Field("start") String start,
            @Field("limit") String limit);

    @FormUrlEncoded
    @POST("user/editprofile")
    Call<Gson_response_login_data> UpdateProfileCeleb(
            @Field("is_online")String is_online);

    @FormUrlEncoded
    @POST("user/editprofile")
    Call<Gson_response_login_data> UpdateProfileCelebPrice(
            @Field("booking_price")String is_online);


    @FormUrlEncoded
    @POST("request/requestlist")
    Call<Gson_Response_Celeb_Request_list> CelebRequest(@Field("start") String start,
                                                        @Field("limit") String limit,
                                                        @Field("status") String status
                                                        );
    @FormUrlEncoded
    @POST("request/requestaction")
    Call<Result> AcceptReject(
            @Field("request_id")String request_id,
            @Field("action")String action);

    @FormUrlEncoded
    @POST("user/celebritydetail")
    Call<Gson_Response_Celeb_profile_details> CelebrityProfile(
            @Field("celeb_id") String user_id,
            @Field("start") String start,
            @Field("limit") String limit
            );

    @FormUrlEncoded
    @POST("user/editprofile")
    Call<Gson_response_login_data> UpdateBankDetails(
            @Field("owner_type") String owner_type,
            @Field("bank_name") String str_banckname,
            @Field("branch_name") String str_branch_name,
            @Field("account_number") String str_account_number,
            @Field("ifsc_code") String str_ifsc_code,
            @Field("beneficiary_name") String str_beneficiary_name);

    @Multipart
    @POST("user/editprofile")
    Call<Gson_response_login_data> UpdateCelebProfile_image(
            @Part("full_name") RequestBody full_name,
            @Part("social_platform_type") RequestBody social_platform_type,
            @Part("social_page_name")RequestBody social_page_name,
            @Part("about_me") RequestBody about_me,
            @Part("categories")RequestBody categories_id,
            @Part("phone_number")RequestBody phone_number,
            @Part MultipartBody.Part requestFile);

    @FormUrlEncoded
    @POST("request/requestdetail")
    Call<Gson_Response_Requestdetail> RequestDetails(
            @Field("request_id") String request_id);

    @Multipart
    @POST("request/requestaction")
    Call<Result> UpdateCelebVideo(
            @Part("request_id") RequestBody request_id,
            @Part("action") RequestBody action,
           @Part MultipartBody.Part requestFile,
            @Part MultipartBody.Part requestFile2);


    @FormUrlEncoded
    @POST("user/editprofile")
    Call<Gson_response_login_data> UpdateProfileCelebAllDetails(
            @Field("full_name")String full_name,
            @Field("phone_number")String phone_number,
            @Field("social_platform_type")String social_platform_type,
            @Field("social_page_name") String social_page_name,
            @Field("about_me")String about_me,
            @Field("categories")String categories_id);


    @FormUrlEncoded
    @POST("request/generatechecksum")
    Call<GsonResponse_checksume> Gereratechecksum(
            @Field("amount") String txn_amount,
            @Field("order_id") String order_id);

    @FormUrlEncoded
    @POST("request/paytmresponse")
    Call<Result> Paymentresponse(
            @Field("request_id") String order_id,
            @Field("transection_id") String transection_id,
            @Field("payment_json") String payment_json,
            @Field("status") String status);


    @FormUrlEncoded
    @POST("request/updaterequest")
    Call<Gson_Response_Requestvideo> requestvideowithout_image_updaterequest(
           @Field("request_id") String request_id,
           @Field("friend_name") String friend_name,
           @Field("phone_number") String phone_number,
           @Field("email") String email,
           @Field("instructions") String instructions,
           @Field("booking_for") String booking_for,
           @Field("is_private") String is_private);

    @Multipart
    @POST("request/updaterequest")
    Call<Gson_Response_Requestvideo> requestvideoUpdate(
            @Part("request_id") RequestBody request_id,
            @Part("friend_name") RequestBody friend_name,
            @Part("phone_number") RequestBody phone_number,
            @Part("email") RequestBody email,
            @Part("instructions")  RequestBody instructions,
            @Part("booking_for")  RequestBody booking_for,
            @Part("is_private") RequestBody is_private,
            @Part MultipartBody.Part requestFile);


    @FormUrlEncoded
    @POST("request/addreview")
    Call<Result> SaveFeedback(
           @Field("video_id") String str_id,
           @Field("ratings") String str_ratign,
           @Field("review") String str_feedback);

    @Multipart
    @POST("user/editprofile")
    Call<Gson_response_login_data> UpdateProfile_introvideo(
            @Part MultipartBody.Part requestFile,
            @Part MultipartBody.Part requestFile1);


    @FormUrlEncoded
    @POST("request/addreview")
    Call<Gson_response_comment> flagunflag(
            @Field("comment_id") String valueOf);
}





