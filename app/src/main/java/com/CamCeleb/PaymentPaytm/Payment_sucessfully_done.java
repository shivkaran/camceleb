package com.CamCeleb.PaymentPaytm;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.CamCeleb.APIs.DashBords;
import com.CamCeleb.CelebSide.Request_Details_screen;
import com.CamCeleb.POJOClass.Result;
import com.CamCeleb.POJOClass.ResultHandler;
import com.CamCeleb.R;

public class Payment_sucessfully_done extends AppCompatActivity {


    LinearLayout ll_sucess, ll_failed;
    String Order_id, status,transection_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_sucessfully_done);


        ll_sucess = findViewById(R.id.ll_sucess);
        ll_failed = findViewById(R.id.ll_failed);

        if (getIntent().getStringExtra("Order_id") != null) {
            Order_id = getIntent().getStringExtra("Order_id");
        }
        if (getIntent().getStringExtra("status") != null) {
            status = getIntent().getStringExtra("status");
        }
        if (getIntent().getStringExtra("transection_id") != null) {
            transection_id = getIntent().getStringExtra("transection_id");
        }


        if (status.equalsIgnoreCase("1")) {
            ll_sucess.setVisibility(View.VISIBLE);
            ll_failed.setVisibility(View.GONE);
        } else {
            ll_failed.setVisibility(View.VISIBLE);
            ll_sucess.setVisibility(View.GONE);
        }

        GetPaymentResponse();


    }

    private void GetPaymentResponse() {

        DashBords dashBords = new DashBords();
        dashBords.PaymentResponse(Order_id, transection_id,status, new ResultHandler<Result>() {
            @Override
            public void onSuccess(Result data) {

                if (data.getCode() == 1) {

                    if (status.equalsIgnoreCase("1")) {
                        startActivity(new Intent(Payment_sucessfully_done.this, Request_Details_screen.class).putExtra("request_id", Order_id));
                        finish();
                    } else {
                        onBackPressed();
                    }

                } else {
                    // Toast.makeText(Payment_sucessfully_done.this, data.getData().get("message").toString(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(String exx) {

            }
        });

    }
}
