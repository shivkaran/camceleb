package com.CamCeleb.PaymentPaytm;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.CamCeleb.Helper.JSONParser;
import com.CamCeleb.POJOClass.RequestVideo.Paytm;
import com.CamCeleb.POJOClass.RequestVideo.Response_request;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PaymentdoneActivity extends AppCompatActivity implements PaytmPaymentTransactionCallback {

    Response_request response_request;
    String amount;

    String MID;
    String ORDER_ID;
    String CUST_ID;
    String INDUSTRY_TYPE_ID;
    String CHANNEL_ID;
    int TXN_AMOUNT;
    String WEBSITE;
    String CALLBACK_URL;
    String CHECKSUMHASH;
    String MOBILE_NO;
    String EMAIL;

    String response = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //  setContentView(R.layout.paymentdone);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);


        long time = System.currentTimeMillis();

        if (getIntent() != null && getIntent().hasExtra("list_of_payment")) {
            response_request = new GsonBuilder().create().fromJson(getIntent().getStringExtra("list_of_payment"), Response_request.class);

            response = getIntent().getStringExtra("list_of_payment");

        }
        if (getIntent() != null && getIntent().hasExtra("amount")) {
            amount = getIntent().getStringExtra("amount");
        }
        ORDER_ID = response_request.getPaytm().getORDERID();



        PaytmPGService Service = PaytmPGService.getProductionService();


        JSONObject jsonObject = response_request.getPaytm().getJSONData();




        HashMap<String, String> yourHashMap = new Gson().fromJson(jsonObject.toString(), HashMap.class);
   //     HashMap<String,String> hashMap = convertToHashMap(response_request.getPaytm().toString());

        PaytmOrder Order = new PaytmOrder(yourHashMap);

        Service.initialize(Order, null);

        Service.startPaymentTransaction(PaymentdoneActivity.this, true, true,
                PaymentdoneActivity.this);



    }

    @Override
    public void onTransactionResponse(Bundle bundle) {

        String RESPCODE = bundle.getString("RESPCODE");
        String RESPMSG = bundle.getString("RESPMSG");
        String TXNID = bundle.getString("TXNID");

        if (RESPCODE.equals("01")) {

            // Toast.makeText(this, RESPMSG, Toast.LENGTH_SHORT).show();

            startActivity(new Intent(PaymentdoneActivity.this, Payment_sucessfully_done.class).putExtra("Order_id",ORDER_ID).putExtra("status","1").putExtra("transection_id",TXNID));
            finish();
//            onBackPressed();

        } else if (RESPCODE.equalsIgnoreCase("330")) {
            // Toast.makeText(this, RESPMSG + " Please Contact to Admin", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(PaymentdoneActivity.this, Payment_sucessfully_done.class).putExtra("Order_id",ORDER_ID).putExtra("status","0").putExtra("transection_id",TXNID));
            finish();
        }else {
            onBackPressed();
        }

        Log.e("RESPCODE ", " RESPCODE true " + bundle);
    }

    @Override
    public void networkNotAvailable() {
        onBackPressed();
    }

    @Override
    public void clientAuthenticationFailed(String s) {
        onBackPressed();
    }

    @Override
    public void someUIErrorOccurred(String s) {
        Log.e("checksum ", " ui fail respon  " + s);
        onBackPressed();
    }

    @Override
    public void onErrorLoadingWebPage(int i, String s, String s1) {
        Log.e("checksum ", " error loading pagerespon true " + s + "  s1 " + s1);
        onBackPressed();
    }

    @Override
    public void onBackPressedCancelTransaction() {
        Log.e("checksum ", " cancel call back respon  ");
        onBackPressed();
    }

    @Override
    public void onTransactionCancel(String s, Bundle bundle) {
        Log.e("checksum ", "  transaction cancel ");
        onBackPressed();

    }


}
