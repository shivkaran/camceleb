package com.CamCeleb.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.CamCeleb.APIs.DashBords;
import com.CamCeleb.POJOClass.Celeb_dashoad.Celeb_video;
import com.CamCeleb.POJOClass.RequestList.RequestList;
import com.CamCeleb.POJOClass.Result;
import com.CamCeleb.POJOClass.ResultHandler;
import com.CamCeleb.R;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class CelebVideoAdpterView extends RecyclerView.Adapter<CelebVideoAdpterView.ProductViewHolder> {


    //this context we will use to inflate the layout
    private Context mCtx;

    //we are storing all the products in a list
    private List<Celeb_video> actorList;

    //getting the context and product list with constructor


    public CelebVideoAdpterView(Context context, List<Celeb_video> list) {

        this.mCtx = context;
        actorList = list;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflating and returning our view holder
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.customuserrequestlist, null);
        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ProductViewHolder holder, final int position) {
        final Celeb_video actor = actorList.get(position);

        //binding the data with the viewholder views
        holder.textViewTitle.setText(actor.getUser().getFullName());

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
//        String dateString = formatter.format(new Date(actor.() * 1000L));


        holder.textViewShortDesc.setVisibility(View.GONE);
//        holder.imageView.setImageDrawable(mCtx.getResources().getDrawable(actor.getImage()));

        if (actor.getUser().getProfilePic().isEmpty())
        {
            holder.imageView.setImageResource(R.mipmap.profile_pic);

        }else {
            Picasso.with(mCtx).load(actor.getUser().getProfilePic()).into(holder.imageView);
        }
        holder.btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String request_id = String.valueOf(actor.getId());

                SetLikeUnlike(request_id,position);
            }
        });


    }

    private void SetLikeUnlike(String request_id, final int position) {

        DashBords dashBords = new DashBords();
        dashBords.CancelRequest(mCtx,request_id, new ResultHandler<Result>() {

            @Override
            public void onSuccess(Result data) {
                if (data.getCode() == 1) {
                    actorList.remove(position);
                    notifyItemChanged(position);
                }else {
                    // Toast.makeText(mCtx, "Request already cancelled", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(String exx) {

            }
        });
    }


    @Override
    public int getItemCount() {
        return actorList.size();
    }


    class ProductViewHolder extends RecyclerView.ViewHolder {

        LinearLayout linearLayout;
        TextView textViewTitle, textViewShortDesc;
        ImageView imageView,love;
        Button btn_cancel;

        public ProductViewHolder(View itemView) {
            super(itemView);

            CardView cardView=itemView.findViewById(R.id.user_see_usercardview);
            textViewTitle = itemView.findViewById(R.id.user_see_cust_actor_name);
            textViewShortDesc = itemView.findViewById(R.id.user_see_cust_actor_follwers);
            imageView = itemView.findViewById(R.id.user_see_cust_actor_iamge);
            btn_cancel = itemView.findViewById(R.id.btn_cancel);


        }
    }





}
