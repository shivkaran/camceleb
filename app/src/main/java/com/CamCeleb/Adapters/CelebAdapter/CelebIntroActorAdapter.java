package com.CamCeleb.Adapters.CelebAdapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.CamCeleb.APIs.DashBords;
import com.CamCeleb.Activity.CelebVideoPlayIntro;
import com.CamCeleb.POJOClass.Celeb_profile_details.Celeb_Testimonial;
import com.CamCeleb.POJOClass.Celebdetails.Testimonial;
import com.CamCeleb.POJOClass.Result;
import com.CamCeleb.POJOClass.ResultHandler;
import com.CamCeleb.R;
import com.CamCeleb.UserSide.CommentsActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CelebIntroActorAdapter extends RecyclerView.Adapter<CelebIntroActorAdapter.ProductViewHolder> {


    //this context we will use to inflate the layout
    private Context mCtx;

    //we are storing all the products in a list


    List<Celeb_Testimonial> data;

    //getting the context and product list with constructor
    /*public IntroActorAdapter(Context mCtx, List<ActorIntro> actorIntros1) {
        this.mCtx = mCtx;
        this.actorIntros = actorIntros1;
    }*/

    public CelebIntroActorAdapter(Context mCtx, List<Celeb_Testimonial> testimonials) {
        this.mCtx = mCtx;
        this.data = testimonials;
    }

    @Override
    public CelebIntroActorAdapter.ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflating and returning our view holder
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.customrecycleviewactorintro, null);
        return new CelebIntroActorAdapter.ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final CelebIntroActorAdapter.ProductViewHolder holder, final int position) {

        final Celeb_Testimonial testimonial = data.get(position);


        holder.username.setText(testimonial.getUser().getFullName());

      /*  Picasso.with(mCtx).load(testimonial.getUser().getProfilePic()).into(holder.intro_userimg);
        Picasso.with(mCtx).load(testimonial.getVideoThumb()).into(holder.img_video_thumb);*/



        if (testimonial.getVideoThumb().isEmpty() || testimonial.getVideoThumb().equals("") || testimonial.getVideoThumb().equals("null"))
        {
            holder.img_video_thumb.setBackgroundColor(mCtx.getResources().getColor(R.color.gry));

        }else {
            Picasso.with(mCtx).load(testimonial.getVideoThumb()).into(holder.img_video_thumb);
        }

        if (testimonial.getUser().getProfilePic().isEmpty() || testimonial.getVideoThumb().equals("") )
        {
            holder.intro_userimg.setImageResource(R.mipmap.profile_pic);

        }else {
            Picasso.with(mCtx).load(testimonial.getUser().getProfilePic()).into(holder.intro_userimg);
        }

        holder.txt_retaing.setText(String.valueOf(testimonial.getRating()));
        holder.intro_intro.setText(testimonial.getFeedback());
        holder.like.setText(String.valueOf(testimonial.getLikeCount()));
        holder.comment.setText(String.valueOf(testimonial.getCommentCount()));


        holder.ll_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String str_video_id = String.valueOf(data.get(position).getId());

                Intent intent = new Intent(mCtx, CommentsActivity.class);
                intent.putExtra("video_id", str_video_id);
                intent.putExtra("type", "celeb");
                mCtx.startActivity(intent);
            }
        });
       /* holder.ll_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String str_video_id = String.valueOf(data.get(position).getId());
                boolean is_liked = data.get(position).getIsLiked();
                boolean islike;

                int count_like = testimonial.getLikeCount();

                if (is_liked) {
                    data.get(position).setIsLiked(false);
                    islike = false;
                    count_like = count_like - 1;
                    data.get(position).setLikeCount(count_like);
                } else {
                    data.get(position).setIsLiked(true);
                    islike = true;
                    count_like = count_like + 1;
                    data.get(position).setLikeCount(count_like);
                }

                SetLikeUnlike(str_video_id, holder.like, count_like, islike);
                notifyItemChanged(position);
            }
        });*/


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String str_video_url = data.get(position).getVideo();
                String str_like_count = String.valueOf(data.get(position).getLikeCount());
                String str_comment_count = String.valueOf(data.get(position).getCommentCount());
                String str_id = String.valueOf(data.get(position).getId());
                String str_name = String.valueOf(data.get(position).getUser().getFullName());
                String str_profile_image = String.valueOf(data.get(position).getUser().getProfilePic());


                Intent intent = new Intent(mCtx, CelebVideoPlayIntro.class);
                intent.putExtra("str_video_url", str_video_url);
                intent.putExtra("str_like_count", str_like_count);
                intent.putExtra("str_comment_count", str_comment_count);
                intent.putExtra("str_id", str_id);
                intent.putExtra("str_name", str_name);
                intent.putExtra("str_profile_image", str_profile_image);

                mCtx.startActivity(intent);

            }
        });


    }

    private void SetLikeUnlike(String str_video_id, final TextView like, final int count_like, final boolean is_liked) {
        DashBords dashBords = new DashBords();
        dashBords.LikeUnlike(str_video_id, new ResultHandler<Result>() {


            @Override
            public void onSuccess(Result data) {
                if (data.getCode() == 1) {

                    like.setText(String.valueOf(count_like));

                }
            }

            @Override
            public void onFailure(String exx) {

            }
        });
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setDataChange(List<Celeb_Testimonial> testimonials) {
        data = testimonials;
        notifyDataSetChanged();
    }


    class ProductViewHolder extends RecyclerView.ViewHolder {

        TextView username, like, comment, txt_retaing, intro_intro;
        ImageView intro_userimg, img_video_thumb;

        LinearLayout ll_like, ll_comment;

        public ProductViewHolder(View itemView) {
            super(itemView);

            CardView cardView = itemView.findViewById(R.id.usercardviewcustm);
            username = itemView.findViewById(R.id.intro_username);
            intro_intro = itemView.findViewById(R.id.intro_intro);
            like = itemView.findViewById(R.id.intro_likes);
            comment = itemView.findViewById(R.id.intro_comments);
            intro_userimg = itemView.findViewById(R.id.intro_userimg);
            img_video_thumb = itemView.findViewById(R.id.img_video_thumb);
            txt_retaing = itemView.findViewById(R.id.txt_retaing);
            ll_like = itemView.findViewById(R.id.ll_like);
            ll_comment = itemView.findViewById(R.id.ll_comment);


           /* cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mCtx, CelebVideoPlayIntro.class);
                    mCtx.startActivity(intent);
                }
            });*/


        }
    }
}
