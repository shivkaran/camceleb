package com.CamCeleb.Adapters.CelebAdapter;

import android.content.Context;
import android.content.Intent;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.CamCeleb.CelebSide.CaptureVideo;
import com.CamCeleb.CelebSide.Request_Details_screen;
import com.CamCeleb.POJOClass.Celeb_Home_Request.Celeb_Reuest_list;
import com.CamCeleb.R;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Celeb_Pending_Adapter extends RecyclerView.Adapter<Celeb_Pending_Adapter.ProductViewHolder> {

    private Context mCtx;

    List<Celeb_Reuest_list> data;
    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;


    public Celeb_Pending_Adapter(Context mCtx, List<Celeb_Reuest_list> list) {

        this.mCtx = mCtx;
        data = list;

    }


    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.list_of_celeb_pending_request, null);
        return new ProductViewHolder(view);


    }

    @Override
    public void onBindViewHolder(final ProductViewHolder holder, final int position) {

       /* holder.names.setText(actorList.getData()[position].getName());

        holder.textViewTitle.setText(actorList.getData()[position].getData().get(0).getFullName());
        Picasso.with(mCtx).load(actorList.getData()[position].getData().get(0).getProfilePic()).into(holder.imageView);
*/


        long time = data.get(position).getDate() * 1000L;

        String timestr = getTimeAgo(time);

     /*   if (data.get(position).getRequestedBy().getProfilePic().isEmpty())
        {
            holder.img_profile.setImageDrawable(mCtx.getResources().getDrawable(R.mipmap.upload_pic));
        }else {
            Picasso.with(mCtx).load(data.get(position).getRequestedBy().getProfilePic()).into(holder.img_profile);
        }
*/

        if (data.get(position).getFriendImage().isEmpty()) {
            if (data.get(position).getFriendName().isEmpty()) {
              //  Picasso.with(mCtx).load(data.get(position).getRequestedBy().getProfilePic()).into(holder.img_profile);
                if (data.get(position).getRequestedBy().getProfilePic().isEmpty()) {
                    holder.img_profile.setImageDrawable(mCtx.getResources().getDrawable(R.mipmap.profile_pic));
                } else {
                    Picasso.with(mCtx).load(data.get(position).getRequestedBy().getProfilePic()).into(holder.img_profile);
                }
            } else {
                holder.img_profile.setImageDrawable(mCtx.getResources().getDrawable(R.mipmap.profile_pic));
            }
        } else {
            Picasso.with(mCtx).load(data.get(position).getFriendImage()).into(holder.img_profile);
        }


        holder.txt_instructions.setText(data.get(position).getRequestedBy().getFullName());


        // date of diffrents get

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String dateString = formatter.format(new Date());


        Calendar cal = GregorianCalendar.getInstance();
        cal.setTime(new Date(time));
        cal.add(Calendar.DAY_OF_YEAR, 7);
        Date sevendate = cal.getTime();
        SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy");
        final String date = sdf1.format(sevendate);


        int dateDifference = (int) getDateDiff(new SimpleDateFormat("dd/MM/yyyy"), dateString, date) - 1;

        if (dateDifference < 1) {
            String date_convert = convertDate(String.valueOf(time), "dd MMM yyyy");
            holder.txt_time.setText(date_convert);
        } else {
            System.out.println("dateDifference: " + dateDifference);
            holder.txt_time.setText(String.valueOf(dateDifference) + "d left");
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String request_id = data.get(position).getId();
                String status = data.get(position).getStatus();
                String dateof_reminder = holder.txt_time.getText().toString();
                String profile_image = "";
                String instructions = data.get(position).getInstructions();
                String name_of_requested_by = data.get(position).getRequestedBy().getFullName();

                if (data.get(position).getFriendImage().isEmpty()) {
                    if (data.get(position).getFriendName().isEmpty()) {
                        profile_image = data.get(position).getRequestedBy().getProfilePic();
                        //    Picasso.with(mCtx).load(data.get(position).getRequestedBy().getProfilePic()).into(holder.img_profile);

                    } else {
                        profile_image = "";
                    }
                } else {
                    Picasso.with(mCtx).load(data.get(position).getFriendImage()).into(holder.img_profile);
                    profile_image = data.get(position).getFriendImage();
                }


                mCtx.startActivity(new Intent(mCtx, Request_Details_screen.class)
                        .putExtra("request_id", request_id)
                        .putExtra("status", status)
                        .putExtra("dateof_reminder", dateof_reminder)
                        .putExtra("profile_image", profile_image)
                        .putExtra("instructions", instructions)
                        .putExtra("name_of_requested_by", name_of_requested_by));

            }
        });

        holder.ll_capture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String profile_image = "";
                String instructions = data.get(position).getInstructions();
                String name_of_requested_by = "";
                String request_id = data.get(position).getId();

                if (data.get(position).getFriendName().isEmpty()) {
                    name_of_requested_by = data.get(position).getRequestedBy().getFullName();
                } else {
                    name_of_requested_by = data.get(position).getFriendName();
                }



                if (data.get(position).getFriendImage().isEmpty()) {

                    if (data.get(position).getFriendName().isEmpty()) {
                        profile_image = data.get(position).getRequestedBy().getProfilePic();
                        //    Picasso.with(mCtx).load(data.get(position).getRequestedBy().getProfilePic()).into(holder.img_profile);

                    } else {
                        profile_image = "";
                    }
                } else {
                    Picasso.with(mCtx).load(data.get(position).getFriendImage()).into(holder.img_profile);
                    profile_image = data.get(position).getFriendImage();
                }



                Intent intent = new Intent(mCtx, CaptureVideo.class);
                intent.putExtra("str_name", name_of_requested_by);
                intent.putExtra("str_profile_image", profile_image);
                intent.putExtra("instructions", instructions);
                intent.putExtra("Request_id", request_id);
                mCtx.startActivity(intent);
            }
        });

    }

    public static String convertDate(String dateInMilliseconds, String dateFormat) {
        return DateFormat.format(dateFormat, Long.parseLong(dateInMilliseconds)).toString();
    }

    public static long getDateDiff(SimpleDateFormat format, String oldDate, String newDate) {
        try {
            return TimeUnit.DAYS.convert(format.parse(newDate).getTime() - format.parse(oldDate).getTime(), TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }


    private String getTimeAgo(long time) {

        if (time < 1000000000000L) {
            time *= 1000;
        }

        long now = System.currentTimeMillis();
        if (time > now || time <= 0) {
            return null;
        }


        final long diff = now - time;
        if (diff < MINUTE_MILLIS) {
            return "just now";
        } else if (diff < 2 * MINUTE_MILLIS) {
            return "a minute ago";
        } else if (diff < 50 * MINUTE_MILLIS) {
            return diff / MINUTE_MILLIS + " minutes ago";
        } else if (diff < 90 * MINUTE_MILLIS) {
            return "an hour ago";
        } else if (diff < 24 * HOUR_MILLIS) {
            return diff / HOUR_MILLIS + " hours ago";
        } else if (diff < 48 * HOUR_MILLIS) {
            return "yesterday";
        } else {
//            return diff / DAY_MILLIS + " days ago";

            return "Last week";
        }
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setDataChange(List<Celeb_Reuest_list> list) {
        data = list;
        notifyDataSetChanged();
    }


    class ProductViewHolder extends RecyclerView.ViewHolder {


        ImageView img_profile;
        TextView txt_time, txt_instructions, txt_reject, txt_Accept;

        LinearLayout ll_capture;


        public ProductViewHolder(View itemView) {
            super(itemView);

            img_profile = itemView.findViewById(R.id.img_profile);
            txt_time = itemView.findViewById(R.id.txt_time);
            txt_instructions = itemView.findViewById(R.id.txt_instructions);

            ll_capture = itemView.findViewById(R.id.ll_capture);

           /* CardView cardView = itemView.findViewById(R.id.usercardview);

            names = itemView.findViewById(R.id.name_names);
            textViewTitle = itemView.findViewById(R.id.cust_actor_name);
            textViewShortDesc = itemView.findViewById(R.id.cust_actor_follwers);
            imageView = itemView.findViewById(R.id.cust_actor_iamge);
            love = itemView.findViewById(R.id.love);

            love.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (love.isClickable()) {
                        love.setImageResource(R.mipmap.favorites_added);
                    }
                }
            });


            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mCtx, ActorIntroductionPage.class);
                    mCtx.startActivity(intent);
                }
            });*/
        }
    }


}

