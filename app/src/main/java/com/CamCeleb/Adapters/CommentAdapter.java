package com.CamCeleb.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.CamCeleb.POJOClass.Comment.Response_comment_list;
import com.CamCeleb.R;
import com.squareup.picasso.Picasso;

import org.apache.commons.lang3.StringEscapeUtils;

import java.util.ArrayList;
import java.util.List;

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.MyViewHolder> {

    List<Response_comment_list> data;
    Context context;


    List<Integer> integerList = new ArrayList<>();

    int selecteditem = -1;

    public CommentAdapter(Context context, List<Response_comment_list> list) {

        this.context = context;
        this.data = list;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.customcommentslist, null);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        Response_comment_list response_comment_list = data.get(position);

        String str_id = String.valueOf(response_comment_list.getId());
//        String str_comment =response_comment_list.getComment();
        String str_comment = StringEscapeUtils.unescapeJava(response_comment_list.getComment());

        String str_is_flagged = response_comment_list.getIsFlagged();
        String str_full_name = response_comment_list.getUser().getFullName();
        String str_profile_pic = response_comment_list.getUser().getProfilePic();


        holder.comment_profile_comment.setText(str_comment);
        holder.comment_profile_name.setText(str_full_name);

        if (str_profile_pic.isEmpty() || str_profile_pic.equals(null) || str_profile_pic.equals("null")) {
//            holder.comment_profile_image.setImageDrawable(context.getResources().getDrawable(R.mipmap.profile_pic));
            holder.comment_profile_image.setImageResource(R.mipmap.profile_pic);
        } else {
            Picasso.with(context).load(str_profile_pic).into(holder.comment_profile_image);
        }

        if (response_comment_list.getIsFlagged().equals("0"))
        {
            holder.txt_flag.setText("Flag");

        }else {

            holder.txt_flag.setText("UnFlag");
        }


      /*  if (selecteditem == position)
        {
            holder.txt_flag.setText("UnFlag");
        }*/

        if (integerList.contains(position)) {
            holder.txt_flag.setText("UnFlag");
        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setDataList(List<Response_comment_list> response_comment_lists) {

        data = response_comment_lists;
        notifyDataSetChanged();
    }

    public void Flagset(int adapterPosition) {

        selecteditem = adapterPosition;
        integerList.add(adapterPosition);
        notifyDataSetChanged();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView comment_profile_name, comment_profile_comment;
        ImageView comment_profile_image;
        TextView txt_flag;

        public RelativeLayout viewBackground;
        public LinearLayout viewForeground;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            comment_profile_comment = itemView.findViewById(R.id.comment_profile_comment);
            comment_profile_name = itemView.findViewById(R.id.comment_profile_name);
            comment_profile_image = itemView.findViewById(R.id.comment_profile_image);
            viewBackground = itemView.findViewById(R.id.view_background);
            viewForeground = itemView.findViewById(R.id.view_foreground);
            txt_flag = itemView.findViewById(R.id.txt_flag);

        }
    }
}
