package com.CamCeleb.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.CamCeleb.APIs.DashBords;
import com.CamCeleb.Activity.ActorIntroductionPage;
import com.CamCeleb.POJOClass.Result;
import com.CamCeleb.POJOClass.ResultHandler;
import com.CamCeleb.POJOClass.SearchData.CelebOfDay;
import com.CamCeleb.POJOClass.SearchData.RecentSearch;
import com.CamCeleb.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class Search_data_Recent_Adapter extends RecyclerView.Adapter<Search_data_Recent_Adapter.MyViewHolder> {

    Context context;

    List<RecentSearch> data;

    public Search_data_Recent_Adapter(Context context, List<RecentSearch> celebOfDay) {
        this.context = context;
        this.data = celebOfDay;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.customrecyclerviewuser, null);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        holder.textViewTitle.setText(data.get(position).getFullName());

        if (data.get(position).getProfilePic().isEmpty()) {
            holder.imageView.setImageResource(R.mipmap.profile_pic);
        } else {
            Picasso.with(context).load(data.get(position).getProfilePic()).into(holder.imageView);

        }
        holder.cust_actor_follwers.setText(data.get(position).getFollowers());

        if (data.get(position).getIsFavourite())
        {
//            Picasso.with(mCtx).load(String.valueOf(mCtx.getResources().getDrawable(R.mipmap.celeb_profile_fav))).into(holder.love);
            holder.love.setImageDrawable(context.getResources().getDrawable(R.mipmap.celeb_profile_fav));

        }else {
            holder.love.setImageDrawable(context.getResources().getDrawable(R.mipmap.favorites));

        }
//        Picasso.with(mCtx).load(String.valueOf(actorList.get(position).getFaviourateCount())).into(holder.love);
        holder.img_scoial_type.setVisibility(View.VISIBLE);
        String social_platform_type = data.get(position).getSocial_platform_type();

        if (social_platform_type.equalsIgnoreCase("Facebook")) {
            holder.img_scoial_type.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_facebook));
        } else if (social_platform_type.equalsIgnoreCase("Twitter")) {
            holder.img_scoial_type.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_twitter));
        } else if (social_platform_type.equalsIgnoreCase("Instagram")) {
            holder.img_scoial_type.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_instagram));
        } else if (social_platform_type.equalsIgnoreCase("Youtube")) {
            holder.img_scoial_type.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_youtube));
        } else if (social_platform_type.equalsIgnoreCase("TikTok")) {
            holder.img_scoial_type.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_tiktok));
        }else {
            holder.img_scoial_type.setVisibility(View.GONE);
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id = String.valueOf(data.get(position).getId());
                Intent intent = new Intent(context, ActorIntroductionPage.class);
                intent.putExtra("id",id);
                context.startActivity(intent);
            }
        });

        holder.love.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String str_video_id = String.valueOf(data.get(position).getId());
                boolean is_liked = data.get(position).getIsFavourite();
                if (is_liked) {
                    holder.love.setImageDrawable(context.getResources().getDrawable(R.mipmap.favorites));
                    data.get(position).setIsFavourite(false);
                } else {
                    holder.love.setImageDrawable(context.getResources().getDrawable(R.mipmap.celeb_profile_fav));
                    data.get(position).setIsFavourite(true);
                }

                notifyItemChanged(position);

                SetLikeUnlike(str_video_id, holder.love,is_liked,position);

            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        LinearLayout linearLayout;
        TextView seeall, names;
        ImageView imageView, love,img_scoial_type;
        TextView textViewTitle, textViewShortDesc,cust_actor_follwers;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            CardView cardView = itemView.findViewById(R.id.usercardview);

            names = itemView.findViewById(R.id.name_names);
            textViewTitle = itemView.findViewById(R.id.cust_actor_name);
            imageView = itemView.findViewById(R.id.cust_actor_iamge);
            love = itemView.findViewById(R.id.love);
            img_scoial_type = itemView.findViewById(R.id.img_scoial_type);
            cust_actor_follwers = itemView.findViewById(R.id.cust_actor_follwers);

            love.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (love.isClickable()) {
                        love.setImageResource(R.mipmap.favorites_added);
                    }
                }
            });

        }
    }

    private void SetLikeUnlike(String str_video_id, ImageView love, final boolean is_liked, final int position) {
        DashBords dashBords = new DashBords();
        dashBords.addremovefavourite(str_video_id, new ResultHandler<Result>() {


            @Override
            public void onSuccess(Result data1) {
                if (data1.getCode() == 1) {
                   /* if (is_liked)
                    {
                        data.get(position).setIsFavourite(false);
                    }else {
                        data.get(position).setIsFavourite(true);
                    }

                    notifyItemChanged(position);*/
                }
            }

            @Override
            public void onFailure(String exx) {

            }
        });
    }
}
