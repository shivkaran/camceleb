package com.CamCeleb.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.CamCeleb.APIs.DashBords;
import com.CamCeleb.Activity.ActorIntroductionPage;
import com.CamCeleb.POJOClass.Dashboard.Response_dashboard_datum;
import com.CamCeleb.POJOClass.Result;
import com.CamCeleb.POJOClass.ResultHandler;
import com.CamCeleb.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ChildActorAdapter extends RecyclerView.Adapter<ChildActorAdapter.ProductViewHolder> {

    private Context mCtx;
    private List<Response_dashboard_datum> actorList;


    public ChildActorAdapter(Context context, List<Response_dashboard_datum> actorList) {
        this.mCtx = context;
        this.actorList = actorList;

    }


    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.customrecyclerviewuser, null);
        return new ChildActorAdapter.ProductViewHolder(view);


    }

    @Override
    public void onBindViewHolder(final ProductViewHolder holder, final int position) {

        //        holder.names.setText(actorList.getData()[position].getName());


        holder.img_scoial_type.setVisibility(View.VISIBLE);

        holder.textViewTitle.setText(actorList.get(position).getFullName());

        if (actorList.get(position).getProfilePic().isEmpty()) {
            holder.imageView.setImageResource(R.mipmap.profile_pic);
        } else {
            Picasso.with(mCtx).load(actorList.get(position).getProfilePic()).error(R.mipmap.profile_pic).into(holder.imageView);

        }
        holder.cust_actor_follwers.setText(actorList.get(position).getFollowers());

        if (actorList.get(position).getIsFavourite()) {
//            Picasso.with(mCtx).load(String.valueOf(mCtx.getResources().getDrawable(R.mipmap.celeb_profile_fav))).into(holder.love);
            holder.love.setImageDrawable(mCtx.getResources().getDrawable(R.mipmap.celeb_profile_fav));

        } else {
            holder.love.setImageDrawable(mCtx.getResources().getDrawable(R.mipmap.favorites));

        }
//        Picasso.with(mCtx).load(String.valueOf(actorList.get(position).getFaviourateCount())).into(holder.love);

        String social_platform_type = actorList.get(position).getSocial_platform_type();

        if (social_platform_type.equalsIgnoreCase("Facebook")) {
            holder.img_scoial_type.setImageDrawable(mCtx.getResources().getDrawable(R.drawable.ic_facebook));
        } else if (social_platform_type.equalsIgnoreCase("Twitter")) {
            holder.img_scoial_type.setImageDrawable(mCtx.getResources().getDrawable(R.drawable.ic_twitter));
        } else if (social_platform_type.equalsIgnoreCase("Instagram")) {
            holder.img_scoial_type.setImageDrawable(mCtx.getResources().getDrawable(R.drawable.ic_instagram));
        } else if (social_platform_type.equalsIgnoreCase("Youtube")) {
            holder.img_scoial_type.setImageDrawable(mCtx.getResources().getDrawable(R.drawable.ic_youtube));
        } else if (social_platform_type.equalsIgnoreCase("TikTok")) {
            holder.img_scoial_type.setImageDrawable(mCtx.getResources().getDrawable(R.drawable.ic_tiktok));
        }else {
            holder.img_scoial_type.setVisibility(View.GONE);
        }



        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id = String.valueOf(actorList.get(position).getId());
                Intent intent = new Intent(mCtx, ActorIntroductionPage.class);
                intent.putExtra("id", id);
                mCtx.startActivity(intent);
            }
        });

        holder.love.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String str_video_id = String.valueOf(actorList.get(position).getId());
                boolean is_liked = actorList.get(position).getIsFavourite();
                if (is_liked) {
                    holder.love.setImageDrawable(mCtx.getResources().getDrawable(R.mipmap.favorites));
                    actorList.get(position).setIsFavourite(false);
                } else {
                    holder.love.setImageDrawable(mCtx.getResources().getDrawable(R.mipmap.celeb_profile_fav));
                    actorList.get(position).setIsFavourite(true);
                }

                notifyItemChanged(position);

                SetLikeUnlike(str_video_id, holder.love, is_liked, position);

            }
        });


    }

    private void SetLikeUnlike(String str_video_id, ImageView love, final boolean is_liked, final int position) {
        DashBords dashBords = new DashBords();
        dashBords.addremovefavourite(str_video_id, new ResultHandler<Result>() {


            @Override
            public void onSuccess(Result data) {
                if (data.getCode() == 1) {
                    if (is_liked) {
                        actorList.get(position).setIsFavourite(false);
                    } else {
                        actorList.get(position).setIsFavourite(true);
                    }

//                    notifyItemChanged(position);
                }
            }

            @Override
            public void onFailure(String exx) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return actorList.size();
    }


    class ProductViewHolder extends RecyclerView.ViewHolder {

        LinearLayout linearLayout;
        TextView seeall, names;
        ImageView imageView, love,img_scoial_type;
        TextView textViewTitle, textViewShortDesc, cust_actor_follwers;


        public ProductViewHolder(View itemView) {
            super(itemView);


            CardView cardView = itemView.findViewById(R.id.usercardview);

            names = itemView.findViewById(R.id.name_names);
            textViewTitle = itemView.findViewById(R.id.cust_actor_name);
            imageView = itemView.findViewById(R.id.cust_actor_iamge);
            love = itemView.findViewById(R.id.love);
            cust_actor_follwers = itemView.findViewById(R.id.cust_actor_follwers);
            img_scoial_type = itemView.findViewById(R.id.img_scoial_type);

          /*  love.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (love.isClickable()) {
                        love.setImageResource(R.mipmap.favorites_added);
                    }
                }
            });
*/

        }
    }

}