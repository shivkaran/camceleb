package com.CamCeleb.Adapters;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import com.CamCeleb.Activity.ActorIntroductionPage;
import com.CamCeleb.POJOClass.DashboardList;
import com.CamCeleb.R;
import com.squareup.picasso.Picasso;

public class ActorAdapter extends RecyclerView.Adapter<ActorAdapter.ProductViewHolder> {

    private Context mCtx;


    private DashboardList actorList;
    private DashboardList actorList1;


    public ActorAdapter(Context mCtx, DashboardList productList) {
        this.mCtx = mCtx;
        this.actorList = productList;
        this.actorList1 = productList;
    }


    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.customrecyclerviewuser, null);
        return new ProductViewHolder(view);


    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position) {

        holder.names.setText(actorList.getData()[position].getName());

        holder.textViewTitle.setText(actorList.getData()[position].getData().get(0).getFullName());
        Picasso.with(mCtx).load(actorList.getData()[position].getData().get(0).getProfilePic()).into(holder.imageView);


    }



    @Override
    public int getItemCount() {
        return actorList.getData().length;
    }


    class ProductViewHolder extends RecyclerView.ViewHolder {

        LinearLayout linearLayout;
        TextView seeall, names;
        ImageView imageView, love;
        TextView textViewTitle, textViewShortDesc;


        public ProductViewHolder(View itemView) {
            super(itemView);


            CardView cardView = itemView.findViewById(R.id.usercardview);

            names = itemView.findViewById(R.id.name_names);
            textViewTitle = itemView.findViewById(R.id.cust_actor_name);
            textViewShortDesc = itemView.findViewById(R.id.cust_actor_follwers);
            imageView = itemView.findViewById(R.id.cust_actor_iamge);
            love = itemView.findViewById(R.id.love);

            love.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (love.isClickable()) {
                        love.setImageResource(R.mipmap.favorites_added);
                    }
                }
            });


            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mCtx, ActorIntroductionPage.class);
                    mCtx.startActivity(intent);
                }
            });
        }
    }



}

