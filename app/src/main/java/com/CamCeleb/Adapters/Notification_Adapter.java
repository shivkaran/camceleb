package com.CamCeleb.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.CamCeleb.CelebSide.CelebHomeActivity;
import com.CamCeleb.CelebSide.Request_Details_screen;
import com.CamCeleb.POJOClass.Notification.Notification_list_data;
import com.CamCeleb.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class Notification_Adapter extends RecyclerView.Adapter<Notification_Adapter.MyViewHolder> {


    Context context;
    List<Notification_list_data> data;

    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;

    String user_type;

    boolean flag = true;


    public Notification_Adapter(Context context, List<Notification_list_data> notificationList, String celeb) {

        this.context = context;
        data = notificationList;
        user_type = celeb;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_of_notification, parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        final Notification_list_data notification_list_data = data.get(position);

        String name = notification_list_data.getUser().getFullName();
        String description = notification_list_data.getText();
        String video_type = notification_list_data.getType();
        String photo = notification_list_data.getUser().getProfilePic();
        String type = notification_list_data.getType();


        long time = notification_list_data.getDate() * 1000L;

        String str_TimeAgo = getTimeAgo(time);

        holder.txt_time.setText(str_TimeAgo);




        holder.txt_name.setText(name);

        if (photo.isEmpty()) {
            holder.img_profile.setImageDrawable(context.getResources().getDrawable(R.mipmap.profile_pic));
        } else {
            if (!photo.equals("null") || !photo.equals(null)) {
                Picasso.with(context).load(photo) .into(holder.img_profile);
            } else {
                holder.img_profile.setImageDrawable(context.getResources().getDrawable(R.mipmap.profile_pic));
            }
        }

        if (type.equals("request_received") || type.equals("video_uploaded") || type.equals("request_accepted")) {

            holder.img_flow.setBackgroundResource(R.drawable.ic_bubbles);
        }

        if (type.equals("feedback_received")) {
            holder.img_flow.setBackgroundResource(R.drawable.ic_feedback_received);
        }

        if (type.equals("order_reminder")) {
            holder.img_flow.setBackgroundResource(R.drawable.ic_reminder);
        }
        if (type.equals("amount_received")) {
            holder.img_flow.setBackgroundResource(R.drawable.ic_income);
        }

        holder.txt_descripton.setText(description);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String type = notification_list_data.getType();
                String request_id = String.valueOf(notification_list_data.getVideoId());

                /*if (type.equalsIgnoreCase("video_uploaded")) {

                }*/
                if (type.equalsIgnoreCase("feedback_received")) {

                    context.startActivity(new Intent(context, CelebHomeActivity.class).putExtra("notificationtype","feedback_received"));

                } else {
                    context.startActivity(new Intent(context, Request_Details_screen.class).putExtra("request_id", request_id));
                }
            }
        });

    }

    private String getTimeAgo(long time) {

        if (time < 1000000000000L) {
            time *= 1000;
        }

        long now = System.currentTimeMillis();
        if (time > now || time <= 0) {
            return null;
        }


        final long diff = now - time;
        if (diff < MINUTE_MILLIS) {
            return "just now";
        } else if (diff < 2 * MINUTE_MILLIS) {
            return "a min ago";
        } else if (diff < 50 * MINUTE_MILLIS) {
            return diff / MINUTE_MILLIS + " min ago";
        } else if (diff < 90 * MINUTE_MILLIS) {
            return "an hour ago";
        } else if (diff < 24 * HOUR_MILLIS) {
            return diff / HOUR_MILLIS + " hours ago";
        } else if (diff < 48 * HOUR_MILLIS) {
            return "yesterday";
        } else {
            return diff / DAY_MILLIS + " days ago";

//            return "Last week";
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView img_profile, img_flow;
        TextView txt_name, txt_descripton, txt_time, txt_date_time;

        TextView txt_other_date_time;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            img_profile = itemView.findViewById(R.id.img_user_profile);
            txt_name = itemView.findViewById(R.id.txt_name);
            txt_descripton = itemView.findViewById(R.id.txt_descripton);
            txt_time = itemView.findViewById(R.id.txt_time);
            txt_date_time = itemView.findViewById(R.id.txt_date_time);
            img_flow = itemView.findViewById(R.id.img_flow);
            txt_other_date_time = itemView.findViewById(R.id.txt_other_date_time);
        }
    }
}
