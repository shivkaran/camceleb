package com.CamCeleb.Adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.CamCeleb.Activity.Category_list_screen;
import com.CamCeleb.POJOClass.DashBorsData;
import com.CamCeleb.POJOClass.DashboardList;
import com.CamCeleb.POJOClass.User;
import com.CamCeleb.R;
import com.google.gson.Gson;

import java.io.Serializable;

public class ParentActorAdapter extends RecyclerView.Adapter<ParentActorAdapter.ActorplaceHolder> {

    Context context;
    DashboardList actorList;


     RecyclerView.RecycledViewPool recycledViewPool=new RecyclerView.RecycledViewPool();

    public ParentActorAdapter(Context context, DashboardList actorList) {
        this.context = context;
        this.actorList = actorList;
    }


    @NonNull
    @Override
    public ActorplaceHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.parent_recycler, null);
        return new ParentActorAdapter.ActorplaceHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ActorplaceHolder holder, final int position) {

        holder.name.setText(actorList.getData()[position].getName());


        holder.seeall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String str_name = actorList.getData()[position].getName();
                DashBorsData dashBorsData = actorList.getData()[position];


                Intent i = new Intent(context, Category_list_screen.class);
                i.putExtra("list_of_actors", new Gson().toJson(dashBorsData));
                context.startActivity(i);


               // context.startActivity(new Intent(context, Category_list_screen.class).putExtra("list_of_actors", dashBorsData));


            }
        });


//        linearLayoutManager.setInitialPrefetchItemCount(actorList.getData()[position].getData().size());

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);

        ChildActorAdapter childActorAdapter = new ChildActorAdapter(context, actorList.getData()[position].getData());
        holder.recyclerView.setAdapter(childActorAdapter);
        holder.recyclerView.setLayoutManager(linearLayoutManager);
        holder.recyclerView.setRecycledViewPool(recycledViewPool);

    }

    @Override
    public int getItemCount() {
        return actorList.getData().length;
    }

    public class ActorplaceHolder extends RecyclerView.ViewHolder {

        TextView name,seeall;
        RecyclerView recyclerView;


        public ActorplaceHolder(@NonNull View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.name_names);
            recyclerView = itemView.findViewById(R.id.recy_single);
            seeall = itemView.findViewById(R.id.seeall);
        }
    }
}
