package com.CamCeleb.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.CamCeleb.APIs.DashBords;
import com.CamCeleb.CelebSide.Request_Details_screen;
import com.CamCeleb.POJOClass.ActorSee;
import com.CamCeleb.POJOClass.RequestList.RequestList;
import com.CamCeleb.POJOClass.Result;
import com.CamCeleb.POJOClass.ResultHandler;
import com.CamCeleb.R;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class UserAdpterView extends RecyclerView.Adapter<UserAdpterView.ProductViewHolder> {


    //this context we will use to inflate the layout
    private Context mCtx;

    //we are storing all the products in a list
    private List<RequestList> actorList;

    //getting the context and product list with constructor


    public UserAdpterView(Context context, List<RequestList> list) {

        this.mCtx = context;
        actorList = list;
    }

    @Override
    public UserAdpterView.ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflating and returning our view holder
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.customuserrequestlist, null);
        return new UserAdpterView.ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ProductViewHolder holder, final int position) {
        final RequestList actor = actorList.get(position);

        //binding the data with the viewholder views
        holder.textViewTitle.setText(actor.getCeleb().getFullName());

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
        String dateString = formatter.format(new Date(actor.getDate() * 1000L));

        String status = String.valueOf(actor.getStatus());

        if (status.equalsIgnoreCase("1"))
        {
            holder.btn_cancel.setBackground(mCtx.getResources().getDrawable(R.drawable.backdraceleb_orng_layout));
            holder.btn_cancel.setText("Pending");
        }
        else if (status.equalsIgnoreCase("2"))
        {
            holder.btn_cancel.setBackground(mCtx.getResources().getDrawable(R.drawable.backdraceleb_orng));
            holder.btn_cancel.setText("Accepted");

        }else if (status.equalsIgnoreCase("4"))
        {
            holder.btn_cancel.setBackground(mCtx.getResources().getDrawable(R.drawable.backdraceleb_light_gery));
            holder.btn_cancel.setText("Completed");
        }
        else if (status.equalsIgnoreCase("5"))
        {
            holder.btn_cancel.setBackground(mCtx.getResources().getDrawable(R.drawable.customusercanclebtn));
            holder.btn_cancel.setText("Cancelled");
        }else if (status.equalsIgnoreCase("3"))
        {
            holder.btn_cancel.setBackground(mCtx.getResources().getDrawable(R.drawable.customusercanclebtn));
            holder.btn_cancel.setText("Rejected");
        }else if (status.equalsIgnoreCase("8"))
        {
            holder.btn_cancel.setBackground(mCtx.getResources().getDrawable(R.drawable.backdraceleb_light_gery));
            holder.btn_cancel.setText("Expired");
        }


        holder.textViewShortDesc.setText(dateString);
//        holder.imageView.setImageDrawable(mCtx.getResources().getDrawable(actor.getImage()));


        if (actor.getCeleb().getProfilePic().isEmpty())
        {

        }else {
            Picasso.with(mCtx).load(actor.getCeleb().getProfilePic()).into(holder.imageView);
        }



       /* holder.btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String request_id = String.valueOf(actor.getId());



                //SetLikeUnlike(request_id,position);
            }
        });*/
       holder.itemView.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               String request_id = String.valueOf(actor.getId());

               mCtx.startActivity(new Intent(mCtx, Request_Details_screen.class).putExtra("request_id",request_id).putExtra("profile","profileview"));
           }
       });


    }

    private void SetLikeUnlike(String request_id, final int position) {

        DashBords dashBords = new DashBords();
        dashBords.CancelRequest(mCtx,request_id, new ResultHandler<Result>() {

            @Override
            public void onSuccess(Result data) {
                if (data.getCode() == 1) {
                    actorList.remove(position);
                    notifyItemChanged(position);
                }else {
                    // Toast.makeText(mCtx, "Request already cancelled", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(String exx) {

            }
        });
    }


    @Override
    public int getItemCount() {
        return actorList.size();
    }


    class ProductViewHolder extends RecyclerView.ViewHolder {

        LinearLayout linearLayout;
        TextView textViewTitle, textViewShortDesc;
        ImageView imageView,love;
        Button btn_cancel;

        public ProductViewHolder(View itemView) {
            super(itemView);

            CardView cardView=itemView.findViewById(R.id.user_see_usercardview);
            textViewTitle = itemView.findViewById(R.id.user_see_cust_actor_name);
            textViewShortDesc = itemView.findViewById(R.id.user_see_cust_actor_follwers);
            imageView = itemView.findViewById(R.id.user_see_cust_actor_iamge);
            btn_cancel = itemView.findViewById(R.id.btn_cancel);


        }
    }





}
