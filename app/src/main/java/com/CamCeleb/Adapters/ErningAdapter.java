package com.CamCeleb.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.CamCeleb.POJOClass.Celeb_Home_Request.Celeb_Reuest_list;
import com.CamCeleb.R;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class ErningAdapter extends RecyclerView.Adapter<ErningAdapter.MyViewHolder> {

    Context context;
    List<Celeb_Reuest_list> data;

    private int lastSelectedPosition = -1;


    public ErningAdapter(Context context, List<Celeb_Reuest_list> list) {
        this.context = context;
        data = list;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.customerninglist, null);


        return new MyViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        Celeb_Reuest_list celeb_reuest_list = data.get(position);

        String request_id = celeb_reuest_list.getId();
        String full_name = celeb_reuest_list.getRequestedBy().getFullName();
        String profile_pic = celeb_reuest_list.getRequestedBy().getProfilePic();
        String send_to_name = celeb_reuest_list.getCeleb().getFullName();
        String phone_no = celeb_reuest_list.getPhoneNumber();
        String booked_for = celeb_reuest_list.getRequestedBy().getFullName();
        String description = celeb_reuest_list.getInstructions();

        String price = celeb_reuest_list.getPrice();


        SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
        String dateString = formatter.format(new Date(celeb_reuest_list.getDate() * 1000L));
        holder.erning_profile_oder_date.setText("on " + dateString);




        if (profile_pic.equals(null) || profile_pic.equals("null")) {
            holder.erning_profile_image.setImageDrawable(context.getResources().getDrawable(R.mipmap.profile_pic));

        } else {
            Picasso.with(context).load(profile_pic).into(holder.erning_profile_image);
        }

        if (lastSelectedPosition == position)
        {
            holder.ll_deatils.setVisibility(View.VISIBLE);
        }else {
            holder.ll_deatils.setVisibility(View.GONE);
        }




        holder.erning_profile_name.setText(full_name);
        holder.erning_profile_oder_no.setText("Order ID:#CAM" + request_id);
        holder.erning_profile_send_to_value.setText(send_to_name);
        holder.erning_profile_phone_value.setText(phone_no);
        holder.erning_profile_messae.setText(description);
        holder.erning_profile_booked_for_value.setText(booked_for);


        if (!price.equals("null")) {

            int service_price = Integer.parseInt(price);

            double firstprice = service_price / 1.18;
            double ccTax = firstprice * 0.25;
            double gst = firstprice * 0.18;

            double earning = firstprice - ccTax;

            String string_v =   new DecimalFormat("#.##").format(Math.round(earning));


            holder.erning_profile_money.setText("+ ₹" + String.valueOf(string_v));
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (lastSelectedPosition == position)
                {
                    lastSelectedPosition = -1;
                }else {
                    lastSelectedPosition = position;

                }
                notifyDataSetChanged();
            }
        });


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView erning_profile_image;
        TextView erning_profile_name, erning_profile_oder_no, erning_profile_oder_date, erning_profile_send_to_value, erning_profile_phone_value;
        TextView erning_profile_booked_for_value, erning_profile_messae, erning_profile_money;
        LinearLayout ll_deatils;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            erning_profile_image = itemView.findViewById(R.id.erning_profile_image);
            erning_profile_name = itemView.findViewById(R.id.erning_profile_name);
            erning_profile_oder_no = itemView.findViewById(R.id.erning_profile_oder_no);
            erning_profile_oder_date = itemView.findViewById(R.id.erning_profile_oder_date);
            erning_profile_send_to_value = itemView.findViewById(R.id.erning_profile_send_to_value);
            erning_profile_phone_value = itemView.findViewById(R.id.erning_profile_phone_value);
            erning_profile_booked_for_value = itemView.findViewById(R.id.erning_profile_booked_for_value);
            erning_profile_messae = itemView.findViewById(R.id.erning_profile_messae);
            erning_profile_money = itemView.findViewById(R.id.erning_profile_money);
            ll_deatils = itemView.findViewById(R.id.ll_deatils);


        }
    }
}
