package com.CamCeleb.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.CamCeleb.Activity.ActorIntroductionPage;
import com.CamCeleb.Activity.CelebVideoPlayIntro;
import com.CamCeleb.POJOClass.Favourite.Favourite_list;
import com.CamCeleb.POJOClass.Favourite.Video_list;
import com.CamCeleb.R;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class User_Video_ProfileApdater extends RecyclerView.Adapter<User_Video_ProfileApdater.MyViewHolder> {
    Context context;

    LayoutInflater layoutInflater;

    List<Video_list> data_list;



    public User_Video_ProfileApdater(Context context, List<Video_list> favouriteList) {

        this.context = context;
        data_list = favouriteList;

    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.customcelebprofile, null);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        holder.names.setText(data_list.get(position).getUser().getFullName());

        if (data_list.get(position).getUser().getProfilePic().isEmpty()) {
            holder.Profile_image.setImageResource(R.mipmap.profile_pic);
        } else {
            Picasso.with(context).load(data_list.get(position).getUser().getProfilePic()).into(holder.Profile_image);

        }

        if (data_list.get(position).getVideoThumb().isEmpty()) {

            holder.Profile_image.setBackgroundColor(context.getResources().getColor(R.color.gry));


        } else {

            Picasso.with(context).load(data_list.get(position).getVideoThumb()).into(holder.Videothumb);

        }


        SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
        String dateString = formatter.format(new Date(data_list.get(position).getDate() * 1000L));

holder.celebprofile_time.setText(dateString);
//        Picasso.with(mCtx).load(String.valueOf(actorList.get(position).getFaviourateCount())).into(holder.love);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String id = String.valueOf(data_list.get(position).getId());

                String str_video_url = data_list.get(position).getVideo();
                String str_like_count = String.valueOf(data_list.get(position).getLikeCount());
                String str_comment_count = String.valueOf(data_list.get(position).getCommentCount());
                String str_id = String.valueOf(data_list.get(position).getId());
                String str_name = String.valueOf(data_list.get(position).getUser().getFullName());
                String str_profile_image = String.valueOf(data_list.get(position).getUser().getProfilePic());
                String is_my_video = String.valueOf(data_list.get(position).getIs_my_video());
                String feedback = data_list.get(position).getFeedback();
                String is_liked = String.valueOf(data_list.get(position).getIsLiked());


                Intent intent = new Intent(context, CelebVideoPlayIntro.class);
                intent.putExtra("str_video_url", str_video_url);
                intent.putExtra("str_like_count", str_like_count);
                intent.putExtra("str_comment_count", str_comment_count);
                intent.putExtra("str_id", str_id);
                intent.putExtra("str_name", str_name);
                intent.putExtra("is_liked", is_liked);
                intent.putExtra("str_profile_image", str_profile_image);
                if (feedback.equalsIgnoreCase("")) {
                    intent.putExtra("is_my_video", is_my_video);
                }
                context.startActivity(intent);


              /*  Intent intent = new Intent(context, ActorIntroductionPage.class);
                intent.putExtra("id",id);
                context.startActivity(intent);*/
            }
        });

    }


    @Override
    public int getItemCount() {
        return data_list.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {


        TextView celebprofile_time, names;
        ImageView Profile_image,Videothumb;



        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            names = itemView.findViewById(R.id.celebprofile_username);
            Profile_image = itemView.findViewById(R.id.celebprofile_userimg);
            Videothumb = itemView.findViewById(R.id.celebprofile_iamge);
            celebprofile_time = itemView.findViewById(R.id.celebprofile_time);

           /* love.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (love.isClickable()) {
                        love.setImageResource(R.mipmap.favorites_added);
                    }
                }
            });*/
        }
    }
}
