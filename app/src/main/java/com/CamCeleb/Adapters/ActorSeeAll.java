package com.CamCeleb.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.CamCeleb.APIs.DashBords;
import com.CamCeleb.Activity.ActorIntroductionPage;
import com.CamCeleb.POJOClass.ActorSee;
import com.CamCeleb.POJOClass.Dashboard.Response_dashboard_datum;
import com.CamCeleb.POJOClass.Result;
import com.CamCeleb.POJOClass.ResultHandler;
import com.CamCeleb.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ActorSeeAll extends RecyclerView.Adapter<ActorSeeAll.ProductViewHolder> {


    //this context we will use to inflate the layout
    private Context mCtx;

    //we are storing all the products in a list
    private List<Response_dashboard_datum> actorSees;

    //getting the context and product list with constructor
    public ActorSeeAll(Context mCtx, List<Response_dashboard_datum> actorSees1) {
        this.mCtx = mCtx;
        this.actorSees = actorSees1;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflating and returning our view holder
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.customseeallviews, null);
        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ProductViewHolder holder, final int position) {
        //getting the product of the specified position
        final Response_dashboard_datum actorSee = actorSees.get(position);

        //binding the data with the viewholder views
        holder.textViewTitle.setText(actorSee.getFullName());
        holder.textViewShortDesc.setText(actorSee.getFollowers());
//        holder.imageView.setImageDrawable(mCtx.getResources().getDrawable(actorSee.getImage()));



        String social_platform_type = actorSee.getSocial_platform_type();

        if (social_platform_type.equalsIgnoreCase("Facebook")) {
            holder.img_scoial_type.setImageDrawable(mCtx.getResources().getDrawable(R.drawable.ic_facebook));
        } else if (social_platform_type.equalsIgnoreCase("Twitter")) {
            holder.img_scoial_type.setImageDrawable(mCtx.getResources().getDrawable(R.drawable.ic_twitter));
        } else if (social_platform_type.equalsIgnoreCase("Instagram")) {
            holder.img_scoial_type.setImageDrawable(mCtx.getResources().getDrawable(R.drawable.ic_instagram));
        } else if (social_platform_type.equalsIgnoreCase("Youtube")) {
            holder.img_scoial_type.setImageDrawable(mCtx.getResources().getDrawable(R.drawable.ic_youtube));
        } else if (social_platform_type.equalsIgnoreCase("TikTok")) {
            holder.img_scoial_type.setImageDrawable(mCtx.getResources().getDrawable(R.drawable.ic_tiktok));
        }




        if (actorSee.getProfilePic().isEmpty()) {
            holder.imageView.setImageResource(R.mipmap.profile_pic);
        } else {
            Picasso.with(mCtx).load(actorSee.getProfilePic()).into(holder.imageView);

        }

        if (actorSee.getIsFavourite())
        {
            holder.love.setImageDrawable(mCtx.getResources().getDrawable(R.mipmap.celeb_profile_fav));
        }else {
            holder.love.setImageDrawable(mCtx.getResources().getDrawable(R.mipmap.favorites));
        }

        holder.love.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /*String str_video_id = String.valueOf(actorSee.getId());
                boolean is_liked = actorSee.getIsFavourite();*/
                String str_video_id = String.valueOf(actorSee.getId());
                boolean is_liked = actorSee.getIsFavourite();
                if (is_liked) {
                    holder.love.setImageDrawable(mCtx.getResources().getDrawable(R.mipmap.favorites));
                    actorSee.setIsFavourite(false);
                } else {
                    holder.love.setImageDrawable(mCtx.getResources().getDrawable(R.mipmap.celeb_profile_fav));
                    actorSee.setIsFavourite(true);
                }

                notifyItemChanged(position);
                SetLikeUnlike(str_video_id, holder.love,is_liked,position);

            }
        });



        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String id = String.valueOf(actorSee.getId());
                Intent intent = new Intent(mCtx, ActorIntroductionPage.class);
                intent.putExtra("id",id);
                mCtx.startActivity(intent);
            }
        });
    }

    private void SetLikeUnlike(String str_video_id, ImageView love, final boolean is_liked, final int position) {
        DashBords dashBords = new DashBords();
        dashBords.addremovefavourite(str_video_id, new ResultHandler<Result>() {


            @Override
            public void onSuccess(Result data) {
                if (data.getCode() == 1) {
                    /*if (is_liked)
                    {
                        actorSees.get(position).setIsFavourite(false);
                    }else {
                        actorSees.get(position).setIsFavourite(true);
                    }

                    notifyItemChanged(position);*/
                }
            }

            @Override
            public void onFailure(String exx) {

            }
        });
    }


    @Override
    public int getItemCount() {
        return actorSees.size();
    }


    class ProductViewHolder extends RecyclerView.ViewHolder {

        TextView textViewTitle, textViewShortDesc;
        ImageView imageView,love,img_scoial_type;

        public ProductViewHolder(View itemView) {
            super(itemView);

            CardView cardView=itemView.findViewById(R.id.see_usercardview);
            textViewTitle = itemView.findViewById(R.id.see_cust_actor_name);
            textViewShortDesc = itemView.findViewById(R.id.see_cust_actor_follwers);
            imageView = itemView.findViewById(R.id.see_cust_actor_iamge);
            img_scoial_type = itemView.findViewById(R.id.img_scoial_type);
            love=itemView.findViewById(R.id.see_love);



        /*    love.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (love.isClickable()){
                        love.setImageResource(R.mipmap.favorites_added);
                    }
                }
            });

            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(mCtx, ActorIntroductionPage.class);
                    mCtx.startActivity(intent);
                }
            });*/

        }
    }
}