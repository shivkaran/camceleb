package com.CamCeleb;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.CamCeleb.APIs.Authorization;
import com.CamCeleb.CelebSide.CelebHomeActivity;
import com.CamCeleb.Activity.LoginAndSignActivity;
import com.CamCeleb.POJOClass.OTPVerification.Gson_Response_OTP_Resend;
import com.CamCeleb.POJOClass.ResultHandler;
import com.chaos.view.PinView;

public class OtpCelebActivity extends AppCompatActivity {

    TextView textView, email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_celeb);
        getSupportActionBar().hide();


        ((PinView) findViewById(R.id.firstPinView_celeb)).setAnimationEnable(true);


        Button button = findViewById(R.id.otp_celeb_button);
        ImageView backbutton = findViewById(R.id.otpbackbtn);
        textView = findViewById(R.id.celeb_resendotp);
        email = findViewById(R.id.emailget_otp);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OtpCelebActivity.this, CelebHomeActivity.class);
                startActivity(intent);
            }
        });
        Bundle bundle = getIntent().getExtras();


        String emails = email.getText().toString();
        bundle.putString("email", emails.toString());
        email.setText(emails);

        backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OtpCelebActivity.this, LoginAndSignActivity.class);
                startActivity(intent);
            }
        });

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ProgressDialog progressDialog = new ProgressDialog(OtpCelebActivity.this);
                progressDialog.show();
                String emails = email.getText().toString();
                String otps = null;


                Authorization authorization = new Authorization();
                authorization.ResendOtp(emails, otps, new ResultHandler<Gson_Response_OTP_Resend>() {
                    @Override
                    public void onSuccess(Gson_Response_OTP_Resend data) {

                    }

                    @Override
                    public void onFailure(String exx) {

                    }
                });


                progressDialog.dismiss();
            }
        });
    }


}

