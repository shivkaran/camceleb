package com.CamCeleb.CelebSide;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.CamCeleb.APIs.Celeb_DashBords;
import com.CamCeleb.Adapters.ErningAdapter;
import com.CamCeleb.POJOClass.Celeb_Home_Request.Celeb_Reuest_list;
import com.CamCeleb.POJOClass.Celeb_Home_Request.Gson_Response_Celeb_Request_list;
import com.CamCeleb.POJOClass.ResultHandler;
import com.CamCeleb.R;
import com.facebook.shimmer.ShimmerFrameLayout;

import java.util.ArrayList;
import java.util.List;

public class EraningActivity extends AppCompatActivity {

    ImageView backbtn;

    RecyclerView list_of_earning;
    List<Celeb_Reuest_list> list;

    TextView txt_message;

    ShimmerFrameLayout parentShimmerLayout;

    RelativeLayout ll_back;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eraning);
        getSupportActionBar().hide();


        parentShimmerLayout = findViewById(R.id.parentShimmerLayout);

        parentShimmerLayout.startShimmerAnimation();


        backbtn = findViewById(R.id.erningback);
        ll_back = findViewById(R.id.ll_back);
        list_of_earning = findViewById(R.id.list_of_earning);
        txt_message = findViewById(R.id.txt_message);
        GetNewRequest();

        ll_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               onBackPressed();
            }
        });
    }



    private void GetNewRequest() {
        Celeb_DashBords dashBords = new Celeb_DashBords();

        dashBords.CelebDash("0", "100","4",new ResultHandler<Gson_Response_Celeb_Request_list>() {

            @Override
            public void onSuccess(Gson_Response_Celeb_Request_list data) {

                if (data.getSuccess() == 1) {

                    parentShimmerLayout.stopShimmerAnimation();
                    parentShimmerLayout.setVisibility(View.GONE);

                    list = new ArrayList<>();

                    if (data.getData().getList().size() != 0) {

                        for (int i = 0; i < data.getData().getList().size(); i++) {
                            Celeb_Reuest_list celeb_reuest_list = data.getData().getList().get(i);

                            if (celeb_reuest_list.getStatus().equalsIgnoreCase("4")) {
                                list.add(celeb_reuest_list);
                            }
                        }

                        if (list.size() != 0) {
                            txt_message.setVisibility(View.GONE);
                            ErningAdapter celeb_request_adapter = new ErningAdapter(EraningActivity.this, list);
                            list_of_earning.setAdapter(celeb_request_adapter);
                            list_of_earning.setLayoutManager(new LinearLayoutManager(EraningActivity.this));
                        }else {
                            txt_message.setVisibility(View.VISIBLE);
                        }

                    }else {
                        txt_message.setVisibility(View.VISIBLE);
                    }
                }else {
                    parentShimmerLayout.stopShimmerAnimation();
                    parentShimmerLayout.setVisibility(View.GONE);
                }

            }

            @Override
            public void onFailure(String exx) {
                parentShimmerLayout.stopShimmerAnimation();
                parentShimmerLayout.setVisibility(View.GONE);
            }
        });
    }



}
