package com.CamCeleb.CelebSide;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.CamCeleb.APIs.Celeb_DashBords;
import com.CamCeleb.APIs.DashBords;
import com.CamCeleb.Activity.CelebVideoPlayIntro;
import com.CamCeleb.POJOClass.Request_Details.Gson_Response_Requestdetail;
import com.CamCeleb.POJOClass.Result;
import com.CamCeleb.POJOClass.ResultHandler;
import com.CamCeleb.POJOClass.User;
import com.CamCeleb.R;
import com.CamCeleb.UserSide.Edit_Request_details;
import com.CamCeleb.UserSide.UserHomeActivity;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.concurrent.TimeUnit;

public class Request_Details_screen extends AppCompatActivity {


    String request_id, status, dateof_reminder, profile_image = "", instructions, name_of_requested_by, video, like_count, comment_count, price = "", transaction_id;

    ImageView img_back, img_profile;

    TextView txt_status, txt_time, txt_instructions, txt_request_name, txt_reject, txt_Accept_button, txt_make_video;

    LinearLayout ll_accept_reject, ll_makeVideo, ll_transection_details, ll_edit_cancel;

    TextView txt_earnng, txt_service_price, txt_total_amount, user_name, txt_cancel, txt_Edit;

    ShimmerFrameLayout parentShimmerLayout;

    String user_type = "";
    String profile = "";

    TextView txt_your_earnng, txt_serverce_free;

    boolean flag = false;

    public static SharedPreferences preferences;

    RelativeLayout ll_back;
    BroadcastReceiver broadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request__details_screen);
        getSupportActionBar().hide();

        parentShimmerLayout = findViewById(R.id.parentShimmerLayout);

        parentShimmerLayout.startShimmerAnimation();

        preferences = getSharedPreferences(getPackageName() + "_preferences", MODE_PRIVATE);
        String session = preferences.getString("SessionData", "");
        Gson gson = new Gson();
        User user = gson.fromJson(session, User.class);
        user_type = String.valueOf(user.getUserType());


        if (getIntent().getStringExtra("request_id") != null) {
            request_id = getIntent().getStringExtra("request_id");
        }
        if (getIntent().getStringExtra("profile") != null) {
            profile = getIntent().getStringExtra("profile");
        }

        img_back = findViewById(R.id.img_back);
        img_profile = findViewById(R.id.img_profile);
        txt_status = findViewById(R.id.txt_status);
        txt_time = findViewById(R.id.txt_time);
        txt_instructions = findViewById(R.id.txt_instructions);
        txt_request_name = findViewById(R.id.txt_request_name);
        txt_reject = findViewById(R.id.txt_reject);
        txt_Accept_button = findViewById(R.id.txt_Accept_button);
        ll_accept_reject = findViewById(R.id.ll_accept_reject);
        ll_makeVideo = findViewById(R.id.ll_makeVideo);
        txt_make_video = findViewById(R.id.txt_make_video);
        ll_transection_details = findViewById(R.id.ll_transection_details);
        txt_earnng = findViewById(R.id.txt_earnng);
        txt_service_price = findViewById(R.id.txt_service_price);
        txt_total_amount = findViewById(R.id.txt_total_amount);
        user_name = findViewById(R.id.user_name);
        ll_edit_cancel = findViewById(R.id.ll_edit_cancel);
        txt_cancel = findViewById(R.id.txt_cancel);
        txt_Edit = findViewById(R.id.txt_Edit);
        txt_your_earnng = findViewById(R.id.txt_your_earnng);
        txt_serverce_free = findViewById(R.id.txt_serverce_free);
        ll_back = findViewById(R.id.ll_back);

        ll_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        getRequestDetails();


        txt_reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SetAcceptReject(request_id, "3");

            }
        });
        txt_Accept_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                SetAcceptReject(request_id, "2");
            }
        });

        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SetRequestCancel(request_id);
            }
        });
        txt_Edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flag = true;
                startActivity(new Intent(Request_Details_screen.this, Edit_Request_details.class).putExtra("request_id", request_id));
            }
        });


        ll_makeVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (status.equalsIgnoreCase("2")) {
                    Intent intent = new Intent(Request_Details_screen.this, CaptureVideo.class);
                    intent.putExtra("str_name", name_of_requested_by);
                    intent.putExtra("str_profile_image", profile_image);
                    intent.putExtra("instructions", instructions);
                    intent.putExtra("Request_id", request_id);
                    startActivity(intent);
                } else {
                   /* String str_video_url = data_list.get(position).getVideo();
                    String str_like_count = String.valueOf(data_list.get(position).getLikeCount());
                    String str_comment_count = String.valueOf(data_list.get(position).getCommentCount());
                    String str_id = String.valueOf(data_list.get(position).getId());
                    String str_name = String.valueOf(data_list.get(position).getUser().getFullName());
                    String str_profile_image = String.valueOf(data_list.get(position).getUser().getProfilePic());


                    */

                   String string_name = user_name.getText().toString().trim();

                    Intent intent = new Intent(Request_Details_screen.this, CelebVideoPlayIntro.class);
                    intent.putExtra("str_video_url", video);
                    intent.putExtra("str_like_count", like_count);
                    intent.putExtra("str_comment_count", comment_count);
                    intent.putExtra("str_id", request_id);
                    intent.putExtra("str_name", string_name);
                    intent.putExtra("str_profile_image", profile_image);

                    startActivity(intent);
                }
            }
        });
        registerReceiver();

    }

    private void registerReceiver() {

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getStringExtra("type").equalsIgnoreCase("feedback_received")) {
                    startActivity(new Intent(context, CelebHomeActivity.class).putExtra("notificationtype", "feedback_received"));
                    finish();
                } else {

                    getRequestDetails();
                }
            }
        };
        registerReceiver(broadcastReceiver, new IntentFilter("pushnotifiaction"));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (status.equalsIgnoreCase("1") || status.equalsIgnoreCase("2")) {
            if (user_type.equalsIgnoreCase("1")) {
                if (profile.equalsIgnoreCase("")) {
                    startActivity(new Intent(this, UserHomeActivity.class));
                    finishAffinity();
                }
            }else {
                super.onBackPressed();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (broadcastReceiver != null) {
            unregisterReceiver(broadcastReceiver);
        }
    }

    private void SetRequestCancel(final String request_id) {


        final Dialog dialog = new Dialog(this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custom_alert_dailog);

        TextView text = (TextView) dialog.findViewById(R.id.txt_message);
        text.setText("Are you sure you want to cancel this request?");

        TextView dialogButton = dialog.findViewById(R.id.submit);
        TextView cancel = dialog.findViewById(R.id.cancel);

        cancel.setVisibility(View.VISIBLE);
        dialogButton.setText("Yes");
        cancel.setText("No");

        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DashBords dashBords = new DashBords();
                dashBords.CancelRequest(Request_Details_screen.this, request_id, new ResultHandler<Result>() {

                    @Override
                    public void onSuccess(Result data) {
                        if (data.getCode() == 1) {
                            getRequestDetails();
                        } else {

                        }
                    }

                    @Override
                    public void onFailure(String exx) {

                    }
                });
                dialog.dismiss();

            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

        dialog.show();


    }

    private void getRequestDetails() {

        Celeb_DashBords celeb_dashBords = new Celeb_DashBords();

        celeb_dashBords.RequestDetails(Request_Details_screen.this, request_id, new ResultHandler<Gson_Response_Requestdetail>() {
            @Override
            public void onSuccess(Gson_Response_Requestdetail data) {

                if (data.getSuccess() == 1) {
                    Interview(data);
                } else {

                }

            }

            @Override
            public void onFailure(String exx) {

            }
        });


    }

    private void Interview(Gson_Response_Requestdetail data) {


        int date_time = Integer.parseInt(data.getData().getDate());

        long time = date_time * 1000L;

        if (date_time != 0) {

            String date_convert = convertDate(String.valueOf(time), "dd MMM yyyy");
            dateof_reminder = String.valueOf(date_convert);

        } else {
            dateof_reminder = "";
        }
        status = String.valueOf(data.getData().getStatus());

        if (user_type.equalsIgnoreCase("1")) {

//            profile_image = data.getData().getCeleb().getProfilePic();
            name_of_requested_by = data.getData().getRequestedBy().getFullName();

            txt_request_name.setText("Requested by " + name_of_requested_by.trim());


        } else if (user_type.equalsIgnoreCase("2")) {
//            profile_image = data.getData().getRequestedBy().getProfilePic();
            name_of_requested_by = data.getData().getRequestedBy().getFullName();
            user_name.setText(data.getData().getRequestedBy().getFullName());

            txt_request_name.setText("Requested by " + name_of_requested_by.trim());
        }

        if (data.getData().getFriendImage().isEmpty()) {
            if (data.getData().getFriendName().isEmpty()) {
                profile_image = data.getData().getRequestedBy().getProfilePic();
                user_name.setText(data.getData().getRequestedBy().getFullName());

            } else {
                profile_image = "";
                user_name.setText(data.getData().getFriendName());

            }
        } else {

            profile_image = data.getData().getFriendImage();
            if (data.getData().getFriendName().isEmpty()) {
                user_name.setText(data.getData().getRequestedBy().getFullName());
            } else {
                user_name.setText(data.getData().getFriendName());
            }
        }


        if (profile_image.isEmpty()) {
            img_profile.setImageDrawable(getResources().getDrawable(R.mipmap.profile_pic));
        } else {
            Picasso.with(this).load(profile_image).into(img_profile);
        }




        instructions = data.getData().getInstructions();

        System.out.println(instructions);


        video = data.getData().getVideo();
        like_count = String.valueOf(data.getData().getLikeCount());
        comment_count = String.valueOf(data.getData().getCommentCount());
        price = String.valueOf(data.getData().getPrice());
        transaction_id = String.valueOf(data.getData().getTransactionId());

        if (!price.equals("")) {

            if (user_type.equalsIgnoreCase("1")) {

                int service_price = Integer.parseInt(price);
                double firstprice = service_price / 1.18;
                double gst = firstprice * 0.18;

                double earning = service_price - gst;

                int total_amount = (int) Math.round(service_price);
                int total_earning = (int) Math.round(earning);
                int total_ccTax = (int) Math.round(gst);

                txt_total_amount.setText(String.valueOf(price));
                txt_earnng.setText(String.valueOf(total_earning));
                txt_service_price.setText(String.valueOf(total_ccTax));

            } else {
                int service_price = Integer.parseInt(price);

                double firstprice = service_price / 1.18;
                double ccTax = firstprice * 0.25;
                double gst = firstprice * 0.18;

                double earning = firstprice - ccTax;

                int total_amount = (int) Math.round(firstprice);
                int total_earning = (int) Math.round(earning);
                int total_ccTax = (int) Math.round(ccTax);


                System.out.println("Service price " + earning);

                txt_total_amount.setText(String.valueOf(total_amount));
                txt_earnng.setText(String.valueOf(total_earning));
                txt_service_price.setText(String.valueOf(total_ccTax));
            }
        }


        txt_time.setText(dateof_reminder);
        user_name.setVisibility(View.VISIBLE);
        txt_instructions.setText(instructions);


        if (status.equalsIgnoreCase("1")) {


            if (date_time != 0) {

                SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                String dateString = formatter.format(new Date(time));


                Calendar cal = GregorianCalendar.getInstance();
                cal.setTime(new Date(time));
                cal.add(Calendar.DAY_OF_YEAR, 7);
                Date sevendate = cal.getTime();
                SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy");
                final String date = sdf1.format(sevendate);


                int dateDifference = (int) getDateDiff(new SimpleDateFormat("dd/MM/yyyy"), dateString, date) - 1;
                System.out.println("dateDifference: " + dateDifference);


                txt_time.setText(String.valueOf(dateDifference) + "d left");
            }
            if (user_type.equalsIgnoreCase("1")) {

                txt_status.setText("Pending");
                ll_accept_reject.setVisibility(View.GONE);
                ll_makeVideo.setVisibility(View.GONE);
                ll_edit_cancel.setVisibility(View.VISIBLE);
                ll_transection_details.setVisibility(View.VISIBLE);
                if (user_type.equalsIgnoreCase("1")) {

                    txt_your_earnng.setText("Celeb Price:");
                    txt_serverce_free.setText("GST:");

                }

            } else {
                txt_status.setText("Pending");

            }

        } else if (status.equalsIgnoreCase("2")) {

            if (user_type.equalsIgnoreCase("1")) {

                txt_status.setText("Accepted");
                txt_status.setBackground(getResources().getDrawable(R.drawable.backdraceleb_orng));
                ll_accept_reject.setVisibility(View.GONE);
                ll_makeVideo.setVisibility(View.GONE);
                ll_edit_cancel.setVisibility(View.GONE);
                ll_transection_details.setVisibility(View.VISIBLE);
                if (user_type.equalsIgnoreCase("1")) {
                    txt_your_earnng.setText("Celeb Price:");
                    txt_serverce_free.setText("GST:");

                }

            } else {
                txt_status.setBackground(getResources().getDrawable(R.drawable.backdraceleb_orng));
                txt_status.setText("Accepted");
                ll_accept_reject.setVisibility(View.GONE);
                ll_makeVideo.setVisibility(View.VISIBLE);

            }


        } else if (status.equalsIgnoreCase("3")) {

            txt_status.setBackground(getResources().getDrawable(R.drawable.customusercanclebtn));
            txt_status.setText("Rejected");
            ll_accept_reject.setVisibility(View.GONE);
            ll_makeVideo.setVisibility(View.GONE);
            ll_transection_details.setVisibility(View.VISIBLE);
            if (user_type.equalsIgnoreCase("1")) {
                txt_your_earnng.setText("Celeb Price:");
                txt_serverce_free.setText("GST:");
                ll_edit_cancel.setVisibility(View.GONE);

            }

        } else if (status.equalsIgnoreCase("4")) {
            txt_status.setBackground(getResources().getDrawable(R.drawable.backdraceleb_light_gery));
            txt_status.setText("Completed");
            ll_accept_reject.setVisibility(View.GONE);
            ll_makeVideo.setVisibility(View.VISIBLE);
            ll_transection_details.setVisibility(View.VISIBLE);
            txt_make_video.setText("View Video");
            if (user_type.equalsIgnoreCase("1")) {
                txt_your_earnng.setText("Celeb Price:");
                txt_serverce_free.setText("GST:");
                ll_makeVideo.setVisibility(View.VISIBLE);
                ll_edit_cancel.setVisibility(View.GONE);

            }


        } else if (status.equalsIgnoreCase("5")) {

            txt_status.setBackground(getResources().getDrawable(R.drawable.backdraceleb_light_gery));
            txt_status.setText("Cancelled");
            ll_accept_reject.setVisibility(View.GONE);
            ll_makeVideo.setVisibility(View.GONE);

            ll_transection_details.setVisibility(View.VISIBLE);
            if (user_type.equalsIgnoreCase("1")) {
                txt_your_earnng.setText("Celeb Price:");
                txt_serverce_free.setText("GST:");
                ll_edit_cancel.setVisibility(View.GONE);

            }


        } else if (status.equalsIgnoreCase("8")) {
            txt_status.setBackground(getResources().getDrawable(R.drawable.backdraceleb_light_gery));
            ll_accept_reject.setVisibility(View.GONE);
            ll_makeVideo.setVisibility(View.GONE);
            txt_status.setText("Expired");

            ll_transection_details.setVisibility(View.VISIBLE);
            if (user_type.equalsIgnoreCase("1")) {
                txt_your_earnng.setText("Celeb Price:");
                txt_serverce_free.setText("GST:");
                ll_edit_cancel.setVisibility(View.GONE);

            }
        } else {
            txt_status.setBackground(getResources().getDrawable(R.drawable.customusercanclebtn));
            txt_status.setText("Cancelled");
            ll_accept_reject.setVisibility(View.GONE);
            ll_makeVideo.setVisibility(View.GONE);

            ll_transection_details.setVisibility(View.VISIBLE);
            if (user_type.equalsIgnoreCase("1")) {
                txt_your_earnng.setText("Celeb Price:");
                txt_serverce_free.setText("GST:");
                ll_edit_cancel.setVisibility(View.GONE);

            }
        }

        parentShimmerLayout.stopShimmerAnimation();
        parentShimmerLayout.setVisibility(View.GONE);


    }

    public static long getDateDiff(SimpleDateFormat format, String oldDate, String newDate) {
        try {
            return TimeUnit.DAYS.convert(format.parse(newDate).getTime() - format.parse(oldDate).getTime(), TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static String convertDate(String dateInMilliseconds, String dateFormat) {
        return DateFormat.format(dateFormat, Long.parseLong(dateInMilliseconds)).toString();
    }

    private void SetAcceptReject(String request_id, final String action) {

        final ProgressDialog progressDialog = ProgressDialog.show(this, null, "Please wait", true, false);


        Celeb_DashBords celeb_dashBords = new Celeb_DashBords();

        celeb_dashBords.AcceptReject(request_id, action, new ResultHandler<Result>() {
            @Override
            public void onSuccess(Result response) {
                progressDialog.dismiss();
                if (response.getCode() == 1) {

                    getRequestDetails();
/*
                    Intent in = new Intent("pushnotifiaction");
                    Bundle extras = new Bundle();
                    extras.putString("status","update");
                    in.putExtras(extras);
                    sendBroadcast(in);*/

                }

            }

            @Override
            public void onFailure(String exx) {
                progressDialog.dismiss();
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();

        getRequestDetails();

    }


}
