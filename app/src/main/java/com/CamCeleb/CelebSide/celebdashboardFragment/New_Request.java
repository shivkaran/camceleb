package com.CamCeleb.CelebSide.celebdashboardFragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.CamCeleb.APIs.Celeb_DashBords;
import com.CamCeleb.Adapters.CelebAdapter.Celeb_Request_Adapter;
import com.CamCeleb.CelebSide.CaptureVideo;
import com.CamCeleb.POJOClass.Celeb_Home_Request.Celeb_Reuest_list;
import com.CamCeleb.POJOClass.Celeb_Home_Request.Gson_Response_Celeb_Request_list;
import com.CamCeleb.POJOClass.ResultHandler;
import com.CamCeleb.POJOClass.User;
import com.CamCeleb.R;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;


public class New_Request extends Fragment {

    RecyclerView list_of_new_reuest;

    List<Celeb_Reuest_list> list;

    LinearLayout ll_share;

    public static SharedPreferences preferences;

    ShimmerFrameLayout parentShimmerLayout;

    String user_id = "";
    String user_introvideo = "";
    String session;
    ImageView img_capturevide;
    TextView txt_message;

    String start = "0";
    String limit = "10";
    boolean isLoading = false;

    Celeb_Request_Adapter celeb_request_adapter;
    String is_last="";
    public New_Request() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_new__request, container, false);


        preferences = getActivity().getSharedPreferences(getActivity().getPackageName() + "_preferences", getActivity().MODE_PRIVATE);

        session = preferences.getString("SessionData", "");
        Gson gson = new Gson();
        User user = gson.fromJson(session, User.class);

        user_id = String.valueOf(user.getId());
        user_introvideo = String.valueOf(user.getIntroVideo());


        parentShimmerLayout = view.findViewById(R.id.parentShimmerLayout);

        parentShimmerLayout.startShimmerAnimation();


        System.out.println(user_id);


        list_of_new_reuest = view.findViewById(R.id.list_of_new_reuest);
        ll_share = view.findViewById(R.id.ll_share);
        img_capturevide = view.findViewById(R.id.img_capturevide);
        txt_message = view.findViewById(R.id.txt_message);


        ll_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!user_introvideo.equals("")) {
                    String subject = "Hey! Checkout my profile on CamCeleb.";

                    String body = subject + " \n\n https://www.camceleb.com/celebprofile/" + user_id;


                    Intent txtIntent = new Intent(android.content.Intent.ACTION_SEND);
                    txtIntent.setType("text/plain");
                    txtIntent.putExtra(android.content.Intent.EXTRA_TEXT, body);
                    startActivity(Intent.createChooser(txtIntent, "Share"));
                } else {
                    Intent intent = new Intent(getActivity(), CaptureVideo.class);
                    intent.putExtra("introvideo", "user_introvideo");
                    startActivity(intent);
                }


            }
        });

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        GetNewRequest();


        list_of_new_reuest.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                if (!isLoading) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == list.size() - 1) {
                        //bottom of list!

                        if (is_last.equals("N")) {
                            int countoflimit = Integer.parseInt(limit);

                            countoflimit = countoflimit + 10;

                            limit = String.valueOf(countoflimit);
                            GetNewRequest1();
                        }
//                        isLoading = true;
                    }
                }
            }
        });


    }

    private void GetNewRequest1() {


        Celeb_DashBords dashBords = new Celeb_DashBords();

        dashBords.CelebDash(start, limit,"1",new ResultHandler<Gson_Response_Celeb_Request_list>() {

            @Override
            public void onSuccess(Gson_Response_Celeb_Request_list data) {

                if (data.getSuccess() == 1) {

                    list = new ArrayList<>();

                     is_last = data.getData().getIsLast();

                    if (data.getData().getList() != null) {

                        if (data.getData().getList().size() != 0) {

                            for (int i = 0; i < data.getData().getList().size(); i++) {
                                Celeb_Reuest_list celeb_reuest_list = data.getData().getList().get(i);

                                if (celeb_reuest_list.getStatus().equalsIgnoreCase("1")) {
                                    list.add(celeb_reuest_list);
                                }
                            }


                            if (list.size() != 0){
                                celeb_request_adapter.setDataChange(list);
                            }else {
                                if (is_last.equals("N"))
                                {
                                    System.out.println("limit count "+ limit);

                                    int countoflimit = Integer.parseInt(limit);

                                    countoflimit = countoflimit + 20;

                                    limit = String.valueOf(countoflimit);
                                    GetNewRequest();

                                }
                            }



                        }
                    }
                } else {
                    parentShimmerLayout.stopShimmerAnimation();
                    parentShimmerLayout.setVisibility(View.GONE);
                }

            }

            @Override
            public void onFailure(String exx) {
                parentShimmerLayout.stopShimmerAnimation();
                parentShimmerLayout.setVisibility(View.GONE);

            }
        });
    }

    private void GetNewRequest() {

        Celeb_DashBords dashBords = new Celeb_DashBords();

        dashBords.CelebDash("0", "10","1", new ResultHandler<Gson_Response_Celeb_Request_list>() {

            @Override
            public void onSuccess(Gson_Response_Celeb_Request_list data) {

                if (data.getSuccess() == 1) {

                    list = new ArrayList<>();

                    parentShimmerLayout.stopShimmerAnimation();
                    parentShimmerLayout.setVisibility(View.GONE);
                    is_last = data.getData().getIsLast();
                    if (data.getData().getList() != null) {

                        if (data.getData().getList().size() != 0) {

                            for (int i = 0; i < data.getData().getList().size(); i++) {
                                Celeb_Reuest_list celeb_reuest_list = data.getData().getList().get(i);

                                if (celeb_reuest_list.getStatus().equalsIgnoreCase("1")) {
                                    list.add(celeb_reuest_list);
                                }
                            }

                            if (list.size() != 0) {
                                list_of_new_reuest.setVisibility(View.VISIBLE);
                                ll_share.setVisibility(View.GONE);
                            } /*else {



                                list_of_new_reuest.setVisibility(View.GONE);
                                ll_share.setVisibility(View.VISIBLE);
                            }*/
                            else {
                                if (is_last.equals("N"))
                                {
                                    System.out.println("limit count "+ limit);

                                    int countoflimit = Integer.parseInt(limit);

                                    countoflimit = countoflimit + 20;

                                    limit = String.valueOf(countoflimit);
                                    GetNewRequest1();


                                }else {
                                    list_of_new_reuest.setVisibility(View.GONE);
                                    ll_share.setVisibility(View.VISIBLE);
                                }
                            }

                            parentShimmerLayout.stopShimmerAnimation();
                            parentShimmerLayout.setVisibility(View.GONE);


                            celeb_request_adapter = new Celeb_Request_Adapter(getActivity(), list);
                            list_of_new_reuest.setAdapter(celeb_request_adapter);
                            list_of_new_reuest.setLayoutManager(new LinearLayoutManager(getActivity()));

                        } else {
                            if (!user_introvideo.equals("")) {
                                list_of_new_reuest.setVisibility(View.GONE);
                                ll_share.setVisibility(View.VISIBLE);
                                txt_message.setText("Tap to share your profile");
                            } else {
                                ll_share.setVisibility(View.VISIBLE);
                                img_capturevide.setBackgroundResource(R.drawable.ic_video_from_dashboard);
                                txt_message.setText("Tap to Capture Introduction Video");
                            }
                        }
                    } else {
                        if (!user_introvideo.equals("")) {
                            list_of_new_reuest.setVisibility(View.GONE);
                            ll_share.setVisibility(View.VISIBLE);
                            txt_message.setText("Tap to share your profile");
                        } else {
                            ll_share.setVisibility(View.VISIBLE);
                            img_capturevide.setBackgroundResource(R.drawable.ic_video_from_dashboard);
                            txt_message.setText("Tap to Capture Introduction Video");
                        }
                    }
                } else {
                    parentShimmerLayout.stopShimmerAnimation();
                    parentShimmerLayout.setVisibility(View.GONE);
                }

            }

            @Override
            public void onFailure(String exx) {
                parentShimmerLayout.stopShimmerAnimation();
                parentShimmerLayout.setVisibility(View.GONE);

            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void onResume() {
        super.onResume();
        GetNewRequest();

    }


}
