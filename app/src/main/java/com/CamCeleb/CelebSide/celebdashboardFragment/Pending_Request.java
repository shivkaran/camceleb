package com.CamCeleb.CelebSide.celebdashboardFragment;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.CamCeleb.APIs.Celeb_DashBords;
import com.CamCeleb.Adapters.CelebAdapter.Celeb_Pending_Adapter;
import com.CamCeleb.Adapters.CelebAdapter.Celeb_complate_Adapter;
import com.CamCeleb.POJOClass.Celeb_Home_Request.Celeb_Reuest_list;
import com.CamCeleb.POJOClass.Celeb_Home_Request.Gson_Response_Celeb_Request_list;
import com.CamCeleb.POJOClass.ResultHandler;
import com.CamCeleb.R;
import com.facebook.shimmer.ShimmerFrameLayout;

import java.util.ArrayList;
import java.util.List;

public class Pending_Request extends Fragment {


    RecyclerView list_of_new_reuest;

    List<Celeb_Reuest_list> list;
    ShimmerFrameLayout parentShimmerLayout;
    TextView txt_message;

    String start = "0";
    String limit = "10";
    boolean isLoading = false;

    Celeb_Pending_Adapter celeb_request_adapter;
    String is_last="";



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_pending__request, container, false);

        parentShimmerLayout = view.findViewById(R.id.parentShimmerLayout);

        parentShimmerLayout.startShimmerAnimation();

        list_of_new_reuest = view.findViewById(R.id.list_of_new_reuest);
        txt_message = view.findViewById(R.id.txt_message);


        return view;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        GetNewRequest();


        list_of_new_reuest.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                if (!isLoading) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == list.size() - 1) {
                        //bottom of list!

                        if (is_last.equals("N")) {
                            int countoflimit = Integer.parseInt(limit);

                            countoflimit = countoflimit + 10;

                            limit = String.valueOf(countoflimit);
                            GetNewRequest1();
                        }
//                        isLoading = true;
                    }
                }
            }
        });


    }

    private void GetNewRequest1() {
        Celeb_DashBords dashBords = new Celeb_DashBords();

        dashBords.CelebDash(start, limit,"2", new ResultHandler<Gson_Response_Celeb_Request_list>() {

            @Override
            public void onSuccess(Gson_Response_Celeb_Request_list data) {

                if (data.getSuccess() == 1) {

                    list = new ArrayList<>();
                    is_last = data.getData().getIsLast();

                    for (int i = 0; i < data.getData().getList().size(); i++) {
                        Celeb_Reuest_list celeb_reuest_list = data.getData().getList().get(i);

                        if (celeb_reuest_list.getStatus().equalsIgnoreCase("2")) {
                            list.add(celeb_reuest_list);
                        }
                    }

                    celeb_request_adapter.setDataChange(list);
                }else {
                    parentShimmerLayout.stopShimmerAnimation();
                    parentShimmerLayout.setVisibility(View.GONE);
                    txt_message.setVisibility(View.VISIBLE);
                    list_of_new_reuest.setVisibility(View.GONE);
                }

            }

            @Override
            public void onFailure(String exx) {

            }
        });
    }

    private void GetNewRequest() {
        Celeb_DashBords dashBords = new Celeb_DashBords();

        dashBords.CelebDash("0", "10","2", new ResultHandler<Gson_Response_Celeb_Request_list>() {

            @Override
            public void onSuccess(Gson_Response_Celeb_Request_list data) {

                if (data.getSuccess() == 1) {

                    list = new ArrayList<>();

                    is_last = data.getData().getIsLast();

                    for (int i = 0; i < data.getData().getList().size(); i++) {
                        Celeb_Reuest_list celeb_reuest_list = data.getData().getList().get(i);

                        if (celeb_reuest_list.getStatus().equalsIgnoreCase("2")) {
                            list.add(celeb_reuest_list);
                        }
                    }
                    parentShimmerLayout.stopShimmerAnimation();
                    parentShimmerLayout.setVisibility(View.GONE);

                    if (list.size() != 0) {
                        txt_message.setVisibility(View.GONE);
                        celeb_request_adapter = new Celeb_Pending_Adapter(getActivity(), list);
                        list_of_new_reuest.setAdapter(celeb_request_adapter);
                        list_of_new_reuest.setLayoutManager(new LinearLayoutManager(getActivity()));
                    }else {
                        txt_message.setVisibility(View.VISIBLE);
                        list_of_new_reuest.setVisibility(View.GONE);
                    }

                }else {
                    parentShimmerLayout.stopShimmerAnimation();
                    parentShimmerLayout.setVisibility(View.GONE);
                    txt_message.setVisibility(View.VISIBLE);
                    list_of_new_reuest.setVisibility(View.GONE);
                }

            }

            @Override
            public void onFailure(String exx) {

            }
        });
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void onResume() {
        super.onResume();
        GetNewRequest();
    }
}
