package com.CamCeleb.CelebSide;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.CamCeleb.APIs.Celeb_DashBords;
import com.CamCeleb.APIs.Helper;
import com.CamCeleb.Helper.Utill;
import com.CamCeleb.POJOClass.Categoryfield;
import com.CamCeleb.POJOClass.Login_model.Gson_response_login_data;
import com.CamCeleb.POJOClass.ResultHandler;
import com.CamCeleb.POJOClass.User;
import com.CamCeleb.R;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.libaml.android.view.chip.ChipLayout;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CelebEditProfileActivity extends AppCompatActivity {

    TextView change;
    String[] countries = {"Actor", "Comedian", "Singer", "Youtuber", "Sport Person", "Influencers"};


    ImageView editprofile_imagechangeforceleb;
    TextView txt_Change_Profile_Photo, txt_owner, txt_bank_name, txt_bank_branch, txt_account_no, txt_ifsc, txt_beneficiary_name, usereditprofile_cancel, usereditprofile_done;

    EditText edt_full_name, edt_email, edt_mobileno, edt_bio, edt_social;
    public static SharedPreferences preferences;
    String session;

    ChipLayout chip;
    List<String> data_of_category;
    List<String> data_of_category_all;
    List<String> data_of_category_remember;
    String categorylist = "";

    ArrayList<Categoryfield> categoryfields;
    private static final int CAMERA_REQUEST_CODE_VEDIO = 101;
    Uri profileUri = null;

    Spinner list_of_categorry_spinner;
    ArrayAdapter<String> adapter;

    ImageView imageview_social;
    String social_platform_type;

    boolean bank_details = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_celeb_edit_profile);
        getSupportActionBar().hide();


        preferences = getSharedPreferences(getPackageName() + "_preferences", MODE_PRIVATE);
        session = preferences.getString("SessionData", "");
        Gson gson = new Gson();
        final User user = gson.fromJson(session, User.class);
        categorylist = preferences.getString("categorylist", "");
        try {
            categoryfields = new ArrayList<>();
            JSONArray jsonArray = new JSONArray(categorylist);

            for (int i = 0; i < jsonArray.length(); i++) {

                JSONObject jsonObject = jsonArray.getJSONObject(i);

                String id = jsonObject.getString("id");
                String name = jsonObject.getString("name");
                categoryfields.add(new Categoryfield(id, name));


            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        change = findViewById(R.id.text_changebank_det);
        editprofile_imagechangeforceleb = findViewById(R.id.editprofile_imagechangeforceleb);
        txt_Change_Profile_Photo = findViewById(R.id.txt_Change_Profile_Photo);
        txt_owner = findViewById(R.id.txt_owner);
        txt_bank_name = findViewById(R.id.txt_bank_name);
        txt_bank_branch = findViewById(R.id.txt_bank_branch);
        txt_account_no = findViewById(R.id.txt_account_no);
        txt_ifsc = findViewById(R.id.txt_ifsc);
        usereditprofile_cancel = findViewById(R.id.usereditprofile_cancel);
        usereditprofile_done = findViewById(R.id.usereditprofile_done);
        edt_full_name = findViewById(R.id.edt_full_name);
        edt_email = findViewById(R.id.edt_email);
        edt_mobileno = findViewById(R.id.edt_mobileno);
        edt_bio = findViewById(R.id.edt_bio);
        edt_social = findViewById(R.id.edt_social);
        list_of_categorry_spinner = findViewById(R.id.list_of_categorry_spinner);
        imageview_social = findViewById(R.id.imageview_social);
        txt_beneficiary_name = findViewById(R.id.txt_beneficiary_name);


        if (user.getProfilePic().isEmpty()) {
            editprofile_imagechangeforceleb.setImageDrawable(getResources().getDrawable(R.mipmap.profile_pic));

        } else {
//            Picasso.with(this).load(user.getProfilePic()).into(editprofile_imagechangeforceleb);
            Picasso.with(this).load(Utill.getRightAngleImage(user.getProfilePic()))
                    .into(editprofile_imagechangeforceleb);
        }
        edt_email.setEnabled(false);
        txt_owner.setText(user.getOwnerType());
        txt_bank_name.setText(user.getBankName());
        txt_bank_branch.setText(user.getBranchName());
        txt_account_no.setText(user.getAccountNumber());
        txt_ifsc.setText(user.getIfscCode());
        edt_full_name.setText(user.getFullName());
        edt_email.setText(user.getEmail());
        edt_mobileno.setText(user.getPhoneNumber());
        edt_bio.setText(user.getAboutMe());
        edt_social.setText(user.getSocialPageName());
        txt_beneficiary_name.setText(user.getBeneficiary_name());

        social_platform_type = user.getSocialPlatformType();


        if (social_platform_type.equalsIgnoreCase("Facebook")) {
            imageview_social.setImageDrawable(getResources().getDrawable(R.drawable.ic_facebook));
        } else if (social_platform_type.equalsIgnoreCase("Twitter")) {
            imageview_social.setImageDrawable(getResources().getDrawable(R.drawable.ic_twitter));
        } else if (social_platform_type.equalsIgnoreCase("Instagram")) {
            imageview_social.setImageDrawable(getResources().getDrawable(R.drawable.ic_instagram));
        } else if (social_platform_type.equalsIgnoreCase("Youtube")) {
            imageview_social.setImageDrawable(getResources().getDrawable(R.drawable.ic_youtube));
        } else if (social_platform_type.equalsIgnoreCase("TikTok")) {
            imageview_social.setImageDrawable(getResources().getDrawable(R.drawable.ic_tiktok));
        }

        imageview_social.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popup = new PopupMenu(CelebEditProfileActivity.this, imageview_social);
                //Inflating the Popup using xml file
                popup.getMenuInflater()
                        .inflate(R.menu.poupup_menu, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        if (item.getTitle().toString().equalsIgnoreCase("Facebook")) {
                            imageview_social.setImageDrawable(getResources().getDrawable(R.drawable.ic_facebook));
                            social_platform_type = "Facebook";
                        } else if (item.getTitle().toString().equalsIgnoreCase("Twitter")) {
                            imageview_social.setImageDrawable(getResources().getDrawable(R.drawable.ic_twitter));
                            social_platform_type = "Twitter";
                        } else if (item.getTitle().toString().equalsIgnoreCase("Instagram")) {
                            imageview_social.setImageDrawable(getResources().getDrawable(R.drawable.ic_instagram));
                            social_platform_type = "Instagram";
                        } else if (item.getTitle().toString().equalsIgnoreCase("Youtube")) {
                            imageview_social.setImageDrawable(getResources().getDrawable(R.drawable.ic_youtube));
                            social_platform_type = "Youtube";
                        } else if (item.getTitle().toString().equalsIgnoreCase("TikTok")) {
                            imageview_social.setImageDrawable(getResources().getDrawable(R.drawable.ic_tiktok));
                            social_platform_type = "TikTok";
                        }
                        return true;
                    }
                });

                popup.show();
            }
        });

        data_of_category = new ArrayList<>();
        data_of_category_remember = new ArrayList<>();


        if (user.getCategories() == null) {
            for (int i = 0; i < categoryfields.size(); i++) {

                String category_name = categoryfields.get(i).getName();

                data_of_category_remember.add(category_name);


            }
        } else {
            String[] array = user.getCategories().split(",");


            for (int i = 0; i < categoryfields.size(); i++) {

                String category_name = categoryfields.get(i).getName();

                if (user.getCategories().contains(category_name)) {
                    data_of_category.add(category_name);
                } else {
                    if (data_of_category.contains(category_name)) {

                    } else {
                        data_of_category_remember.add(category_name);
                    }
                }

            }
        }


//        data_of_category = Arrays.asList(array);

        chip = (ChipLayout) findViewById(R.id.edit_chipText);
      /*  adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, data_of_category_remember);
        chip.setAdapter(adapter);*/
        chip.setText(data_of_category);


        chip.setOnChipItemChangeListener(new ChipLayout.ChipItemChangeListener() {
            @Override
            public void onChipAdded(int pos, String txt) {
            }

            @Override
            public void onChipRemoved(int pos, String txt) {
                System.out.println(txt);
                data_of_category_remember.add(txt);
                data_of_category.remove(txt);
                adapter = new ArrayAdapter<String>(CelebEditProfileActivity.this, android.R.layout.simple_list_item_1, data_of_category_remember);
                chip.setAdapter(adapter);
            }
        });

        chip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertStringList("Chip", data_of_category_remember);
            }
        });


      /*  chip.addLayoutTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                adapter.getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });*/



      /*  chip.setOnClickListener(ClickListener);
        chip.setOnItemClickListener(ItemClickListener);
        chip.addLayoutTextChangedListener(TextChangedListener);
        chip.setOnFocusChangeListener(FocusChangeListener);
       */


        change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String str_bankname = user.getBankName();
                String str_branch = user.getBranchName();
                String str_Number = user.getAccountNumber();
                String str_code = user.getIfscCode();
                String str_beneficiary_name = user.getBeneficiary_name();
                String str_Owner = user.getOwnerType();


                bank_details = true;

                Intent intent = new Intent(CelebEditProfileActivity.this, ChangeBankdetails.class);
                intent.putExtra("Owner", str_Owner);
                intent.putExtra("Bankname", str_bankname);
                intent.putExtra("Bankbranch", str_branch);
                intent.putExtra("AccountNumber", str_Number);
                intent.putExtra("IFSCcode", str_code);
                intent.putExtra("beneficiary_name", str_beneficiary_name);
                startActivity(intent);
            }
        });

        editprofile_imagechangeforceleb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                    requestMultiplePermissions();

                } else {

                    Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                    if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {
                        startActivityForResult(takeVideoIntent, CAMERA_REQUEST_CODE_VEDIO);
                    }
                }

            }
        });
        txt_Change_Profile_Photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                    requestMultiplePermissions();

                } else {

                    Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                    if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {
                        startActivityForResult(takeVideoIntent, CAMERA_REQUEST_CODE_VEDIO);
                    }
                }

            }
        });
        usereditprofile_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        usereditprofile_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UpdateProfile();
            }
        });

    }

    private void alertStringList(String title, final List<String> list) {

        final CharSequence[] items = list.toArray(new CharSequence[list.size()]);

        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this, R.style.MyDialogTheme);
        builder.setTitle("Category");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                String txt = (String) items[item];


                for (int i = 0; i < data_of_category_remember.size(); i++) {
                    if (txt.equals(data_of_category_remember.get(i))) {
                        data_of_category_remember.remove(i);
                    }
                }
                data_of_category.add(txt);
                chip.setText(data_of_category);

            }
        });
        builder.show();


    }


    private void UpdateProfile() {

        Celeb_DashBords dashBords = new Celeb_DashBords();


        String category_for_chip = String.valueOf(chip.getText());

        List<String> categorylist = new ArrayList<>();

        for (int i = 0; i < categoryfields.size(); i++) {
            String name_of_category = categoryfields.get(i).getName();
            String name_of_category_id = categoryfields.get(i).getId();
            if (category_for_chip.contains(name_of_category)) {
                categorylist.add(name_of_category_id);
            }
        }

        String category_list_id = categorylist.toString().replace("[", "");
        String category_list_id1 = category_list_id.replace("]", "");
        String category_list_id2 = category_list_id1.replace(" ", "");

        System.out.println(categorylist.toString());

        String full_name = edt_full_name.getText().toString().trim();
        String phone_number = edt_mobileno.getText().toString().trim();
        String email = edt_email.getText().toString().trim();
        String social_page_name = edt_social.getText().toString().trim();
        String categories_id = category_list_id2;
        String about_me = edt_bio.getText().toString().trim();


        if (profileUri != null) {

            final ProgressDialog progressDialog = new ProgressDialog(CelebEditProfileActivity.this);
            progressDialog.show();

            dashBords.UpdateProfile_withImage(Helper.createRequestBody(full_name), Helper.createRequestBody(email),
                    Helper.createRequestBody(social_platform_type),
                    Helper.createRequestBody(social_page_name),
                    Helper.createRequestBody(about_me),
                    Helper.createRequestBody(categories_id), Helper.createRequestBody(phone_number), profileUri, new ResultHandler<Gson_response_login_data>() {
                        @Override
                        public void onSuccess(final Gson_response_login_data data) {
                            if (data.getSuccess() == 1) {

                                final Dialog dialog = new Dialog(CelebEditProfileActivity.this);
                                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                                dialog.setCancelable(false);
                                dialog.setContentView(R.layout.custom_alert_dailog);

                                TextView text = (TextView) dialog.findViewById(R.id.txt_message);
                                TextView cancel = (TextView) dialog.findViewById(R.id.cancel);
                                text.setText("Profile Updated Successfully!");

                                TextView dialogButton = dialog.findViewById(R.id.submit);
                                dialogButton.setText("OK");
                                dialogButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        data.getData().SaveSession();
                                        progressDialog.dismiss();
                                        dialog.dismiss();
                                        onBackPressed();

                                    }
                                });


                                dialog.show();


                            }

                        }

                        @Override
                        public void onFailure(String exx) {
                            progressDialog.dismiss();
                            final Dialog dialog = new Dialog(CelebEditProfileActivity.this);
                            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                            dialog.setCancelable(false);
                            dialog.setContentView(R.layout.custom_alert_dailog);

                            TextView text = (TextView) dialog.findViewById(R.id.txt_message);
                            TextView cancel = (TextView) dialog.findViewById(R.id.cancel);
                            text.setText("Something went wrong please try again!");

                            TextView dialogButton = dialog.findViewById(R.id.submit);
                            dialogButton.setText("OK");
                            dialogButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    dialog.dismiss();


                                }
                            });


                            dialog.show();

                        }
                    });

        } else {
            final ProgressDialog progressDialog = new ProgressDialog(CelebEditProfileActivity.this);
            progressDialog.show();

            dashBords.UpdateProfileAllFeild(full_name, email, phone_number, social_platform_type, social_page_name, about_me, categories_id, new ResultHandler<Gson_response_login_data>() {
                @Override
                public void onSuccess(final Gson_response_login_data data) {
                    if (data.getSuccess() == 1) {
                       /* AlertDialog.Builder builder = new AlertDialog.Builder(CelebEditProfileActivity.this, R.style.MyDialogTheme);
                        builder.setTitle("CamCeleb");
                        builder.setMessage("Profile Updated Successfully!");
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                data.getData().SaveSession();
                                progressDialog.dismiss();
                                onBackPressed();

                            }
                        });

                        builder.show();
*/
                        final Dialog dialog = new Dialog(CelebEditProfileActivity.this);
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                        dialog.setCancelable(false);
                        dialog.setContentView(R.layout.custom_alert_dailog);

                        TextView text = (TextView) dialog.findViewById(R.id.txt_message);
                        TextView cancel = (TextView) dialog.findViewById(R.id.cancel);
                        text.setText("Profile Updated Successfully!");

                        TextView dialogButton = dialog.findViewById(R.id.submit);
                        dialogButton.setText("OK");
                        dialogButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                data.getData().SaveSession();
                                progressDialog.dismiss();
                                dialog.dismiss();
                                onBackPressed();

                            }
                        });


                        dialog.show();

                    }
                }

                @Override
                public void onFailure(String exx) {
                    progressDialog.dismiss();
                    final Dialog dialog = new Dialog(CelebEditProfileActivity.this);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    dialog.setCancelable(false);
                    dialog.setContentView(R.layout.custom_alert_dailog);

                    TextView text = (TextView) dialog.findViewById(R.id.txt_message);
                    TextView cancel = (TextView) dialog.findViewById(R.id.cancel);
                    text.setText("Something went wrong please try again!");

                    TextView dialogButton = dialog.findViewById(R.id.submit);
                    dialogButton.setText("OK");
                    dialogButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            dialog.dismiss();


                        }
                    });


                    dialog.show();
                }
            });
        }


    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    private void requestMultiplePermissions() {

        if (ContextCompat.checkSelfPermission(CelebEditProfileActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) + ContextCompat.checkSelfPermission(CelebEditProfileActivity.this, Manifest.permission.CAMERA) + ContextCompat.checkSelfPermission(CelebEditProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(CelebEditProfileActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(CelebEditProfileActivity.this, Manifest.permission.CAMERA) || ActivityCompat.shouldShowRequestPermissionRationale(CelebEditProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                Snackbar.make(CelebEditProfileActivity.this.findViewById(android.R.id.content),
                        "Please Grant Permissions to upload Image",
                        Snackbar.LENGTH_INDEFINITE).setAction("Enable",
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, CAMERA_REQUEST_CODE_VEDIO);
                            }
                        }).show();
            } else {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, CAMERA_REQUEST_CODE_VEDIO);
            }
        } else {
//            selectImage();

            CropImage.activity()
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setAspectRatio(1,1)
                    .start(this);
        }
    }

    private void selectImage() {

        final Dialog dialog = new Dialog(this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.upload_photo_custome_dialog);

        TextView txt_camera = (TextView) dialog.findViewById(R.id.txt_camera);
        TextView txt_gallery = (TextView) dialog.findViewById(R.id.txt_gallery);
        TextView txt_cancel = (TextView) dialog.findViewById(R.id.txt_cancel);

        txt_camera.setText(getResources().getString(R.string.camera));
        txt_gallery.setText(getResources().getString(R.string.gallery));
        txt_cancel.setText(getResources().getString(R.string.cancel));


        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        txt_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, 1);

            }
        });
        txt_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, 2);
            }
        });

        dialog.show();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
       /* if (resultCode == RESULT_OK) {
            if (requestCode == 1) {

                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                File destination = new File(Environment.getExternalStorageDirectory(),
                        System.currentTimeMillis() + ".jpg");
                FileOutputStream fo;
                try {
                    destination.createNewFile();
                    fo = new FileOutputStream(destination);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }


                Uri outputFileUri = getImageUri(getApplicationContext(), thumbnail);


                thumbnail = Utill.ExifInterfaceImage(destination, thumbnail);


                profileUri = outputFileUri;

                System.out.println("path of image from gallery......******************........." + destination + "");

                editprofile_imagechangeforceleb.setImageBitmap(thumbnail);


            } else if (requestCode == 2) {

                Uri selectedImage = data.getData();
                String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String picturePath = c.getString(columnIndex);
                c.close();
                Bitmap thumbnail = (BitmapFactory.decodeFile(picturePath));
                System.out.println("path of image from gallery......******************........." + picturePath + "");

                File destination = new File(picturePath);

                thumbnail = Utill.ExifInterfaceImage(destination, thumbnail);


                profileUri = selectedImage;
                editprofile_imagechangeforceleb.setImageBitmap(thumbnail);
            }
        }*/
        CropImage.ActivityResult result = CropImage.getActivityResult(data);
        if (resultCode == RESULT_OK) {
            Uri resultUri = result.getUri();

            profileUri = resultUri;
            editprofile_imagechangeforceleb.setImageURI(resultUri);

        } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
            Exception error = result.getError();
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (bank_details) {
            preferences = getSharedPreferences(getPackageName() + "_preferences", MODE_PRIVATE);
            session = preferences.getString("SessionData", "");
            Gson gson = new Gson();
            final User user = gson.fromJson(session, User.class);

            txt_owner.setText(user.getOwnerType());
            txt_bank_name.setText(user.getBankName());
            txt_bank_branch.setText(user.getBranchName());
            txt_account_no.setText(user.getAccountNumber());
            txt_ifsc.setText(user.getIfscCode());
            txt_beneficiary_name.setText(user.getBeneficiary_name());


        }
    }
}
