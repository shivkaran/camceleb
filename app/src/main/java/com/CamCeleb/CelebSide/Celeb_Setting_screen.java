package com.CamCeleb.CelebSide;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.CamCeleb.APIs.Authorization;
import com.CamCeleb.Activity.LoginAndSignActivity;
import com.CamCeleb.ChangePasswordActivity;
import com.CamCeleb.POJOClass.ResultHandler;
import com.CamCeleb.POJOClass.User;
import com.CamCeleb.R;
import com.CamCeleb.SplashActivity;
import com.CamCeleb.UserSide.UserEditProfileActivity;
import com.CamCeleb.UserSide.User_Setting;
import com.facebook.login.LoginManager;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

public class Celeb_Setting_screen extends AppCompatActivity {

    public static SharedPreferences preferences;


    LinearLayout ll_profile, ll_privacy, ll_tearm, ll_faq, ll_logout,ll_Change_password,ll_transaction,ll_contact;
    ImageView img_back;
    TextView txt_share_profile;
    String user_id = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_celeb__setting_screen);
        getSupportActionBar().hide();


        preferences = getSharedPreferences(getPackageName() + "_preferences", MODE_PRIVATE);
        String  session = preferences.getString("SessionData", "");
        Gson gson = new Gson();
        User user = gson.fromJson(session, User.class);
        user_id = String.valueOf(user.getId());


        final String faqs = preferences.getString("faqs", "");
        final String terms = preferences.getString("terms", "");
        final String privacy = preferences.getString("privacy", "");

        ll_profile = findViewById(R.id.ll_profile);
        ll_Change_password = findViewById(R.id.ll_Change_password);
        ll_transaction = findViewById(R.id.ll_transaction);
        ll_privacy = findViewById(R.id.ll_privacy);
        ll_tearm = findViewById(R.id.ll_tearm);
        ll_faq = findViewById(R.id.ll_faq);
        ll_logout = findViewById(R.id.ll_logout);
        txt_share_profile = findViewById(R.id.txt_share_profile);
        img_back = findViewById(R.id.img_back);
        ll_contact = findViewById(R.id.ll_contact);


        ll_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Celeb_Setting_screen.this, CelebEditProfileActivity.class);
                startActivity(intent);
            }
        });

        ll_Change_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Celeb_Setting_screen.this, ChangePasswordActivity.class);
                startActivity(intent);
            }
        });

        ll_transaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Celeb_Setting_screen.this, CaptureVideo.class);
                intent.putExtra("introvideo","user_introvideo");
                startActivity(intent);
            }
        });
        ll_privacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(privacy));
                startActivity(browserIntent);
            }
        });

        ll_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.camceleb.com/contact"));
                startActivity(browserIntent);
            }
        });
        ll_tearm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(terms));
                startActivity(browserIntent);
            }
        });
        ll_faq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(faqs));
                startActivity(browserIntent);
            }
        });

        ll_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              /*  AlertDialog.Builder builder = new AlertDialog.Builder(Celeb_Setting_screen.this,R.style.MyDialogTheme);
                builder.setTitle("CamCeleb");
                builder.setMessage("Are You sure you want to logout?");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                        Authorization authorization = new Authorization();
                        authorization.Logout(Celeb_Setting_screen.this,new ResultHandler<JsonObject>() {
                            @Override
                            public void onSuccess(JsonObject data) {

                                SharedPreferences preferences = getSharedPreferences(getPackageName() + "_preferences", MODE_PRIVATE);

                                preferences.edit().clear().commit();
                                LoginManager.getInstance().logOut();

                                Intent intent = new Intent(Celeb_Setting_screen.this, LoginAndSignActivity.class);
                                startActivity(intent);
                                finishAffinity();
                            }

                            @Override
                            public void onFailure(String exx) {

                            }
                        });

                    }
                });

                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                });
                builder.show();*/

                final Dialog dialog = new Dialog(Celeb_Setting_screen.this);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.setCancelable(false);
                dialog.setContentView(R.layout.custom_alert_dailog);

                TextView text = (TextView) dialog.findViewById(R.id.txt_message);
                TextView cancel = (TextView) dialog.findViewById(R.id.cancel);
                text.setText("Are You sure you want to logout?");

                TextView dialogButton =  dialog.findViewById(R.id.submit);
                dialogButton.setText("Yes");
                cancel.setText("No");
                cancel.setVisibility(View.VISIBLE);
                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        dialog.dismiss();

                        Authorization authorization = new Authorization();
                        authorization.Logout(Celeb_Setting_screen.this,new ResultHandler<JsonObject>() {
                            @Override
                            public void onSuccess(JsonObject data) {

                                SharedPreferences preferences = getSharedPreferences(getPackageName() + "_preferences", MODE_PRIVATE);

                                preferences.edit().clear().commit();
                                LoginManager.getInstance().logOut();

                                Intent intent = new Intent(Celeb_Setting_screen.this, LoginAndSignActivity.class);
                                startActivity(intent);
                                finishAffinity();
                            }

                            @Override
                            public void onFailure(String exx) {

                            }
                        });
                    }
                });

                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.show();

            }
        });

        txt_share_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String subject = "Hey! Checkout my profile on CamCeleb.";

                String body = subject +" \n\n https://www.camceleb.com/celebprofile/"+user_id;


                Intent txtIntent = new Intent(android.content.Intent.ACTION_SEND);
                txtIntent .setType("text/plain");
                txtIntent .putExtra(android.content.Intent.EXTRA_TEXT, body);
                startActivity(Intent.createChooser(txtIntent ,"Share"));
            }
        });

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });



    }
}
