package com.CamCeleb.CelebSide;

import android.Manifest;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.CamCeleb.R;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.FFmpegExecuteResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;
import com.otaliastudios.cameraview.CameraException;
import com.otaliastudios.cameraview.CameraLogger;
import com.otaliastudios.cameraview.CameraOptions;
import com.otaliastudios.cameraview.CameraView;
import com.otaliastudios.cameraview.PictureResult;
import com.otaliastudios.cameraview.VideoResult;
import com.otaliastudios.cameraview.controls.Mode;
import com.otaliastudios.cameraview.controls.Preview;
import com.otaliastudios.cameraview.frame.Frame;
import com.otaliastudios.cameraview.frame.FrameProcessor;
import com.otaliastudios.cameraview.CameraListener;
import com.otaliastudios.cameraview.overlay.OverlayLayout;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class CaptureVideo extends AppCompatActivity implements SurfaceHolder.Callback{



    String  instructions,str_name,str_profile_image="",Request_id,user_introvideo="";

    private SurfaceHolder surfaceHolder;
    private SurfaceView surfaceView;
    public MediaRecorder mrec = new MediaRecorder();

    File video;
    private Camera mCamera;

    ImageView startRecording,intro_userimg,img_close;

    TextView intro_username,intro_username_descrption;

    boolean aBoolean = true;

    Chronometer cmTimer;



    private final static CameraLogger LOG = CameraLogger.create("DemoApp");
    private final static boolean USE_FRAME_PROCESSOR = true;
    private final static boolean DECODE_BITMAP = true;

    private CameraView camera;
    private ViewGroup controlPanel;
    private long mCaptureTime;
    private int maxDuration = 0;
    private String videoPath;


    @SuppressLint("WrongThread")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_capture_video);

        getSupportActionBar().hide();
        Intent intent = getIntent();

        cmTimer = (Chronometer) findViewById(R.id.cmTimer);
        intro_userimg =findViewById(R.id.intro_userimg);
        img_close =findViewById(R.id.img_close);
        intro_username =findViewById(R.id.intro_username);
        intro_username_descrption =findViewById(R.id.intro_username_descrption);
        startRecording =findViewById(R.id.btnCaptureVideo);


        if (intent.getStringExtra("introvideo") != null) {

            user_introvideo = intent.getStringExtra("introvideo");

        }


        if (ContextCompat.checkSelfPermission(CaptureVideo.this, Manifest.permission.READ_EXTERNAL_STORAGE) + ContextCompat.checkSelfPermission(CaptureVideo.this, Manifest.permission.CAMERA) + ContextCompat.checkSelfPermission(CaptureVideo.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(CaptureVideo.this, Manifest.permission.READ_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(CaptureVideo.this, Manifest.permission.CAMERA) || ActivityCompat.shouldShowRequestPermissionRationale(CaptureVideo.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 100);

            } else {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 100);
            }
        }




        if (intent.getStringExtra("instructions") != null) {
            instructions = intent.getStringExtra("instructions");
            intro_username_descrption.setText(instructions);

        }if (intent.getStringExtra("Request_id") != null) {
            Request_id = intent.getStringExtra("Request_id");


        }
        if (intent.getStringExtra("str_name") != null) {
            str_name = intent.getStringExtra("str_name");
            intro_username.setText(str_name);
        } if (intent.getStringExtra("str_profile_image") != null) {

            str_profile_image = intent.getStringExtra("str_profile_image");

            if (str_profile_image.equalsIgnoreCase(""))
            {
                intro_userimg.setImageDrawable(getResources().getDrawable(R.mipmap.profile_pic));
            }else {
                Picasso.with(this).load(str_profile_image).into(intro_userimg);
            }
        }



        Drawable drawable = getResources().getDrawable(R.mipmap.video_watermark);
        Bitmap  bitmap = ((BitmapDrawable) drawable).getBitmap();

        ContextWrapper cw = new ContextWrapper(this);
        File directory = Environment.getExternalStorageDirectory();
        final File file = new File(directory, "UniqueFileName" + ".png");

        if (!file.exists()) {
            Log.d("path", file.toString());
            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
                fos.flush();
                fos.close();
            } catch (java.io.IOException e) {
                e.printStackTrace();
            }
        }


        CameraLogger.setLogLevel(CameraLogger.LEVEL_VERBOSE);
        final View watermark = findViewById(R.id.watermark);
        camera = findViewById(R.id.camera);
        camera.setLifecycleOwner(this);

        camera.addCameraListener(new CameraListener() {
            @Override
            public void onCameraOpened(@NonNull CameraOptions options) {
              /*  ViewGroup group = (ViewGroup) controlPanel.getChildAt(0);
                for (int i = 0; i < group.getChildCount(); i++) {
//                OptionView view = (OptionView) group.getChildAt(i);
//                view.onCameraOpened(camera, options);
                }*/
            }

            @Override
            public void onCameraError(@NonNull CameraException exception) {
                super.onCameraError(exception);
                message("Got CameraException #" + exception.getReason(), true);
            }

            @Override
            public void onPictureTaken(@NonNull PictureResult result) {
                super.onPictureTaken(result);
                if (camera.isTakingVideo()) {
                    //  message("Captured while taking video. Size=" + result.getSize(), false);
                    return;
                }
                mCaptureTime = 0;
                LOG.w("onPictureTaken called! Launched activity.");
            }

            @Override
            public void onVideoTaken(@NonNull VideoResult result) {
                super.onVideoTaken(result);
                LOG.w("onVideoTaken called! Launching activity."+result.getFile());
                //   VideoPreviewActivity.setVideoResult(result);
         /*   Intent intent = new Intent(CameraActivity.this, VideoPreviewActivity.class);
            startActivity(intent);*/
                LOG.w("onVideoTaken called! Launched activity.");
                maxDuration = result.getMaxDuration() / 1000;
                videoPath = result.getFile().getAbsolutePath();

                if (user_introvideo.equals("")) {

                    doReplaceTheFrame(file.getAbsolutePath());
                  /*  activity_video_preview.setVideoResult(result, Request_id);
                    Intent intent = new Intent(CaptureVideo.this, activity_video_preview.class);
                    intent.putExtra("user_introvideo", user_introvideo);
                    startActivity(intent);
                    finish();*/
                }else {
                    activity_video_preview.setVideoResult(result, "");
                    Intent intent = new Intent(CaptureVideo.this, activity_video_preview.class);
                    intent.putExtra("user_introvideo", user_introvideo);
                    startActivity(intent);
                    finish();
                }
            }

            @Override
            public void onVideoRecordingStart() {
                super.onVideoRecordingStart();
                LOG.w("onVideoRecordingStart!");
            }

            @Override
            public void onVideoRecordingEnd() {
                super.onVideoRecordingEnd();
                message("Video taken. Processing...", false);
                LOG.w("onVideoRecordingEnd!");
            }

            @Override
            public void onExposureCorrectionChanged(float newValue, @NonNull float[] bounds, @Nullable PointF[] fingers) {
                super.onExposureCorrectionChanged(newValue, bounds, fingers);
                message("Exposure correction:" + newValue, false);
            }

            @Override
            public void onZoomChanged(float newValue, @NonNull float[] bounds, @Nullable PointF[] fingers) {
                super.onZoomChanged(newValue, bounds, fingers);
                message("Zoom:" + newValue, false);
            }
        });







        if (USE_FRAME_PROCESSOR) {
            camera.addFrameProcessor(new FrameProcessor() {
                private long lastTime = System.currentTimeMillis();

                @Override
                public void process(@NonNull Frame frame) {
                    long newTime = frame.getTime();
                    long delay = newTime - lastTime;
                    lastTime = newTime;
                    LOG.e("Frame delayMillis:", delay, "FPS:", 1000 / delay);
                    if (DECODE_BITMAP) {
                        YuvImage yuvImage = new YuvImage(frame.getData(), ImageFormat.NV21,
                                frame.getSize().getWidth(),
                                frame.getSize().getHeight(),
                                null);
                        ByteArrayOutputStream jpegStream = new ByteArrayOutputStream();
                        yuvImage.compressToJpeg(new Rect(0, 0,
                                frame.getSize().getWidth(),
                                frame.getSize().getHeight()), 100, jpegStream);
                        byte[] jpegByteArray = jpegStream.toByteArray();
                        Bitmap bitmap = BitmapFactory.decodeByteArray(jpegByteArray, 0, jpegByteArray.length);
                        //noinspection ResultOfMethodCallIgnored
                        bitmap.toString();
                    }
                }
            });


            ValueAnimator animator = ValueAnimator.ofFloat(1F, 0.8F);
            animator.setDuration(300);
            animator.setRepeatCount(ValueAnimator.INFINITE);
            animator.setRepeatMode(ValueAnimator.REVERSE);
            animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    float scale = (float) animation.getAnimatedValue();
                    watermark.setScaleX(scale);
                    watermark.setScaleY(scale);
                    watermark.setRotation(watermark.getRotation() + 2);
                }
            });
            animator.start();

        }



        startRecording.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (aBoolean)
                {
                    cmTimer.setBase(SystemClock.elapsedRealtime());
                    cmTimer.start();

                    img_close.setVisibility(View.GONE);
                    startRecording.setImageDrawable(getResources().getDrawable(R.drawable.record));
                    aBoolean = false;
                    captureVideo();
                }
                else {
                    cmTimer.stop();
                    startRecording.setImageDrawable(getResources().getDrawable(R.drawable.record_button));
                    camera.stopVideo();
                    aBoolean = true;
                    img_close.setVisibility(View.VISIBLE);
                }

            }
        });


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                cmTimer.stop();
                startRecording.setImageDrawable(getResources().getDrawable(R.drawable.record_button));
                camera.stopVideo();
                aBoolean = true;
                img_close.setVisibility(View.VISIBLE);

            }}, 90000);



        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCamera != null){
                    mCamera.release();        // release the camera for other applications
                    mCamera = null;
                }
                onBackPressed();
            }
        });




    }

    private void captureVideo() {
        if (camera.getMode() == Mode.PICTURE) {
//            message("Can't record HQ videos while in PICTURE mode.", false);
            return;
        }
        if (camera.isTakingPicture() || camera.isTakingVideo()) return;
//        message("Recording start", true);

        camera.takeVideo(new File(getFilesDir(), "video.mp4"), 90000);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        menu.add(0, 0, 0, "StartRecording");
        menu.add(0, 1, 0, "StopRecording");

        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case 0:
                try {
                    startRecording();
                } catch (Exception e) {
                    String message = e.getMessage();
                    Log.i(null, "Problem Start"+message);
                    mrec.release();
                }
                break;

            case 1: //GoToAllNotes
                mrec.stop();
                mrec.release();
                mrec = null;
                break;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    protected void startRecording() throws IOException
    {
        mrec = new MediaRecorder();  // Works well
        mCamera.unlock();

        mrec.setCamera(mCamera);

        mrec.setPreviewDisplay(surfaceHolder.getSurface());
        mrec.setVideoSource(MediaRecorder.VideoSource.CAMERA);
        mrec.setAudioSource(MediaRecorder.AudioSource.MIC);

        mrec.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH));
        mrec.setPreviewDisplay(surfaceHolder.getSurface());
        mrec.setOutputFile("/sdcard/zzzz.3gp");

        mrec.prepare();
        mrec.start();
    }


    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width,
                               int height) {
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        if (mCamera != null){

            mCamera = Camera.open(Camera.CameraInfo.CAMERA_FACING_FRONT);

            Camera.Parameters params = mCamera.getParameters();
            mCamera.setParameters(params);
            mCamera.setDisplayOrientation(90);
        }
        else {
            // Toast.makeText(getApplicationContext(), "Camera not available!", Toast.LENGTH_LONG).show();
            finish();
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        mCamera.stopPreview();
        mCamera.release();
    }



    private void message(@NonNull String content, boolean important) {
        if (important) {
            LOG.w(content);
            // Toast.makeText(this, content, Toast.LENGTH_LONG).show();
        } else {
            LOG.i(content);
            // Toast.makeText(this, content, Toast.LENGTH_SHORT).show();
        }
    }

    private void doReplaceTheFrame(String image) {
        final ProgressDialog progressDialog = ProgressDialog.show(this,null,"Processing",true,false);



        String[] complexCommand;

        int frameCount = (int) (maxDuration * 30);
        int startTimeFrame = frameCount - 5;

        File f = new File("/storage/emulated/0");

        String rootPath = f.getPath();


        complexCommand =
                new String[]{"-y", "-i", videoPath, "-i", image, "-filter_complex",
                        "[0:v][1:v]overlay=" + 0 + ":" + 200 + 10 +
                                ":enable='between" + "(t," + 0 + "," + 100 + ")"
                                + "'[out]", "-map", "[out]", Environment.getExternalStorageDirectory() + "/outputFrame.mp4"};

        /*complexCommand = new String[]{"ffmpeg", "-y", "-i", videoPath, "-strict",
                "experimental", "-vf",
                Environment.getExternalStorageDirectory().getAbsolutePath()
                + "/UniqueFileName.jpg [watermark]; [in][watermark] overlay=main_w-overlay_w-10:10 [out]",
                "-s", "160x120", "-r", "30", "-b", "15496k", "-vcodec", "mpeg4", "-ab", "48000",
                "-ac", "2", "-ar", "22050",
                Environment.getExternalStorageDirectory() + "/outputFrame.mp4"};*/

        complexCommand =
                new String[]{"ffmpeg -i "+videoPath+" -i "+image+" -filter_complex 'overlay=10:main_h-overlay_h-10' "+
                        Environment.getExternalStorageDirectory() + "/outputFrame.mp4"};

      /*  complexCommand =
                new String[]{"-y", "-i", videoPath, "-i", image, "-filter_complex", "overlay=10:main_h-overlay_h-10",
                        Environment.getExternalStorageDirectory() + "/outputFrame.mp4"};*/


//        {"-i", videoPath, "-i", waterMark.toString(), "-filter_complex", "[0:v]pad=iw:if(lte(ih\\,iw)\\,ih\\,2*trunc(iw*16/9/2)):(ow-iw)/2:(oh-ih)/2[v0];[1:v][v0]scale2ref[v1][v0];[v0][v1]overlay=x=(W-w)/2:y=(H-h)/2[v]", "-map", "[v]", "-map", "0:a", "-c:v", "libx264", "-preset", "ultrafast", "-r", myFrameRate, outputPath};



  complexCommand =
          new String[]{"-y", "-i", videoPath, "-i", image, "-filter_complex", "overlay=10:main_h-overlay_h-10", "-map", "0:a", "-c:v", "libx264", "-preset", "ultrafast", "-r","25",Environment.getExternalStorageDirectory() + "/outputFrame.mp4"};

        FFmpeg ffmpeg = FFmpeg.getInstance(this);

        try {
            //Load the binary
            ffmpeg.loadBinary(new LoadBinaryResponseHandler() {

                @Override
                public void onFailure() {
                }

                @Override
                public void onStart() {
                }


                @Override
                public void onSuccess() {
//                    Toast.makeText(getApplicationContext(), "Successful",
//                            Toast.LENGTH_LONG).show();
                }

                @Override
                public void onFinish() {
                }
            });
        } catch (FFmpegNotSupportedException e) {
            // Handle if FFmpeg is not supported by device
            Toast.makeText(getApplicationContext(), "Not Supported by Device",
                    Toast.LENGTH_LONG).show();
        }

        try {

            final String finalRootPath = rootPath;


           /* final ProgressDialog pd = new ProgressDialog(this);
            pd.show();
            pd.setTitle("Please wait...");*/
            ffmpeg.execute(complexCommand, new FFmpegExecuteResponseHandler() {
                @Override
                public void onSuccess(String message) {

                    progressDialog.dismiss();
                    Log.d("Success", message);

                    Toast.makeText(getApplicationContext(), "Successful" + finalRootPath
                                    .toString(),
                            Toast.LENGTH_LONG).show();
                    Uri path = Uri.parse(Environment.getExternalStorageDirectory() + "/outputFrame.mp4");


                    File path_file = new File(String.valueOf(path));

                    activity_video_preview.setVideoResultmakevideo(path_file, Request_id);
                    Intent intent = new Intent(CaptureVideo.this, activity_video_preview.class);
                    intent.putExtra("user_introvideo", user_introvideo);
                    startActivity(intent);
                    finish();




//                    playVideo(path.toString());

                }

                @Override
                public void onStart() {
                    Log.d("Start", "merge started");
                }

                @Override
                public void onProgress(String message) {
                    Log.d("progress", message);
//                    pd.show();
                }

                @Override
                public void onFailure(String message) {
                    Log.d("failure", message);
                    Toast.makeText(CaptureVideo.this, "Failed"+message, Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
//                    pd.dismiss();

                }


                @Override
                public void onFinish() {
                    Log.d("finish", "merge finish");
//                    progressDialog.dismiss();
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            e.printStackTrace();
        }
    }
}
