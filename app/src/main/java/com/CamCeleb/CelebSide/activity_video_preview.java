package com.CamCeleb.CelebSide;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.text.format.Formatter;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.loader.content.CursorLoader;

import com.CamCeleb.APIs.Celeb_DashBords;
import com.CamCeleb.APIs.Helper;
import com.CamCeleb.POJOClass.AuthToken;
import com.CamCeleb.POJOClass.Login_model.Gson_response_login_data;
import com.CamCeleb.POJOClass.Result;
import com.CamCeleb.POJOClass.ResultHandler;
import com.CamCeleb.R;
import com.otaliastudios.cameraview.VideoResult;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;

public class activity_video_preview extends AppCompatActivity {
    private static String Request;
    private VideoView videoView;

    private static VideoResult videoResult;

    RelativeLayout ll_submit, ll_reset;
    ImageView img_close;
    ImageView imageView;

    String user_introvideo = "";

    AuthToken auth_token;

    String image_path = "";

    public static void setVideoResult(@Nullable VideoResult result, String request_id) {
        videoResult = result;
        Request = request_id;
    }


    File image_upload;
    private static File Vide_path;

    public static void setVideoResultmakevideo(File path_file, String request_id) {
        Vide_path = path_file;
        Request = request_id;
    }

    @SuppressLint("WrongThread")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_preview);
        getSupportActionBar().hide();
        Intent intent = getIntent();

        auth_token = new AuthToken();



        if (intent.getStringExtra("user_introvideo") != null) {
            user_introvideo = intent.getStringExtra("user_introvideo");
        }

        final VideoResult result = videoResult;
        if (result != null) {
          /*  finish();
            return;*/

            Vide_path = result.getFile();


        }





        if (ContextCompat.checkSelfPermission(activity_video_preview.this, Manifest.permission.READ_EXTERNAL_STORAGE) + ContextCompat.checkSelfPermission(activity_video_preview.this, Manifest.permission.CAMERA) + ContextCompat.checkSelfPermission(activity_video_preview.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(activity_video_preview.this, Manifest.permission.READ_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(activity_video_preview.this, Manifest.permission.CAMERA) || ActivityCompat.shouldShowRequestPermissionRationale(activity_video_preview.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 100);

            } else {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 100);
            }
        }


        videoView = findViewById(R.id.video);
        ll_submit = findViewById(R.id.ll_submit);
        ll_reset = findViewById(R.id.ll_reset);
        img_close = findViewById(R.id.img_close);
        imageView = findViewById(R.id.imageView);


        playVideo();


       /* File directory = Environment.getExternalStorageDirectory();
        File file = new File(directory, "UniqueFileName" + ".jpg");

        System.out.println(file.toString());



        final File file2 = new File( Environment.getExternalStorageDirectory()+"/"+ System.currentTimeMillis() + ".mp4");


        GiraffeCompressor.create()
                .input(videoResult.getFile())
                .output(file2)
                .bitRate(3073600)
                .resizeFactor(1.0f)
                .watermark(file.toString())
                .ready()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<GiraffeCompressor.Result>() {
                    @Override
                    public void onCompleted() {
                        file2.getAbsolutePath();
                        long length = file2.length();
                        String s = "";
                        System.out.println("Shiv karan  "+length);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(GiraffeCompressor.Result result) {
                        String msg = String.format("compress completed \ntake time:%s ms \nout put file:%s", result.getCostTime(), result.getOutput());
                        msg = msg + "\ninput file size:" + Formatter.formatFileSize(getApplication(), videoResult.getFile().length());
                        msg = msg + "\nout file size:" + Formatter.formatFileSize(getApplication(), new File(result.getOutput()).length());
                        System.out.println("Shiv karan  "+msg);

                        videoView.setVideoURI(Uri.fromFile(file2));
                    }
                });


*/


        videoView.setVideoURI(Uri.fromFile(Vide_path));





        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        ll_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        ll_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               /* try {
                    uploadVideo();
                } catch (ParseException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
*/
                UpdalodRequestVideo();

//                new UploadFileToServer().execute();


//                // Toast.makeText(activity_video_preview.this, "show message video upload", Toast.LENGTH_SHORT).show();
            }
        });
        System.out.println("shiv sk " + Formatter.formatFileSize(getApplication(), Vide_path.length()));


    }

    private void UpdalodRequestVideo() {


        String request_id = Request;
        String action = "1";


        Bitmap bMap = ThumbnailUtils.createVideoThumbnail(String.valueOf(Vide_path), MediaStore.Video.Thumbnails.MINI_KIND);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bMap.compress(Bitmap.CompressFormat.JPEG, 100, stream);


        Uri video_thumb = getImageUri(this, bMap);


//        System.out.println("shiv sk " + videoResult.getVideoCodec());


        if (!request_id.equals("")) {

            Celeb_DashBords celeb_dashBords = new Celeb_DashBords();

            celeb_dashBords.requestactionVideo(activity_video_preview.this, Helper.createRequestBody(request_id), Helper.createRequestBody(action), Vide_path, video_thumb, new ResultHandler<Result>() {
                @Override
                public void onSuccess(Result data) {
                    if (data.getCode() == 1) {

                        final Dialog dialog = new Dialog(activity_video_preview.this);
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));                        dialog.setCancelable(false);
                        dialog.setContentView(R.layout.custom_alert_dailog);

                        TextView text = (TextView) dialog.findViewById(R.id.txt_message);
                        TextView cancel = (TextView) dialog.findViewById(R.id.cancel);
                        text.setText("Video upload successfully");

                        TextView dialogButton =  dialog.findViewById(R.id.submit);
                        dialogButton.setText("OK");
                        dialogButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                                startActivity(new Intent(activity_video_preview.this, CelebHomeActivity.class));
                                finishAffinity();

                            }
                        });


                        dialog.show();



                    } else {

                        final Dialog dialog = new Dialog(activity_video_preview.this);
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                        dialog.setCancelable(false);
                        dialog.setContentView(R.layout.custom_alert_dailog);

                        TextView text = (TextView) dialog.findViewById(R.id.txt_message);
                        TextView cancel = (TextView) dialog.findViewById(R.id.cancel);
                        text.setText("Video upload Failed Try again!");

                        TextView dialogButton =  dialog.findViewById(R.id.submit);
                        dialogButton.setText("OK");
                        dialogButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();


                            }
                        });


                        dialog.show();
                    }

                }

                @Override
                public void onFailure(String exx) {

                    final Dialog dialog = new Dialog(activity_video_preview.this);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    dialog.setCancelable(false);
                    dialog.setContentView(R.layout.custom_alert_dailog);

                    TextView text = (TextView) dialog.findViewById(R.id.txt_message);
                    TextView cancel = (TextView) dialog.findViewById(R.id.cancel);
                    text.setText("Video upload Failed Try again!");

                    TextView dialogButton =  dialog.findViewById(R.id.submit);
                    dialogButton.setText("OK");
                    dialogButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();


                        }
                    });


                    dialog.show();
                }
            });
        } else {
            Celeb_DashBords celeb_dashBords = new Celeb_DashBords();

            celeb_dashBords.UpdateProfile_introvideo(activity_video_preview.this, Vide_path, video_thumb, new ResultHandler<Gson_response_login_data>() {
                @Override
                public void onSuccess(Gson_response_login_data data) {
                    if (data.getSuccess() == 1) {

                        data.getData().SaveSession();

                        final Dialog dialog = new Dialog(activity_video_preview.this);
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));                        dialog.setCancelable(false);
                        dialog.setContentView(R.layout.custom_alert_dailog);

                        TextView text = (TextView) dialog.findViewById(R.id.txt_message);
                        TextView cancel = (TextView) dialog.findViewById(R.id.cancel);
                        text.setText("Video upload successfully");

                        TextView dialogButton =  dialog.findViewById(R.id.submit);
                        dialogButton.setText("OK");
                        dialogButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                                Intent intent = new Intent(activity_video_preview.this, CelebHomeActivity.class);
                                startActivity(intent);
                                finishAffinity();

                            }
                        });


                        dialog.show();


                    } else {
                        final Dialog dialog = new Dialog(activity_video_preview.this);
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));                        dialog.setCancelable(false);
                        dialog.setContentView(R.layout.custom_alert_dailog);

                        TextView text = (TextView) dialog.findViewById(R.id.txt_message);
                        TextView cancel = (TextView) dialog.findViewById(R.id.cancel);
                        text.setText("Video upload Failed Try again!");

                        TextView dialogButton =  dialog.findViewById(R.id.submit);
                        dialogButton.setText("OK");
                        dialogButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();


                            }
                        });


                        dialog.show();
                    }

                }

                @Override
                public void onFailure(String exx) {
                    final Dialog dialog = new Dialog(activity_video_preview.this);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    dialog.setCancelable(false);
                    dialog.setContentView(R.layout.custom_alert_dailog);

                    TextView text = (TextView) dialog.findViewById(R.id.txt_message);
                    TextView cancel = (TextView) dialog.findViewById(R.id.cancel);
                    text.setText("Video upload Failed Try again!");

                    TextView dialogButton =  dialog.findViewById(R.id.submit);
                    dialogButton.setText("OK");
                    dialogButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();


                        }
                    });


                    dialog.show();
                }
            });
        }

    }

    void playVideo() {
        if (!videoView.isPlaying()) {
            videoView.start();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (!isChangingConfigurations()) {
            setVideoResult(null, "");
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        image_path = path;
        return Uri.parse(path);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 100 && resultCode == RESULT_OK && data != null) {
            //the image URI
            //calling the upload file method after choosing the file

        }
    }

/*    private class UploadFileToServer extends AsyncTask<Void, Integer, String> {

         ProgressDialog progressDialog11;
        @Override
        protected void onPreExecute() {
            // setting progress bar to zero

            progressDialog11  = ProgressDialog.show(activity_video_preview.this, null, "Please wait", true, false);

            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            // Making progress bar visible
         *//*   progressDialog11.setVisibility(View.VISIBLE);

            // updating progress bar value
            progressBar.setProgress(progress[0]);

            // updating percentage value
            txtPercentage.setText(String.valueOf(progress[0]) + "%");*//*
        }

        @Override
        protected String doInBackground(Void... params) {
            return uploadFile();
        }

        @SuppressWarnings("deprecation")
        private String uploadFile() {
            String responseString = null;

            HttpClient httpclient = new DefaultHttpClient();

            httpclient.getConnectionManager().getSchemeRegistry().register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));


            HttpPost httppost = new HttpPost(Urls.BASE_URL+"request/requestaction");

            httppost.setHeader(HTTP.CONTENT_TYPE, "application/json");
            httppost.setHeader("Authorization", auth_token.getAuthToken());



            try {
                AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
                        new AndroidMultiPartEntity.ProgressListener() {

                            @Override
                            public void transferred(long num) {
//                                publishProgress((int) ((num / (float) totalSize) * 100));
                            }
                        });



                File sourceFile = videoResult.getFile();
                File file2 = new File(image_path);
                String request_id = Request;
                String action = "1";

                System.out.println(image_path);


                // Adding file data to http body
                entity.addPart("video", new FileBody(sourceFile));
                entity.addPart("video_thumb", new FileBody(image_upload));

                // Extra parameters if you want to pass to server
                entity.addPart("request_id",new StringBody(request_id));
                entity.addPart("action", new StringBody(action));

//                totalSize = entity.getContentLength();
                httppost.setEntity(entity);

                // Making server call
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity r_entity = response.getEntity();

                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200) {
                    // Server response
                    responseString = EntityUtils.toString(r_entity);
                } else {
                    responseString = "Error occurred! Http Status Code: "
                            + statusCode;
                }

            } catch (ClientProtocolException e) {
                responseString = e.toString();
            } catch (IOException e) {
                responseString = e.toString();
            }

            return responseString;

        }

        @Override
        protected void onPostExecute(String result) {
            Log.e("shiv", "Response from server: " + result);
            progressDialog11.dismiss();


            super.onPostExecute(result);
        }

    }*/


    private String getRealPathFromURI(Uri profile_pic) {
        String[] proj = {MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(getApplicationContext(), profile_pic, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }


    private void uploadVideo() throws ParseException, IOException {


        String request_id = Request;
        String action = "1";


        Bitmap bMap = ThumbnailUtils.createVideoThumbnail(String.valueOf(videoResult.getFile()), MediaStore.Video.Thumbnails.MINI_KIND);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bMap.compress(Bitmap.CompressFormat.JPEG, 100, stream);


        Uri video_thumb = getImageUri(this, bMap);


        System.out.println("shiv sk " + videoResult.getSize());



        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        HttpClient httpclient = new DefaultHttpClient();

        httpclient.getConnectionManager().getSchemeRegistry().register(
                new Scheme("https", SSLSocketFactory.getSocketFactory(), 443)
        );

        HttpPost httppost = new HttpPost("https://www.camceleb.com/api/v1/request/requestaction");
        httppost.setHeader(HTTP.CONTENT_TYPE, "application/x-www-form-urlencoded");
        httppost.setHeader("Authorization", auth_token.getAuthToken());


        File file2 = new File(getRealPathFromURI(video_thumb));


        FileBody filebodyVideo = new FileBody(videoResult.getFile());
        FileBody filebodyimage = new FileBody(file2);
        StringBody request_id_str = new StringBody(request_id);
        StringBody action_str = new StringBody("1");

        MultipartEntity reqEntity = new MultipartEntity();
        reqEntity.addPart("videoFile", filebodyVideo);
        reqEntity.addPart("video_thumb", filebodyimage);
        reqEntity.addPart("request_id", request_id_str);
        reqEntity.addPart("action", action_str);

        httppost.setEntity(reqEntity);

        // DEBUG
        System.out.println( "executing request " + httppost.getRequestLine( ) );
        HttpResponse response = httpclient.execute( httppost );
        HttpEntity resEntity = response.getEntity( );

        // DEBUG
        System.out.println("shivkaran  "+ response.getStatusLine( ) );
        if (resEntity != null) {
            System.out.println( EntityUtils.toString( resEntity ) );
        } // end if

        if (resEntity != null) {
            resEntity.consumeContent( );
        } // end if

        httpclient.getConnectionManager( ).shutdown( );
    }

}
