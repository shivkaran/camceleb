package com.CamCeleb.CelebSide;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.CamCeleb.APIs.Celeb_DashBords;
import com.CamCeleb.POJOClass.Login_model.Gson_response_login_data;
import com.CamCeleb.POJOClass.ResultHandler;
import com.CamCeleb.R;

public class ChangeBankdetails extends AppCompatActivity {

    ImageView bankchange;

    String str_bankname = "";
    String str_branch = "";
    String str_Number = "";
    String str_code = "";
    String str_Owner = "";
    String beneficiary_name = "";

    EditText change_bankname, change_bankbranch, change_accountnumber, change_ifsccode, change_banfit_name, change_owner;

    Button change_submit;

    String[] follwerscount = {"Me", "Manager"};
    Spinner spinner;

    RelativeLayout ll_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_bankdetails);
        getSupportActionBar().hide();


        if (getIntent().getStringExtra("Owner") != null) {
            str_Owner = getIntent().getStringExtra("Owner");
        }
        if (getIntent().getStringExtra("Bankname") != null) {
            str_bankname = getIntent().getStringExtra("Bankname");
        }
        if (getIntent().getStringExtra("Bankbranch") != null) {
            str_branch = getIntent().getStringExtra("Bankbranch");
        }
        if (getIntent().getStringExtra("AccountNumber") != null) {
            str_Number = getIntent().getStringExtra("AccountNumber");
        }
        if (getIntent().getStringExtra("IFSCcode") != null) {
            str_code = getIntent().getStringExtra("IFSCcode");
        }
        if (getIntent().getStringExtra("beneficiary_name") != null) {
            beneficiary_name = getIntent().getStringExtra("beneficiary_name");
        }


        System.out.println(str_Owner);


        change_bankname = findViewById(R.id.change_bankname);
        change_bankbranch = findViewById(R.id.change_bankbranch);
        change_accountnumber = findViewById(R.id.change_accountnumber);
        change_ifsccode = findViewById(R.id.change_ifsccode);
        change_submit = findViewById(R.id.change_submit);
        change_banfit_name = findViewById(R.id.change_banfit_name);
        spinner = findViewById(R.id.spinner);
        ll_back = findViewById(R.id.ll_back);

        bankchange = findViewById(R.id.bankback);

        change_bankname.setText(str_bankname);
        change_bankbranch.setText(str_branch);
        change_accountnumber.setText(str_Number);
        change_ifsccode.setText(str_code);
        change_banfit_name.setText(beneficiary_name);


        ArrayAdapter adapters = new ArrayAdapter(ChangeBankdetails.this, android.R.layout.simple_dropdown_item_1line, follwerscount);
        spinner.setAdapter(adapters);

        if (str_Owner.equalsIgnoreCase("Me")) {
            spinner.setSelection(0);
        } else if (str_Owner.equalsIgnoreCase("Manager")) {
            spinner.setSelection(1);
        }

        ll_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();

            }
        });

        change_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String str_beneficiary_name = change_banfit_name.getText().toString().trim();
                String str_banckname = change_bankname.getText().toString().trim();
                String str_branch_name = change_bankbranch.getText().toString().trim();
                String str_account_number = change_accountnumber.getText().toString().trim();
                String str_ifsc_code = change_ifsccode.getText().toString().trim();
                String strchange_owner = spinner.getSelectedItem().toString().trim();

                if (str_beneficiary_name.equals("")) {
                    change_banfit_name.setError("Please enter Beneficiary Name");
                } else if (strchange_owner.equals("")) {
                    change_owner.setError("Please enter Owner");
                } else if (str_banckname.equals("")) {
                    change_bankname.setError("Please enter Bank Name");
                } else if (str_branch_name.equals("")) {
                    change_bankbranch.setError("Please enter Branch Name");

                } else if (str_account_number.equals("")) {
                    change_accountnumber.setError("Please enter Account number");

                } else if (str_ifsc_code.equals("")) {
                    change_ifsccode.setError("Please enter IFSC Code");

                } else {


                    UpdateBanckDetails(strchange_owner, str_banckname, str_branch_name, str_account_number, str_ifsc_code, str_beneficiary_name);
                }


            }
        });
    }

    private void UpdateBanckDetails(String strchange_owner, String str_banckname, String str_branch_name, String str_account_number, String str_ifsc_code, String str_beneficiary_name) {

        final ProgressDialog progressDialog = new ProgressDialog(ChangeBankdetails.this);
        progressDialog.show();
        Celeb_DashBords celeb_dashBords = new Celeb_DashBords();

        celeb_dashBords.UpdateBankDetails(strchange_owner, str_banckname, str_branch_name, str_account_number, str_ifsc_code, str_beneficiary_name, new ResultHandler<Gson_response_login_data>() {
            @Override
            public void onSuccess(Gson_response_login_data data) {
                if (data.getSuccess() == 1) {
                    progressDialog.dismiss();
                    data.getData().SaveSession();
                    change_bankname.setText(data.getData().getBankName());
                    change_bankbranch.setText(data.getData().getBranchName());
                    change_accountnumber.setText(data.getData().getAccountNumber());
                    change_ifsccode.setText(data.getData().getIfscCode());

                    final Dialog dialog = new Dialog(ChangeBankdetails.this);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    dialog.setCancelable(false);
                    dialog.setContentView(R.layout.custom_alert_dailog);

                    TextView text = (TextView) dialog.findViewById(R.id.txt_message);
                    TextView cancel = (TextView) dialog.findViewById(R.id.cancel);
                    text.setText("Bank Details Updated Successfully!");

                    TextView dialogButton = dialog.findViewById(R.id.submit);
                    dialogButton.setText("OK");
                    dialogButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();

                            onBackPressed();
                        }
                    });


                    dialog.show();


                } else {
                    progressDialog.dismiss();
                    final Dialog dialog = new Dialog(ChangeBankdetails.this);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    dialog.setCancelable(false);
                    dialog.setContentView(R.layout.custom_alert_dailog);

                    TextView text = (TextView) dialog.findViewById(R.id.txt_message);
                    TextView cancel = (TextView) dialog.findViewById(R.id.cancel);
                    text.setText("Bank Details not Updated try again!");

                    TextView dialogButton = dialog.findViewById(R.id.submit);
                    dialogButton.setText("OK");
                    dialogButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();

                        }
                    });


                    dialog.show();

                }
            }

            @Override
            public void onFailure(String exx) {
                final Dialog dialog = new Dialog(ChangeBankdetails.this);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.setCancelable(false);
                dialog.setContentView(R.layout.custom_alert_dailog);

                TextView text = (TextView) dialog.findViewById(R.id.txt_message);
                TextView cancel = (TextView) dialog.findViewById(R.id.cancel);
                text.setText("Bank Details not Updated try again!");

                TextView dialogButton = dialog.findViewById(R.id.submit);
                dialogButton.setText("OK");
                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();

                    }
                });


                dialog.show();

            }
        });
    }
}
