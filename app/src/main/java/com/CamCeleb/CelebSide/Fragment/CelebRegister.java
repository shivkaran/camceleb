package com.CamCeleb.CelebSide.Fragment;


import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.CamCeleb.APIs.Authorization;
import com.CamCeleb.APIs.Helper;
import com.CamCeleb.Activity.LoginAndSignActivity;
import com.CamCeleb.CelebSide.CelebHomeActivity;
import com.CamCeleb.POJOClass.Categoryfield;
import com.CamCeleb.POJOClass.Login_model.Gson_response_login_data;
import com.CamCeleb.POJOClass.ResultHandler;
import com.CamCeleb.R;
import com.CamCeleb.UserSide.OTPVerificationActivity;
import com.libaml.android.view.chip.ChipLayout;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class CelebRegister extends Fragment {

    TextView frameLayout;
    Uri profileUrl = null;
    String[] countries = {"Actor", "Comedian", "Singer", "Youtuber", "Sport Person", "Influencers"};
    Spinner spinner, follwspinner;
    EditText fullname, email, password, conpassword, phonenumber, pricepervideo, wherefind, urls, amount, introdction;
    Button celebtn;
    //    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    String emailPattern = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]+";
    ImageView profile_image;
    String[] findname = {"FaceBook", "Instagram", "Twitter", "Youtube", "TikTok"};
    String[] follwerscount = {"M   (Millennials)", "K   (Thousand)"};
    TextView textspinn;


    ChipLayout chip;
    List<String> data_of_category;
    List<String> data_of_category_all;
    List<String> data_of_category_remember;
    String categorylist = "";
    public static SharedPreferences preferences;
    ArrayList<Categoryfield> categoryfields;
    ArrayAdapter<String> adapter3;
    TextView txt_terms_condition,txt_category;
    TextView txt_add_photo;
    boolean aBoolean = true;


    public CelebRegister() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

       /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,

                    Uri.parse("package:" + getContext().getPackageName()));

            startActivity(intent);
            return;
        }*/
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

       /* if (requestCode == 100 && resultCode == RESULT_OK && data != null) {
            //the image URI
            Uri selectedImage = data.getData();

            String[] filePath = {MediaStore.Images.Media.DATA};
            Cursor c = getActivity().getContentResolver().query(selectedImage, filePath, null, null, null);
            c.moveToFirst();
            int columnIndex = c.getColumnIndex(filePath[0]);
            String picturePath = c.getString(columnIndex);
            c.close();
            Bitmap thumbnail = (BitmapFactory.decodeFile(picturePath));
            System.out.println("path of image from gallery......******************........." + picturePath + "");


            File pictureFile = new File(picturePath);

            thumbnail =   Utill.ExifInterfaceImage(pictureFile,thumbnail);

//            profile_image.setImageBitmap(thumbnail);


              profile_image.setImageURI(selectedImage);

            profileUrl = selectedImage;
            //calling the upload file method after choosing the file

        }*/
        CropImage.ActivityResult result = CropImage.getActivityResult(data);
        if (resultCode == RESULT_OK) {
            Uri resultUri = result.getUri();

            profileUrl = resultUri;
            profile_image.setImageURI(resultUri);

        } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
            Exception error = result.getError();
        }
    }


    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_celeb_register, container, false);

        preferences = getActivity().getSharedPreferences(getActivity().getPackageName() + "_preferences", MODE_PRIVATE);


        chip = (ChipLayout) view.findViewById(R.id.edit_chipText);
        profile_image = view.findViewById(R.id.celeb_image);
        spinner = view.findViewById(R.id.spinner);
        follwspinner = view.findViewById(R.id.spinnerfollw);
        fullname = view.findViewById(R.id.celec_fullname);
        frameLayout = view.findViewById(R.id.celeb_aleradyaccount);
        celebtn = view.findViewById(R.id.celec_submitbutton);
        email = view.findViewById(R.id.celec_email);
        password = view.findViewById(R.id.celec_password);
        conpassword = view.findViewById(R.id.celec_confirmpassword);
        phonenumber = view.findViewById(R.id.celec_phonenumber);
        textspinn = view.findViewById(R.id.textspinn);
        pricepervideo = view.findViewById(R.id.celec_pricevideo);
        /*wherefind = view.findViewById(R.id.celec_wherefindyou);*/
        urls = view.findViewById(R.id.celec_url);
        amount = view.findViewById(R.id.celec_followers);
        introdction = view.findViewById(R.id.celec_intro);
        txt_add_photo = view.findViewById(R.id.txt_add_photo);

        txt_terms_condition = view.findViewById(R.id.txt_terms_condition);
        txt_category = view.findViewById(R.id.txt_category);






        data_of_category_all = new ArrayList<>();
        data_of_category = new ArrayList<>();
        data_of_category_remember = new ArrayList<>();

        categorylist = preferences.getString("categorylist", "");
        try {
            categoryfields = new ArrayList<>();
            JSONArray jsonArray = new JSONArray(categorylist);

            for (int i = 0; i < jsonArray.length(); i++) {

                JSONObject jsonObject = jsonArray.getJSONObject(i);

                String id = jsonObject.getString("id");
                String name = jsonObject.getString("name");
                categoryfields.add(new Categoryfield(id, name));
                data_of_category_remember.add(name);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

       /* adapter3 = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, data_of_category_remember);
        chip.setAdapter(adapter3);

//        chip.setText(data_of_category);*/


        txt_category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertStringList("Chip", data_of_category_remember);
            }
        });


        chip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertStringList("Chip", data_of_category_remember);
            }
        });

     /*   chip.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                return false;
            }
        });
*/


        chip.setOnChipItemChangeListener(new ChipLayout.ChipItemChangeListener() {
            @Override
            public void onChipAdded(int pos, String txt) {

            }

            @Override
            public void onChipRemoved(int pos, String txt) {
                System.out.println(txt);
                data_of_category_remember.add(txt);

                data_of_category.remove(txt);

                adapter3 = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, data_of_category_remember);
                chip.setAdapter(adapter3);
            }
        });

        txt_terms_condition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.camceleb.com/terms-condition"));
                startActivity(browserIntent);
            }
        });

//

        pricepervideo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (!s.toString().equalsIgnoreCase("")) {
                    int price = Integer.parseInt(s.toString());

                    if (price < 100) {
                        pricepervideo.setError("Booking amount must be more then 100");
                        pricepervideo.requestFocus();
                    }
                }



            }
        });


        profile_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //opening file chooser
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(getContext(),
                        READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {


                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{READ_EXTERNAL_STORAGE},
                            100);


                } else {
                    CropImage.activity()
                            .setGuidelines(CropImageView.Guidelines.ON)
                            .setAspectRatio(1, 1)
                            .start(getActivity());
                }

            }
        });

        txt_add_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(getContext(),
                        READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {


                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{READ_EXTERNAL_STORAGE},
                            100);


                } else {
                    CropImage.activity()
                            .setGuidelines(CropImageView.Guidelines.ON)
                            .setAspectRatio(1, 1)
                            .start(getActivity());
                }
            }
        });


        ArrayAdapter adapter = new ArrayAdapter(getContext(), android.R.layout.simple_dropdown_item_1line, findname);
        spinner.setAdapter(adapter);
        ArrayAdapter adapters = new ArrayAdapter(getContext(), android.R.layout.simple_dropdown_item_1line, follwerscount);
        follwspinner.setAdapter(adapters);


        textspinn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(getActivity(), textspinn);
                //Inflating the Popup using xml file
                popup.getMenuInflater()
                        .inflate(R.menu.menu_popup_flowing_count, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        if (item.getTitle().toString().equalsIgnoreCase("K (Thousand)")) {

                            textspinn.setText("K");

                        } else if (item.getTitle().toString().equalsIgnoreCase("M (Millennials)")) {
                            textspinn.setText("M");
                        }
                        return true;
                    }
                });

                popup.show();
            }
        });


        celebtn.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                int price = 0;
                if (!pricepervideo.getText().toString().trim().isEmpty()) {
                    price = Integer.parseInt(pricepervideo.getText().toString().trim());
                }
                if (fullname.getText().toString().isEmpty()) {
                    fullname.setError("Please enter full name");
                    fullname.requestFocus();
                }
                if (email.getText().toString().isEmpty()) {
                    email.setError("Please enter email");
                    email.requestFocus();
                } else if (!email.getText().toString().matches(emailPattern)) {
                    email.setError("Please enter valid email");
                    email.requestFocus();

                } else if (password.getText().toString().isEmpty()) {
                    password.setError("Please enter password");
                    password.requestFocus();

                } else if (password.getText().toString().trim().length() < 5) {
                    password.setError("Please enter password minimum 5 characters");
                    password.requestFocus();
                } else if (conpassword.getText().toString().isEmpty()) {
                    conpassword.setError("Please enter confirm password");
                    conpassword.requestFocus();

                } else if (!conpassword.getText().toString().equals(password.getText().toString())) {
                    conpassword.setError("Confirm password must match with password");
                    conpassword.requestFocus();

                } else if (phonenumber.getText().toString().isEmpty()) {
                    phonenumber.setError("Please enter contact number");
                    phonenumber.requestFocus();
                } else if (pricepervideo.getText().toString().isEmpty()) {
                    pricepervideo.setError("Please enter price");
                    pricepervideo.requestFocus();

                } else if (introdction.getText().toString().isEmpty()) {
                    introdction.setError("Please enter Intro");
                    introdction.requestFocus();
                } else if (profileUrl == null) {
                    Toast.makeText(getContext(), "please select profile picture ", Toast.LENGTH_SHORT).show();
                } else if (price < 100) {
                    pricepervideo.setError("Booking amount must be more then 100");
                    pricepervideo.requestFocus();
                } else {
                    String category_for_chip = String.valueOf(chip.getText());

                    List<String> categorylist = new ArrayList<>();

                    for (int i = 0; i < categoryfields.size(); i++) {
                        String name_of_category = categoryfields.get(i).getName();
                        String name_of_category_id = categoryfields.get(i).getId();
                        if (category_for_chip.contains(name_of_category)) {
                            categorylist.add(name_of_category_id);
                        }
                    }

                    String category_list_id = categorylist.toString().replace("[", "");
                    String category_list_id1 = category_list_id.replace("]", "");
                    String category_list_id2 = category_list_id1.replace(" ", "");

                    System.out.println(categorylist.toString());


                    String names = fullname.getText().toString().trim();
                    String emails = email.getText().toString().trim();
                    String passwords = password.getText().toString().trim();
                    String phonenumbers = phonenumber.getText().toString().trim();
                    String booking_prices = pricepervideo.getText().toString().trim();
                    String social_page_names = urls.getText().toString().trim();
                    String social_platform_types = String.valueOf(spinner.getSelectedItem());
                    String followers_counts = amount.getText().toString() + textspinn.getText().toString().trim();
                    String categoriess = category_list_id2;
                    String about_mes = introdction.getText().toString().trim();


                    Authorization authorization = new Authorization();

                    authorization.userSignUp(getActivity(), Helper.createRequestBody("C"), Helper.createRequestBody(names), Helper.createRequestBody(emails), Helper.createRequestBody(passwords), Helper.createRequestBody(phonenumbers),
                            Helper.createRequestBody(booking_prices), Helper.createRequestBody(social_page_names)
                            , Helper.createRequestBody(social_platform_types), Helper.createRequestBody(followers_counts),
                            Helper.createRequestBody(categoriess), Helper.createRequestBody(about_mes), profileUrl, new ResultHandler<Gson_response_login_data>() {
                                @Override
                                public void onSuccess(Gson_response_login_data data) {

                                    if (data.getSuccess() == 1) {

                                        if (data.getData().getIdVerification().equals("noverify")) {

                                            data.getData().SaveSession();
                                            Intent intent = new Intent(getContext(), OTPVerificationActivity.class);
                                            startActivity(intent);

                                        } else if (data.getData().getAdmin_verification().equals("N")) {
                                            ShowDailog("Your profile is under review.\nPlease wait for admin process");
                                        } else {
                                            data.getData().SaveSession();
                                            Intent intent = new Intent(getContext(), CelebHomeActivity.class);
                                            startActivity(intent);
                                            getActivity().finish();

                                        }
                                    } else {
                                        String message = data.getError().toString();
                                        String message1 = message.replace("[", "");
                                        String message2 = message1.replace("]", "");
                                        ShowDailog(message2);
                                    }
                                }

                                @Override
                                public void onFailure(String exx) {

                                }
                            });

                }
            }

        });


        frameLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(getContext(), LoginAndSignActivity.class);
                startActivity(intent);
                getActivity().finish();

            }
        });


        return view;
    }

    private void alertStringList(String title, final List<String> list) {

        final CharSequence[] items = list.toArray(new CharSequence[list.size()]);

        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity(), R.style.MyDialogTheme);
        builder.setTitle("Category");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                String txt = (String) items[item];


                for (int i = 0; i < data_of_category_remember.size(); i++) {
                    if (txt.equals(data_of_category_remember.get(i))) {
                        data_of_category_remember.remove(i);
                    }
                }
                txt_category.setVisibility(View.GONE);
                chip.setVisibility(View.VISIBLE);
                data_of_category.add(txt);
                chip.setText(data_of_category);

            }
        });
        builder.show();


    }

    private void ShowDailog(String message) {


        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dailog_layout);

        TextView text = (TextView) dialog.findViewById(R.id.txt_message);
        text.setText(message.toString());

        Button dialogButton = (Button) dialog.findViewById(R.id.submit);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(getContext(), LoginAndSignActivity.class);
                startActivity(intent);
                getActivity().finish();

            }
        });

        dialog.show();


    }


}








