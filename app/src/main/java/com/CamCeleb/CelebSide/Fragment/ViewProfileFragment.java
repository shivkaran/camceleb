package com.CamCeleb.CelebSide.Fragment;


import android.animation.ObjectAnimator;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.CamCeleb.APIs.Celeb_DashBords;
import com.CamCeleb.Adapters.CelebAdapter.CelebIntroActorAdapter;
import com.CamCeleb.CelebSide.Celeb_Setting_screen;
import com.CamCeleb.POJOClass.Celeb_profile_details.Gson_Response_Celeb_profile_details;
import com.CamCeleb.POJOClass.Celeb_profile_details.Response_Celeb_profile_details;
import com.CamCeleb.POJOClass.ResultHandler;
import com.CamCeleb.POJOClass.User;
import com.CamCeleb.R;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

/**
 * A simple {@link Fragment} subclass.
 */
public class ViewProfileFragment extends Fragment implements Player.EventListener {


    PlayerView videoFullScreenPlayer;
    SimpleExoPlayer player;
    ImageView setting, profile_image, image_share;
    TextView txt_user_name, txt_category, txt_about_me, txt_name_of_testimonils, tv_progress_count;

    RecyclerView actor_intro_recycler;

    ProgressBar progress;
    public static SharedPreferences preferences;
    String session;

    String user_id = "";
    User user;

    ShimmerFrameLayout parentShimmerLayout;

    LinearLayout ll_header;
    String videourl = "";
    boolean aBoolean_vlume = true;


    String start = "0";
    String limit = "10";
    boolean isLoading = false;

    Response_Celeb_profile_details response_celeb_profile_details;
    CelebIntroActorAdapter adapter;


    public ViewProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_view_profile, container, false);

        preferences = getActivity().getSharedPreferences(getActivity().getPackageName() + "_preferences", getActivity().MODE_PRIVATE);

        session = preferences.getString("SessionData", "");
        Gson gson = new Gson();
        user = gson.fromJson(session, User.class);
        user_id = String.valueOf(user.getId());


        parentShimmerLayout = view.findViewById(R.id.parentShimmerLayout);
        parentShimmerLayout.startShimmerAnimation();
        ll_header = view.findViewById(R.id.ll_ll_header);

        profile_image = view.findViewById(R.id.profile_image);
        image_share = view.findViewById(R.id.image_share);
        txt_user_name = view.findViewById(R.id.txt_user_name);
        txt_category = view.findViewById(R.id.txt_category);
        txt_about_me = view.findViewById(R.id.txt_about_me);
        txt_name_of_testimonils = view.findViewById(R.id.txt_name_of_testimonils);
        actor_intro_recycler = view.findViewById(R.id.actor_intro_recycler);
        setting = view.findViewById(R.id.setting);
        progress = view.findViewById(R.id.progress_bar);
        tv_progress_count = view.findViewById(R.id.txt_count);

        videoFullScreenPlayer = view.findViewById(R.id.videoFullScreenPlayer);
        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), Celeb_Setting_screen.class));
            }
        });

        /*if (user.getIsProfileCompleted() == 1) {
            progress.setProgress(100);
            tv_progress_count.setText("100%");
        } else {
            progress.setProgress(50);
            tv_progress_count.setText("50%");
        }
*/

        if (player == null) {
            // 1. Create a default TrackSelector
          /*  int MIN_BUFFER_DURATION = 300;
            //Max Video you want to buffer during PlayBack
            int MAX_BUFFER_DURATION = 500;
            //Min Video you want to buffer before start Playing it
            int MIN_PLAYBACK_START_BUFFER = 500;
            //Min video You want to buffer when user resumes video
            int MIN_PLAYBACK_RESUME_BUFFER = 500;

            LoadControl loadControl = new DefaultLoadControl(
                    new DefaultAllocator(true, 16),
                    MIN_BUFFER_DURATION,
                    MAX_BUFFER_DURATION,
                    MIN_PLAYBACK_START_BUFFER,
                    MIN_PLAYBACK_RESUME_BUFFER, -1, true);
*/
            LoadControl loadControl = new DefaultLoadControl();
            BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
            TrackSelection.Factory videoTrackSelectionFactory =
                    new AdaptiveTrackSelection.Factory(bandwidthMeter);
            TrackSelector trackSelector =
                    new DefaultTrackSelector(videoTrackSelectionFactory);
            // 2. Create the player
            player = ExoPlayerFactory.newSimpleInstance(new DefaultRenderersFactory(getActivity()), trackSelector, loadControl);
            videoFullScreenPlayer.setPlayer(player);
            videoFullScreenPlayer.getPlayer();

        }


        videoFullScreenPlayer.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (aBoolean_vlume) {
                    player.setVolume(0f);
                    aBoolean_vlume = false;
                } else {
                    player.setVolume(100f);
                    aBoolean_vlume = true;
                }
                return false;
            }
        });

        Getprofiledetails();


        image_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String subject = "Hey! Checkout my profile on CamCeleb.";

                String body = subject + " \n\n https://www.camceleb.com/celebprofile/" + user_id;


                Intent txtIntent = new Intent(android.content.Intent.ACTION_SEND);
                txtIntent.setType("text/plain");
                txtIntent.putExtra(android.content.Intent.EXTRA_TEXT, body);
                startActivity(Intent.createChooser(txtIntent, "Share"));
            }
        });


        actor_intro_recycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                if (!isLoading) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == response_celeb_profile_details.getTestimonials().size() - 1) {
                        //bottom of list!

                        int countoflimit = Integer.parseInt(limit);

                        countoflimit = countoflimit + 10;

                        limit = String.valueOf(countoflimit);
                        Getprofiledetails1();
//                        isLoading = true;
                    }
                }
            }
        });


        return view;
    }

    private void Getprofiledetails() {

        Celeb_DashBords celeb_dashBords = new Celeb_DashBords();

        celeb_dashBords.celebritydetail(user_id, "0", "10", new ResultHandler<Gson_Response_Celeb_profile_details>() {
            @Override
            public void onSuccess(Gson_Response_Celeb_profile_details data) {
                if (data.getSuccess() == 1) {

                    InitView(data.getData());

                }
            }

            @Override
            public void onFailure(String exx) {
                parentShimmerLayout.stopShimmerAnimation();
                parentShimmerLayout.setVisibility(View.GONE);
                ll_header.setVisibility(View.VISIBLE);
            }
        });


    }

    private void Getprofiledetails1() {

        Celeb_DashBords celeb_dashBords = new Celeb_DashBords();

        celeb_dashBords.celebritydetail(user_id, start, limit, new ResultHandler<Gson_Response_Celeb_profile_details>() {
            @Override
            public void onSuccess(Gson_Response_Celeb_profile_details data) {
                if (data.getSuccess() == 1) {



                    adapter.setDataChange(data.getData().getTestimonials());
                    txt_name_of_testimonils.setText("Testimonials(" + data.getData().getTestimonials().size() + ")");
                   /* response_celeb_profile_details = data.getData();

                    adapter.notify();*/

                  /*  adapter = new CelebIntroActorAdapter(getActivity(), response_celeb_profile_details.getTestimonials());
                    //setting adapter to recyclerview
                    actor_intro_recycler.setAdapter(adapter);
//                    adapter.notifyDataSetChanged();*/

                }
            }

            @Override
            public void onFailure(String exx) {
                parentShimmerLayout.stopShimmerAnimation();
                parentShimmerLayout.setVisibility(View.GONE);
                ll_header.setVisibility(View.VISIBLE);
            }
        });


    }

    private void InitView(Response_Celeb_profile_details data) {


        response_celeb_profile_details = data;

        parentShimmerLayout.stopShimmerAnimation();
        parentShimmerLayout.setVisibility(View.GONE);
        ll_header.setVisibility(View.VISIBLE);

        videourl = data.getIntroVideo();
        buildMediaSource(Uri.parse(data.getIntroVideo()));

        if (data.getProfilePic().isEmpty()) {
            profile_image.setImageDrawable(getResources().getDrawable(R.mipmap.profile_pic));
        } else {
            Picasso.with(getActivity()).load(data.getProfilePic()).into(profile_image);
        }
        txt_user_name.setText(data.getFullName());
        txt_about_me.setText(data.getAboutMe());
        txt_category.setText(data.getCategories());


        txt_name_of_testimonils.setText("Testimonials(" + data.getTestimonials().size() + ")");
        actor_intro_recycler.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        actor_intro_recycler.setLayoutManager(layoutManager);


        adapter = new CelebIntroActorAdapter(getActivity(), response_celeb_profile_details.getTestimonials());
        //setting adapter to recyclerview
        actor_intro_recycler.setAdapter(adapter);

        int count = 0;

        if (!data.getProfilePic().isEmpty()) {
            count = 30;
        }
        if (!user.getIntroVideo().isEmpty()) {
            count = count + 30;
        }
        if (!user.getBankName().isEmpty() && !user.getAccountNumber().isEmpty()) {
            count = count + 40;
        }


        if (user.getIsProfileCompleted() == 1) {
//            progress.setProgress(50);
            ObjectAnimator.ofInt(progress, "progress", count)
                    .setDuration(1000)
                    .start();
            tv_progress_count.setText(count + "%");
        } else {
            ObjectAnimator.ofInt(progress, "progress", count)
                    .setDuration(1000)
                    .start();
            tv_progress_count.setText(count + "%");
        }


    }

    private void buildMediaSource(Uri mUri) {
        // Measures bandwidth during playback. Can be null if not required.
        DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        // Produces DataSource instances through which media data is loaded.
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(getActivity(),
                Util.getUserAgent(getActivity(), getString(R.string.app_name)), bandwidthMeter);
        // This is the MediaSource representing the media to be played.
        MediaSource videoSource = new ExtractorMediaSource.Factory(dataSourceFactory).createMediaSource(mUri);
        // Prepare the player with the source.
        player.prepare(videoSource);
        player.getPlaybackParameters();
        player.setRepeatMode(Player.REPEAT_MODE_ONE);
        player.setPlayWhenReady(true);
        player.addListener(this);

    }


    private PackageManager getPackageManager() {
        return null;
    }

    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        if (playbackState == Player.STATE_ENDED) {
            player.getPlaybackLooper();
        }
    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {
        player.getPlaybackLooper();
    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

    }

    @Override
    public void onPositionDiscontinuity(int reason) {

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onSeekProcessed() {

    }

    @Override
    public void onPause() {
        super.onPause();
        if (player != null) {
            player.stop();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (player != null) {
            player.stop();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!videourl.equals("")) {
            buildMediaSource(Uri.parse(videourl));
        }

        preferences = getActivity().getSharedPreferences(getActivity().getPackageName() + "_preferences", getActivity().MODE_PRIVATE);

        session = preferences.getString("SessionData", "");
        Gson gson = new Gson();
        user = gson.fromJson(session, User.class);


        Getprofiledetails();


    }
}
