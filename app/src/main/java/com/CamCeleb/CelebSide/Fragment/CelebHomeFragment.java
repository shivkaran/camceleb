package com.CamCeleb.CelebSide.Fragment;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.viewpager.widget.ViewPager;

import com.CamCeleb.APIs.Celeb_DashBords;
import com.CamCeleb.CelebSide.celebdashboardFragment.Complate_Request;
import com.CamCeleb.CelebSide.celebdashboardFragment.New_Request;
import com.CamCeleb.CelebSide.celebdashboardFragment.Pending_Request;
import com.CamCeleb.POJOClass.Celeb_Home_Request.Gson_Response_Celeb_Request_list;
import com.CamCeleb.POJOClass.ResultHandler;
import com.CamCeleb.R;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class CelebHomeFragment extends Fragment {


    String pending_count = "";
    String accepted_count = "";
    String completed_count = "";

    TabLayout tabLayout;
    ViewPager viewPager;

    ShimmerFrameLayout parentShimmerLayout;

    private static final int REQUEST_VIDEO_CAPTURE = 300;

    boolean flag = false;

    public static SharedPreferences preferences;


    BroadcastReceiver broadcastReceiver;

    FragmentManager childFragmentManager;

    ViewPagerAdapter adapter;

    public CelebHomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        childFragmentManager = getChildFragmentManager();

    }

    @Override
    public void onStart() {
        super.onStart();


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_celeb_home, container, false);


        flag = true;

        preferences = getActivity().getSharedPreferences(getActivity().getPackageName() + "_preferences", MODE_PRIVATE);


        parentShimmerLayout = view.findViewById(R.id.parentShimmerLayout);

        parentShimmerLayout.startShimmerAnimation();


        tabLayout = view.findViewById(R.id.tablayout);
        viewPager = view.findViewById(R.id.viewPage);


        tabLayout.setupWithViewPager(viewPager);


        viewPager.setOffscreenPageLimit(3);


        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);


       /* final Handler handler=new Handler();

        final Runnable updateTask=new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getActivity(), "datta", Toast.LENGTH_SHORT).show();
                String updatestartus = preferences.getString("updatestartus", "");
                if (!updatestartus.equals("")) {
                    ViewPageAPiCall();
                }else {
                    Toast.makeText(getActivity(), "datta", Toast.LENGTH_SHORT).show();
                }

            }
        };

        handler.postDelayed(updateTask,2000);
*/


        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        ViewPageAPiCall();

        registerReceiver();
    }

    private void registerReceiver() {

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getStringExtra("request_id") != null) {

                    ViewPageAPiCall1();
                }
                if (intent.getStringExtra("status") != null) {

                    ViewPageAPiCall1();
                }
            }
        };
        getActivity().registerReceiver(broadcastReceiver, new IntentFilter("pushnotifiaction"));
    }

    private void ViewPageAPiCall() {

        Celeb_DashBords dashBords = new Celeb_DashBords();

        dashBords.CelebDash("0", "100","1",new ResultHandler<Gson_Response_Celeb_Request_list>() {

            @Override
            public void onSuccess(Gson_Response_Celeb_Request_list data) {

                if (data.getSuccess() == 1) {

                    pending_count = String.valueOf(data.getData().getPendingCount());
                    accepted_count = String.valueOf(data.getData().getAcceptedCount());
                    completed_count = String.valueOf(data.getData().getCompletedCount());
                    SharedPreferences.Editor ed = preferences.edit();
                    ed.putString("updatestartus", ""); //
                    ed.commit();


                    setupViewPager(viewPager);
                }

            }

            @Override
            public void onFailure(String exx) {

            }
        });

    }
    private void ViewPageAPiCall1() {

        Celeb_DashBords dashBords = new Celeb_DashBords();

        dashBords.CelebDash("0", "100","1",new ResultHandler<Gson_Response_Celeb_Request_list>() {

            @Override
            public void onSuccess(Gson_Response_Celeb_Request_list data) {

                if (data.getSuccess() == 1) {

                    pending_count = String.valueOf(data.getData().getPendingCount());
                    accepted_count = String.valueOf(data.getData().getAcceptedCount());
                    completed_count = String.valueOf(data.getData().getCompletedCount());
                    SharedPreferences.Editor ed = preferences.edit();
                    ed.putString("updatestartus", ""); //
                    ed.commit();


                    viewPager.removeAllViews();
                    setupViewPager(viewPager);


                }

            }

            @Override
            public void onFailure(String exx) {

            }
        });

    }

    private void setupViewPager(ViewPager viewPager) {

        adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new New_Request(), "NEW\n" + pending_count);
        adapter.addFragment(new Pending_Request(), "PENDING\n" + accepted_count);
        adapter.addFragment(new Complate_Request(), "COMPLETED\n" + completed_count);
        viewPager.setAdapter(adapter);

        parentShimmerLayout.stopShimmerAnimation();
        parentShimmerLayout.setVisibility(View.GONE);


    }


    class ViewPagerAdapter extends FragmentStatePagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (flag) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    ViewPageAPiCall1();
                }
            }, 500);
        }
    }


}
