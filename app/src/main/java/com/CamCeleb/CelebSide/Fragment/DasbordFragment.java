package com.CamCeleb.CelebSide.Fragment;


import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.CamCeleb.APIs.Celeb_DashBords;
import com.CamCeleb.Adapters.celeb_Video_ProfileApdater;
import com.CamCeleb.CelebSide.EraningActivity;
import com.CamCeleb.POJOClass.Celeb_dashoad.Celeb_video;
import com.CamCeleb.POJOClass.Celeb_dashoad.Gson_response_celeb_dashbord;
import com.CamCeleb.POJOClass.Login_model.Gson_response_login_data;
import com.CamCeleb.POJOClass.ResultHandler;
import com.CamCeleb.POJOClass.User;
import com.CamCeleb.R;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.suke.widget.SwitchButton;

import java.text.DecimalFormat;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class DasbordFragment extends Fragment {


    ViewGroup viewGroup;

    FrameLayout first, secoend, three;

    ImageView userProfile;
    TextView txt_user_name, txt_follwere, txt_rating, txt_earned, txt_booking_price, txt_videos_count;

    String session;
    ProgressBar progress;
    SwitchButton switch_online;

    RecyclerView profilelastviewfav;
    String price;
    public static SharedPreferences preferences;

    LinearLayout ll_profile_details, ll_view_myvideo;

    ShimmerFrameLayout parentShimmerLayout;

    TextView txt_message;


    String start = "0";
    String limit = "10";
    boolean isLoading = false;

    celeb_Video_ProfileApdater celebProfileApdater;

    List<Celeb_video> favouriteList;

    public DasbordFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_dasbord, container, false);

        parentShimmerLayout = view.findViewById(R.id.parentShimmerLayout);
        parentShimmerLayout.startShimmerAnimation();

//        gridView = view.findViewById(R.id.viewprofile_uservideos);
        first = view.findViewById(R.id.firstfr);
        secoend = view.findViewById(R.id.twofr);
        three = view.findViewById(R.id.threefr);
        userProfile = view.findViewById(R.id.img_profile);
        txt_user_name = view.findViewById(R.id.txt_user_name);
        txt_follwere = view.findViewById(R.id.txt_follwere);
        txt_rating = view.findViewById(R.id.txt_rating);
        txt_earned = view.findViewById(R.id.txt_earned);
        switch_online = view.findViewById(R.id.switch_online);
        txt_videos_count = view.findViewById(R.id.txt_videos_count);
        txt_booking_price = view.findViewById(R.id.txt_booking_price);
        profilelastviewfav = view.findViewById(R.id.profilelastviewfav);
        ll_profile_details = view.findViewById(R.id.ll_profile_details);
        txt_message = view.findViewById(R.id.txt_no_validation);
        ll_view_myvideo = view.findViewById(R.id.ll_view_myvideo);


        preferences = getActivity().getSharedPreferences(getActivity().getPackageName() + "_preferences", getActivity().MODE_PRIVATE);

        session = preferences.getString("SessionData", "");
        Gson gson = new Gson();
        User user = gson.fromJson(session, User.class);


        price = user.getBookingPrice();
        txt_booking_price.setText(price);


        GetDashbordData();


        profilelastviewfav.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                if (!isLoading) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == favouriteList.size() - 1) {
                        //bottom of list!

                        int countoflimit = Integer.parseInt(limit);

                        countoflimit = countoflimit + 10;

                        limit = String.valueOf(countoflimit);
                        GetNewRequest1();
//                        isLoading = true;
                    }
                }
            }
        });

        first.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getContext(), EraningActivity.class);
                startActivity(intent);
            }
        });


        switch_online.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SwitchButton view, boolean isChecked) {

                String value = "";
                if (isChecked) {
                    value = "1";

                } else {
                    value = "0";
                }
                ProfileUpdateData(value, "online");

            }
        });

        secoend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final View dialogView = LayoutInflater.from(getContext()).inflate(R.layout.erningeditdailog, viewGroup, false);

                AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), R.style.MyDialogTheme);
                builder.setView(dialogView);
                final AlertDialog alertDialog = builder.create();


                Button update_rate = dialogView.findViewById(R.id.update_rate);
                Button cancle_rate = dialogView.findViewById(R.id.cancle_rate);

                final EditText edt_price_update = dialogView.findViewById(R.id.edt_price_update);

                edt_price_update.setText(price);

                edt_price_update.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                        if (!s.toString().equalsIgnoreCase("")) {
                            int price = Integer.parseInt(s.toString());

                            if (price < 100) {
                                edt_price_update.setError("Booking amount must be more then 100");
                                edt_price_update.requestFocus();
                            }
                        }


                    }
                });

                update_rate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String str_price_update = edt_price_update.getText().toString();
                        int price = Integer.parseInt(str_price_update);

                        if (str_price_update.equalsIgnoreCase("")) {
                            edt_price_update.setError("Please enter price");
                        } else if (price < 100) {
                            edt_price_update.setError("Booking amount must be more then 100");
                            edt_price_update.requestFocus();
                        } else {
                            ProfileUpdateData(str_price_update, "price");
                            alertDialog.dismiss();
                        }

                        //// Toast.makeText(getContext(),"Your Price Updated ",Toast.LENGTH_LONG).show();
                    }
                });
                cancle_rate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        alertDialog.dismiss();
                    }
                });

                //Now we need an AlertDialog.Builder object


                alertDialog.show();
            }
        });


        return view;
    }

    private void GetNewRequest1() {

        Celeb_DashBords celeb_dashBords = new Celeb_DashBords();

        celeb_dashBords.Dashboard(start, limit, new ResultHandler<Gson_response_celeb_dashbord>() {
            @Override
            public void onSuccess(Gson_response_celeb_dashbord data) {

                if (data.getSuccess() == 1) {

                    if (data.getData().getVideos().size() != 0) {

                        favouriteList = data.getData().getVideos();

                        celebProfileApdater.setDataChange(favouriteList);


                    }

                }

            }

            @Override
            public void onFailure(String exx) {

            }
        });

    }

    private void ProfileUpdateData(String isonline, String type) {

        Celeb_DashBords celeb_dashBords = new Celeb_DashBords();

        celeb_dashBords.UpdateProfile(isonline, type, new ResultHandler<Gson_response_login_data>() {
            @Override
            public void onSuccess(Gson_response_login_data data) {

                if (data.getSuccess() == 1) {
                    int is_online = Integer.parseInt(data.getData().getIsOnline());
                    String str_earned = data.getData().getBookingPrice();

                    price = str_earned;
                    User user = data.getData();

                    user.SaveSession();

                    txt_booking_price.setText(str_earned);

                    if (is_online == 1) {
                        switch_online.setChecked(true);
                    } else {
                        switch_online.setChecked(false);
                    }
                }

            }

            @Override
            public void onFailure(String exx) {

            }
        });

    }

    private void GetDashbordData() {

        Celeb_DashBords celeb_dashBords = new Celeb_DashBords();

        celeb_dashBords.Dashboard("0", "10", new ResultHandler<Gson_response_celeb_dashbord>() {
            @Override
            public void onSuccess(Gson_response_celeb_dashbord data) {

                if (data.getSuccess() == 1) {

                    double floatrating = Double.parseDouble(new DecimalFormat("##.##").format(Float.parseFloat(data.getData().getRating())));


                    String profile_pic = data.getData().getProfilePic();
                    String full_name = data.getData().getFullName();
                    String followers_count = data.getData().getFollowersCount();
                    String rating = String.valueOf(floatrating);
                    String earnings = Format(Integer.valueOf(data.getData().getEarnings()));
                    String booking_price = String.valueOf(data.getData().getBookingPrice());
                    String is_online = String.valueOf(data.getData().getIsOnline());
                    String videos_count = String.valueOf(data.getData().getVideosCount());


                    if (profile_pic.isEmpty() || profile_pic.equals(null) || profile_pic.equals("null")) {
//            holder.comment_profile_image.setImageDrawable(context.getResources().getDrawable(R.mipmap.profile_pic));
                        userProfile.setImageResource(R.mipmap.profile_pic);
                    } else {

                        Picasso.with(getActivity()).load(profile_pic).into(userProfile);
                    }
                    txt_user_name.setText(full_name);
                    txt_follwere.setText(followers_count);
                    txt_rating.setText(String.valueOf(rating) + " Ratings");
                    txt_videos_count.setText(videos_count);
                    txt_earned.setText(earnings);
//                    txt_booking_price.setText(booking_price);

                    if (is_online.equalsIgnoreCase("1")) {
                        switch_online.setChecked(true);

                    } else {
                        switch_online.setChecked(false);
                    }

                    if (data.getData().getVideos().size() != 0) {

                        favouriteList = data.getData().getVideos();

                        txt_message.setVisibility(View.GONE);
                        celebProfileApdater = new celeb_Video_ProfileApdater(getActivity(), favouriteList);

                        profilelastviewfav.setAdapter(celebProfileApdater);
                        LinearLayoutManager layoutManager = new GridLayoutManager(getActivity(), 2);
                        profilelastviewfav.setLayoutManager(layoutManager);

                    } else {
                        txt_message.setVisibility(View.VISIBLE);
                    }


                    parentShimmerLayout.stopShimmerAnimation();
                    parentShimmerLayout.setVisibility(View.GONE);
                    ll_profile_details.setVisibility(View.VISIBLE);
                    ll_view_myvideo.setVisibility(View.VISIBLE);

                } else {

                    parentShimmerLayout.stopShimmerAnimation();
                    parentShimmerLayout.setVisibility(View.GONE);
                    ll_profile_details.setVisibility(View.VISIBLE);
                    ll_view_myvideo.setVisibility(View.VISIBLE);
                    // Toast.makeText(getContext(), "Server down please Try Again", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(String exx) {
                parentShimmerLayout.stopShimmerAnimation();
                parentShimmerLayout.setVisibility(View.GONE);
                ll_profile_details.setVisibility(View.VISIBLE);
                ll_view_myvideo.setVisibility(View.VISIBLE);
            }
        });
    }

    public String Format(Integer number) {
        String[] suffix = new String[]{"K", "M", "B", "T"};
        int size = (number.intValue() != 0) ? (int) Math.log10(number) : 0;
        if (size >= 3) {
            while (size % 3 != 0) {
                size = size - 1;
            }
        }
        double notation = Math.pow(10, size);
        String result = (size >= 3) ? +(Math.round((number / notation) * 100) / 100.0d) + suffix[(size / 3) - 1] : +number + "";
        return result;
    }

}
