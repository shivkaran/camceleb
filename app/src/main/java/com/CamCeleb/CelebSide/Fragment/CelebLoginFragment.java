package com.CamCeleb.CelebSide.Fragment;


import android.Manifest;
import android.app.Dialog;
import android.app.KeyguardManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.security.keystore.KeyProperties;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.CamCeleb.APIs.Authorization;
import com.CamCeleb.Activity.LoginAndSignActivity;
import com.CamCeleb.Activity.RegisterActivity;
import com.CamCeleb.CelebSide.CelebHomeActivity;
import com.CamCeleb.ForgotPassword;
import com.CamCeleb.Helper.Utill;
import com.CamCeleb.Insta.FingerprintHandlerCeleb;
import com.CamCeleb.POJOClass.Login_model.Gson_response_login_data;
import com.CamCeleb.POJOClass.ResultHandler;
import com.CamCeleb.R;
import com.CamCeleb.UserSide.OTPVerificationActivity;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import static android.content.Context.KEYGUARD_SERVICE;
import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class CelebLoginFragment extends Fragment {

    TextView donthaveacc, fotgotpassword;
    EditText email, password;
    Button submitbtn, celebtech;
    //    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    String emailPattern = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]+";
    LinearLayout linearLayout;
    private static final String KEY_NAME = "yourKey";
    private Cipher cipher;
    private KeyStore keyStore;
    private KeyGenerator keyGenerator;
    private TextView textView;

    private FingerprintManager.CryptoObject cryptoObject;
    private FingerprintManager fingerprintManager;
    private KeyguardManager keyguardManager;

    private SharedPreferences loginPreferences;
    private SharedPreferences.Editor loginPrefsEditor;

    private Boolean saveLogin;

    CheckBox check_bo_fingerLock;


    public CelebLoginFragment() {
        // Required empty public constructor
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            try {
                InputMethodManager mImm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                mImm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
                mImm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
            } catch (Exception e) {

            }
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_celeb_login, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        loginPreferences = getActivity().getSharedPreferences("loginPrefsceleb", MODE_PRIVATE);
        loginPrefsEditor = loginPreferences.edit();




        donthaveacc = view.findViewById(R.id.frag_celeb_creataccount);
        fotgotpassword = view.findViewById(R.id.frag_celeb_forgotpassword);
        email = view.findViewById(R.id.frag_celeb_email);
        password = view.findViewById(R.id.frag_celeb_password);
        submitbtn = view.findViewById(R.id.frag_celeb_submitbtn);
        celebtech = view.findViewById(R.id.frag_celeb_techid);
        check_bo_fingerLock = view.findViewById(R.id.checkbox);

        celebtech.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ShowDailogfubgerDailog();

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                    keyguardManager =
                            (KeyguardManager) getActivity().getSystemService(KEYGUARD_SERVICE);
                    fingerprintManager =
                            (FingerprintManager) getActivity().getSystemService(getContext().FINGERPRINT_SERVICE);

                    if (!fingerprintManager.isHardwareDetected()) {

//                textView.setText("Your device doesn't support fingerprint authentication");
                        ShowDailog("Your device doesn't support fingerprint authentication");
                    }

                    if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {

//                textView.setText("Please enable the fingerprint permission");
                        ShowDailog("Please enable the fingerprint permission");
                    }
                    if (!fingerprintManager.hasEnrolledFingerprints()) {

                    }

                    if (!keyguardManager.isKeyguardSecure()) {

//                textView.setText("Please enable lockscreen security in your device's Settings");
                        ShowDailog("Please enable lockscreen security in your device's Settings");
                    } else {

                        try {

                            generateKey();
                        } catch (FingerprintException e) {
                            e.printStackTrace();
                        }

                        if (initCipher()) {

                            cryptoObject = new FingerprintManager.CryptoObject(cipher);

                            FingerprintHandlerCeleb helper = new FingerprintHandlerCeleb(getContext());
                            helper.startAuth(fingerprintManager, cryptoObject);
                        }
                    }
                }
            }

            private void generateKey() throws FingerprintException {
                try {
                    keyStore = KeyStore.getInstance("AndroidKeyStore");

                    keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");
                    keyStore.load(null);
                    keyGenerator.init(new
                            KeyGenParameterSpec.Builder(KEY_NAME,
                            KeyProperties.PURPOSE_ENCRYPT |
                                    KeyProperties.PURPOSE_DECRYPT)
                            .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                            .setUserAuthenticationRequired(true)
                            .setEncryptionPaddings(
                                    KeyProperties.ENCRYPTION_PADDING_PKCS7)
                            .build());

                    keyGenerator.generateKey();

                } catch (KeyStoreException
                        | NoSuchAlgorithmException
                        | NoSuchProviderException
                        | InvalidAlgorithmParameterException
                        | CertificateException
                        | IOException exc) {
                    exc.printStackTrace();
                    throw new FingerprintException(exc);
                }
            }


            public boolean initCipher() {
                try {

                    cipher = Cipher.getInstance(
                            KeyProperties.KEY_ALGORITHM_AES + "/"
                                    + KeyProperties.BLOCK_MODE_CBC + "/"
                                    + KeyProperties.ENCRYPTION_PADDING_PKCS7);
                } catch (NoSuchAlgorithmException |
                        NoSuchPaddingException e) {
                    throw new RuntimeException("Failed to get Cipher", e);
                }

                try {
                    keyStore.load(null);
                    SecretKey key = (SecretKey) keyStore.getKey(KEY_NAME,
                            null);
                    cipher.init(Cipher.ENCRYPT_MODE, key);

                    return true;
                } catch (KeyPermanentlyInvalidatedException e) {

                    return false;
                } catch (KeyStoreException | CertificateException
                        | UnrecoverableKeyException | IOException
                        | NoSuchAlgorithmException | InvalidKeyException e) {
                    throw new RuntimeException("Failed to init Cipher", e);
                }
            }

            class FingerprintException extends Exception {
                public FingerprintException(Exception e) {
                    super(e);
                }

            }
        });


        submitbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (email.getText().toString().isEmpty()) {
                    email.setError("Please enter email");
                    email.requestFocus();
                } else if (!email.getText().toString().matches(emailPattern)) {
                    email.setError("Please enter valid email");
                    email.requestFocus();

                } else if (password.getText().toString().isEmpty()) {
                    password.setError("Please enter password");
                    password.requestFocus();

                } /*else if (password.getText().toString().length() < 5) {
                    password.setError("Please enter password minimum 5 characters");
                    password.requestFocus();
                }*/ else {

                    if (Utill.isOnline(getActivity())) {
                        final ProgressDialog progressDialog = new ProgressDialog(getContext());
                        progressDialog.show();

                        final String emailis = email.getText().toString().trim();
                        final String passwords = password.getText().toString().trim();


                        Authorization authorization = new Authorization();
                        authorization.LoginData(emailis, passwords, "C", new ResultHandler<Gson_response_login_data>() {
                            @Override
                            public void onSuccess(Gson_response_login_data data) {

                                if (data.getSuccess() == 1) {

                                    if (check_bo_fingerLock.isChecked()) {
                                        loginPrefsEditor.putBoolean("saveLogin", true);
                                        loginPrefsEditor.putString("username", emailis);
                                        loginPrefsEditor.putString("password", passwords);
                                        loginPrefsEditor.commit();
                                    } else {
                                        loginPrefsEditor.clear();
                                        loginPrefsEditor.commit();

                                    }
                                    progressDialog.dismiss();

                                    if (data.getData().getIdVerification().equals("noverify")) {

                                        data.getData().SaveSession();
                                        // Toast.makeText(getContext(), " First Verifly Your Account", Toast.LENGTH_SHORT).show();
                                        progressDialog.dismiss();
                                        Intent intent = new Intent(getContext(), OTPVerificationActivity.class);
                                        startActivity(intent);

                                    } else if (data.getData().getAdmin_verification().equals("N"))
                                    {
                                        ShowDailogVerifiy("Your profile is under review and you will get an confirmation email to continue using Camceleb");
                                    }else {
                                        data.getData().SaveSession();
                                        Intent intent = new Intent(getContext(), CelebHomeActivity.class);
                                        startActivity(intent);
                                        getActivity().finish();
                                        progressDialog.dismiss();
                                    }
                                }else {
                                    progressDialog.dismiss();
                                    String message = data.getError().toString();
                                    String message1 = message.replace("[","");
                                    String message2 = message1.replace("]","");
                                    ShowDailog(message2);
                                }
                            }

                            @Override
                            public void onFailure(String exx) {
                                progressDialog.dismiss();
                                ShowDailog("Login Failed please Try Again");
                                Log.d("msg", exx);
                            }
                        });
                    }else{
                        ShowDailog(getResources().getString(R.string.intentermessage));
                    }

                }
            }

        });

        celebtech.setVisibility(View.GONE);

        saveLogin = loginPreferences.getBoolean("saveLogin", false);
        if (saveLogin == true) {
//            editTextUsername.setText(loginPreferences.getString("username", ""));
//            editTextPassword.setText(loginPreferences.getString("password", ""));
//            saveLoginCheckBox.setChecked(true);

            celebtech.setVisibility(View.VISIBLE);
            check_bo_fingerLock.setChecked(true);


        }




        donthaveacc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getContext(), RegisterActivity.class);
                intent.putExtra("usertype","celeb");
                startActivity(intent);

                getActivity().onBackPressed();


            }
        });

        fotgotpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getContext(), ForgotPassword.class);
                startActivity(intent);
            }
        });
        return view;
    }

    private void ShowDailogVerifiy(String login_failed_please_try_again) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custom_alert_dailog_otp);

        TextView text = (TextView) dialog.findViewById(R.id.txt_message);
        TextView txt_title = (TextView) dialog.findViewById(R.id.txt_title);
        text.setText(login_failed_please_try_again);
        txt_title.setText("CamCeleb");

        TextView dialogButton =  dialog.findViewById(R.id.submit);

        dialogButton.setText("Ok");


        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

            }
        });


        dialog.show();
    }


    private void ShowDailog(String login_failed_please_try_again) {

        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custom_alert_dailog);

        TextView text = (TextView) dialog.findViewById(R.id.txt_message);
        text.setText(login_failed_please_try_again);

        TextView dialogButton =  dialog.findViewById(R.id.submit);

        dialogButton.setText("Ok");


        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

            }
        });


        dialog.show();
    }

    private void ShowDailogfubgerDailog() {

        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.fingerlockscreen);
        dialog.show();
    }
}
