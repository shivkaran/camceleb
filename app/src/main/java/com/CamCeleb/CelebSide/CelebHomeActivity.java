package com.CamCeleb.CelebSide;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.CamCeleb.R;
import com.CamCeleb.SplashActivity;
import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class CelebHomeActivity extends AppCompatActivity {
    BottomNavigationView navView;
    private View notificationBadge;

    BroadcastReceiver broadcastReceiver;

    int count = 0;

    boolean flag = false;

    public static SharedPreferences preferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_celeb_home);
        getSupportActionBar().hide();
        navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.celebhome_home, R.id.celebhome_dashboard, R.id.celebhome_notifications, R.id.celebhome_profile)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);


        registerReceiver();

        if (getIntent().getStringExtra("notificationtype") != null) {
            if (getIntent().getStringExtra("notificationtype").equalsIgnoreCase("feedback_received")) {
                navView.setSelectedItemId(R.id.celebhome_profile);

            }

        }

        preferences = getSharedPreferences(getPackageName() + "_preferences", MODE_PRIVATE);

        if (preferences.getString("NOTICOUNT", "") != null) {

            String count =  SplashActivity.preferences.getString("NOTICOUNT", "");

            if (!count.equals(""))
            {
                int  countvlaue = Integer.parseInt(count);

                addBadgeView(String.valueOf(countvlaue));

            }

        }



    }

    private void registerReceiver() {

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                count = count + 1;

                addBadgeView(String.valueOf(count));

            }
        };
        registerReceiver(broadcastReceiver, new IntentFilter("pushnotifiaction"));
    }


    private void addBadgeView(String badge) {

        flag = true;
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) navView.getChildAt(0);
        BottomNavigationItemView itemView = (BottomNavigationItemView) menuView.getChildAt(2);

        notificationBadge = LayoutInflater.from(this).inflate(R.layout.view_notification_badge, menuView, false);
        TextView txtbadge = notificationBadge.findViewById(R.id.badge);

        txtbadge.setText((CharSequence) badge);

        if (badge.equalsIgnoreCase("")) {


            String count_str =  SplashActivity.preferences.getString("NOTICOUNT", "");

            if (count_str.equals("")) {

                txtbadge.setVisibility(View.GONE);

                if (itemView.getChildAt(2) != null) {
                    itemView.removeViewAt(2);
                }
                count = 0;
            }

        } else {
            txtbadge.setVisibility(View.VISIBLE);
            itemView.addView(notificationBadge);
        }


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (broadcastReceiver != null) {
            unregisterReceiver(broadcastReceiver);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (flag) {
            addBadgeView("");
        }
    }
}
