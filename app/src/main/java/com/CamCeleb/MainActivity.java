package com.CamCeleb;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.CamCeleb.Activity.LoginAndSignActivity;
import com.gohn.parallaxviewpager.ParallaxViewPager;

public class MainActivity extends AppCompatActivity {

    Button loginbtn;
    LinearLayout sliderDotspanel;
    ImageView[] dots;
    int dotscount;

    ImageView image1,image2,image3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sliderDotspanel = (LinearLayout) findViewById(R.id.SliderDots);


        getSupportActionBar().hide();
        loginbtn = findViewById(R.id.sample_login);
        image1 = findViewById(R.id.image1);
        image2 = findViewById(R.id.image2);
        image3 = findViewById(R.id.image3);
        ParallaxViewPager pager3 = (ParallaxViewPager) findViewById(R.id.pager3);


        pager3.addMovementToView(R.id.title, 0.5f);
        pager3.addMovementToView(R.id.text, 0.5f);
        pager3.addMovementToView(R.id.image, 0.5f);


        pager3.setAdapter(new SampleAdapter(getLayoutInflater(), ""));

        image1.setImageDrawable(getResources().getDrawable(R.drawable.cricle_img));



        pager3.addOnPageChangeListener(new ParallaxViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

               /* for(int i = 0; i< dotscount; i++){
                    dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.non_active_dot));
                }

                dots[position].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.active_dot));
*/
               if (position == 0)
               {
                   image1.setImageDrawable(getResources().getDrawable(R.drawable.cricle_img));
                   image2.setImageDrawable(getResources().getDrawable(R.drawable.circle_shape));
                   image3.setImageDrawable(getResources().getDrawable(R.drawable.circle_shape));
               }
                if (position == 1)
                {
                    image1.setImageDrawable(getResources().getDrawable(R.drawable.circle_shape));
                    image2.setImageDrawable(getResources().getDrawable(R.drawable.cricle_img));
                    image3.setImageDrawable(getResources().getDrawable(R.drawable.circle_shape));
                }
                if (position == 2)
                {
                    image1.setImageDrawable(getResources().getDrawable(R.drawable.circle_shape));
                    image2.setImageDrawable(getResources().getDrawable(R.drawable.circle_shape));
                    image3.setImageDrawable(getResources().getDrawable(R.drawable.cricle_img));
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });




      /*  dots = new ImageView[3];

        for (int i = 0; i < dotscount; i++) {

            dots[i] = new ImageView(this);
            dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.non_active_dot));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            params.setMargins(8, 0, 8, 0);

            sliderDotspanel.addView(dots[i], params);

            dots[0].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.active_dot));


            pager3.addOnPageChangeListener(new ParallaxViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {

                    for(int i = 0; i< dotscount; i++){
                        dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.non_active_dot));
                    }

                    dots[position].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.active_dot));

                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });

        }
*/
        loginbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this, LoginAndSignActivity.class);
                startActivity(intent);
                finish();

            }
        });


    }


}

