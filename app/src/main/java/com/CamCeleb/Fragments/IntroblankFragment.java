package com.CamCeleb.Fragments;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.CamCeleb.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class IntroblankFragment extends Fragment {


    public IntroblankFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_introblank, container, false);
    }

}
