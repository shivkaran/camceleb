package com.CamCeleb.Fragments;


import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.CamCeleb.APIs.Authorization;
import com.CamCeleb.APIs.Helper;
import com.CamCeleb.Activity.LoginAndSignActivity;
import com.CamCeleb.Helper.Utill;
import com.CamCeleb.POJOClass.Login_model.Gson_response_login_data;
import com.CamCeleb.POJOClass.ResultHandler;
import com.CamCeleb.R;
import com.CamCeleb.UserSide.OTPVerificationActivity;
import com.CamCeleb.UserSide.UserHomeActivity;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.app.Activity.RESULT_OK;


public class FanRegister extends Fragment {

    Button fanbtn;
    Uri profileUri = null;
    TextView loginhere;
    public static final int PICK_IMAGE = 1;
    private static final int REQUEST_GALLERY_CODE = 200;
    private static final int READ_REQUEST_CODE = 300;
    ImageView profile_pic;
    EditText name, email, password, conpassword, phonenumber;
    //    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    String emailPattern = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]+";

    TextView txt_terms_condition;
    TextView txt_add_photo;

    public FanRegister() {
        // Required empty public constructor
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

       /* if (requestCode == 100 && resultCode == RESULT_OK && data != null) {
            //the image URI

            if (data.getData() != null) {
              *//*  Uri selectedImage = data.getData();


                String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor c = getActivity().getContentResolver().query(selectedImage, filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String picturePath = c.getString(columnIndex);
                c.close();
                Bitmap thumbnail = (BitmapFactory.decodeFile(picturePath));
                System.out.println("path of image from gallery......******************........." + picturePath + "");

//                File pictureFile = new File(picturePath);


//                thumbnail = Utill.ExifInterfaceImage(pictureFile, thumbnail);

                profile_pic.setImageBitmap(thumbnail);
                profileUri = selectedImage;*//*

                Uri selectedImage = data.getData();
                String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor c = getActivity().getContentResolver().query(selectedImage, filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String picturePath = c.getString(columnIndex);
                c.close();
                Bitmap thumbnail = (BitmapFactory.decodeFile(picturePath));
                System.out.println("path of image from gallery......******************........." + picturePath + "");

                profileUri = selectedImage;

                profile_pic.setImageURI(selectedImage);



                //calling the upload file method after choosing the file
            }else {
                Toast.makeText(getActivity(), "Image not selected try again", Toast.LENGTH_SHORT).show();
            }
        }else {
            Toast.makeText(getActivity(), "Image not selected try again", Toast.LENGTH_SHORT).show();

        }*/
        CropImage.ActivityResult result = CropImage.getActivityResult(data);
        if (resultCode == RESULT_OK) {
            Uri resultUri = result.getUri();

            profileUri = resultUri;
            profile_pic.setImageURI(resultUri);

        } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
            Exception error = result.getError();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_fan_register, container, false);

        profile_pic = view.findViewById(R.id.fan_image);
        txt_terms_condition = view.findViewById(R.id.txt_terms_condition);
        fanbtn = view.findViewById(R.id.fan_submitbutton);
        name = view.findViewById(R.id.fan_fullname);
        email = view.findViewById(R.id.fan_email);
        password = view.findViewById(R.id.fan_password);
        conpassword = view.findViewById(R.id.fan_confirmpassword);
        phonenumber = view.findViewById(R.id.fan_phonenumber);
        // profile_pic = view.findViewById(R.id.fan_image);
        loginhere = view.findViewById(R.id.fan_btm_account);
        txt_add_photo = view.findViewById(R.id.txt_add_photo);


        profile_pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                //opening file chooser

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(getContext(),
                        READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{READ_EXTERNAL_STORAGE},
                            100);


                } else {
                   /* Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(i, 100);*/
                   /* Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                    // Start the Intent
                    startActivityForResult(galleryIntent, 100);*/

                    CropImage.activity()
                            .setGuidelines(CropImageView.Guidelines.ON)
                            .setAspectRatio(1,1)
                            .start(getActivity());
                }


            }
        });

        txt_add_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(getContext(),
                        READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{READ_EXTERNAL_STORAGE},
                            100);


                } else {
                    CropImage.activity()
                            .setGuidelines(CropImageView.Guidelines.ON)
                            .setAspectRatio(1,1)
                            .start(getActivity());
                }


            }
        });


        loginhere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });


        txt_terms_condition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.camceleb.com/terms-condition"));
                startActivity(browserIntent);
            }
        });


        fanbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (name.getText().toString().isEmpty()) {
                    name.setError("Please enter full name");
                }
                if (email.getText().toString().isEmpty()) {
                    email.setError("Please enter email");
                } else if (!email.getText().toString().matches(emailPattern)) {
                    email.setError("Please enter valid email");

                } else if (password.getText().toString().isEmpty()) {
                    password.setError("Please enter password");


                } else if (password.getText().toString().length() < 5) {
                    password.setError("Please enter password minimum 5 characters");

                }
                else if (conpassword.getText().toString().isEmpty()) {
                    conpassword.setError("Please enter confirm password");
                } else if (!conpassword.getText().toString().equals(password.getText().toString())) {
                    conpassword.setError("Confirm password must match with password");
                } else if (phonenumber.getText().toString().isEmpty()) {
                    phonenumber.setError("Please enter contact number");
                } else if (phonenumber.getText().toString().length() < 10) {
                    phonenumber.setError("Please provide a valid 10 digit contact number");


                } else if (profileUri == null) {
                    Toast.makeText(getContext(), "please select profile picture ", Toast.LENGTH_SHORT).show();
                } else {


                    final String names = name.getText().toString().trim();
                    final String emails = email.getText().toString().trim();
                    final String passwords = password.getText().toString().trim();
                    final String phonenumbers = phonenumber.getText().toString().trim();


                    Log.d("NAme1", names);
                    Log.d("Emails1", emails);
                    Log.d("Password1", passwords);
                    Log.d("numbers1", phonenumbers);
                    Log.d("images123", profileUri.toString());
                    Log.d("image", profile_pic.toString());


                    final Authorization authorization = new Authorization();
                    authorization.userSignUp(getActivity(), Helper.createRequestBody("F"), Helper.createRequestBody(names), Helper.createRequestBody(emails), Helper.createRequestBody(passwords), Helper.createRequestBody(phonenumbers),
                            Helper.createRequestBody(""), Helper.createRequestBody(""), Helper.createRequestBody(""), Helper.createRequestBody(""), Helper.createRequestBody(""), Helper.createRequestBody(""),
                            profileUri, new ResultHandler<Gson_response_login_data>() {
                                @Override
                                public void onSuccess(Gson_response_login_data data) {

                                    if (data.getSuccess() == 1) {


                                        if (data.getData().getIdVerification().equals("noverify")) {

                                            data.getData().SaveSession();
                                            Intent intent = new Intent(getContext(), OTPVerificationActivity.class);
                                            startActivity(intent);

                                        } else {
                                            data.getData().SaveSession();
                                            Intent intent = new Intent(getContext(), UserHomeActivity.class);
                                            startActivity(intent);
                                            getActivity().finish();
                                        }
                                    } else {
                                        String message = data.getError().toString();
                                        String message1 = message.replace("[", "");
                                        String message2 = message1.replace("]", "");
                                        ShowDailog(message2);
                                    }

                                }

                                @Override
                                public void onFailure(String exx) {

                                }
                            });



                   /* progressDialog.dismiss();
                    Intent intent = new Intent(getContext(), LoginAndSignActivity.class);
                    startActivity(intent);*/


                }
            }

        });
        return view;
    }

    private void ShowDailog(String message) {


        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dailog_layout);

        TextView text = (TextView) dialog.findViewById(R.id.txt_message);
        text.setText(message.toString());

        Button dialogButton = (Button) dialog.findViewById(R.id.submit);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(getContext(), LoginAndSignActivity.class);
                startActivity(intent);
                getActivity().finish();

            }
        });

        dialog.show();


    }

}






















































  /* if (name.getText().toString().isEmpty()) {
                    name.setError("Please enter full name");
                }
                if (email.getText().toString().isEmpty()) {
                    email.setError("Please enter email");
                } else {
                    if (email.getText().toString().trim().matches(emailPattern)) {
                    } else {
                        email.setError("Please enter valid email");
                    }
                }

                if (email.getText().toString().isEmpty()) {
                    email.setError("Please enter email");
                } else {
                    if (email.getText().toString().trim().matches(emailPattern)) {
                    } else {
                        email.setError("Please enter valid email");
                    }
                }
                if (password.getText().toString().isEmpty()) {
                    password.setError("Please enter password");
                }
                if (conpassword.getText().toString().isEmpty()) {
                    conpassword.setError("Please enter confirm password");
                }
                if (conpassword.getText().toString().isEmpty()) {
                    conpassword.setError("Please enter confirm password");

                    if (!conpassword.equals(password)) {
                        conpassword.setError("Confirm password must match with password");
                    }
                }
                if (phonenumber.getText().toString().isEmpty()) {
                    phonenumber.setError("Please enter contact number");
                }
                else {
                    Intent intent = new Intent(getContext(), OTPVerificationActivity.class);
                    startActivity(intent);
                    getActivity().onBackPressed();
                }
            }
        });*/



   /*String full_name = name.getText().toString().trim();
                String emails = email.getText().toString().trim();
                String passwords = password.getText().toString().trim();
                String phone_number = phonenumber.getText().toString();
                String profile_pics= profile_pic.toString();


                //Defining the user object as we need to pass it with the call

                Retrofit retrofit = new APIManager().getService();
                final APIService client = retrofit.create(APIService.class);
                Call<Result> call = client.RegisterUser(full_name, emails, passwords, phone_number, profile_pics,"F");

                //calling the api
                call.enqueue(new Callback<Result>() {
                    @Override
                    public void onResponse(Call<Result> call, Response<Result> response) {

                        // Toast.makeText(getApplicationContext(), response.body().toString(), Toast.LENGTH_LONG).show();
                        int code = response.body().getCode();
                        String[] e = response.body().getError();
                        JsonObject data = response.body().getData();

                        Log.d("getTokenresponseone", String.valueOf(code));
                        Log.d("getTokenresponsecodeone", String.valueOf(e));
                        Log.d("getTokenresponserrorone", String.valueOf(data));
                        //if there is no error
                        if (code == 0) {
                            //starting profile activity

                            startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                        }
                    }

                    @Override
                    public void onFailure(Call<Result> call, Throwable t) {

                        // Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });




*/