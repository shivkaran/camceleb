package com.CamCeleb.Fragments;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.CamCeleb.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class EmptyRequestFragment extends Fragment {


    public EmptyRequestFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_empty_request, container, false);


        return view;
    }

}
