package com.CamCeleb.Fragments;


import android.Manifest;
import android.app.Dialog;
import android.app.KeyguardManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.security.keystore.KeyProperties;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.CamCeleb.APIs.Authorization;
import com.CamCeleb.Activity.RegisterActivity;
import com.CamCeleb.ForgotPassword;
import com.CamCeleb.Helper.Utill;
import com.CamCeleb.Insta.FingerprintHandler;
import com.CamCeleb.Insta.InstagramApp;
import com.CamCeleb.POJOClass.Login_model.Gson_response_login_data;
import com.CamCeleb.POJOClass.ResultHandler;
import com.CamCeleb.R;
import com.CamCeleb.UserSide.GetSaveData;
import com.CamCeleb.UserSide.OTPVerificationActivity;
import com.CamCeleb.UserSide.UserHomeActivity;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginBehavior;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Arrays;
import java.util.HashMap;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import static android.content.Context.KEYGUARD_SERVICE;
import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment implements View.OnClickListener {


    private static final String EMAIL = "email";
    private static final String PublicProfile = "publicprofile";
    private static final String TAG = "Camceleb";
    CallbackManager callbackManager;
    TextView donthaveacc, forgotpassword;
    EditText email, password;
    Button logininsta, btnlogin, signbtn;
    LoginButton loginButton;
    SignInButton signInButton;
    String emailPattern = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]+";
    //    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    //  String emailPattern = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    CheckBox checkBox;
    Button btn_fingerlock;
    String datatype;
    public static SharedPreferences preferences;


    private static final String KEY_NAME = "yourKey";
    private Cipher cipher;
    private KeyStore keyStore;
    private KeyGenerator keyGenerator;
    private TextView textView;

    private FingerprintManager.CryptoObject cryptoObject;
    private FingerprintManager fingerprintManager;
    private KeyguardManager keyguardManager;


    private HashMap<String, String> hashMap = new HashMap<String, String>();
    InstagramApp instagramApp;


    private SharedPreferences loginPreferences;
    private SharedPreferences.Editor loginPrefsEditor;

    private Boolean saveLogin;

    CheckBox check_bo_fingerLock;


    RelativeLayout img_facebook;
    RelativeLayout login_google;

    //    google login
    GoogleSignInOptions gso;
    GoogleSignInClient mGoogleSignInClient;
    private FirebaseAuth mAuth;

    private Handler handler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {

            if (msg.what == InstagramApp.WHAT_FINALIZE) {
                hashMap = instagramApp.getUserInfo();

            } else if (msg.what == InstagramApp.WHAT_FINALIZE) {
                // Toast.makeText(getContext(), "Check Your Network", Toast.LENGTH_SHORT).show();
            }
            return false;
        }

    });

    public LoginFragment() {
        // Required empty public constructor
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            try {
                InputMethodManager mImm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                mImm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
                mImm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
            } catch (Exception e) {

            }
        }
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_login, container, false);


        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        FacebookSdk.sdkInitialize(getActivity().getApplicationContext());

        initialization(view);
        viewClickListerners();

        mAuth = FirebaseAuth.getInstance();


        loginPreferences = getActivity().getSharedPreferences("loginPrefs", MODE_PRIVATE);
        loginPrefsEditor = loginPreferences.edit();

        img_facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginButton.performClick();
            }
        });


        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestIdToken("555372711845-15lk8aapfg3imnnnmtpkrk7cralgiv7k.apps.googleusercontent.com")
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(getActivity(), gso);


        login_google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                signInButton.performClick();

//                mAuth.getInstance().signOut();

                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, 100);

            }
        });


        loginButton.setFragment(this);
        btn_fingerlock.setVisibility(View.GONE);

        callbackManager = CallbackManager.Factory.create();

        loginButton.setReadPermissions(Arrays.asList(EMAIL));


     /*   instagramApp = new InstagramApp(getContext(), AppConfig.CLIENT_Id, AppConfig.CLIENT_SECRET_Id, AppConfig.CALLBack_Url);


        instagramApp.setListener(new InstagramApp.OAuthAuthenticationListener() {
            @Override
            public void onSuccess() {

                instagramApp.fetchUserName(handler);

                String emails = instagramApp.getUserName();
                String name = instagramApp.getName();
                String ids = instagramApp.getId();


                FacebookApiCall("I", "Y", emails, ids, name);




                *//*final ProgressDialog progressDialog1 = new ProgressDialog(getContext());
                progressDialog1.show();


                Authorization authorization = new Authorization();

                authorization.SocialLogindata("I", "Y", emails, ids, name, "", new ResultHandler<Gson_response_login_data>() {
                    @Override
                    public void onSuccess(Gson_response_login_data data) {

                        if (data.getData().getIdVerification().equals("noverify")) {
                            progressDialog1.dismiss();
                            data.getData().SaveSession();
                            // Toast.makeText(getContext(), " First Verifly Your Account", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getContext(), OTPVerificationActivity.class);
                            startActivity(intent);

                        } else {
                            progressDialog1.dismiss();
                            data.getData().SaveSession();
                            Intent intent = new Intent(getContext(), UserHomeActivity.class);
                            startActivity(intent);
                            getActivity().finish();
                        }
                    }

                    @Override
                    public void onFailure(String e) {
                        if (e == "is_email_required") {
                            Intent intent = new Intent(getContext(), GetSaveData.class);
                            startActivity(intent);
                        } else {
                            // Toast.makeText(getContext(), e, Toast.LENGTH_SHORT).show();
                        }
                    }
                });*//*

            }

            @Override
            public void onFail(String error) {

                // Toast.makeText(getContext(), error, Toast.LENGTH_SHORT).show();

            }
        });*/


//        Google throw login


        loginButton.setLoginBehavior(LoginBehavior.WEB_ONLY);
        loginButton.setReadPermissions(Arrays.asList("email", "public_profile"));
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {


                setFacebookData(loginResult);

            }

            @Override
            public void onCancel() {

                System.out.println("Failed");
            }

            @Override
            public void onError(FacebookException exception) {

                System.out.println(exception.toString());
            }
        });

        saveLogin = loginPreferences.getBoolean("saveLogin", false);
        if (saveLogin == true) {
            btn_fingerlock.setVisibility(View.VISIBLE);
            check_bo_fingerLock.setChecked(true);

        }

      /*  check_bo_fingerLock.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                {
                    loginPrefsEditor.putBoolean("saveLogin", true);
                    loginPrefsEditor.putString("username", username);
                    loginPrefsEditor.putString("password", password);
                    loginPrefsEditor.commit();
                }
            }
        });*/


        return view;
    }

    private void setFacebookData(LoginResult loginResult) {

        GraphRequest request = GraphRequest.newMeRequest(
                loginResult.getAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(
                            JSONObject object,
                            GraphResponse response) {
                        try {
                            Log.d("shiv", "fb json object: " + object);
                            Log.d("shiv", "fb graph response: " + response);

                            final String id = object.getString("id");
                            final String full_name = object.getString("first_name");


                            String email = "";
                            if (object.has("email")) {
                                email = object.getString("email");
                            }


                            FacebookApiCall("F", "Y", email, id, full_name);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,first_name,email"); // id,first_name,last_name,email,gender,birthday,cover,picture.type(large)
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void FacebookApiCall(final String social_type, String is_provided_by_user, final String email, final String id, final String full_name) {

        final ProgressDialog progressDialog1 = new ProgressDialog(getContext());
        progressDialog1.show();


        Authorization authorization = new Authorization();

        authorization.SocialLogindata(social_type, is_provided_by_user, email, id, full_name, "", new ResultHandler<Gson_response_login_data>() {

            @Override
            public void onSuccess(Gson_response_login_data data) {

                if (data.getSuccess() == 1) {

                    String responseStg = new Gson().toJson(data.getData());


//                    String responseStg = data.getData().toString();
                    try {
                        JSONObject responseObj = new JSONObject(responseStg);

                        if (responseObj.has("is_email_required")) {

                            if (data.getData().getIs_email_required().equalsIgnoreCase("Y")) {
                                Intent intent = new Intent(getContext(), GetSaveData.class);
                                intent.putExtra("id", id);
                                intent.putExtra("name", full_name);
                                startActivity(intent);
                            } else {
                                if (data.getData().getIdVerification().equals("noverify")) {
                                    progressDialog1.dismiss();
                                    data.getData().SaveSession();
                                    // Toast.makeText(getContext(), " First Verifly Your Account", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(getContext(), OTPVerificationActivity.class);
                                    startActivity(intent);

                                } else {
                                    progressDialog1.dismiss();
                                    data.getData().SaveSession();
                                    Intent intent = new Intent(getContext(), UserHomeActivity.class);
                                    startActivity(intent);
                                    getActivity().finish();
                                }
                            }

                        } else {
                            if (responseObj.has("id_verification")) {

                                if (data.getData().getIdVerification().equals("noverify")) {
                                    progressDialog1.dismiss();
                                    data.getData().SaveSession();
                                    // Toast.makeText(getContext(), " First Verifly Your Account", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(getContext(), OTPVerificationActivity.class);
                                    startActivity(intent);

                                } else {
                                    progressDialog1.dismiss();
                                    data.getData().SaveSession();
                                    Intent intent = new Intent(getContext(), UserHomeActivity.class);
                                    startActivity(intent);
                                    getActivity().finish();
                                }
                            } else {
                                FacebookApiCall(social_type, "Y", email, id, full_name);
                            }
                        }
                    } catch (Exception e) {

                    }
                } else {
                    progressDialog1.dismiss();
                    ShowDailog("Login Failed please Try Again");
                }
            }

            @Override
            public void onFailure(String e) {


            }
        });
    }

    private void viewClickListerners() {
        btnlogin.setOnClickListener(this);
        forgotpassword.setOnClickListener(this);
        donthaveacc.setOnClickListener(this);
        btn_fingerlock.setOnClickListener(this);
        signbtn.setOnClickListener(this);

    }

    private void initialization(View view) {
        donthaveacc = view.findViewById(R.id.txt_frnd_register);
        forgotpassword = view.findViewById(R.id.fanlogin_forgotpassword);
        email = view.findViewById(R.id.log_frg_email);
        password = view.findViewById(R.id.log_frg_password);
        btn_fingerlock = view.findViewById(R.id.btn_fingerlock);
        signbtn = view.findViewById(R.id.frg_login_sighbtn);
        btnlogin = view.findViewById(R.id.login_instagram);
        logininsta = view.findViewById(R.id.login_instagram);
        loginButton = view.findViewById(R.id.login_facebook);
        signInButton = view.findViewById(R.id.sign_in_button);
        check_bo_fingerLock = view.findViewById(R.id.check_bo_fingerLock);
        img_facebook = view.findViewById(R.id.img_facebook);
        login_google = view.findViewById(R.id.login_google);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.login_instagram:
                LoginUser();
                break;

            case R.id.fanlogin_forgotpassword:
                Intent intent = new Intent(getContext(), ForgotPassword.class);
                startActivity(intent);
                break;
            case R.id.txt_frnd_register:

                Intent register_intent = new Intent(getContext(), RegisterActivity.class);
                register_intent.putExtra("usertype", "fan");
                startActivity(register_intent);

                break;

            case R.id.btn_fingerlock:
                FingerUnLock();
                break;

            case R.id.frg_login_sighbtn:
                if (Utill.isOnline(getActivity())) {
                    CheckLogin();
                } else {
                    ShowDailog(getResources().getString(R.string.intentermessage));
                }

                break;
        }
    }

    private void CheckLogin() {

        if (email.getText().toString().isEmpty()) {
            email.setError("Please enter email");
            email.requestFocus();
        } else if (!email.getText().toString().matches(emailPattern)) {
            email.setError("Please enter valid email");
            email.requestFocus();

        } else if (password.getText().toString().isEmpty()) {
            password.setError("Please enter password");
            password.requestFocus();

        } /*else if (password.getText().toString().length() < 5) {
            password.setError("Please enter password minimum 5 characters");
            password.requestFocus();

        }*/ else {
            final ProgressDialog progressDialog = new ProgressDialog(getContext());
            progressDialog.show();

            final String emailis = email.getText().toString().trim();
            final String passwords = password.getText().toString().trim();

            final ProgressDialog progressDialog1 = ProgressDialog.show(getActivity(), null, "please wait", false);

            progressDialog.dismiss();
            Authorization authorization = new Authorization();
            authorization.LoginData(emailis, passwords, "F", new ResultHandler<Gson_response_login_data>() {

                @Override
                public void onSuccess(Gson_response_login_data data) {

                    if (data.getSuccess() == 1) {


                        if (check_bo_fingerLock.isChecked()) {
                            loginPrefsEditor.putBoolean("saveLogin", true);
                            loginPrefsEditor.putString("username", emailis);
                            loginPrefsEditor.putString("password", passwords);
                            loginPrefsEditor.commit();
                        } else {
                            loginPrefsEditor.clear();
                            loginPrefsEditor.commit();

                        }


                        if (data.getData().getIdVerification().equals("noverify")) {
                            progressDialog1.dismiss();
                            data.getData().SaveSession();
                            // Toast.makeText(getContext(), " First Verifly Your Account", Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                            Intent intent = new Intent(getContext(), OTPVerificationActivity.class);
                            startActivity(intent);

                        } else {
                            progressDialog1.dismiss();
                            data.getData().SaveSession();
                            progressDialog.dismiss();
                            Intent intent = new Intent(getContext(), UserHomeActivity.class);
                            startActivity(intent);
                            getActivity().finish();
                        }
                    } else {
                        progressDialog1.dismiss();
                        String message = data.getError().toString();
                        String message1 = message.replace("[", "");
                        String message2 = message1.replace("]", "");
                        ShowDailog(message2);
                    }
                }

                @Override
                public void onFailure(String exx) {
                    progressDialog1.dismiss();
                    Log.d("msg", exx);
                    ShowDailog("Login Failed please Try Again");
                }
            });
            progressDialog.dismiss();

        }
    }

    private void ShowDailog(String login_failed_please_try_again) {

        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custom_alert_dailog);

        TextView text = (TextView) dialog.findViewById(R.id.txt_message);
        text.setText(login_failed_please_try_again);

        TextView dialogButton = dialog.findViewById(R.id.submit);

        dialogButton.setText("Ok");


        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

            }
        });


        dialog.show();

    }

    private void FingerUnLock() {

        ShowDailogfubgerDailog();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            keyguardManager =
                    (KeyguardManager) getActivity().getSystemService(KEYGUARD_SERVICE);
            fingerprintManager =
                    (FingerprintManager) getActivity().getSystemService(getContext().FINGERPRINT_SERVICE);

            if (!fingerprintManager.isHardwareDetected()) {

//                textView.setText("Your device doesn't support fingerprint authentication");
                ShowDailog("Your device doesn't support fingerprint authentication");
            }

            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {

//                textView.setText("Please enable the fingerprint permission");
                ShowDailog("Please enable the fingerprint permission");
            }
            if (!fingerprintManager.hasEnrolledFingerprints()) {

            }

            if (!keyguardManager.isKeyguardSecure()) {

//                textView.setText("Please enable lockscreen security in your device's Settings");
                ShowDailog("Please enable lockscreen security in your device's Settings");

            } else {


                generateKey();

                if (initCipher()) {

                    cryptoObject = new FingerprintManager.CryptoObject(cipher);

                    FingerprintHandler helper = new FingerprintHandler(getContext());
                    helper.startAuth(fingerprintManager, cryptoObject);
                }
            }
        } else {

        }

    }

    private void ShowDailogfubgerDailog() {

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.fingerlockscreen);
        dialog.show();
    }


    private void LoginUser() {
        instagramApp.authorize();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        super.onActivityResult(requestCode, resultCode, data);


        callbackManager.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 100) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);

           /* Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);


                System.out.println("Shivkaran   " + account.getId());

            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.w(TAG, "Google sign in failed", e);
                // ...
            }
*/
        }


    }

    private void handleSignInResult(Task<GoogleSignInAccount> task) {
        try {
            GoogleSignInAccount account = task.getResult(ApiException.class);

            String email = account.getEmail();
            String id = account.getId();
            String full_name = account.getDisplayName();

            System.out.println(email+" "+id+"  "+full_name);

            // Signed in successfully, show authenticated UI.
            FacebookApiCall("G", "Y", email, id, full_name);

        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.getMessage());
            ShowDailog(e.getMessage());

        }

    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleSignInClient.signOut();
    }

    private void generateKey() {
        try {
            keyStore = KeyStore.getInstance("AndroidKeyStore");

            keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");
            keyStore.load(null);
            keyGenerator.init(new
                    KeyGenParameterSpec.Builder(KEY_NAME,
                    KeyProperties.PURPOSE_ENCRYPT |
                            KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(
                            KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    .build());

            keyGenerator.generateKey();

        } catch (KeyStoreException
                | NoSuchAlgorithmException
                | NoSuchProviderException
                | InvalidAlgorithmParameterException
                | CertificateException
                | IOException exc) {
            exc.printStackTrace();

        }
    }

    public boolean initCipher() {
        try {

            cipher = Cipher.getInstance(
                    KeyProperties.KEY_ALGORITHM_AES + "/"
                            + KeyProperties.BLOCK_MODE_CBC + "/"
                            + KeyProperties.ENCRYPTION_PADDING_PKCS7);
        } catch (NoSuchAlgorithmException |
                NoSuchPaddingException e) {
            throw new RuntimeException("Failed to get Cipher", e);
        }

        try {
            keyStore.load(null);
            SecretKey key = (SecretKey) keyStore.getKey(KEY_NAME,
                    null);
            cipher.init(Cipher.ENCRYPT_MODE, key);

            return true;
        } catch (KeyPermanentlyInvalidatedException e) {

            return false;
        } catch (KeyStoreException | CertificateException
                | UnrecoverableKeyException | IOException
                | NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException("Failed to init Cipher", e);
        }
    }

}

