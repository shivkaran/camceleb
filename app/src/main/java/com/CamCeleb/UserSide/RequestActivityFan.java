package com.CamCeleb.UserSide;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.CamCeleb.Activity.RequestForCelebActivity;
import com.CamCeleb.POJOClass.User;
import com.CamCeleb.R;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;

public class RequestActivityFan extends AppCompatActivity {


    public static SharedPreferences preferences;

    EditText edt_phone_no, edt_email, edt_bio, edt_full_name;
    TextView txt_celeb_name, txt_back;
    ImageView intro_userimgci, img_photo;

    RadioButton request_me, request_friend;

    RadioGroup booking_option;
    User user;
    LinearLayout linerfriendview;
    private static final int CAMERA_REQUEST_CODE_VEDIO = 101;

    Uri str_image_path;
    String booking_for = "M";
    String is_private = "0";
    String celeb_id = "", celeb_name = "", price = "", profile_image_celeb = "";

    Button requestpage_submit;
    CheckBox request_privatevideo;
    String friend_name, phone_number, email, instructions;

    String type = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_fan);
        getSupportActionBar().hide();
        preferences = getSharedPreferences(getPackageName() + "_preferences", MODE_PRIVATE);

        String session = preferences.getString("SessionData", "");

        Gson gson = new Gson();
        user = gson.fromJson(session, User.class);

        if (getIntent().getStringExtra("celeb_id") != null) {
            celeb_id = getIntent().getStringExtra("celeb_id");
        }
        if (getIntent().getStringExtra("celeb_name") != null) {
            celeb_name = getIntent().getStringExtra("celeb_name");
        }
        if (getIntent().getStringExtra("price") != null) {
            price = getIntent().getStringExtra("price");
        }
        if (getIntent().getStringExtra("profile_image_celeb") != null) {
            profile_image_celeb = getIntent().getStringExtra("profile_image_celeb");
        }


        img_photo = findViewById(R.id.img_photo);
        edt_phone_no = findViewById(R.id.edt_phone_no);
        edt_email = findViewById(R.id.edt_email);
        edt_bio = findViewById(R.id.edt_bio);
        txt_celeb_name = findViewById(R.id.txt_celeb_name);
        edt_full_name = findViewById(R.id.edt_full_name);
        intro_userimgci = findViewById(R.id.intro_userimgci);
        booking_option = findViewById(R.id.booking_option);
        linerfriendview = findViewById(R.id.linerfriendview);
        txt_back = findViewById(R.id.txt_back);

        request_me = findViewById(R.id.request_me);
        request_friend = findViewById(R.id.request_friend);
        requestpage_submit = findViewById(R.id.requestpage_submit);
        request_privatevideo = findViewById(R.id.request_privatevideo);


        edt_phone_no.setText(user.getPhoneNumber());
        edt_email.setText(user.getEmail());
        edt_full_name.setText(user.getFullName());
        edt_bio.setText(user.getAboutMe());

        txt_celeb_name.setText(celeb_name);


        request_me.setChecked(true);

        if (profile_image_celeb.equalsIgnoreCase("")) {
            intro_userimgci.setImageDrawable(getResources().getDrawable(R.mipmap.profile_pic));
        } else {

            Picasso.with(this).load(profile_image_celeb).into(intro_userimgci);
        }

        txt_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        request_privatevideo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    is_private = "1";
                } else {
                    is_private = "0";
                }
            }
        });


        booking_option.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected

                View radioButton = booking_option.findViewById(checkedId);
                int index = booking_option.indexOfChild(radioButton);

                // Add logic here

                switch (index) {
                    case 0: // first button
                        edt_phone_no.setText(user.getPhoneNumber());
                        edt_email.setText(user.getEmail());
                        edt_full_name.setText(user.getFullName());
                        edt_bio.setText(user.getAboutMe());
                        linerfriendview.setVisibility(View.GONE);
                        booking_for = "M";
                        break;
                    case 1: // secondbutton
                        edt_phone_no.setText("");
                        edt_email.setText("");
                        edt_full_name.setText("");
                        edt_bio.setText("");
                        linerfriendview.setVisibility(View.VISIBLE);
                        booking_for = "F";
                        break;
                }

            }
        });


        linerfriendview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                    requestMultiplePermissions();

                } else {

                    Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                    if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {
                        startActivityForResult(takeVideoIntent, CAMERA_REQUEST_CODE_VEDIO);
                    }
                }

            }
        });


        requestpage_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
                String emailPattern = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]+";
                friend_name = edt_full_name.getText().toString();
                phone_number = edt_phone_no.getText().toString();
                email = edt_email.getText().toString();
                instructions = edt_bio.getText().toString();


                if (friend_name.equals("")) {
                    edt_full_name.setError("Please enter full name");

                } else if (phone_number.equals("'")) {
                    edt_phone_no.setError("Please provide a valid 10 digit contact number");

                } else if (email.equals("")) {
                    edt_email.setError("Please enter email");

                } else if (!edt_email.getText().toString().matches(emailPattern)) {
                    edt_email.setError("Please enter valid email");

                } else if (instructions.equals("")) {
                    edt_bio.setError("Please enter instructions");
                } else if (edt_bio.getText().toString().trim().length() < 20) {
                    edt_bio.setError("Please enter minimum 20 instructions ");
                } else if (edt_bio.getText().toString().trim().length() > 250) {
                    edt_bio.setError("Please enter maximum 250 instructions ");
                } else {

                    if (str_image_path != null) {

                        Intent intent = new Intent(RequestActivityFan.this, RequestForCelebActivity.class);
                        intent.putExtra("friend_name", friend_name);
                        intent.putExtra("phone_number", phone_number);
                        intent.putExtra("email", email);
                        intent.putExtra("instructions", instructions);
                        intent.putExtra("booking_for", booking_for);
                        intent.putExtra("is_private", is_private);
                        intent.putExtra("str_image_path", str_image_path.toString());
                        intent.putExtra("celeb_name", celeb_name);
                        intent.putExtra("celeb_id", celeb_id);
                        intent.putExtra("price", price);
                        intent.putExtra("type", type);
                        startActivity(intent);
                    } else {
                        Intent intent = new Intent(RequestActivityFan.this, RequestForCelebActivity.class);
                        intent.putExtra("friend_name", friend_name);
                        intent.putExtra("phone_number", phone_number);
                        intent.putExtra("email", email);
                        intent.putExtra("instructions", instructions);
                        intent.putExtra("booking_for", booking_for);
                        intent.putExtra("is_private", is_private);
                        intent.putExtra("str_image_path", "");
                        intent.putExtra("celeb_name", celeb_name);
                        intent.putExtra("celeb_id", celeb_id);
                        intent.putExtra("price", price);
                        intent.putExtra("type", type);
                        startActivity(intent);
                    }
//                    SetRequest();

                }

            }
        });

    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    private void requestMultiplePermissions() {

        if (ContextCompat.checkSelfPermission(RequestActivityFan.this, Manifest.permission.READ_EXTERNAL_STORAGE) + ContextCompat.checkSelfPermission(RequestActivityFan.this, Manifest.permission.CAMERA) + ContextCompat.checkSelfPermission(RequestActivityFan.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(RequestActivityFan.this, Manifest.permission.READ_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(RequestActivityFan.this, Manifest.permission.CAMERA) || ActivityCompat.shouldShowRequestPermissionRationale(RequestActivityFan.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                Snackbar.make(RequestActivityFan.this.findViewById(android.R.id.content),
                        "Please Grant Permissions to upload Image",
                        Snackbar.LENGTH_INDEFINITE).setAction("Enable",
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, CAMERA_REQUEST_CODE_VEDIO);
                            }
                        }).show();
            } else {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, CAMERA_REQUEST_CODE_VEDIO);
            }
        } else {
//            selectImage();
            CropImage.activity()
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setAspectRatio(1, 1)
                    .start(this);
        }
    }

    private void selectImage() {

        final Dialog dialog = new Dialog(this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.upload_photo_custome_dialog);

        TextView txt_camera = (TextView) dialog.findViewById(R.id.txt_camera);
        TextView txt_gallery = (TextView) dialog.findViewById(R.id.txt_gallery);
        TextView txt_cancel = (TextView) dialog.findViewById(R.id.txt_cancel);

        txt_camera.setText(getResources().getString(R.string.camera));
        txt_gallery.setText(getResources().getString(R.string.gallery));
        txt_cancel.setText(getResources().getString(R.string.cancel));


        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        txt_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, 1);

            }
        });
        txt_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, 2);
            }
        });

        dialog.show();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
/*
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                type = "camera";
                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                File destination = new File(Environment.getExternalStorageDirectory(),
                        System.currentTimeMillis() + ".jpg");
                FileOutputStream fo;
                try {
                    destination.createNewFile();
                    fo = new FileOutputStream(destination);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Uri tempUri = getImageUri(getApplicationContext(), thumbnail);


                str_image_path = tempUri;

//                File destination= new File(picturePath);

                thumbnail =   Utill.ExifInterfaceImage(destination,thumbnail);


                System.out.println("path of image from gallery......******************........." + destination + "");

                img_photo.setImageBitmap(thumbnail);


            } else if (requestCode == 2) {
                type = "gallery";
                Uri selectedImage = data.getData();
                String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String picturePath = c.getString(columnIndex);
                c.close();
                Bitmap thumbnail = (BitmapFactory.decodeFile(picturePath));
                System.out.println("path of image from gallery......******************........." + picturePath + "");


                File destination= new File(picturePath);

                thumbnail =   Utill.ExifInterfaceImage(destination,thumbnail);


                str_image_path = selectedImage;
                img_photo.setImageBitmap(thumbnail);
            }
        }
*/

        CropImage.ActivityResult result = CropImage.getActivityResult(data);
        if (resultCode == RESULT_OK) {
            Uri resultUri = result.getUri();

            str_image_path = resultUri;
            img_photo.setImageURI(resultUri);

        } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
            Exception error = result.getError();
        }
    }


    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }
}
