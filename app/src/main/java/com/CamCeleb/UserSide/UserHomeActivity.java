package com.CamCeleb.UserSide;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.CamCeleb.R;
import com.CamCeleb.SplashActivity;
import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class UserHomeActivity extends AppCompatActivity {
    BottomNavigationView navView;
    private View notificationBadge;

    BroadcastReceiver broadcastReceiver;
    public static SharedPreferences preferences;

    int count = 0;
    boolean flag = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_home);

        getSupportActionBar().hide();

        navView = findViewById(R.id.navuser_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.user_home, R.id.user_dashboard, R.id.user_notifications, R.id.user_profile)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.navuser_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);


        registerReceiver();


        //navView.setNotification(notification, bottomNavigation.getItemsCount() - 1);


//        navView.getOrCreateBadge(R.id.user_notifications).setNumber(1);
      /*  BottomNavigationMenuView menuView = (BottomNavigationMenuView) navView.getChildAt(0);
        BottomNavigationItemView itemView = (BottomNavigationItemView) menuView.getChildAt(2);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addBadgeView("");
            }
        });*/


        preferences = getSharedPreferences(getPackageName() + "_preferences", MODE_PRIVATE);

        if (preferences.getString("NOTICOUNT", "") != null) {

            String count = preferences.getString("NOTICOUNT", "");

            if (!count.equals("")) {
                int count_int = Integer.parseInt(count) + 1;

                addBadgeView(String.valueOf(count_int));

            }
        }

    }


    private void registerReceiver() {

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                count = count + 1;

                addBadgeView(String.valueOf(count));

            }
        };
        registerReceiver(broadcastReceiver, new IntentFilter("pushnotifiaction"));
    }


    private void addBadgeView(String badge) {

        flag = true;
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) navView.getChildAt(0);
        BottomNavigationItemView itemView = (BottomNavigationItemView) menuView.getChildAt(2);

        notificationBadge = LayoutInflater.from(this).inflate(R.layout.view_notification_badge, menuView, false);
        TextView txtbadge = notificationBadge.findViewById(R.id.badge);

        txtbadge.setText((CharSequence) badge);

        if (badge.equalsIgnoreCase("")) {

            String count_str = SplashActivity.preferences.getString("NOTICOUNT", "");

            if (count_str.equals("")) {

                txtbadge.setVisibility(View.GONE);

                if (itemView.getChildAt(2) != null) {
                    itemView.removeViewAt(2);
                }
                count = 0;
            }

        } else {
            txtbadge.setVisibility(View.VISIBLE);
            itemView.addView(notificationBadge);
        }


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (broadcastReceiver != null) {
            unregisterReceiver(broadcastReceiver);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (flag) {
            addBadgeView("");
        }
    }
}
