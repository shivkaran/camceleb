package com.CamCeleb.UserSide;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.CamCeleb.APIs.Authorization;
import com.CamCeleb.Activity.LoginAndSignActivity;
import com.CamCeleb.POJOClass.Login_model.Gson_response_login_data;
import com.CamCeleb.R;
import com.CamCeleb.Insta.InstagramApp;
import com.CamCeleb.POJOClass.ResultHandler;
import com.CamCeleb.POJOClass.User;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONObject;

public class GetSaveData extends AppCompatActivity {

    EditText email, name, number;
    InstagramApp instagramApp;
    Button button;
    TextView textView;


    String social_id = "", str_name = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_save_data);
        getSupportActionBar().hide();


        if (getIntent().getStringExtra("id") != null)
        {
            social_id = getIntent().getStringExtra("id");
        }
        if (getIntent().getStringExtra("name") != null)
        {
            str_name = getIntent().getStringExtra("name");
        }

        email = findViewById(R.id.getsave_email);
        name = findViewById(R.id.getsave_fullname);
        number = findViewById(R.id.getsave_phonenumber);
        button=findViewById(R.id.getsave_submitbutton);
        textView=findViewById(R.id.getsave_btm_account);


        name.setText(str_name);


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String str_email = email.getText().toString();
                String str_name = name.getText().toString();
                String phonenumber = number.getText().toString();

                setFacebookData(social_id,str_email,str_name,phonenumber);


            /*    Intent intent=new Intent(GetSaveData.this, UserHomeActivity.class);
                startActivity(intent);*/
            }
        });

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(GetSaveData.this, LoginAndSignActivity.class);
                startActivity(intent);
            }
        });


        String emails = email.getText().toString().trim();
        String names = name.getText().toString().trim();
        String numbers = number.getText().toString().trim();

        JsonObject jsonObject=new JsonObject();





    }

    private void setFacebookData(final String social_id, final String str_email, final String str_name, final String phonenumber) {
        final ProgressDialog progressDialog1 = new ProgressDialog(this);
        progressDialog1.show();

        Authorization authorization = new Authorization();

        authorization.SocialLogindata("F", "Y", str_email, social_id, str_name,phonenumber, new ResultHandler<Gson_response_login_data>() {

            @Override
            public void onSuccess(Gson_response_login_data data) {

                if (data.getSuccess() == 1) {

                    String responseStg = new Gson().toJson(data.getData());
                    try {
                        JSONObject responseObj = new JSONObject(responseStg);

                        if (responseObj.has("is_email_required")) {

                            if (data.getData().getIs_email_required().equalsIgnoreCase("Y")) {
                                Intent intent = new Intent(GetSaveData.this, GetSaveData.class);
                                intent.putExtra("id", data.getData().getId());
                                intent.putExtra("name", data.getData().getFullName());
                                startActivity(intent);
                            } else {
                                if (data.getData().getIdVerification().equals("noverify")) {
                                    progressDialog1.dismiss();
                                    data.getData().SaveSession();
                                    // Toast.makeText(GetSaveData.this, " First Verifly Your Account", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(GetSaveData.this, OTPVerificationActivity.class);
                                    startActivity(intent);

                                } else {
                                    progressDialog1.dismiss();
                                    data.getData().SaveSession();
                                    Intent intent = new Intent(GetSaveData.this, UserHomeActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            }

                        } else {
                            if (responseObj.has("id_verification")) {

                                if (data.getData().getIdVerification().equals("noverify")) {
                                    progressDialog1.dismiss();
                                    data.getData().SaveSession();
                                    // Toast.makeText(GetSaveData.this, " First Verifly Your Account", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(GetSaveData.this, OTPVerificationActivity.class);
                                    startActivity(intent);
                                    finish();

                                } else {
                                    progressDialog1.dismiss();
                                    data.getData().SaveSession();
                                    Intent intent = new Intent(GetSaveData.this, UserHomeActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            }else {
                                setFacebookData(social_id,str_email,str_name,phonenumber);

                            }
                        }
                    } catch (Exception e) {

                    }
                } else {
                    progressDialog1.dismiss();
                }
            }

            @Override
            public void onFailure(String e) {
                progressDialog1.dismiss();
            }
        });

    }
}
