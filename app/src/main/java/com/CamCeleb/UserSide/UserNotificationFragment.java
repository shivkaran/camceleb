package com.CamCeleb.UserSide;


import android.app.AlertDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.CamCeleb.APIs.DashBords;
import com.CamCeleb.Adapters.Notification_Adapter;
import com.CamCeleb.POJOClass.Notification.Gson_response_notification;
import com.CamCeleb.POJOClass.Notification.Notification_list_data;
import com.CamCeleb.POJOClass.ResultHandler;
import com.CamCeleb.R;
import com.CamCeleb.SplashActivity;
import com.facebook.shimmer.ShimmerFrameLayout;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserNotificationFragment extends Fragment {


    ViewGroup viewGroup;
    CardView cardView;


    RecyclerView list_of_notification, list_of_notification_yestday, list_of_notification_other;
    TextView txt_message;

    LinearLayout ll_today, ll_yestday, ll_other;


    List<Notification_list_data> list_data_today;
    List<Notification_list_data> list_data_yestday;
    List<Notification_list_data> list_data_other;

    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;


    ShimmerFrameLayout parentShimmerLayout;


    String start = "0";
    String limit = "100";

    boolean isLoading = false;
    String isLast = "";

    public UserNotificationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_user_notification, container, false);


        SharedPreferences.Editor ed = SplashActivity.preferences.edit();
        ed.putString("NOTICOUNT", "");
        ed.commit();


        parentShimmerLayout = view.findViewById(R.id.parentShimmerLayout);
        parentShimmerLayout.startShimmerAnimation();


//        cardView = view.findViewById(R.id.noti_cardview);
        viewGroup = view.findViewById(android.R.id.content);
        //  ratingBar=view.findViewById(R.id.noti_ratestar);
//        cardView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                showCustomDialog();
//            }
//        });


        list_of_notification = view.findViewById(R.id.list_of_notification);
        list_of_notification_yestday = view.findViewById(R.id.list_of_notification_yestday);
        list_of_notification_other = view.findViewById(R.id.list_of_notification_other);
        txt_message = view.findViewById(R.id.txt_message);
        ll_today = view.findViewById(R.id.ll_today);
        ll_yestday = view.findViewById(R.id.ll_yestday);
        ll_other = view.findViewById(R.id.ll_other);

        GetNotificationList();


        list_of_notification_other.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                if (!isLoading) {

                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == list_data_other.size()- 1) {
                        //bottom of list!

                        if (isLast.equalsIgnoreCase("Y")) {

                            int countoflimit = Integer.parseInt(limit);

                            countoflimit = countoflimit + 10;

                            limit = String.valueOf(countoflimit);
                            GetNotificationList();
                            isLoading = true;
                        }
                    }
                }
            }
        });


        return view;
    }

    private void GetNotificationList() {

        DashBords notificationDashBords = new DashBords();

        notificationDashBords.Notifiactionlist(start, limit, new ResultHandler<Gson_response_notification>() {
            @Override
            public void onSuccess(Gson_response_notification data) {

                if (data.getSuccess() == 1) {

                    if (data.getData().getNotificationList().size() != 0) {

                        txt_message.setVisibility(View.GONE);
                        isLast = data.getData().getIsLast();

                        isLoading = false;

                        Calendar mDate = Calendar.getInstance();
                        list_data_today = new ArrayList<>();
                        list_data_yestday = new ArrayList<>();
                        list_data_other = new ArrayList<>();
                        for (int i = 0; i < data.getData().getNotificationList().size(); i++) {

                            Notification_list_data notification_list_data = data.getData().getNotificationList().get(i);


                            long time = notification_list_data.getDate()*1000L;

                            String str_TimeAgo = getTimeAgo(time);

                            if (str_TimeAgo.equalsIgnoreCase("yesterday")) {

                                list_data_yestday.add(notification_list_data);

                            }else if (str_TimeAgo.equalsIgnoreCase("other")) {

                                list_data_other.add(notification_list_data);
                            }else {
                                list_data_today.add(notification_list_data);
                            }

                        }

                        parentShimmerLayout.stopShimmerAnimation();
                        parentShimmerLayout.setVisibility(View.GONE);


                        if (list_data_today.size() == 0) {
                            ll_today.setVisibility(View.GONE);
                        } else {
                            ll_today.setVisibility(View.VISIBLE);
                            Notification_Adapter notification_adapter = new Notification_Adapter(getActivity(), list_data_today, "Friend");
                            list_of_notification.setAdapter(notification_adapter);
                            list_of_notification.setLayoutManager(new LinearLayoutManager(getContext()));

                        }


                        if (list_data_yestday.size() == 0) {
                            ll_yestday.setVisibility(View.GONE);
                        } else {
                            ll_yestday.setVisibility(View.VISIBLE);
                            Notification_Adapter notification_adapter = new Notification_Adapter(getActivity(), list_data_yestday, "Friend");
                            list_of_notification_yestday.setAdapter(notification_adapter);
                            list_of_notification_yestday.setLayoutManager(new LinearLayoutManager(getContext()));

                        }

                        if (list_data_other.size() == 0) {
                            ll_other.setVisibility(View.GONE);
                        } else {
                            ll_other.setVisibility(View.VISIBLE);
                            Notification_Adapter notification_adapter = new Notification_Adapter(getActivity(), list_data_other, "Friend");
                            list_of_notification_other.setAdapter(notification_adapter);
                            list_of_notification_other.setLayoutManager(new LinearLayoutManager(getContext()));

                        }

                    } else {
                        parentShimmerLayout.stopShimmerAnimation();
                        parentShimmerLayout.setVisibility(View.GONE);
                        ll_today.setVisibility(View.GONE);
                        ll_yestday.setVisibility(View.GONE);
                        ll_other.setVisibility(View.GONE);
                        txt_message.setVisibility(View.VISIBLE);
                    }
                }

            }

            @Override
            public void onFailure(String exx) {
                // Toast.makeText(getContext(), "", Toast.LENGTH_SHORT).show();
            }
        });


    }

    private String getTimeAgo(long time) {

        if (time < 1000000000000L) {
            time *= 1000;
        }

        long now = System.currentTimeMillis();
        if (time > now || time <= 0) {
            return null;
        }


        final long diff = now - time;
        if (diff < MINUTE_MILLIS) {
            return "just now";
        } else if (diff < 2 * MINUTE_MILLIS) {
            return "a minute ago";
        } else if (diff < 50 * MINUTE_MILLIS) {
            return diff / MINUTE_MILLIS + " minutes ago";
        } else if (diff < 90 * MINUTE_MILLIS) {
            return "an hour ago";
        } else if (diff < 24 * HOUR_MILLIS) {
            return diff / HOUR_MILLIS + " hours ago";
        } else if (diff < 48 * HOUR_MILLIS) {
            return "yesterday";
        } else {
            return "other";
        }
    }

    public String getSmsTodayYestFromMilli(long msgTimeMillis) {

        Calendar messageTime = Calendar.getInstance();
        messageTime.setTimeInMillis(msgTimeMillis);
        // get Currunt time
        Calendar now = Calendar.getInstance();

        final String strTimeFormate = "h:mm aa";
        final String strDateFormate = "dd/MM/yyyy h:mm aa";

        if (now.get(Calendar.DATE) == messageTime.get(Calendar.DATE)
                &&
                ((now.get(Calendar.MONTH) == messageTime.get(Calendar.MONTH)))
                &&
                ((now.get(Calendar.YEAR) == messageTime.get(Calendar.YEAR)))
        ) {

//            return "today" + DateFormat.format(strTimeFormate, messageTime);
            return "today";

        } else if (
                ((now.get(Calendar.DATE) - messageTime.get(Calendar.DATE)) == 1)
                        &&
                        ((now.get(Calendar.MONTH) == messageTime.get(Calendar.MONTH)))
                        &&
                        ((now.get(Calendar.YEAR) == messageTime.get(Calendar.YEAR)))
        ) {
//            return "yesterday" + DateFormat.format(strTimeFormate, messageTime);
            return "yesterday";
        } else {
            return "other";
        }
    }

    private void showCustomDialog() {


        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(getContext()).inflate(R.layout.my_dialog, viewGroup, false);

        Button button = dialogView.findViewById(R.id.submit_rating);
        final RatingBar ratingBar = dialogView.findViewById(R.id.noti_ratestar);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String rating = String.valueOf(ratingBar.getRating());
                // Toast.makeText(getContext(), rating, Toast.LENGTH_LONG).show();
            }
        });

        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext(),R.style.MyDialogTheme);


        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);

        //finally creating the alert dialog and displaying it
        AlertDialog alertDialog = builder.create();


        alertDialog.show();
    }

}
