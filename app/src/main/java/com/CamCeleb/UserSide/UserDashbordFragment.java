package com.CamCeleb.UserSide;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.CamCeleb.APIs.DashBords;
import com.CamCeleb.Adapters.Search_data_Adapter;
import com.CamCeleb.Adapters.Search_data_Recent_Adapter;
import com.CamCeleb.Adapters.Search_data_Trending_Adapter;
import com.CamCeleb.POJOClass.ResultHandler;
import com.CamCeleb.POJOClass.SearchData.Gson_Response_search_data;
import com.CamCeleb.POJOClass.SearchData.Search_response_data;
import com.CamCeleb.R;
import com.facebook.shimmer.ShimmerFrameLayout;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserDashbordFragment extends Fragment {

    RecyclerView list_celeb_of_day, list_recent_search, list_trendings;

    TextView txt_celeb_of_day, txt_recent_search, txt_trendings,txt_message;

    EditText linsearchview;

    String search_text = "";

    LinearLayout ll_treding, ll_recent, ll_celeb_of_day;

    ShimmerFrameLayout parentShimmerLayout;

    public UserDashbordFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onStart() {
        super.onStart();


        SearchDataResponse("0", "100", search_text);


    }

    private void SearchDataResponse(String start, String linit, String search_text) {


        DashBords d = new DashBords();
        d.DashBordSearch(start, linit, search_text, new ResultHandler<Gson_Response_search_data>() {
            @Override
            public void onSuccess(Gson_Response_search_data data) {

                InitView(data.getData());
                Log.d("CALL", "CALLMETHOD");

            }
            @Override
            public void onFailure(String exx) {

            }
        });
    }

    private void InitView(Search_response_data data) {


        //recyclerView.setHasFixedSize(true);
        //  recyclerViewsin.setHasFixedSize(true);

        parentShimmerLayout.stopShimmerAnimation();
        parentShimmerLayout.setVisibility(View.GONE);

        if (search_text.equalsIgnoreCase("")) {

            if (data.getCelebOfDay().size() != 0) {

                ll_celeb_of_day.setVisibility(View.VISIBLE);
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(
                        getContext(),
                        LinearLayoutManager.HORIZONTAL,
                        false
                );
                linearLayoutManager.setInitialPrefetchItemCount(data.getCelebOfDay().size());
                txt_celeb_of_day.setText("Celebrity of the day");
                list_celeb_of_day.setLayoutManager(linearLayoutManager);
                Search_data_Adapter adapter = new Search_data_Adapter(getContext(), data.getCelebOfDay());
                list_celeb_of_day.setAdapter(adapter);
            } else {

                ll_celeb_of_day.setVisibility(View.GONE);
            }
            if (data.getRecentSearch().size() != 0) {

                ll_recent.setVisibility(View.VISIBLE);
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(
                        getContext(),
                        LinearLayoutManager.HORIZONTAL,
                        false
                );
                linearLayoutManager.setInitialPrefetchItemCount(data.getCelebOfDay().size());
                txt_recent_search.setText("Recent Searches");
                list_recent_search.setLayoutManager(linearLayoutManager);
                Search_data_Recent_Adapter adapter = new Search_data_Recent_Adapter(getContext(), data.getRecentSearch());
                list_recent_search.setAdapter(adapter);


            } else {

                ll_recent.setVisibility(View.GONE);

            }
            if (data.getTrendings().size() != 0) {
                ll_treding.setVisibility(View.VISIBLE);
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(
                        getContext(),
                        LinearLayoutManager.HORIZONTAL,
                        false
                );
                linearLayoutManager.setInitialPrefetchItemCount(data.getCelebOfDay().size());
                txt_trendings.setText("Trending");
                list_trendings.setLayoutManager(linearLayoutManager);
                Search_data_Trending_Adapter adapter = new Search_data_Trending_Adapter(getContext(), data.getTrendings());
                list_trendings.setAdapter(adapter);
            } else {
                ll_treding.setVisibility(View.GONE);
            }

        } else {

            if (data.getRecentSearch().size() != 0) {

                ll_recent.setVisibility(View.VISIBLE);
                txt_recent_search.setText("Recent Searches");
                list_recent_search.setLayoutManager(new GridLayoutManager(getContext(), 2));
                Search_data_Recent_Adapter adapter = new Search_data_Recent_Adapter(getContext(), data.getRecentSearch());
                list_recent_search.setAdapter(adapter);


            } else {
                ll_recent.setVisibility(View.GONE);
            }


        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_user_dashbord, container, false);

        parentShimmerLayout = view.findViewById(R.id.parentShimmerLayout);

        parentShimmerLayout.startShimmerAnimation();

        list_celeb_of_day = view.findViewById(R.id.list_celeb_of_day);
        list_recent_search = view.findViewById(R.id.list_recent_search);
        list_trendings = view.findViewById(R.id.list_trendings);
        linsearchview = view.findViewById(R.id.linsearchview);
        ll_treding = view.findViewById(R.id.ll_treding);
        ll_celeb_of_day = view.findViewById(R.id.ll_celeb_of_day);
        ll_recent = view.findViewById(R.id.ll_recent);
        txt_message = view.findViewById(R.id.txt_message);


        txt_celeb_of_day = view.findViewById(R.id.txt_celeb_of_day);
        txt_recent_search = view.findViewById(R.id.txt_recent_search);
        txt_trendings = view.findViewById(R.id.txt_trendings);
        list_celeb_of_day.setHasFixedSize(true);


        linsearchview.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                search_text = linsearchview.getText().toString();
                if (search_text.equals("")) {
                    SearchDataResponse("0", "50", search_text);
                } else {
                    SearchDataResponsedata("0", "50", search_text);
                }
            }
        });


        return view;
    }

    private void SearchDataResponsedata(String start, String linit, String search_text) {
        DashBords d = new DashBords();
        d.DashBordSearch(start, linit, search_text, new ResultHandler<Gson_Response_search_data>() {
            @Override
            public void onSuccess(Gson_Response_search_data data) {

                InitViewdetails(data.getData());
                Log.d("CALL", "CALLMETHOD");

            }

            @Override
            public void onFailure(String exx) {
            }
        });
    }

    private void InitViewdetails(Search_response_data data) {
        if (data.getRecentSearch().size() != 0) {

            ll_recent.setVisibility(View.VISIBLE);
            ll_celeb_of_day.setVisibility(View.GONE);
            ll_treding.setVisibility(View.GONE);
            txt_message.setVisibility(View.GONE);

            txt_recent_search.setText("Recent Searches");
            list_recent_search.setLayoutManager(new GridLayoutManager(getContext(), 2));
            Search_data_Recent_Adapter adapter = new Search_data_Recent_Adapter(getContext(), data.getRecentSearch());
            list_recent_search.setAdapter(adapter);


        } else {

            txt_message.setVisibility(View.VISIBLE);
            ll_recent.setVisibility(View.GONE);
        }

    }
}

