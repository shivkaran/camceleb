package com.CamCeleb.UserSide;

import android.content.ClipData;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.CamCeleb.APIs.DashBords;
import com.CamCeleb.Adapters.CommentAdapter;
import com.CamCeleb.Helper.RecyclerItemTouchHelper;
import com.CamCeleb.POJOClass.Comment.Comment_send.Gson_response_Comment_send;
import com.CamCeleb.POJOClass.Comment.Gson_response_comment;
import com.CamCeleb.POJOClass.Comment.Response_comment_list;
import com.CamCeleb.POJOClass.ResultHandler;
import com.CamCeleb.R;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.snackbar.Snackbar;

import org.apache.commons.lang3.StringEscapeUtils;

import java.util.List;

public class CommentsActivity extends AppCompatActivity implements RecyclerItemTouchHelper.RecyclerItemTouchHelperListener{

    RecyclerView listView;

    String video_id;

    EditText edt_comment;
    Button btn_send_comment;
    ShimmerFrameLayout parentShimmerLayout;

    String type="";
    RelativeLayout ll_bottom,ll_back;
    List<Response_comment_list> response_comment_lists;
    CommentAdapter commentAdapter;

    String start = "0";
    String limit = "10";
    boolean isLoading = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);
        getSupportActionBar().hide();

        Intent intent = getIntent();

        if (intent.getStringExtra("video_id") != null)
        {
            video_id = intent.getStringExtra("video_id");
        }
        if (intent.getStringExtra("type") != null)
        {
            type = intent.getStringExtra("type");
        }
        parentShimmerLayout = findViewById(R.id.parentShimmerLayout);

        parentShimmerLayout.startShimmerAnimation();


        listView = findViewById(R.id.commentslist);
        edt_comment = findViewById(R.id.edt_comment);
        btn_send_comment = findViewById(R.id.btn_send_comment);
        ll_bottom = findViewById(R.id.ll_bottom);
        ll_back = findViewById(R.id.ll_back);
        ImageView imageView = findViewById(R.id.commentsbackbtn);

        ll_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               onBackPressed();
            }
        });


        if (type.equals("celeb"))
        {
            ll_bottom.setVisibility(View.GONE);
        }

        btn_send_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String str_message = edt_comment.getText().toString();

                if (!str_message.equals(""))
                {
                    SendMessage(str_message);

                }
            }
        });

        GetCommentList();


        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this);
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(listView);



        listView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                if (!isLoading) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() ==response_comment_lists.size() - 1) {
                        //bottom of list!

                        int countoflimit = Integer.parseInt(limit);

                        countoflimit = countoflimit + 10;

                        limit = String.valueOf(countoflimit);
                        GetNewRequest1();
//                        isLoading = true;
                    }
                }
            }
        });



    }

    private void GetNewRequest1() {
        DashBords dashBords = new DashBords();

        System.out.println("video id "+  video_id);

        dashBords.CommentList(start,limit,video_id, new ResultHandler<Gson_response_comment>() {
            @Override
            public void onSuccess(Gson_response_comment data) {

                if (data.getSuccess() == 1) {

                    response_comment_lists = data.getData().getList();
                    commentAdapter.setDataList(response_comment_lists);
                }

            }

            @Override
            public void onFailure(String exx) {

            }


        });


    }

    private void SendMessage(String str_message) {
        edt_comment.setText("");

        String toServerUnicodeEncoded = StringEscapeUtils.escapeJava(str_message);

        System.out.println(toServerUnicodeEncoded);

        if (!toServerUnicodeEncoded.equalsIgnoreCase(""))
        {
            DashBords dashBords = new DashBords();
            dashBords.CommentSend(video_id, str_message, new ResultHandler<Gson_response_Comment_send>() {
                @Override
                public void onSuccess(Gson_response_Comment_send data) {
                    if (data.getSuccess() == 1) {

                        GetCommentList();

                    } else {

                    }
                }

                @Override
                public void onFailure(String exx) {

                }
            });


        }



    }

    private void GetCommentList() {
        DashBords dashBords = new DashBords();

        System.out.println("video id "+  video_id);

        dashBords.CommentList("0","10",video_id, new ResultHandler<Gson_response_comment>() {
            @Override
            public void onSuccess(Gson_response_comment data) {

                if (data.getSuccess() == 1) {
                    InitView(data.getData().getList());

                } else {
                    parentShimmerLayout.stopShimmerAnimation();
                    parentShimmerLayout.setVisibility(View.GONE);
                }

            }

            @Override
            public void onFailure(String exx) {

            }


        });

    }

    private void InitView(List<Response_comment_list> list) {

        parentShimmerLayout.stopShimmerAnimation();
        parentShimmerLayout.setVisibility(View.GONE);

        response_comment_lists = list;


        commentAdapter = new CommentAdapter(CommentsActivity.this, response_comment_lists);
        listView.setAdapter(commentAdapter);
        listView.setLayoutManager(new GridLayoutManager(CommentsActivity.this,1));

    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {

        if (viewHolder instanceof CommentAdapter.MyViewHolder) {
            // get the removed item name to display it in snack bar
            String name = response_comment_lists.get(viewHolder.getAdapterPosition()).getComment();
            /*final Item deletedItem = response_comment_lists.get(viewHolder.getAdapterPosition());
            final int deletedIndex = viewHolder.getAdapterPosition();
*/
            commentAdapter.Flagset(viewHolder.getAdapterPosition());

            isFlagUpdate(response_comment_lists.get(position).getId());


        }
    }

    private void isFlagUpdate(Integer id) {
        DashBords dashBords = new DashBords();

        System.out.println("video id "+  video_id);

        dashBords.flagunflag(id, new ResultHandler<Gson_response_comment>() {
            @Override
            public void onSuccess(Gson_response_comment data) {

                if (data.getSuccess() == 1)
                {
                    Toast.makeText(CommentsActivity.this, "Flag add Successfully", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(String exx) {

            }


        });


    }
}
