package com.CamCeleb.UserSide;


import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.CamCeleb.APIs.Authorization;
import com.CamCeleb.APIs.DashBords;
import com.CamCeleb.Activity.LoginAndSignActivity;
import com.CamCeleb.Adapters.ParentActorAdapter;
import com.CamCeleb.POJOClass.ActorSee;
import com.CamCeleb.POJOClass.DashBorsData;
import com.CamCeleb.POJOClass.DashboardList;
import com.CamCeleb.POJOClass.ResultHandler;
import com.CamCeleb.R;
import com.facebook.login.LoginManager;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.JsonObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserHomeFragment extends Fragment {

    ImageView imageView;
    RecyclerView recyclerView, seeallreview, recyclerViewsin;
    ArrayList<DashboardList> actorList;
    ArrayList<DashBorsData> dashboardLists;
    ArrayList<ActorSee> actorSees;
    RecyclerView.LayoutManager mLayoutManager;
    LinearLayout mainview, sellalldata;
    TextView seeall;
    TextView textView;
    ShimmerFrameLayout parentShimmerLayout;
    ParentActorAdapter adapter;

    SwipeRefreshLayout mSwipeRefreshLayout;

    String start = "0";
    String limit = "10";

    DashboardList dashboardList;
    boolean isLoading = false;

    public UserHomeFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public void onStart() {
        super.onStart();



    }

    private void InitView(DashboardList actorList) {


        //recyclerView.setHasFixedSize(true);
        //  recyclerViewsin.setHasFixedSize(true);


        if (actorList.getData() != null) {

            dashboardList = actorList;

            parentShimmerLayout.stopShimmerAnimation();
            parentShimmerLayout.setVisibility(View.GONE);

            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));


            adapter = new ParentActorAdapter(getContext(), dashboardList);
            //   ParentActorAdapter adapter1 = new ParentActorAdapter(getContext(), actorList);


            recyclerView.setAdapter(adapter);
        }else {
            parentShimmerLayout.stopShimmerAnimation();
            parentShimmerLayout.setVisibility(View.GONE);

            final Dialog dialog = new Dialog(getActivity());
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.custom_alert_dailog);

            TextView text = (TextView) dialog.findViewById(R.id.txt_message);
            text.setText("No Data Found");

            TextView dialogButton =  dialog.findViewById(R.id.submit);
            dialogButton.setText("Ok");

            dialogButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    dialog.dismiss();
                }
            });

            dialog.show();
        }
        // recyclerViewsin.setAdapter(adapter1);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_user_home, container, false);

        parentShimmerLayout = view.findViewById(R.id.parentShimmerLayout);
        mSwipeRefreshLayout = (SwipeRefreshLayout)view.findViewById(R.id.swiperefresh);

        parentShimmerLayout.startShimmerAnimation();



        mainview = view.findViewById(R.id.user_linear_main);

        seeall = view.findViewById(R.id.seeall);
        textView = view.findViewById(R.id.name_names);
        recyclerViewsin = view.findViewById(R.id.recy_single);
        recyclerView = view.findViewById(R.id.user_recyclerviewhome);
     //   seeallreview = view.findViewById(R.id.user_recyclerviewhomelayout);


     /*   mLayoutManager = new GridLayoutManager(getContext(), 2);
        seeallreview.setLayoutManager(mLayoutManager);*/

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getAPicall();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

        getAPicall();



      /*  recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                if (!isLoading) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == dashboardList.getData().length - 1) {
                        //bottom of list!

                        int countoflimit = Integer.parseInt(limit);

                        countoflimit = countoflimit+10;

                        limit = String.valueOf(countoflimit);
                        getAPicall();
                        isLoading = true;
                    }
                }
            }
        });*/


        return view;
    }

    private void getAPicall() {


        DashBords d = new DashBords();


        d.DashBord("",start,limit,new ResultHandler<DashboardList>() {
            @Override
            public void onSuccess(DashboardList data) {

                InitView(data);
                Log.d("CALL", "CALLMETHOD");

            }

            @Override
            public void onFailure(String exx) {

            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();

    }
}
