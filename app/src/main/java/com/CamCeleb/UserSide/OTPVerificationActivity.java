package com.CamCeleb.UserSide;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.SystemClock;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.CamCeleb.APIs.Authorization;
import com.CamCeleb.Activity.LoginAndSignActivity;
import com.CamCeleb.CelebSide.CelebHomeActivity;
import com.CamCeleb.Helper.Utill;
import com.CamCeleb.POJOClass.OTPVerification.Gson_Response_OTP_Resend;
import com.CamCeleb.POJOClass.ResultHandler;
import com.CamCeleb.POJOClass.User;
import com.CamCeleb.R;
import com.chaos.view.PinView;
import com.google.gson.Gson;


public class OTPVerificationActivity extends AppCompatActivity implements View.OnClickListener {

    TextView otp_emailget, txt_resend;
    Button otp_button;
    PinView pinView;
    public static SharedPreferences preferences;
    User user;
    String user_type, Admin_verification;
    ImageView otpbackbtn;
    boolean aBoolean = true;

    Chronometer cmTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otpverification);
        getSupportActionBar().hide();

        preferences = getSharedPreferences(getPackageName() + "_preferences", MODE_PRIVATE);

        otp_emailget = findViewById(R.id.otp_emailget);
        txt_resend = findViewById(R.id.txt_resend);
        pinView = findViewById(R.id.firstPinView);
        otp_button = findViewById(R.id.otp_button);
        otpbackbtn = findViewById(R.id.otpbackbtn);
        cmTimer = findViewById(R.id.cmTimer);


        ((PinView) findViewById(R.id.firstPinView)).setAnimationEnable(true);


        String session = preferences.getString("SessionData", "");

        Gson gson = new Gson();
        user = gson.fromJson(session, User.class);   //get data from response
        otp_emailget.setText(user.getEmail());


        if (user.getUserType() == 1) {
            user_type = "F";
        } else {
            user_type = "C";
        }

        Admin_verification = user.getAdmin_verification();



       /* int stoppedMilliseconds = 0;

        cmTimer.setText("00:30");

        String chronoText = mChronometer.getText().toString();
        String array[] = chronoText.split(":");
        if (array.length == 2) {
            stoppedMilliseconds = Integer.parseInt(array[0]) * 60 * 1000
                    + Integer.parseInt(array[1]) * 1000;
        } else if (array.length == 3) {
            stoppedMilliseconds = Integer.parseInt(array[0]) * 60 * 60 * 1000
                    + Integer.parseInt(array[1]) * 60 * 1000
                    + Integer.parseInt(array[2]) * 1000;
        }*/

      /*  cmTimer.setBase(SystemClock.elapsedRealtime());
        cmTimer.start();*/

      /*  cmTimer.setBase(SystemClock.elapsedRealtime() - 30*1000);
        cmTimer.start();*/


        CountDownTimer cT =  new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {


                String v = String.format("%02d", millisUntilFinished/60000);
                int va = (int)( (millisUntilFinished%60000)/1000);
                cmTimer.setText(v+":"+String.format("%02d",va));
            }

            public void onFinish() {
                txt_resend.setVisibility(View.VISIBLE);
                cmTimer.setVisibility(View.GONE);
            }
        };
        cT.start();



        txt_resend.setOnClickListener(this);
        otpbackbtn.setOnClickListener(this);
        otp_button.setOnClickListener(this);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                cmTimer.stop();

                aBoolean = true;
                txt_resend.setVisibility(View.VISIBLE);
                cmTimer.setVisibility(View.GONE);

            }}, 30000);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txt_resend:
//                ResendOtp();
                if (Utill.isOnline(this)) {
                    ResendOtp();
                } else {
                    ShowDailog(getResources().getString(R.string.intentermessage));
                }
                break;

            case R.id.otpbackbtn:
                onBackPressed();
                break;

            case R.id.otp_button:
                String emails = pinView.getText().toString().trim();
                if (!emails.equals("")) {

//                    VerificationOTP();
                    if (Utill.isOnline(this)) {
                        VerificationOTP();
                    } else {
                        ShowDailog(getResources().getString(R.string.intentermessage));
                    }
                } else {
                    ShowDailog("Please enter OTP");
                    // Toast.makeText(OTPVerificationActivity.this, "Please enter OTP", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void VerificationOTP() {

        String emails = otp_emailget.getText().toString().trim();
        String str_OTP = pinView.getText().toString();

        Authorization authorization = new Authorization();

        authorization.VerificationOTP(emails, str_OTP, new ResultHandler<Gson_Response_OTP_Resend>() {

            @Override
            public void onSuccess(Gson_Response_OTP_Resend data) {

                if (data.getSuccess() == 1) {

                    if (user_type.equals("F")) {
                        Intent intent = new Intent(OTPVerificationActivity.this, UserHomeActivity.class);
                        startActivity(intent);
                        finish();

                    } else {

                        if (Admin_verification.equalsIgnoreCase("N")) {

                            ShowDailogVerifiy("Your profile is under review and you will get an confirmation email to continue using Camceleb");

                        } else {

                            Intent intent = new Intent(OTPVerificationActivity.this, CelebHomeActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    }

                } else {

                    String message = String.valueOf(data.getError());
                    String message1 = message.replace("[", "");
                    String message2 = message1.replace("]", "");

                    ShowDailog(message2);

                    // Toast.makeText(OTPVerificationActivity.this, message2, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(String exx) {
                ShowDailog("Wrong OTP please Try Again");

            }
        });


    }

    private void ShowDailogVerifiy(String login_failed_please_try_again) {
        final Dialog dialog = new Dialog(this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custom_alert_dailog_otp);

        TextView text = (TextView) dialog.findViewById(R.id.txt_message);
        TextView txt_title = (TextView) dialog.findViewById(R.id.txt_title);
        text.setText(login_failed_please_try_again);
        txt_title.setText("Your email verified successfully!");

        TextView dialogButton =  dialog.findViewById(R.id.submit);

        dialogButton.setText("Ok");


        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                startActivity(new Intent(OTPVerificationActivity.this, LoginAndSignActivity.class));
                finishAffinity();
            }
        });


        dialog.show();
    }

    private void ShowDailog(String login_failed_please_try_again) {

       final Dialog dialog = new Dialog(this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custom_alert_dailog);

        TextView text = (TextView) dialog.findViewById(R.id.txt_message);
        text.setText(login_failed_please_try_again);

        TextView dialogButton =  dialog.findViewById(R.id.submit);

        dialogButton.setText("Ok");


        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

            }
        });


        dialog.show();
    }


    private void ResendOtp() {

        String emails = otp_emailget.getText().toString().trim();
        Authorization authorization = new Authorization();

        authorization.ResendOtp(emails, user_type, new ResultHandler<Gson_Response_OTP_Resend>() {
            @Override
            public void onSuccess(Gson_Response_OTP_Resend data) {


                if (data.getSuccess() == 1) {
                    String message = data.getData().getMessage();
                    String message1 = message.replace("[", "");
                    String message2 = message1.replace("]", "");
                    // Toast.makeText(OTPVerificationActivity.this, message, Toast.LENGTH_SHORT).show();
                    ShowDailog(message2);

                } else {
                    String message = String.valueOf(data.getError());
                    String message1 = message.replace("[", "");
                    String message2 = message1.replace("]", "");
                    // Toast.makeText(OTPVerificationActivity.this, message2, Toast.LENGTH_SHORT).show();
                    ShowDailog(message2);

                }

            }

            @Override
            public void onFailure(String exx) {
                // Toast.makeText(OTPVerificationActivity.this, exx, Toast.LENGTH_SHORT).show();
                ShowDailog("Something went wrong please try again!");

            }
        });

    }
}
