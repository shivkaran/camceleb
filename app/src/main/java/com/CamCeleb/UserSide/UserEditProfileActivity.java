package com.CamCeleb.UserSide;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.exifinterface.media.ExifInterface;

import com.CamCeleb.APIs.DashBords;
import com.CamCeleb.APIs.Helper;
import com.CamCeleb.CelebSide.CelebEditProfileActivity;
import com.CamCeleb.Helper.Utill;
import com.CamCeleb.POJOClass.Login_model.Gson_response_login_data;
import com.CamCeleb.POJOClass.ResultHandler;
import com.CamCeleb.POJOClass.User;
import com.CamCeleb.R;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class UserEditProfileActivity extends AppCompatActivity {

    ImageView imageView;
    TextView  cancle, done, txt_change_profile_image;
    private static int RESULT_LOAD_IMAGE = 1;
    private static int CAMERA_PIC_REQUEST = 1;

    String session;
    ProgressBar progress;

    public static SharedPreferences preferences;

    EditText edt_full_name, edt_email, edt_mobile;

    Uri profileUri = null;
    private static final int CAMERA_REQUEST_CODE_VEDIO = 101;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_edit_profile);
        getSupportActionBar().hide();


        preferences = getSharedPreferences(getPackageName() + "_preferences", MODE_PRIVATE);

        session = preferences.getString("SessionData", "");
        Gson gson = new Gson();
        User user = gson.fromJson(session, User.class);

     //   signout = findViewById(R.id.user_logout);
        cancle = findViewById(R.id.usereditprofile_cancel);
        done = findViewById(R.id.usereditprofile_done);
        imageView = findViewById(R.id.editprofile_imagechange);
    //    chnagepassword = findViewById(R.id.profileedit_changepassword);
        txt_change_profile_image = findViewById(R.id.txt_change_profile_image);
        edt_full_name = findViewById(R.id.edt_full_name);
        edt_email = findViewById(R.id.edt_email);
        edt_mobile = findViewById(R.id.edt_mobile);


        edt_email.setText(user.getEmail());

        edt_email.setEnabled(false);
        edt_full_name.setText(user.getFullName());
        edt_mobile.setText(user.getPhoneNumber());

        if (user.getProfilePic().isEmpty())
        {
            imageView.setImageDrawable(getResources().getDrawable(R.mipmap.profile_pic));

        }else {
//            Picasso.with(this).load(user.getProfilePic()).rotate(90f).into(imageView);
            Picasso.with(this).load(user.getProfilePic()).into(imageView);
        }


        txt_change_profile_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                    requestMultiplePermissions();

                } else {

                    Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                    if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {
                        startActivityForResult(takeVideoIntent, CAMERA_REQUEST_CODE_VEDIO);
                    }
                }

            }
        });

     /*   chnagepassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UserEditProfileActivity.this, ChangePasswordActivity.class);
                startActivity(intent);
            }
        });
*/
        cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                UpdateProfile();

            }
        });

      /*  signout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(UserEditProfileActivity.this);
                builder.setTitle("CamCeleb");
                builder.setMessage("Are You sure you want to logout?");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Authorization authorization = new Authorization();
                        authorization.Logout(new ResultHandler<JsonObject>() {
                            @Override
                            public void onSuccess(JsonObject data) {

                                SharedPreferences preferences = getSharedPreferences(getPackageName() + "_preferences", MODE_PRIVATE);

                                preferences.edit().clear().commit();
                                LoginManager.getInstance().logOut();

                                Intent intent = new Intent(UserEditProfileActivity.this, SplashActivity.class);
                                startActivity(intent);

                            }

                            @Override
                            public void onFailure(String exx) {

                            }
                        });

                    }
                });

                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                });
                builder.show();
            }
        });
*/

    }

    private void UpdateProfile() {

        DashBords dashBords = new DashBords();

        String full_name = edt_full_name.getText().toString();
        String phone_number = edt_mobile.getText().toString();

        if (profileUri != null) {

            final ProgressDialog progressDialog=new ProgressDialog(UserEditProfileActivity.this);
            progressDialog.show();

            dashBords.UpdateProfile_withImage(UserEditProfileActivity.this,Helper.createRequestBody(full_name), Helper.createRequestBody(phone_number), profileUri, new ResultHandler<Gson_response_login_data>() {
                @Override
                public void onSuccess(final Gson_response_login_data data) {
                    if (data.getSuccess() == 1) {

                     /*   data.getData().SaveSession();
                        progressDialog.dismiss();
                        Intent refresh = new Intent(UserEditProfileActivity.this, UserEditProfileActivity.class);
                        startActivity(refresh);
                        finish();*/
                        data.getData().SaveSession();
                        progressDialog.dismiss();
                        ShowDailog("Profile Updated Successfully!");

                    }

                }

                @Override
                public void onFailure(String exx) {

                    progressDialog.dismiss(); final Dialog dialog = new Dialog(UserEditProfileActivity.this);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    dialog.setCancelable(false);
                    dialog.setContentView(R.layout.custom_alert_dailog);

                    TextView text = (TextView) dialog.findViewById(R.id.txt_message);
                    TextView cancel = (TextView) dialog.findViewById(R.id.cancel);
                    text.setText("Something went wrong please try again!");

                    TextView dialogButton = dialog.findViewById(R.id.submit);
                    dialogButton.setText("OK");
                    dialogButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            dialog.dismiss();


                        }
                    });


                    dialog.show();


                }
            });

        }else {
            final ProgressDialog progressDialog=new ProgressDialog(UserEditProfileActivity.this);
            progressDialog.show();

            dashBords.UpdateProfile(full_name, phone_number, new ResultHandler<Gson_response_login_data>() {
                @Override
                public void onSuccess(final Gson_response_login_data data) {
                    if (data.getSuccess() == 1) {
                        data.getData().SaveSession();
                        progressDialog.dismiss();
                        ShowDailog("Profile Updated Successfully!");


                    }
                }

                @Override
                public void onFailure(String exx) {
                    progressDialog.dismiss();
                    final Dialog dialog = new Dialog(UserEditProfileActivity.this);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    dialog.setCancelable(false);
                    dialog.setContentView(R.layout.custom_alert_dailog);

                    TextView text = (TextView) dialog.findViewById(R.id.txt_message);
                    TextView cancel = (TextView) dialog.findViewById(R.id.cancel);
                    text.setText("Something went wrong please try again!");

                    TextView dialogButton = dialog.findViewById(R.id.submit);
                    dialogButton.setText("OK");
                    dialogButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            dialog.dismiss();


                        }
                    });


                    dialog.show();

                }
            });
        }


    }

    private void ShowDailog(String login_failed_please_try_again) {

        final Dialog dialog = new Dialog(this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custom_alert_dailog);

        TextView text = (TextView) dialog.findViewById(R.id.txt_message);
        text.setText(login_failed_please_try_again);

        TextView dialogButton =  dialog.findViewById(R.id.submit);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                onBackPressed();
            }
        });

        dialog.show();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void requestMultiplePermissions() {

        if (ContextCompat.checkSelfPermission(UserEditProfileActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) + ContextCompat.checkSelfPermission(UserEditProfileActivity.this, Manifest.permission.CAMERA) + ContextCompat.checkSelfPermission(UserEditProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(UserEditProfileActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(UserEditProfileActivity.this, Manifest.permission.CAMERA) || ActivityCompat.shouldShowRequestPermissionRationale(UserEditProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                Snackbar.make(UserEditProfileActivity.this.findViewById(android.R.id.content),
                        "Please Grant Permissions to upload Image",
                        Snackbar.LENGTH_INDEFINITE).setAction("Enable",
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, CAMERA_REQUEST_CODE_VEDIO);
                            }
                        }).show();
            } else {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, CAMERA_REQUEST_CODE_VEDIO);
            }
        } else {
//            selectImage();
            CropImage.activity()
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setAspectRatio(1,1)
                    .start(this);
        }
    }

    private void selectImage() {

        final Dialog dialog = new Dialog(this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.upload_photo_custome_dialog);

        TextView txt_camera = (TextView) dialog.findViewById(R.id.txt_camera);
        TextView txt_gallery = (TextView) dialog.findViewById(R.id.txt_gallery);
        TextView txt_cancel = (TextView) dialog.findViewById(R.id.txt_cancel);

        txt_camera.setText(getResources().getString(R.string.camera));
        txt_gallery.setText( getResources().getString(R.string.gallery));
        txt_cancel.setText( getResources().getString(R.string.cancel));


        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        txt_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, 1);

            }
        });
        txt_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, 2);
            }
        });

        dialog.show();






    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        CropImage.ActivityResult result = CropImage.getActivityResult(data);
        if (resultCode == RESULT_OK) {
            Uri resultUri = result.getUri();

            profileUri = resultUri;
            imageView.setImageURI(resultUri);

        } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
            Exception error = result.getError();
        }

    }


    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }
}
