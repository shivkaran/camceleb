package com.CamCeleb.UserSide;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.CamCeleb.APIs.Authorization;
import com.CamCeleb.Activity.LoginAndSignActivity;
import com.CamCeleb.CelebSide.CelebEditProfileActivity;
import com.CamCeleb.ChangePasswordActivity;
import com.CamCeleb.POJOClass.ResultHandler;
import com.CamCeleb.R;
import com.CamCeleb.SplashActivity;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.gson.JsonObject;

public class User_Setting extends AppCompatActivity {

    public static SharedPreferences preferences;

    LinearLayout ll_profile, ll_privacy, ll_tearm, ll_faq, ll_logout,ll_Change_password,ll_transaction,ll_contact;
    ImageView img_back;
    RelativeLayout ll_back;
    GoogleSignInOptions gso;
    GoogleSignInClient mGoogleSignInClient;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user__setting);

        getSupportActionBar().hide();

        preferences = getSharedPreferences(getPackageName() + "_preferences", MODE_PRIVATE);
        String session = preferences.getString("MasterData", "");

        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestIdToken("555372711845-15lk8aapfg3imnnnmtpkrk7cralgiv7k.apps.googleusercontent.com")
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);


        final String faqs = preferences.getString("faqs", "");
        final String terms = preferences.getString("terms", "");
        final String privacy = preferences.getString("privacy", "");
        String _about_us = preferences.getString("_about_us", "");

        System.out.println(faqs);


        ll_profile = findViewById(R.id.ll_profile);
        ll_Change_password = findViewById(R.id.ll_Change_password);
        ll_transaction = findViewById(R.id.ll_transaction);
        ll_privacy = findViewById(R.id.ll_privacy);
        ll_tearm = findViewById(R.id.ll_tearm);
        ll_faq = findViewById(R.id.ll_faq);
        ll_logout = findViewById(R.id.ll_logout);
        img_back = findViewById(R.id.img_back);
        ll_back = findViewById(R.id.ll_back);
        ll_contact = findViewById(R.id.ll_contact);


        ll_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(User_Setting.this, UserEditProfileActivity.class);
                startActivity(intent);
            }
        });

        ll_Change_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(User_Setting.this, ChangePasswordActivity.class);
                startActivity(intent);
            }
        });

        ll_transaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(User_Setting.this, Transaction_history.class);
                startActivity(intent);
            }
        });
        ll_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.camceleb.com/contact"));
                startActivity(browserIntent);
            }
        });
        ll_privacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(privacy));
                startActivity(browserIntent);
            }
        });
        ll_tearm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(terms));
                startActivity(browserIntent);
            }
        });
        ll_faq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(faqs));
                startActivity(browserIntent);
            }
        });

        ll_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final Dialog dialog = new Dialog(User_Setting.this);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.setCancelable(false);
                dialog.setContentView(R.layout.custom_alert_dailog);

                TextView text = (TextView) dialog.findViewById(R.id.txt_message);
                TextView cancel = (TextView) dialog.findViewById(R.id.cancel);
                text.setText("Are You sure you want to logout?");

                TextView dialogButton =  dialog.findViewById(R.id.submit);
                dialogButton.setText("Yes");
                cancel.setText("No");
                cancel.setVisibility(View.VISIBLE);
                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        dialog.dismiss();

                        Authorization authorization = new Authorization();
                        authorization.Logout(User_Setting.this,new ResultHandler<JsonObject>() {
                            @Override
                            public void onSuccess(JsonObject data) {


                                SharedPreferences preferences = getSharedPreferences(getPackageName() + "_preferences", MODE_PRIVATE);
                                preferences.edit().clear().commit();

                                LoginManager.getInstance().logOut();
                                mGoogleSignInClient.signOut();
                                Intent intent = new Intent(User_Setting.this, LoginAndSignActivity.class);
                                startActivity(intent);
                                finishAffinity();

                            }

                            @Override
                            public void onFailure(String exx) {

                            }
                        });
                    }
                });

                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.show();




            /*    AlertDialog.Builder builder = new AlertDialog.Builder(User_Setting.this,R.style.MyDialogTheme);
                builder.setTitle("CamCeleb");
                builder.setMessage("Are You sure you want to logout?");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Authorization authorization = new Authorization();
                        authorization.Logout(User_Setting.this,new ResultHandler<JsonObject>() {
                            @Override
                            public void onSuccess(JsonObject data) {


                                SharedPreferences preferences = getSharedPreferences(getPackageName() + "_preferences", MODE_PRIVATE);
                                preferences.edit().clear().commit();


                               *//* SharedPreferences loginPreferences  = getSharedPreferences("loginPrefs", MODE_PRIVATE);
                                loginPreferences.edit().clear().commit();*//*

                                LoginManager.getInstance().logOut();

                                Intent intent = new Intent(User_Setting.this, LoginAndSignActivity.class);
                                startActivity(intent);
                                finishAffinity();

                            }

                            @Override
                            public void onFailure(String exx) {

                            }
                        });

                    }
                });

                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                });
                builder.show();*/
            }
        });

        ll_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


    }
}
