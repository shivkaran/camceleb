package com.CamCeleb.UserSide;


import android.animation.ObjectAnimator;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.CamCeleb.APIs.DashBords;
import com.CamCeleb.Adapters.CelebProfileApdater;
import com.CamCeleb.Adapters.UserAdpterView;
import com.CamCeleb.Adapters.User_Video_ProfileApdater;
import com.CamCeleb.OtpCelebActivity;
import com.CamCeleb.POJOClass.Favourite.Gson_Response_Favourite_data;
import com.CamCeleb.POJOClass.Favourite.Response_Favourite_data;
import com.CamCeleb.POJOClass.RequestList.Gson_response_requestlist;
import com.CamCeleb.POJOClass.RequestList.Response_RequestList;
import com.CamCeleb.POJOClass.ResultHandler;
import com.CamCeleb.POJOClass.User;
import com.CamCeleb.R;
import com.CamCeleb.UserSide.UserEditProfileActivity;
import com.CamCeleb.UserSide.User_Setting;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserProfileviewFragment extends Fragment {

    FrameLayout editprofile, one, two, three;
//    GridView gridView;
//    RecyclerView requestsview, fevview;

    ImageView profile_image;
    TextView txt_name, tv_progress_count, txt_fav_count, txt_request_count,txt_video_count;

    String session;
    ProgressBar progress;

    public static SharedPreferences preferences;

    RecyclerView profilelastviewfav;

    Response_Favourite_data getFavouriteList;

    Response_RequestList response_requestList;
    TextView txt_type;

    ShimmerFrameLayout parentShimmerLayout;

    LinearLayout ll_header;

    User user;
    TextView txt_no_validation;

    RelativeLayout ll_no_data_avalible;

    public UserProfileviewFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_user_profileview, container, false);

        parentShimmerLayout = view.findViewById(R.id.parentShimmerLayout);
        parentShimmerLayout.startShimmerAnimation();

        return view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        ll_header = view.findViewById(R.id.ll_header);

        preferences = getActivity().getSharedPreferences(getActivity().getPackageName() + "_preferences", getActivity().MODE_PRIVATE);

        session = preferences.getString("SessionData", "");
        Gson gson = new Gson();
        user = gson.fromJson(session, User.class);


        editprofile = view.findViewById(R.id.useredditprofile);
        progress = view.findViewById(R.id.progress);
        profile_image = view.findViewById(R.id.profile_image);
        txt_name = view.findViewById(R.id.txt_name);
        tv_progress_count = view.findViewById(R.id.tv_progress_count);
        profilelastviewfav = view.findViewById(R.id.profilelastviewfav);
        txt_fav_count = view.findViewById(R.id.txt_fav_count);
        txt_request_count = view.findViewById(R.id.txt_request_count);
        txt_video_count = view.findViewById(R.id.txt_video_count);
        txt_type = view.findViewById(R.id.txt_type);
        txt_no_validation = view.findViewById(R.id.txt_no_validation);
        ll_no_data_avalible = view.findViewById(R.id.ll_no_data_avalible);


        one = view.findViewById(R.id.firstfr);
        two = view.findViewById(R.id.twofr);
        three = view.findViewById(R.id.threefr);


        Getrequestlist();

        GetIsfavourite();





        if (user.getProfilePic().isEmpty())
        {
            profile_image.setImageDrawable(getResources().getDrawable(R.mipmap.profile_pic));
        }else {
            Picasso.with(getActivity()).load(user.getProfilePic()).into(profile_image);
        }



        int count = 50;

        if (!user.getProfilePic().isEmpty())
        {
            count =count+50;

        }

        if (user.getIsProfileCompleted() == 1) {
//            progress.setProgress(50);
            ObjectAnimator.ofInt(progress, "progress", count)
                    .setDuration(1000)
                    .start();
            tv_progress_count.setText(count+"%");
        } else {
            progress.setProgress(50);
            tv_progress_count.setText(count+"%");
        }



        txt_name.setText(user.getFullName().trim());


        editprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), User_Setting.class);
                startActivity(intent);
            }
        });


        one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                txt_type.setText("MY REQUESTS");
                if (response_requestList != null) {

                    if (response_requestList.getList().size() != 0) {


                        profilelastviewfav.setVisibility(View.VISIBLE);
                        txt_no_validation.setVisibility(View.GONE);
                        ll_no_data_avalible.setVisibility(View.GONE);

                        UserAdpterView userAdpterView = new UserAdpterView(getActivity(), response_requestList.getList());
                        profilelastviewfav.setAdapter(userAdpterView);
                        LinearLayoutManager layoutManager = new GridLayoutManager(getActivity(), 2);
                        profilelastviewfav.setLayoutManager(layoutManager);
                    }else {
                        profilelastviewfav.setVisibility(View.GONE);
                        txt_no_validation.setVisibility(View.VISIBLE);
                        ll_no_data_avalible.setVisibility(View.VISIBLE);
                    }
                }else {
                    profilelastviewfav.setVisibility(View.GONE);
                    txt_no_validation.setVisibility(View.VISIBLE);
                    ll_no_data_avalible.setVisibility(View.VISIBLE);
                }
            }
        });
        two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                GetIsfavourite();
                txt_type.setText("MY FAVOURITES");
                if (getFavouriteList != null) {

                    if (getFavouriteList.getFavouriteList().size() != 0) {
                        txt_no_validation.setVisibility(View.GONE);
                        ll_no_data_avalible.setVisibility(View.GONE);

                        profilelastviewfav.setVisibility(View.VISIBLE);
                        CelebProfileApdater celebProfileApdater = new CelebProfileApdater(getActivity(), getFavouriteList.getFavouriteList());

                        profilelastviewfav.setAdapter(celebProfileApdater);
                        LinearLayoutManager layoutManager = new GridLayoutManager(getActivity(), 2);
                        profilelastviewfav.setLayoutManager(layoutManager);
                    }else {
                        profilelastviewfav.setVisibility(View.GONE);
                        txt_no_validation.setVisibility(View.VISIBLE);
                        ll_no_data_avalible.setVisibility(View.VISIBLE);
                    }
                }else {
                    profilelastviewfav.setVisibility(View.GONE);
                    txt_no_validation.setVisibility(View.VISIBLE);
                    ll_no_data_avalible.setVisibility(View.VISIBLE);
                }

            }
        });

        three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                txt_type.setText("MY VIDEOS");

                if (getFavouriteList != null)
                {
                    if (getFavouriteList.getVideos().size() != 0) {

                        txt_no_validation.setVisibility(View.GONE);
                        ll_no_data_avalible.setVisibility(View.GONE);
                        profilelastviewfav.setVisibility(View.VISIBLE);

                        User_Video_ProfileApdater celebProfileApdater = new User_Video_ProfileApdater(getActivity(), getFavouriteList.getVideos());

                        profilelastviewfav.setAdapter(celebProfileApdater);
                        LinearLayoutManager layoutManager = new GridLayoutManager(getActivity(), 2);
                        profilelastviewfav.setLayoutManager(layoutManager);
                    }else {
                        profilelastviewfav.setVisibility(View.GONE);
                        txt_no_validation.setVisibility(View.VISIBLE);
                        ll_no_data_avalible.setVisibility(View.VISIBLE);
                    }

                }else {
                    profilelastviewfav.setVisibility(View.GONE);
                    txt_no_validation.setVisibility(View.VISIBLE);
                    ll_no_data_avalible.setVisibility(View.VISIBLE);
                }

            }
        });


    }

    private void GetIsfavourite() {

        DashBords d = new DashBords();


        d.FavouriteList(new ResultHandler<Gson_Response_Favourite_data>() {
            @Override
            public void onSuccess(Gson_Response_Favourite_data data) {

                /*if (user.getIsProfileCompleted() == 1) {
//            progress.setProgress(50);
                    ObjectAnimator.ofInt(progress, "progress", 100)
                            .setDuration(1000)
                            .start();
                    tv_progress_count.setText("100%");
                } else {
                    progress.setProgress(50);
                    tv_progress_count.setText("50%");
                }*/





                getFavouriteList = data.getData();
                int count_of_data = data.getData().getFavouriteList().size();
                int count_of_data_video_count = data.getData().getVideosCount();

                txt_fav_count.setText(String.valueOf(count_of_data));
                txt_video_count.setText(String.valueOf(count_of_data_video_count));

            }

            @Override
            public void onFailure(String exx) {
                    System.out.println("error"+ exx);
            }
        });

    }


    private void Getrequestlist() {

        DashBords dashBords = new DashBords();

        dashBords.RequestList("0", "100", new ResultHandler<Gson_response_requestlist>() {
            @Override
            public void onSuccess(Gson_response_requestlist data) {


                parentShimmerLayout.stopShimmerAnimation();
                parentShimmerLayout.setVisibility(View.GONE);
                ll_header.setVisibility(View.VISIBLE);



                response_requestList = data.getData();

                int count_of_data = data.getData().getList().size();

                txt_request_count.setText(String.valueOf(count_of_data));


                InitViewrequestlist(data);



            }

            @Override
            public void onFailure(String exx) {

            }
        });


    }

    private void InitViewrequestlist(Gson_response_requestlist data) {
        txt_type.setText("MY REQUESTS");

        if (data.getData().getList().size() != 0) {
            txt_no_validation.setVisibility(View.GONE);
            ll_no_data_avalible.setVisibility(View.GONE);
            profilelastviewfav.setVisibility(View.VISIBLE);
            UserAdpterView userAdpterView = new UserAdpterView(getActivity(), data.getData().getList());
            profilelastviewfav.setAdapter(userAdpterView);
            LinearLayoutManager layoutManager = new GridLayoutManager(getActivity(), 2);
            profilelastviewfav.setLayoutManager(layoutManager);
        }else {
            txt_no_validation.setVisibility(View.VISIBLE);
            ll_no_data_avalible.setVisibility(View.VISIBLE);
            profilelastviewfav.setVisibility(View.GONE);
        }

    }


    @Override
    public void onResume() {
        super.onResume();
        preferences = getActivity().getSharedPreferences(getActivity().getPackageName() + "_preferences", getActivity().MODE_PRIVATE);

        session = preferences.getString("SessionData", "");
        Gson gson = new Gson();
        user = gson.fromJson(session, User.class);
        if (user.getProfilePic().isEmpty())
        {
            profile_image.setImageDrawable(getResources().getDrawable(R.mipmap.profile_pic));
        }else {
            Picasso.with(getActivity()).load(user.getProfilePic()).into(profile_image);
        }
        txt_name.setText(user.getFullName().trim());

        int count = 50;

        if (!user.getProfilePic().isEmpty())
        {
            count =count+50;

        }
        if (user.getIsProfileCompleted() == 1) {
//            progress.setProgress(50);
            ObjectAnimator.ofInt(progress, "progress", count)
                    .setDuration(1000)
                    .start();
            tv_progress_count.setText(count+"%");
        } else {
            progress.setProgress(50);
            tv_progress_count.setText(count+"%");
        }

        Getrequestlist();

        GetIsfavourite();

    }
}
