package com.CamCeleb.UserSide;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.CamCeleb.APIs.Celeb_DashBords;
import com.CamCeleb.APIs.DashBords;
import com.CamCeleb.APIs.Helper;
import com.CamCeleb.CelebSide.Request_Details_screen;
import com.CamCeleb.Helper.Utill;
import com.CamCeleb.POJOClass.RequestVideo.Gson_Response_Requestvideo;
import com.CamCeleb.POJOClass.Request_Details.Gson_Response_Requestdetail;
import com.CamCeleb.POJOClass.ResultHandler;
import com.CamCeleb.POJOClass.User;
import com.CamCeleb.R;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class Edit_Request_details extends AppCompatActivity {


    EditText edt_phone_no, edt_email, edt_bio, edt_full_name;
    TextView txt_celeb_name, txt_back;
    ImageView intro_userimgci, img_photo;

    RadioButton request_me, request_friend;


    User user;
    LinearLayout linerfriendview;
    private static final int CAMERA_REQUEST_CODE_VEDIO = 101;

    Uri str_image_path;
    String booking_for;
    String is_private = "0";
    String celeb_id = "", celeb_name = "", price = "";

    Button requestpage_submit;
    CheckBox request_privatevideo;
    String friend_name, phone_number, email, instructions, full_name, profile_pic;

    String type = "";
    String request_id = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit__request_details);
        getSupportActionBar().hide();

        if (getIntent().getStringExtra("request_id") != null) {
            request_id = getIntent().getStringExtra("request_id");
        }


        img_photo = findViewById(R.id.img_photo);
        edt_phone_no = findViewById(R.id.edt_phone_no);
        edt_email = findViewById(R.id.edt_email);
        edt_bio = findViewById(R.id.edt_bio);
        txt_celeb_name = findViewById(R.id.txt_celeb_name);
        edt_full_name = findViewById(R.id.edt_full_name);
        intro_userimgci = findViewById(R.id.intro_userimgci);

        linerfriendview = findViewById(R.id.linerfriendview);
        txt_back = findViewById(R.id.txt_back);

        request_me = findViewById(R.id.request_me);
        request_friend = findViewById(R.id.request_friend);
        requestpage_submit = findViewById(R.id.requestpage_submit);
        request_privatevideo = findViewById(R.id.request_privatevideo);

        getRequestDetails();


        txt_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        request_privatevideo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    is_private = "1";
                } else {
                    is_private = "0";
                }
            }
        });


        request_me.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                request_friend.setChecked(false);
                if (!friend_name.isEmpty()) {
                    edt_phone_no.setText("");
                    edt_email.setText("");
                    edt_full_name.setText("");
                    edt_bio.setText("");
                    linerfriendview.setVisibility(View.GONE);
                    booking_for = "M";

                }else {
                    edt_phone_no.setText(phone_number);
                    edt_email.setText(email);
                    edt_full_name.setText(full_name);
                    edt_bio.setText(instructions);
                    linerfriendview.setVisibility(View.GONE);
                    booking_for = "M";
                }
            }
        });


        request_friend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                request_me.setChecked(false);
                if (friend_name.isEmpty()) {
                    edt_phone_no.setText("");
                    edt_email.setText("");
                    edt_full_name.setText("");
                    edt_bio.setText("");
                    linerfriendview.setVisibility(View.VISIBLE);
                    booking_for = "F";
                }else {
                    edt_phone_no.setText(phone_number);
                    edt_email.setText(email);
                    edt_full_name.setText(friend_name);
                    edt_bio.setText(instructions);
                    linerfriendview.setVisibility(View.VISIBLE);
                    booking_for = "F";
                }
            }
        });


        linerfriendview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                    requestMultiplePermissions();

                } else {

                    Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                    if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {
                        startActivityForResult(takeVideoIntent, CAMERA_REQUEST_CODE_VEDIO);
                    }
                }

            }
        });

        requestpage_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String emailPattern = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]+";
                friend_name = edt_full_name.getText().toString();
                phone_number = edt_phone_no.getText().toString();
                email = edt_email.getText().toString();
                instructions = edt_bio.getText().toString();


                if (friend_name.equals("")) {
                    edt_full_name.setError("Please enter full name");

                } else if (phone_number.equals("'")) {
                    edt_phone_no.setError("Please provide a valid 10 digit contact number");

                } else if (email.equals("")) {
                    edt_email.setError("Please enter email");

                } else if (!edt_email.getText().toString().matches(emailPattern)) {
                    edt_email.setError("Please enter valid email");

                } else if (instructions.equals("")) {
                    edt_bio.setError("Please enter instructions");
                } else if (edt_bio.getText().toString().trim().length() < 20) {
                    edt_bio.setError("Please enter minimum 20 instructions ");
                } else if (edt_bio.getText().toString().trim().length() > 250) {
                    edt_bio.setError("Please enter maximum 250 instructions ");
                } else {

                    ShowDailog("Are you sure you want to Update this request?");


                }
            }
        });

    }

    private void ShowDailog(String login_failed_please_try_again) {

        final Dialog dialog = new Dialog(this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custom_alert_dailog);

        TextView text = (TextView) dialog.findViewById(R.id.txt_message);
        TextView cancel = (TextView) dialog.findViewById(R.id.cancel);
        text.setText(login_failed_please_try_again);
        cancel.setVisibility(View.VISIBLE);
        cancel.setText("No");

        TextView dialogButton =  dialog.findViewById(R.id.submit);
        dialogButton.setText("Yes");
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                UpdateRequest();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

        dialog.show();
    }

    private void UpdateRequest() {
        DashBords dashBords = new DashBords();

        if (booking_for.equals("M")) {

            dashBords.RequestvideoWithoutimageUpdate(Edit_Request_details.this, request_id, "", phone_number, email, instructions, booking_for, is_private, new ResultHandler<Gson_Response_Requestvideo>() {
                @Override
                public void onSuccess(Gson_Response_Requestvideo data) {

                    if (data.getSuccess() == 1) {

                      /*  AlertDialog.Builder builder = new AlertDialog.Builder(Edit_Request_details.this,R.style.MyDialogTheme);
                        builder.setTitle("CamCeleb");
                        builder.setMessage("Request Update Successfully");
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                onBackPressed();
                            }
                        });

                        builder.show();*/
                        final Dialog dialog = new Dialog(Edit_Request_details.this);
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));                        dialog.setCancelable(false);
                        dialog.setContentView(R.layout.custom_alert_dailog);

                        TextView text = (TextView) dialog.findViewById(R.id.txt_message);
                        TextView cancel = (TextView) dialog.findViewById(R.id.cancel);
                        text.setText("Request Update Successfully");

                        TextView dialogButton =  dialog.findViewById(R.id.submit);
                        dialogButton.setText("OK");
                        dialogButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                                onBackPressed();

                            }
                        });


                        dialog.show();
                    }

                }

                @Override
                public void onFailure(String exx) {

                }
            });

        } else {

            if (str_image_path != null) {

                dashBords.Requestvideoupdate(Edit_Request_details.this, Helper.createRequestBody(request_id), Helper.createRequestBody(friend_name), Helper.createRequestBody(phone_number), Helper.createRequestBody(email), Helper.createRequestBody(instructions), Helper.createRequestBody(booking_for), Helper.createRequestBody(is_private), str_image_path, type, new ResultHandler<Gson_Response_Requestvideo>() {
                    @Override
                    public void onSuccess(Gson_Response_Requestvideo data) {
                        if (data.getSuccess() == 1) {
                           /* AlertDialog.Builder builder = new AlertDialog.Builder(
                                    Edit_Request_details.this,R.style.MyDialogTheme);
                            builder.setTitle("CamCeleb");
                            builder.setMessage("Request Update Successfully");
                            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    onBackPressed();
                                }
                            });

                            builder.show();*/
                            final Dialog dialog = new Dialog(Edit_Request_details.this);
                            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));                            dialog.setCancelable(false);
                            dialog.setContentView(R.layout.custom_alert_dailog);

                            TextView text = (TextView) dialog.findViewById(R.id.txt_message);
                            TextView cancel = (TextView) dialog.findViewById(R.id.cancel);
                            text.setText("Request Update Successfully");

                            TextView dialogButton =  dialog.findViewById(R.id.submit);
                            dialogButton.setText("OK");
                            dialogButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                    onBackPressed();

                                }
                            });


                            dialog.show();

                        }
                    }

                    @Override
                    public void onFailure(String exx) {

                    }
                });
            }else {
                dashBords.RequestvideoWithoutimageUpdate(Edit_Request_details.this, request_id, friend_name, phone_number, email, instructions, booking_for, is_private, new ResultHandler<Gson_Response_Requestvideo>() {
                    @Override
                    public void onSuccess(Gson_Response_Requestvideo data) {

                        if (data.getSuccess() == 1) {
                         /*   AlertDialog.Builder builder = new AlertDialog.Builder(Edit_Request_details.this,R.style.MyDialogTheme);
                            builder.setTitle("CamCeleb");
                            builder.setMessage("Request Update Successfully");
                            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    onBackPressed();
                                }
                            });

                            builder.show();*/
                            final Dialog dialog = new Dialog(Edit_Request_details.this);
                            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));                            dialog.setCancelable(false);
                            dialog.setContentView(R.layout.custom_alert_dailog);

                            TextView text = (TextView) dialog.findViewById(R.id.txt_message);
                            TextView cancel = (TextView) dialog.findViewById(R.id.cancel);
                            text.setText("Request Update Successfully");

                            TextView dialogButton =  dialog.findViewById(R.id.submit);
                            dialogButton.setText("OK");
                            dialogButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                    onBackPressed();

                                }
                            });


                            dialog.show();
                        }

                    }

                    @Override
                    public void onFailure(String exx) {

                    }
                });
            }
        }

    }

    private void getRequestDetails() {

        Celeb_DashBords celeb_dashBords = new Celeb_DashBords();

        celeb_dashBords.RequestDetails(Edit_Request_details.this,request_id, new ResultHandler<Gson_Response_Requestdetail>() {
            @Override
            public void onSuccess(Gson_Response_Requestdetail data) {

                if (data.getSuccess() == 1) {
                    Interview(data);
                } else {

                }

            }

            @Override
            public void onFailure(String exx) {

            }
        });


    }

    private void Interview(Gson_Response_Requestdetail data) {

        String request_id = data.getData().getId();
        friend_name = data.getData().getFriendName();
        String friend_image = data.getData().getFriendImage();
        phone_number = data.getData().getPhoneNumber();
        email = data.getData().getEmail();
        instructions = data.getData().getInstructions();
        is_private = data.getData().getIsPrivate();
        full_name = data.getData().getRequestedBy().getFullName();
        profile_pic = data.getData().getRequestedBy().getProfilePic();
        celeb_name = data.getData().getCeleb().getFullName();
        String profile_image_celeb = data.getData().getCeleb().getProfilePic();


        if (profile_image_celeb.isEmpty()) {
            intro_userimgci.setImageDrawable(getResources().getDrawable(R.mipmap.profile_pic));
        } else {
            Picasso.with(this).load(profile_image_celeb).into(intro_userimgci);
        }

        if (friend_name.isEmpty()) {
            linerfriendview.setVisibility(View.GONE);
            img_photo.setImageDrawable(getResources().getDrawable(R.mipmap.profile_pic));
            booking_for = "M";
            edt_full_name.setText(full_name);
        } else {
            linerfriendview.setVisibility(View.VISIBLE);
            booking_for = "F";
            edt_full_name.setText(friend_name);
            if (friend_image.isEmpty())
            {
            }else {
                Picasso.with(this).load(friend_image).into(img_photo);
            }
        }


        edt_phone_no.setText(String.valueOf(data.getData().getPhoneNumber()));
        edt_email.setText(email);
        edt_bio.setText(instructions);
        txt_celeb_name.setText(celeb_name);

        if (booking_for.equalsIgnoreCase("M")) {
            request_me.setChecked(true);
        } else if (booking_for.equalsIgnoreCase("F")) {
            request_friend.setChecked(true);
        }

        if (is_private.equalsIgnoreCase("1")) {
            request_privatevideo.setChecked(true);
        } else if (is_private.equalsIgnoreCase("0")) {
            request_privatevideo.setChecked(false);
        }


    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void requestMultiplePermissions() {

        if (ContextCompat.checkSelfPermission(Edit_Request_details.this, Manifest.permission.READ_EXTERNAL_STORAGE) + ContextCompat.checkSelfPermission(Edit_Request_details.this, Manifest.permission.CAMERA) + ContextCompat.checkSelfPermission(Edit_Request_details.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(Edit_Request_details.this, Manifest.permission.READ_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(Edit_Request_details.this, Manifest.permission.CAMERA) || ActivityCompat.shouldShowRequestPermissionRationale(Edit_Request_details.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                Snackbar.make(Edit_Request_details.this.findViewById(android.R.id.content),
                        "Please Grant Permissions to upload Image",
                        Snackbar.LENGTH_INDEFINITE).setAction("Enable",
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, CAMERA_REQUEST_CODE_VEDIO);
                            }
                        }).show();
            } else {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, CAMERA_REQUEST_CODE_VEDIO);
            }
        } else {
//            selectImage();
            CropImage.activity()
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setAspectRatio(1,1)
                    .start(this);
        }
    }

    private void selectImage() {

        final Dialog dialog = new Dialog(this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.upload_photo_custome_dialog);

        TextView txt_camera = (TextView) dialog.findViewById(R.id.txt_camera);
        TextView txt_gallery = (TextView) dialog.findViewById(R.id.txt_gallery);
        TextView txt_cancel = (TextView) dialog.findViewById(R.id.txt_cancel);

        txt_camera.setText(getResources().getString(R.string.camera));
        txt_gallery.setText( getResources().getString(R.string.gallery));
        txt_cancel.setText( getResources().getString(R.string.cancel));


        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        txt_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, 1);

            }
        });
        txt_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, 2);
            }
        });

        dialog.show();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
       /* if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                type = "camera";
                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                File destination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".png");
                FileOutputStream fo;
                try {
                    destination.createNewFile();
                    fo = new FileOutputStream(destination);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Uri tempUri = getImageUri(getApplicationContext(), thumbnail);


                thumbnail =   Utill.ExifInterfaceImage(destination,thumbnail);

                str_image_path = tempUri;


                System.out.println("path of image from gallery......******************........." + destination + "");

                img_photo.setImageBitmap(thumbnail);


            } else if (requestCode == 2) {
                type = "gallery";
                Uri selectedImage = data.getData();
                String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String picturePath = c.getString(columnIndex);
                c.close();
                Bitmap thumbnail = (BitmapFactory.decodeFile(picturePath));
                System.out.println("path of image from gallery......******************........." + picturePath + "");


                File destination= new File(picturePath);

                thumbnail =   Utill.ExifInterfaceImage(destination,thumbnail);

                str_image_path = selectedImage;
                img_photo.setImageBitmap(thumbnail);
            }
        }*/
        CropImage.ActivityResult result = CropImage.getActivityResult(data);
        if (resultCode == RESULT_OK) {
            Uri resultUri = result.getUri();

            str_image_path = resultUri;
            img_photo.setImageURI(resultUri);

        } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
            Exception error = result.getError();
        }
    }


    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }
}
