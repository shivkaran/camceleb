package com.CamCeleb.UserSide;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.CamCeleb.APIs.Celeb_DashBords;
import com.CamCeleb.Adapters.Friend_ErningAdapter;
import com.CamCeleb.POJOClass.Celeb_Home_Request.Celeb_Reuest_list;
import com.CamCeleb.POJOClass.Celeb_Home_Request.Gson_Response_Celeb_Request_list;
import com.CamCeleb.POJOClass.ResultHandler;
import com.CamCeleb.R;
import com.facebook.shimmer.ShimmerFrameLayout;

import java.util.ArrayList;
import java.util.List;

public class Transaction_history extends AppCompatActivity {

    TextView txt_title;

    ImageView erningback;

    RecyclerView list_of_transation;
    List<Celeb_Reuest_list> list;

    ShimmerFrameLayout parentShimmerLayout;
    RelativeLayout ll_back;

    TextView  txt_nodata;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_history);
        getSupportActionBar().hide();

        parentShimmerLayout = findViewById(R.id.parentShimmerLayout);
        parentShimmerLayout.startShimmerAnimation();

        txt_title = findViewById(R.id.txt_title);
        ll_back = findViewById(R.id.ll_back);
        erningback = findViewById(R.id.erningback);
        list_of_transation = findViewById(R.id.list_of_transation);
        txt_nodata = findViewById(R.id.txt_nodata);

        txt_title.setText("Transactions History");

        ll_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        GetTranstionsHistory();
    }

    private void GetTranstionsHistory() {

        Celeb_DashBords dashBords = new Celeb_DashBords();

        dashBords.CelebDash("0", "100","4", new ResultHandler<Gson_Response_Celeb_Request_list>() {

            @Override
            public void onSuccess(Gson_Response_Celeb_Request_list data) {

                if (data.getSuccess() == 1) {

                    list = new ArrayList<>();

                    if (data.getData().getList().size() != 0) {

                        for (int i = 0; i < data.getData().getList().size(); i++) {
                            Celeb_Reuest_list celeb_reuest_list = data.getData().getList().get(i);

                            if (celeb_reuest_list.getStatus().equalsIgnoreCase("4")) {
                                list.add(celeb_reuest_list);
                            }
                        }


                        parentShimmerLayout.stopShimmerAnimation();
                        parentShimmerLayout.setVisibility(View.GONE);
                        Friend_ErningAdapter celeb_request_adapter = new Friend_ErningAdapter(Transaction_history.this, list);
                        list_of_transation.setAdapter(celeb_request_adapter);
                        list_of_transation.setLayoutManager(new LinearLayoutManager(Transaction_history.this));

                    }else {
                        parentShimmerLayout.stopShimmerAnimation();
                        parentShimmerLayout.setVisibility(View.GONE);

                        txt_nodata.setVisibility(View.VISIBLE);
                        list_of_transation.setVisibility(View.GONE);
                    }
                }else {
                    parentShimmerLayout.stopShimmerAnimation();
                    parentShimmerLayout.setVisibility(View.GONE);

                    txt_nodata.setVisibility(View.VISIBLE);
                    list_of_transation.setVisibility(View.GONE);
                }

            }

            @Override
            public void onFailure(String exx) {
                parentShimmerLayout.stopShimmerAnimation();
                parentShimmerLayout.setVisibility(View.GONE);
                txt_nodata.setVisibility(View.VISIBLE);
                list_of_transation.setVisibility(View.GONE);
            }
        });
    }
}
