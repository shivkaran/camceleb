package com.CamCeleb;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.CamCeleb.APIs.Authorization;
import com.CamCeleb.POJOClass.Result;
import com.CamCeleb.POJOClass.ResultHandler;
import com.CamCeleb.UserSide.Edit_Request_details;

public class ChangePasswordActivity extends AppCompatActivity {

    ImageView imageView;
    EditText oldpass, newpass, conpass;
    Button changpass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        getSupportActionBar().hide();

        oldpass = findViewById(R.id.change_oldpass);
        newpass = findViewById(R.id.change_newpass);
        conpass = findViewById(R.id.change_conpass);
        changpass = findViewById(R.id.celec_submitchange);

        imageView = findViewById(R.id.forgotbackbtn);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();

            }
        });

        changpass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String oldpassw = oldpass.getText().toString();
                String newpassw = newpass.getText().toString();
                String confpassw = conpass.getText().toString();


                if (oldpassw.equalsIgnoreCase("")) {

                    oldpass.setError("Enter old password");

                } else if (newpassw.equalsIgnoreCase("")) {

                    newpass.setError("Enter new password");

                }else if (newpass.getText().toString().length() < 5) {
                    newpass.setError("Please enter password minimum 5 characters");

                } else if (confpassw.equalsIgnoreCase("")) {

                    conpass.setError("Enter Confirm password");

                } else if (!confpassw.equals(newpassw)) {
                    conpass.setError("Confirm password must match with New Password");
                } else {

                    final ProgressDialog progressDialog = new ProgressDialog(ChangePasswordActivity.this);
                    progressDialog.show();


                    Authorization authorization = new Authorization();
                    authorization.Changepassword(oldpassw, newpassw, new ResultHandler<Result>() {
                        @Override
                        public void onSuccess(Result data) {
                            progressDialog.dismiss();
                            if (data.getCode() == 1) {
                                showDaolog("Password Change Successfully done");
                            } else {

                                final Dialog dialog = new Dialog(ChangePasswordActivity.this);
                                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));                                dialog.setCancelable(false);
                                dialog.setContentView(R.layout.custom_alert_dailog);

                                TextView text = (TextView) dialog.findViewById(R.id.txt_message);
                                TextView cancel = (TextView) dialog.findViewById(R.id.cancel);
                                text.setText("Something went wrong please try again!");

                                TextView dialogButton =  dialog.findViewById(R.id.submit);
                                dialogButton.setText("OK");
                                dialogButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialog.dismiss();


                                    }
                                });


                                dialog.show();

                              /*
                                AlertDialog.Builder builder = new AlertDialog.Builder(ChangePasswordActivity.this, R.style.MyDialogTheme);
                                builder.setTitle("CamCeleb");
                                builder.setMessage("Something we wrong please try again!");
                                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });

                                builder.show();*/
                            }
                        }

                        @Override
                        public void onFailure(String exx) {
                            progressDialog.dismiss();
                        }
                    });

                }

            }
        });

    }

    private void showDaolog(String password_change_successfully_done) {
        final Dialog dialog = new Dialog(ChangePasswordActivity.this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custom_alert_dailog);

        TextView text = (TextView) dialog.findViewById(R.id.txt_message);
        TextView cancel = (TextView) dialog.findViewById(R.id.cancel);
        text.setText(password_change_successfully_done);

        TextView dialogButton =  dialog.findViewById(R.id.submit);
        dialogButton.setText("OK");
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                onBackPressed();

            }
        });


        dialog.show();
    }
}
