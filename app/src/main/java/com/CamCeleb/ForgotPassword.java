package com.CamCeleb;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.CamCeleb.APIs.Authorization;
import com.CamCeleb.Helper.Utill;
import com.CamCeleb.POJOClass.OTPVerification.Gson_Response_OTP_Resend;
import com.CamCeleb.POJOClass.ResultHandler;

public class ForgotPassword extends AppCompatActivity {

    TextView textView, sendemail,txt_tryagin;
    EditText editText;
    Button submitbtn, forgotsubmit;
    LinearLayout linearLayout, linshow, textshow,lineargonefail;

    String emailPattern = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]+";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        getSupportActionBar().hide();

        editText = findViewById(R.id.forgot_email);
        textView = findViewById(R.id.reset_forgot_btm_login);
        submitbtn = findViewById(R.id.forgot_btn_submit);
        linearLayout = findViewById(R.id.lineargone);
        lineargonefail = findViewById(R.id.lineargonefail);
        textshow = findViewById(R.id.textshow);
        linshow = findViewById(R.id.linshow);
        sendemail = findViewById(R.id.emailid_pass);
        txt_tryagin = findViewById(R.id.txt_tryagin);



        txt_tryagin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linshow.setVisibility(View.VISIBLE);
                linearLayout.setVisibility(View.GONE);
                lineargonefail.setVisibility(View.GONE);
            }
        });

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        submitbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {


                String email = editText.getText().toString().trim();

                if (email.isEmpty()) {
                    editText.setError("Please enter email");
                } else if (!email.matches(emailPattern)) {
                    editText.setError("Please enter valid email");

                } else {

                    if (Utill.isOnline(ForgotPassword.this)) {

                        final ProgressDialog progressDialog = new ProgressDialog(ForgotPassword.this);
                        progressDialog.show();

                        final String emails = editText.getText().toString();


                        Authorization authorization = new Authorization();
                        authorization.Forgotpassword(emails, new ResultHandler<Gson_Response_OTP_Resend>() {
                            @Override
                            public void onSuccess(Gson_Response_OTP_Resend data) {

                                progressDialog.dismiss();
                                if (data.getSuccess() == 1) {


                                    linshow.setVisibility(View.GONE);
                                    linearLayout.setVisibility(View.VISIBLE);
                                    sendemail.setText(emails);

                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {

                                            onBackPressed();

                                        }
                                    }, 2000);


                                } else {

                                    lineargonefail.setVisibility(View.VISIBLE);
                                    linearLayout.setVisibility(View.GONE);
                                    linshow.setVisibility(View.GONE);

                                   /* String message = data.getError().toString();
                                    String message1 = message.replace("[", "");
                                    String message2 = message1.replace("]", "");
                                    ShowDailog(message2);*/
                                }

                            }

                            @Override
                            public void onFailure(String exx) {
                                progressDialog.dismiss();
                                linshow.setVisibility(View.VISIBLE);
                                linearLayout.setVisibility(View.INVISIBLE);

                            }
                        });
                    } else {
                        ShowDailog(getResources().getString(R.string.intentermessage));
                    }


                }
            }
        });
        textshow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                linshow.setVisibility(v.VISIBLE);
                linearLayout.setVisibility(v.INVISIBLE);

            }
        });


    }

    private void ShowDailog(String login_failed_please_try_again) {

        final Dialog dialog = new Dialog(this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dailog_layout);

        TextView text = (TextView) dialog.findViewById(R.id.txt_message);
        text.setText(login_failed_please_try_again);

        Button dialogButton = (Button) dialog.findViewById(R.id.submit);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }
}
