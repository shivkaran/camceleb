package com.CamCeleb.POJOClass.Notification;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Response_Notification {

    @SerializedName("notification_list")
    @Expose
    private List<Notification_list_data> notificationList = null;
    @SerializedName("is_last")
    @Expose
    private String isLast;
    @SerializedName("message")
    @Expose
    private String message;

    public List<Notification_list_data> getNotificationList() {
        return notificationList;
    }

    public void setNotificationList(List<Notification_list_data> notificationList) {
        this.notificationList = notificationList;
    }

    public String getIsLast() {
        return isLast;
    }

    public void setIsLast(String isLast) {
        this.isLast = isLast;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
