package com.CamCeleb.POJOClass.Notification;

import com.CamCeleb.POJOClass.Celebdetails.Celeb_Data_response;
import com.CamCeleb.POJOClass.Celebdetails.User_Celeb_details;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Notification_list_data {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("video_id")
    @Expose
    private Integer videoId;
    @SerializedName("date")
    @Expose
    private Integer date;
    @SerializedName("user")
    @Expose
    private User_Celeb_details user;
    @SerializedName("amount")
    @Expose
    private Integer amount;
    @SerializedName("rating")
    @Expose
    private String rating;
    @SerializedName("comment")
    @Expose
    private String comment;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getVideoId() {
        return videoId;
    }

    public void setVideoId(Integer videoId) {
        this.videoId = videoId;
    }

    public Integer getDate() {
        return date;
    }

    public void setDate(Integer date) {
        this.date = date;
    }

    public User_Celeb_details getUser() {
        return user;
    }

    public void setUser(User_Celeb_details user) {
        this.user = user;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }


}
