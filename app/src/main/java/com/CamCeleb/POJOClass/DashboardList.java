package com.CamCeleb.POJOClass;

import com.google.gson.annotations.SerializedName;

public class DashboardList {



    @SerializedName("list")
    private DashBorsData [] list;

    public DashboardList( DashBorsData[] list) {

        this.list = list;
    }



    public DashBorsData[] getData() {
        return list;
    }
}
