package com.CamCeleb.POJOClass;

public class CelebDasList {

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getIs_last() {
        return is_last;
    }

    public void setIs_last(String is_last) {
        this.is_last = is_last;
    }

    public String getList() {
        return list;
    }

    public void setList(String list) {
        this.list = list;
    }

    String message,is_last,list;
}
