package com.CamCeleb.POJOClass.RequestVideo;

import android.content.SharedPreferences;

import com.CamCeleb.SplashActivity;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class Paytm  {

    @SerializedName("MID")
    @Expose
    private String mID;
    @SerializedName("ORDER_ID")
    @Expose
    private String oRDERID;
    @SerializedName("CUST_ID")
    @Expose
    private String cUSTID;
    @SerializedName("INDUSTRY_TYPE_ID")
    @Expose
    private String iNDUSTRYTYPEID;
    @SerializedName("CHANNEL_ID")
    @Expose
    private String cHANNELID;
    @SerializedName("TXN_AMOUNT")
    @Expose
    private String tXNAMOUNT;
    @SerializedName("WEBSITE")
    @Expose
    private String wEBSITE;
    @SerializedName("CALLBACK_URL")
    @Expose
    private String cALLBACKURL;
    @SerializedName("EMAIL")
    @Expose
    private String eMAIL;
    @SerializedName("MOBILE_NO")
    @Expose
    private String mOBILENO;
    @SerializedName("CHECKSUMHASH")
    @Expose
    private String cHECKSUMHASH;

   /* public Paytm(String mid, Integer orderId, String custid, String channelid, String txnamount, String website, String callbackurl, String checksumhash, String retail) {

        this.mID = mid;
        this.oRDERID = String.valueOf(orderId);
        this.cUSTID = custid;
        this.cHANNELID = channelid;
        this.tXNAMOUNT = Integer.valueOf(txnamount);
        this.wEBSITE = website;
        this.cALLBACKURL = callbackurl;
        this.cHECKSUMHASH = checksumhash;
        this.iNDUSTRYTYPEID = retail;


    }*/


    public String getMID() {
        return mID;
    }

    public void setMID(String mID) {
        this.mID = mID;
    }

    public String getORDERID() {
        return oRDERID;
    }

    public void setORDERID(String oRDERID) {
        this.oRDERID = oRDERID;
    }

    public String getCUSTID() {
        return cUSTID;
    }

    public void setCUSTID(String cUSTID) {
        this.cUSTID = cUSTID;
    }

    public String getINDUSTRYTYPEID() {
        return iNDUSTRYTYPEID;
    }

    public void setINDUSTRYTYPEID(String iNDUSTRYTYPEID) {
        this.iNDUSTRYTYPEID = iNDUSTRYTYPEID;
    }

    public String getCHANNELID() {
        return cHANNELID;
    }

    public void setCHANNELID(String cHANNELID) {
        this.cHANNELID = cHANNELID;
    }

    public String getTXNAMOUNT() {
        return tXNAMOUNT;
    }

    public void setTXNAMOUNT(String tXNAMOUNT) {
        this.tXNAMOUNT = tXNAMOUNT;
    }

    public String getWEBSITE() {
        return wEBSITE;
    }

    public void setWEBSITE(String wEBSITE) {
        this.wEBSITE = wEBSITE;
    }

    public String getCALLBACKURL() {
        return cALLBACKURL;
    }

    public void setCALLBACKURL(String cALLBACKURL) {
        this.cALLBACKURL = cALLBACKURL;
    }

    public String getEMAIL() {
        return eMAIL;
    }

    public void setEMAIL(String eMAIL) {
        this.eMAIL = eMAIL;
    }

    public String getMOBILENO() {
        return mOBILENO;
    }

    public void setMOBILENO(String mOBILENO) {
        this.mOBILENO = mOBILENO;
    }

    public String getCHECKSUMHASH() {
        return cHECKSUMHASH;
    }

    public void setCHECKSUMHASH(String cHECKSUMHASH) {
        this.cHECKSUMHASH = cHECKSUMHASH;
    }


    public JSONObject getJSONData() {

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("MID", getMID());
            jsonObject.put("ORDER_ID", getORDERID());
            jsonObject.put("CUST_ID", getCUSTID());
            jsonObject.put("INDUSTRY_TYPE_ID", getINDUSTRYTYPEID());
            jsonObject.put("CHANNEL_ID", getCHANNELID());
            jsonObject.put("TXN_AMOUNT", getTXNAMOUNT());
            jsonObject.put("WEBSITE", getWEBSITE());
            jsonObject.put("CALLBACK_URL", getCALLBACKURL());
            jsonObject.put("EMAIL", getEMAIL());
            jsonObject.put("MOBILE_NO", getMOBILENO());
            jsonObject.put("CHECKSUMHASH", getCHECKSUMHASH());


        } catch (JSONException e) {
            e.printStackTrace();
        }



        return jsonObject;
    }
}
