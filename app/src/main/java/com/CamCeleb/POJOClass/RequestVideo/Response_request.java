package com.CamCeleb.POJOClass.RequestVideo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONObject;

public class Response_request {
    @SerializedName("paytm")
    @Expose
    private Paytm paytm;
    @SerializedName("order_id")
    @Expose
    private Integer orderId;
    @SerializedName("message")
    @Expose
    private String message;

    public Paytm getPaytm() {
        return paytm;
    }

    public void setPaytm(Paytm paytm) {
        this.paytm = paytm;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


}
