package com.CamCeleb.POJOClass;

public class ActorSee {
    int image;
    String name;
    String follers;

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFollers() {
        return follers;
    }

    public void setFollers(String follers) {
        this.follers = follers;
    }



    public ActorSee(int image, String name, String follers) {
        this.image = image;
        this.name = name;
        this.follers = follers;
    }
}
