package com.CamCeleb.POJOClass;

import com.CamCeleb.POJOClass.Dashboard.Response_dashboard_datum;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class DashBorsData {
   /* @SerializedName("id")
    String id;

    @SerializedName("name")
    String name;

    @SerializedName("data")
    List<User> users;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(JsonObject data) {

        try {

            JsonArray list = data.get("data").getAsJsonArray();
            this.id = data.get("id").toString();
            this.name = data.get("name").toString();
            for (int i = 0; i < list.size(); i++) {

                JsonObject obj = list.get(i).getAsJsonObject();

                User user = new User();
                user.setFullName(obj.get("full_name").toString());
                user.setProfilePic(obj.get("profile_pic").toString());
                user.setFollowersCount(obj.get("followers").toString());
                user.setFaviourateCount(obj.get("is_favourite").getAsInt());
                user.setId(obj.get("id").getAsInt());


                this.users.add(user);

                Log.d("USerdata", user.getFullName());


            }


        } catch (JsonIOException e) {
            e.printStackTrace();
        }

    }*/
   @SerializedName("id")
   @Expose
   private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("data")
    @Expose
    private List<Response_dashboard_datum> data = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Response_dashboard_datum> getData() {
        return data;
    }

    public void setData(List<Response_dashboard_datum> data) {
        this.data = data;
    }


}


