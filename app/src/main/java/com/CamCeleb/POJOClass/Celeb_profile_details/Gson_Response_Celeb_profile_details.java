package com.CamCeleb.POJOClass.Celeb_profile_details;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Gson_Response_Celeb_profile_details {

    @SerializedName("error")
    @Expose
    private List<Object> error = null;
    @SerializedName("data")
    @Expose
    private Response_Celeb_profile_details data;
    @SerializedName("success")
    @Expose
    private Integer success;

    public List<Object> getError() {
        return error;
    }

    public void setError(List<Object> error) {
        this.error = error;
    }

    public Response_Celeb_profile_details getData() {
        return data;
    }

    public void setData(Response_Celeb_profile_details data) {
        this.data = data;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }
}
