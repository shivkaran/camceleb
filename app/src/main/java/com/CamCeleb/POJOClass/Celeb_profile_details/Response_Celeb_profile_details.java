package com.CamCeleb.POJOClass.Celeb_profile_details;

import com.CamCeleb.POJOClass.Favourite.Video_list;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Response_Celeb_profile_details {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("full_name")
    @Expose
    private String fullName;
    @SerializedName("profile_pic")
    @Expose
    private String profilePic;
    @SerializedName("intro_video")
    @Expose
    private String introVideo;
    @SerializedName("video_thumb")
    @Expose
    private String videoThumb;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("booking_price")
    @Expose
    private String bookingPrice;
    @SerializedName("followers")
    @Expose
    private String followers;
    @SerializedName("is_favourite")
    @Expose
    private Boolean isFavourite;
    @SerializedName("is_online")
    @Expose
    private String isOnline;
    @SerializedName("about_me")
    @Expose
    private String aboutMe;
    @SerializedName("videos_count")
    @Expose
    private Integer videosCount;
    @SerializedName("categories")
    @Expose
    private String categories;
    @SerializedName("is_last")
    @Expose
    private String isLast;
    @SerializedName("videos")
    @Expose
    private List<Video_list> videos = null;
    @SerializedName("testimonials")
    @Expose
    private List<Celeb_Testimonial> testimonials = null;
    @SerializedName("message")
    @Expose
    private String message;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getIntroVideo() {
        return introVideo;
    }

    public void setIntroVideo(String introVideo) {
        this.introVideo = introVideo;
    }

    public String getVideoThumb() {
        return videoThumb;
    }

    public void setVideoThumb(String videoThumb) {
        this.videoThumb = videoThumb;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBookingPrice() {
        return bookingPrice;
    }

    public void setBookingPrice(String bookingPrice) {
        this.bookingPrice = bookingPrice;
    }

    public String getFollowers() {
        return followers;
    }

    public void setFollowers(String followers) {
        this.followers = followers;
    }

    public Boolean getIsFavourite() {
        return isFavourite;
    }

    public void setIsFavourite(Boolean isFavourite) {
        this.isFavourite = isFavourite;
    }

    public String getIsOnline() {
        return isOnline;
    }

    public void setIsOnline(String isOnline) {
        this.isOnline = isOnline;
    }

    public String getAboutMe() {
        return aboutMe;
    }

    public void setAboutMe(String aboutMe) {
        this.aboutMe = aboutMe;
    }

    public Integer getVideosCount() {
        return videosCount;
    }

    public void setVideosCount(Integer videosCount) {
        this.videosCount = videosCount;
    }

    public String getCategories() {
        return categories;
    }

    public void setCategories(String categories) {
        this.categories = categories;
    }

    public String getIsLast() {
        return isLast;
    }

    public void setIsLast(String isLast) {
        this.isLast = isLast;
    }

    public List<Video_list> getVideos() {
        return videos;
    }

    public void setVideos(List<Video_list> videos) {
        this.videos = videos;
    }

    public List<Celeb_Testimonial> getTestimonials() {
        return testimonials;
    }

    public void setTestimonials(List<Celeb_Testimonial> testimonials) {
        this.testimonials = testimonials;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
