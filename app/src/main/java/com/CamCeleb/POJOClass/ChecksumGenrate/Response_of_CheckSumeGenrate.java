package com.CamCeleb.POJOClass.ChecksumGenrate;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Response_of_CheckSumeGenrate {

    @SerializedName("checksum")
    @Expose
    private Checksum checksum;
    @SerializedName("message")
    @Expose
    private String message;

    public Checksum getChecksum() {
        return checksum;
    }

    public void setChecksum(Checksum checksum) {
        this.checksum = checksum;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
