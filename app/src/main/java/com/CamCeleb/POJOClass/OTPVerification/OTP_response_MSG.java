package com.CamCeleb.POJOClass.OTPVerification;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OTP_response_MSG {
    @SerializedName("message")
    @Expose
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
