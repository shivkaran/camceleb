package com.CamCeleb.POJOClass.Celeb_Home_Request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Response_celeb_Reuest {
    @SerializedName("pending_count")
    @Expose
    private Integer pendingCount;
    @SerializedName("accepted_count")
    @Expose
    private Integer acceptedCount;
    @SerializedName("rejected_count")
    @Expose
    private Integer rejectedCount;
    @SerializedName("cancelled_count")
    @Expose
    private Integer cancelledCount;
    @SerializedName("completed_count")
    @Expose
    private Integer completedCount;
    @SerializedName("user_type")
    @Expose
    private String userType;
    @SerializedName("is_last")
    @Expose
    private String isLast;
    @SerializedName("list")
    @Expose
    private List<Celeb_Reuest_list> list = null;
    @SerializedName("message")
    @Expose
    private String message;

    public Integer getPendingCount() {
        return pendingCount;
    }

    public void setPendingCount(Integer pendingCount) {
        this.pendingCount = pendingCount;
    }

    public Integer getAcceptedCount() {
        return acceptedCount;
    }

    public void setAcceptedCount(Integer acceptedCount) {
        this.acceptedCount = acceptedCount;
    }

    public Integer getRejectedCount() {
        return rejectedCount;
    }

    public void setRejectedCount(Integer rejectedCount) {
        this.rejectedCount = rejectedCount;
    }

    public Integer getCancelledCount() {
        return cancelledCount;
    }

    public void setCancelledCount(Integer cancelledCount) {
        this.cancelledCount = cancelledCount;
    }

    public Integer getCompletedCount() {
        return completedCount;
    }

    public void setCompletedCount(Integer completedCount) {
        this.completedCount = completedCount;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getIsLast() {
        return isLast;
    }

    public void setIsLast(String isLast) {
        this.isLast = isLast;
    }

    public List<Celeb_Reuest_list> getList() {
        return list;
    }

    public void setList(List<Celeb_Reuest_list> list) {
        this.list = list;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
