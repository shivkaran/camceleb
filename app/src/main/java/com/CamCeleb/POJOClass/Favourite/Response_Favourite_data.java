package com.CamCeleb.POJOClass.Favourite;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Response_Favourite_data {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("full_name")
    @Expose
    private String fullName;
    @SerializedName("profile_pic")
    @Expose
    private String profilePic;
    @SerializedName("videos_count")
    @Expose
    private Integer videosCount;
    @SerializedName("favourite_list")
    @Expose
    private List<Favourite_list> favouriteList = null;
    @SerializedName("is_last")
    @Expose
    private String isLast;
    @SerializedName("videos")
    @Expose
    private List<Video_list> videos = null;
    @SerializedName("message")
    @Expose
    private String message;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public Integer getVideosCount() {
        return videosCount;
    }

    public void setVideosCount(Integer videosCount) {
        this.videosCount = videosCount;
    }

    public List<Favourite_list> getFavouriteList() {
        return favouriteList;
    }

    public void setFavouriteList(List<Favourite_list> favouriteList) {
        this.favouriteList = favouriteList;
    }

    public String getIsLast() {
        return isLast;
    }

    public void setIsLast(String isLast) {
        this.isLast = isLast;
    }

    public List<Video_list> getVideos() {
        return videos;
    }

    public void setVideos(List<Video_list> videos) {
        this.videos = videos;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
