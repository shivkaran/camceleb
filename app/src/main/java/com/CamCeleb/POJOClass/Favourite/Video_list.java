package com.CamCeleb.POJOClass.Favourite;

import com.CamCeleb.POJOClass.Celebdetails.User_Celeb_details;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Video_list {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("date")
    @Expose
    private Integer date;
    @SerializedName("video")
    @Expose
    private String video;
    @SerializedName("video_thumb")
    @Expose
    private String videoThumb;
    @SerializedName("rating")
    @Expose
    private String rating;
    @SerializedName("feedback")
    @Expose
    private String feedback;
    @SerializedName("like_count")
    @Expose
    private Integer likeCount;
    @SerializedName("comment_count")
    @Expose
    private Integer commentCount;
    @SerializedName("is_liked")
    @Expose
    private Boolean isLiked;
    @SerializedName("user")
    @Expose
    private User_Celeb_details user;
    @SerializedName("is_my_video")
    @Expose
    private Integer is_my_video;

    public Boolean getLiked() {
        return isLiked;
    }

    public void setLiked(Boolean liked) {
        isLiked = liked;
    }

    public Integer getIs_my_video() {
        return is_my_video;
    }

    public void setIs_my_video(Integer is_my_video) {
        this.is_my_video = is_my_video;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDate() {
        return date;
    }

    public void setDate(Integer date) {
        this.date = date;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getVideoThumb() {
        return videoThumb;
    }

    public void setVideoThumb(String videoThumb) {
        this.videoThumb = videoThumb;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public Integer getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(Integer likeCount) {
        this.likeCount = likeCount;
    }

    public Integer getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(Integer commentCount) {
        this.commentCount = commentCount;
    }

    public Boolean getIsLiked() {
        return isLiked;
    }

    public void setIsLiked(Boolean isLiked) {
        this.isLiked = isLiked;
    }

    public User_Celeb_details getUser() {
        return user;
    }

    public void setUser(User_Celeb_details user) {
        this.user = user;
    }

}
