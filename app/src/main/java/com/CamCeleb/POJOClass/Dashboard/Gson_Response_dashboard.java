package com.CamCeleb.POJOClass.Dashboard;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Gson_Response_dashboard {

    @SerializedName("error")
    @Expose
    private List<Object> error = null;
    @SerializedName("data")
    @Expose
    private Response_dashboard_list data;
    @SerializedName("success")
    @Expose
    private Integer success;

    public List<Object> getError() {
        return error;
    }

    public void setError(List<Object> error) {
        this.error = error;
    }

    public Response_dashboard_list getData() {
        return data;
    }

    public void setData(Response_dashboard_list data) {
        this.data = data;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

}
