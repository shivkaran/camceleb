package com.CamCeleb.POJOClass.Dashboard;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Response_dashboard_list {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("data")
    @Expose
    private List<Response_dashboard_datum> data = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Response_dashboard_datum> getData() {
        return data;
    }

    public void setData(List<Response_dashboard_datum> data) {
        this.data = data;
    }
}
