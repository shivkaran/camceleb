package com.CamCeleb.POJOClass;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.CamCeleb.SplashActivity;

import static android.content.Context.MODE_PRIVATE;

public class AuthToken {

    // Context ctx;
    String token;
    String type;

    public String getToken() {

        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAuthToken() {

        Log.d("AuthToken", SplashActivity.preferences.getString("AuthToken", "NOT FOUND"));
        return SplashActivity.preferences.getString("AuthToken", "");
    }


    public Boolean saveAuthToken() {

        String n = getToken().toString();
        SharedPreferences.Editor ed = SplashActivity.preferences.edit();
        ed.putString("AuthToken", getType() + " " + getToken());
        return ed.commit();

    }


}