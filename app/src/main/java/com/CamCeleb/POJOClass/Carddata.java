package com.CamCeleb.POJOClass;

import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Carddata {
    @SerializedName("user_type")
    String user_type;

    @SerializedName("is_last")
    String is_last;

    public List<Requested_by> getRequested() {
        return requested;
    }

    public void setRequested(List<Requested_by> requested_bies) {
        this.requested = requested;
    }

    @SerializedName("list")
    List<Requested_by> requested;

    public String getUser_type() {
        return user_type;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    public String getIs_last() {
        return is_last;
    }

    public void setIs_last(String is_last) {
        this.is_last = is_last;
    }


}




