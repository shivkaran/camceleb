package com.CamCeleb.POJOClass;

public class Categoryfield {
    String id;
    String name;

    public Categoryfield(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public Categoryfield() {

    }

    public String getId() {
        return id;
    }

    public void setId(String  id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
