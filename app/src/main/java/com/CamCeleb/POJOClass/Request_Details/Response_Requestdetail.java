package com.CamCeleb.POJOClass.Request_Details;

import com.CamCeleb.POJOClass.Celeb_Home_Request.RequestedBy;
import com.CamCeleb.POJOClass.RequestList.Celeb;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Response_Requestdetail {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("is_uploaded")
    @Expose
    private String isUploaded;
    @SerializedName("transaction_id")
    @Expose
    private String transactionId;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("video")
    @Expose
    private String video;
    @SerializedName("video_thumb")
    @Expose
    private String videoThumb;
    @SerializedName("like_count")
    @Expose
    private String likeCount;
    @SerializedName("comment_count")
    @Expose
    private String commentCount;
    @SerializedName("instructions")
    @Expose
    private String instructions;
    @SerializedName("friend_name")
    @Expose
    private String friendName;
    @SerializedName("friend_image")
    @Expose
    private String friendImage;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("phone_number")
    @Expose
    private String phoneNumber;
    @SerializedName("is_private")
    @Expose
    private String isPrivate;
    @SerializedName("celeb")
    @Expose
    private Celeb celeb;
    @SerializedName("requested_by")
    @Expose
    private RequestedBy requestedBy;
    @SerializedName("message")
    @Expose
    private String message;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIsUploaded() {
        return isUploaded;
    }

    public void setIsUploaded(String isUploaded) {
        this.isUploaded = isUploaded;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getVideoThumb() {
        return videoThumb;
    }

    public void setVideoThumb(String videoThumb) {
        this.videoThumb = videoThumb;
    }

    public String getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(String likeCount) {
        this.likeCount = likeCount;
    }

    public String getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(String commentCount) {
        this.commentCount = commentCount;
    }

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    public String getFriendName() {
        return friendName;
    }

    public void setFriendName(String friendName) {
        this.friendName = friendName;
    }

    public String getFriendImage() {
        return friendImage;
    }

    public void setFriendImage(String friendImage) {
        this.friendImage = friendImage;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getIsPrivate() {
        return isPrivate;
    }

    public void setIsPrivate(String isPrivate) {
        this.isPrivate = isPrivate;
    }

    public Celeb getCeleb() {
        return celeb;
    }

    public void setCeleb(Celeb celeb) {
        this.celeb = celeb;
    }

    public RequestedBy getRequestedBy() {
        return requestedBy;
    }

    public void setRequestedBy(RequestedBy requestedBy) {
        this.requestedBy = requestedBy;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
