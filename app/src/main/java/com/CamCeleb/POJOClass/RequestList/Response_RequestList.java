package com.CamCeleb.POJOClass.RequestList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Response_RequestList {

    @SerializedName("user_type")
    @Expose
    private String userType;
    @SerializedName("is_last")
    @Expose
    private String isLast;
    @SerializedName("list")
    @Expose
    private List<RequestList> list = null;
    @SerializedName("message")
    @Expose
    private String message;

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getIsLast() {
        return isLast;
    }

    public void setIsLast(String isLast) {
        this.isLast = isLast;
    }

    public List<RequestList> getList() {
        return list;
    }

    public void setList(List<RequestList> list) {
        this.list = list;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


}
