package com.CamCeleb.POJOClass.RequestList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RequestList {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("date")
    @Expose
    private Integer date;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("is_uploaded")
    @Expose
    private String isUploaded;
    @SerializedName("transaction_id")
    @Expose
    private String transactionId;
    @SerializedName("price")
    @Expose
    private Integer price;
    @SerializedName("video")
    @Expose
    private String video;
    @SerializedName("video_thumb")
    @Expose
    private String videoThumb;
    @SerializedName("like_count")
    @Expose
    private Integer likeCount;
    @SerializedName("comment_count")
    @Expose
    private Integer commentCount;
    @SerializedName("instructions")
    @Expose
    private String instructions;
    @SerializedName("friend_name")
    @Expose
    private String friendName;
    @SerializedName("friend_image")
    @Expose
    private String friendImage;
    @SerializedName("celeb")
    @Expose
    private Celeb celeb;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDate() {
        return date;
    }

    public void setDate(Integer date) {
        this.date = date;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getIsUploaded() {
        return isUploaded;
    }

    public void setIsUploaded(String isUploaded) {
        this.isUploaded = isUploaded;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getVideoThumb() {
        return videoThumb;
    }

    public void setVideoThumb(String videoThumb) {
        this.videoThumb = videoThumb;
    }

    public Integer getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(Integer likeCount) {
        this.likeCount = likeCount;
    }

    public Integer getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(Integer commentCount) {
        this.commentCount = commentCount;
    }

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    public String getFriendName() {
        return friendName;
    }

    public void setFriendName(String friendName) {
        this.friendName = friendName;
    }

    public String getFriendImage() {
        return friendImage;
    }

    public void setFriendImage(String friendImage) {
        this.friendImage = friendImage;
    }

    public Celeb getCeleb() {
        return celeb;
    }

    public void setCeleb(Celeb celeb) {
        this.celeb = celeb;
    }
}
