package com.CamCeleb.POJOClass.Celeb_dashoad;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Response_celeb_data {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("full_name")
    @Expose
    private String fullName;
    @SerializedName("profile_pic")
    @Expose
    private String profilePic;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("booking_price")
    @Expose
    private String bookingPrice;
    @SerializedName("followers_count")
    @Expose
    private String followersCount;
    @SerializedName("is_online")
    @Expose
    private String isOnline;
    @SerializedName("favourite_count")
    @Expose
    private String favouriteCount;
    @SerializedName("videos_count")
    @Expose
    private String videosCount;
    @SerializedName("earnings")
    @Expose
    private String earnings;
    @SerializedName("rating")
    @Expose
    private String rating;
    @SerializedName("videos")
    @Expose
    private List<Celeb_video> videos = null;
    @SerializedName("message")
    @Expose
    private String message;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBookingPrice() {
        return bookingPrice;
    }

    public void setBookingPrice(String bookingPrice) {
        this.bookingPrice = bookingPrice;
    }

    public String getFollowersCount() {
        return followersCount;
    }

    public void setFollowersCount(String followersCount) {
        this.followersCount = followersCount;
    }

    public String getIsOnline() {
        return isOnline;
    }

    public void setIsOnline(String isOnline) {
        this.isOnline = isOnline;
    }

    public String getFavouriteCount() {
        return favouriteCount;
    }

    public void setFavouriteCount(String favouriteCount) {
        this.favouriteCount = favouriteCount;
    }

    public String getVideosCount() {
        return videosCount;
    }

    public void setVideosCount(String videosCount) {
        this.videosCount = videosCount;
    }

    public String getEarnings() {
        return earnings;
    }

    public void setEarnings(String earnings) {
        this.earnings = earnings;
    }

    public String  getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public List<Celeb_video> getVideos() {
        return videos;
    }

    public void setVideos(List<Celeb_video> videos) {
        this.videos = videos;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
