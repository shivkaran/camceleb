package com.CamCeleb.POJOClass.Comment;

import com.CamCeleb.POJOClass.Celebdetails.User_Celeb_details;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Response_comment_list {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("is_flagged")
    @Expose
    private String isFlagged;
    @SerializedName("user")
    @Expose
    private User_Celeb_details user;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getIsFlagged() {
        return isFlagged;
    }

    public void setIsFlagged(String isFlagged) {
        this.isFlagged = isFlagged;
    }

    public User_Celeb_details getUser() {
        return user;
    }

    public void setUser(User_Celeb_details user) {
        this.user = user;
    }
}
