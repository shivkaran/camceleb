package com.CamCeleb.POJOClass.Comment.Comment_send;

import com.CamCeleb.POJOClass.Celebdetails.User_Celeb_details;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Comment_send_response {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("is_flagged")
    @Expose
    private Integer isFlagged;
    @SerializedName("user")
    @Expose
    private User_Celeb_details user;
    @SerializedName("message")
    @Expose
    private String message;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getIsFlagged() {
        return isFlagged;
    }

    public void setIsFlagged(Integer isFlagged) {
        this.isFlagged = isFlagged;
    }

    public User_Celeb_details getUser() {
        return user;
    }

    public void setUser(User_Celeb_details user) {
        this.user = user;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


}
