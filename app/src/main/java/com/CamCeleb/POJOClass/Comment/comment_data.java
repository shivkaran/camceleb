package com.CamCeleb.POJOClass.Comment;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class comment_data {

    @SerializedName("is_last")
    @Expose
    private String isLast;
    @SerializedName("list")
    @Expose
    private List<Response_comment_list> list = null;
    @SerializedName("message")
    @Expose
    private String message;

    public String getIsLast() {
        return isLast;
    }

    public void setIsLast(String isLast) {
        this.isLast = isLast;
    }

    public List<Response_comment_list> getList() {
        return list;
    }

    public void setList(List<Response_comment_list> list) {
        this.list = list;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


}
