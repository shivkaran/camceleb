package com.CamCeleb.POJOClass;

import android.content.SharedPreferences;

import com.CamCeleb.SplashActivity;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

public class User {

    @SerializedName("user_type")
    @Expose
    private Integer userType;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("full_name")
    @Expose
    private String fullName;
    @SerializedName("profile_pic")
    @Expose
    private String profilePic;
    @SerializedName("is_profile_completed")
    @Expose
    private Integer isProfileCompleted;
    @SerializedName("intro_video")
    @Expose
    private String introVideo;
    @SerializedName("video_thumb")
    @Expose
    private String videoThumb;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("phone_number")
    @Expose
    private String phoneNumber;
    @SerializedName("booking_price")
    @Expose
    private String bookingPrice;
    @SerializedName("social_platform_type")
    @Expose
    private String socialPlatformType;
    @SerializedName("social_page_name")
    @Expose
    private String socialPageName;
    @SerializedName("followers_count")
    @Expose
    private String followersCount;
    @SerializedName("id_verification")
    @Expose
    private String idVerification;
    @SerializedName("categories")
    @Expose
    private String categories;
    @SerializedName("about_me")
    @Expose
    private String aboutMe;
    @SerializedName("faviourate_count")
    @Expose
    private Integer faviourateCount;
    @SerializedName("earning")
    @Expose
    private Integer earning;
    @SerializedName("is_online")
    @Expose
    private String isOnline;
    @SerializedName("videos_count")
    @Expose
    private Integer videosCount;
    @SerializedName("request_count")
    @Expose
    private Integer requestCount;
    @SerializedName("owner_type")
    @Expose
    private String ownerType;
    @SerializedName("branch_name")
    @Expose
    private String branchName;
    @SerializedName("beneficiary_name")
    @Expose
    private String beneficiary_name;
    @SerializedName("swift_code")
    @Expose
    private String swiftCode;
    @SerializedName("account_number")
    @Expose
    private String accountNumber;
    @SerializedName("bank_name")
    @Expose
    private String bankName;
    @SerializedName("ifsc_code")
    @Expose
    private String ifscCode;
    @SerializedName("rating")
    @Expose
    private String rating;
    @SerializedName("is_email_required")
    @Expose
    private String is_email_required;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("admin_verification")
    @Expose
    private String admin_verification;

    public String getAdmin_verification() {
        return admin_verification;
    }

    public void setAdmin_verification(String admin_verification) {
        this.admin_verification = admin_verification;
    }

    public String getIs_email_required() {
        return is_email_required;
    }

    public void setIs_email_required(String is_email_required) {
        this.is_email_required = is_email_required;
    }

    public String getBeneficiary_name() {
        return beneficiary_name;
    }

    public void setBeneficiary_name(String beneficiary_name) {
        this.beneficiary_name = beneficiary_name;
    }

    public String getOwnerType() {
        return ownerType;
    }

    public void setOwnerType(String ownerType) {
        this.ownerType = ownerType;
    }

    public String getSwiftCode() {
        return swiftCode;
    }

    public void setSwiftCode(String swiftCode) {
        this.swiftCode = swiftCode;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public Integer getIsProfileCompleted() {
        return isProfileCompleted;
    }

    public void setIsProfileCompleted(Integer isProfileCompleted) {
        this.isProfileCompleted = isProfileCompleted;
    }

    public String getIntroVideo() {
        return introVideo;
    }

    public void setIntroVideo(String introVideo) {
        this.introVideo = introVideo;
    }

    public String getVideoThumb() {
        return videoThumb;
    }

    public void setVideoThumb(String videoThumb) {
        this.videoThumb = videoThumb;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getBookingPrice() {
        return bookingPrice;
    }

    public void setBookingPrice(String bookingPrice) {
        this.bookingPrice = bookingPrice;
    }

    public String getSocialPlatformType() {
        return socialPlatformType;
    }

    public void setSocialPlatformType(String socialPlatformType) {
        this.socialPlatformType = socialPlatformType;
    }

    public String getSocialPageName() {
        return socialPageName;
    }

    public void setSocialPageName(String socialPageName) {
        this.socialPageName = socialPageName;
    }

    public String getFollowersCount() {
        return followersCount;
    }

    public void setFollowersCount(String followersCount) {
        this.followersCount = followersCount;
    }

    public String getIdVerification() {
        return idVerification;
    }

    public void setIdVerification(String idVerification) {
        this.idVerification = idVerification;
    }

    public String getCategories() {
        return categories;
    }

    public void setCategories(String categories) {
        this.categories = categories;
    }

    public String getAboutMe() {
        return aboutMe;
    }

    public void setAboutMe(String aboutMe) {
        this.aboutMe = aboutMe;
    }

    public Integer getFaviourateCount() {
        return faviourateCount;
    }

    public void setFaviourateCount(Integer faviourateCount) {
        this.faviourateCount = faviourateCount;
    }

    public Integer getEarning() {
        return earning;
    }

    public void setEarning(Integer earning) {
        this.earning = earning;
    }

    public String getIsOnline() {
        return isOnline;
    }

    public void setIsOnline(String isOnline) {
        this.isOnline = isOnline;
    }

    public Integer getVideosCount() {
        return videosCount;
    }

    public void setVideosCount(Integer videosCount) {
        this.videosCount = videosCount;
    }

    public Integer getRequestCount() {
        return requestCount;
    }

    public void setRequestCount(Integer requestCount) {
        this.requestCount = requestCount;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getIfscCode() {
        return ifscCode;
    }

    public void setIfscCode(String ifscCode) {
        this.ifscCode = ifscCode;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }



    public Boolean SaveSession() {


        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("email", getEmail());
            jsonObject.put("full_name", getFullName());
            jsonObject.put("id", getId());
            jsonObject.put("is_profile_completed", getIsProfileCompleted());
            jsonObject.put("profile_pic", getProfilePic());
            jsonObject.put("video_thumb", getVideoThumb());
            jsonObject.put("phone_number", getPhoneNumber());
            jsonObject.put("message", getMessage());
            jsonObject.put("booking_price", getBookingPrice());
            jsonObject.put("intro_video", getIntroVideo());
            jsonObject.put("branch_name", getBranchName());
            jsonObject.put("user_type", getUserType());
            jsonObject.put("social_page_name", getSocialPageName());
            jsonObject.put("categories", getCategories());
            jsonObject.put("followers_count", getFollowersCount());
            jsonObject.put("about_me", getAboutMe());
            jsonObject.put("account_number", getAccountNumber());
            jsonObject.put("id_verification", getIdVerification());
            jsonObject.put("ifsc_code", getIfscCode());
            jsonObject.put("is_online", getIsOnline());
            jsonObject.put("earning", getEarning());
            jsonObject.put("faviourate_count", getFaviourateCount());
            jsonObject.put("bank_name", getBankName());
            jsonObject.put("beneficiary_name", getBeneficiary_name());
            jsonObject.put("social_platform_type", getSocialPlatformType());
            jsonObject.put("rating", getRating());
            jsonObject.put("owner_type", getOwnerType());
            jsonObject.put("swift_code", getSwiftCode());
            jsonObject.put("admin_verification", getAdmin_verification());

        } catch (JSONException e) {
            e.printStackTrace();
        }


        SharedPreferences.Editor ed = SplashActivity.preferences.edit();
        ed.putString("SessionData", jsonObject.toString()); //
        return ed.commit();


    }

    JSONObject jsonObject = new JSONObject();

    public JSONObject getJsonObject() {
        try {
            jsonObject.get("email");
            jsonObject.get("password");
            jsonObject.get("id_verification");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
}
