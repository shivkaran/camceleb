package com.CamCeleb.POJOClass.SearchData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RecentSearch {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("full_name")
    @Expose
    private String fullName;
    @SerializedName("profile_pic")
    @Expose
    private String profilePic;
    @SerializedName("followers")
    @Expose
    private String followers;
    @SerializedName("is_favourite")
    @Expose
    private Boolean isFavourite;
    @SerializedName("social_platform_type")
    @Expose
    private String social_platform_type;

    public String getSocial_platform_type() {
        return social_platform_type;
    }

    public void setSocial_platform_type(String social_platform_type) {
        this.social_platform_type = social_platform_type;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getFollowers() {
        return followers;
    }

    public void setFollowers(String followers) {
        this.followers = followers;
    }

    public Boolean getIsFavourite() {
        return isFavourite;
    }

    public void setIsFavourite(Boolean isFavourite) {
        this.isFavourite = isFavourite;
    }

}
