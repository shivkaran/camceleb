package com.CamCeleb.POJOClass.SearchData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Search_response_data {

    @SerializedName("celeb_of_day")
    @Expose
    private List<CelebOfDay> celebOfDay = null;
    @SerializedName("recent_search")
    @Expose
    private List<RecentSearch> recentSearch = null;
    @SerializedName("trendings")
    @Expose
    private List<Trending> trendings = null;
    @SerializedName("message")
    @Expose
    private String message;

    public List<CelebOfDay> getCelebOfDay() {
        return celebOfDay;
    }

    public void setCelebOfDay(List<CelebOfDay> celebOfDay) {
        this.celebOfDay = celebOfDay;
    }

    public List<RecentSearch> getRecentSearch() {
        return recentSearch;
    }

    public void setRecentSearch(List<RecentSearch> recentSearch) {
        this.recentSearch = recentSearch;
    }

    public List<Trending> getTrendings() {
        return trendings;
    }

    public void setTrendings(List<Trending> trendings) {
        this.trendings = trendings;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
