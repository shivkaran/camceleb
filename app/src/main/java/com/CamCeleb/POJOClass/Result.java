package com.CamCeleb.POJOClass;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;
import com.google.gson.annotations.Expose;

import java.util.List;


public class Result {

    @SerializedName("error")
    @Expose
    private List<String> error = null;

    @SerializedName("success")
    @Expose
    private int success;

    @SerializedName("data")
    @Expose
    private JsonObject data;

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public void setData(JsonObject data) {
        this.data = data;
    }

    public List<String> getError() {
        return error;
    }

    public void setError(List<String> error) {
        this.error = error;
    }

    public int getCode() {
        return success;
    }

    public JsonObject getData() {
        return data;
    }
}
