package com.CamCeleb.POJOClass;

import com.google.gson.annotations.SerializedName;

public class Requested_by {


    @SerializedName("id")
    String id;
    @SerializedName("date")
    String date;
    @SerializedName("status")
    String status;
    @SerializedName("instructions")
    String instructions;
    @SerializedName("friend_name")
    String friend_name;
    @SerializedName("friend_image")
    String friend_image;
    @SerializedName("requested_by")
    User requested_by;

    public User getRequested_by() {
        return requested_by;
    }

    public void setRequested_by(User requested_by) {
        this.requested_by = requested_by;
    }





    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    public String getFriend_name() {
        return friend_name;
    }

    public void setFriend_name(String friend_name) {
        this.friend_name = friend_name;
    }

    public String getFriend_image() {
        return friend_image;
    }

    public void setFriend_image(String friend_image) {
        this.friend_image = friend_image;
    }


}
