package com.CamCeleb.POJOClass;

import android.content.SharedPreferences;
import android.util.Log;

import com.CamCeleb.SplashActivity;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MasterModel {


    List<Categoryfield> categoryfields;


    String privacy;
    String terms;
    String faqs;
    String aboutus;


    public  List<Categoryfield> getCategoryfields() {
        return categoryfields;
    }

    public void setCategoryfields(JSONArray categories) {
        categoryfields = new ArrayList<>();

        for (int i = 0; i < categories.length(); i++) {
            try {
                JSONObject jsonObject = categories.getJSONObject(i);
                String _Name = jsonObject.get("name").toString();
                String _Id = jsonObject.get("id").toString();

                Categoryfield categoryfield=new Categoryfield();
                categoryfield.setName(_Name);
                categoryfield.setId(_Id);

                categoryfields.add(categoryfield);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }


    public String getPrivacy() {
        return privacy;
    }

    public void setPrivacy(String privacy) {
        this.privacy = privacy;
    }

    public String getTerms() {
        return terms;
    }

    public void setTerms(String terms) {
        this.terms = terms;
    }

    public String getFaqs() {
        return faqs;
    }

    public void setFaqs(String faqs) {
        this.faqs = faqs;
    }

    public String getAboutus() {
        return aboutus;
    }

    public void setAboutus(String aboutus) {
        this.aboutus = aboutus;
    }


    public Boolean SaveSession() {


        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("faqs", getFaqs());
            jsonObject.put("terms", getTerms());
            jsonObject.put("privacy", getPrivacy());
            jsonObject.put("_about_us", getAboutus());

        } catch (JSONException e) {
            e.printStackTrace();
        }


        SharedPreferences.Editor ed = SplashActivity.preferences.edit();
        ed.putString("MasterData", jsonObject.toString()); //
        return ed.commit();


    }

}
