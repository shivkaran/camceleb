package com.CamCeleb;


import android.content.Context;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.CamCeleb.POJOClass.Carddata;
import com.CamCeleb.POJOClass.Requested_by;
import com.bumptech.glide.Glide;
import com.mindorks.placeholderview.SwipePlaceHolderView;
import com.mindorks.placeholderview.annotations.Layout;
import com.mindorks.placeholderview.annotations.Resolve;
import com.mindorks.placeholderview.annotations.View;
import com.mindorks.placeholderview.annotations.swipe.SwipeCancelState;
import com.mindorks.placeholderview.annotations.swipe.SwipeIn;
import com.mindorks.placeholderview.annotations.swipe.SwipeInState;
import com.mindorks.placeholderview.annotations.swipe.SwipeOut;
import com.mindorks.placeholderview.annotations.swipe.SwipeOutState;

import okhttp3.RequestBody;

@Layout(R.layout.tinder_card_view)


public class TinderCard {

    @View(R.id.profileImageView)
    private ImageView profileImageView;

    @View(R.id.nameAgeTxt)
    private TextView nameAgeTxt;

    @View(R.id.nameagedese)
    private TextView namedese;

    @View(R.id.locationNameTxt)
    private TextView locationNameTxt;


    private Requested_by mProfile;
    private Context mContext;
    private SwipePlaceHolderView mSwipeView;


    public TinderCard(Context mContext, Requested_by profile, SwipePlaceHolderView mSwipeView) {
        mContext = mContext;
        mProfile = profile;
        mSwipeView = mSwipeView;
    }

    @Resolve
    private void onResolved() {

        namedese.setText((CharSequence) mProfile.getInstructions());
        locationNameTxt.setText(mProfile.getRequested_by().getFullName());


        if (mProfile.getFriend_image().isEmpty()) {
            nameAgeTxt.setText(mProfile.getRequested_by().getFullName());
            if (mProfile.getRequested_by().getProfilePic().isEmpty()) {
                profileImageView.setImageResource(R.mipmap.profile_pic);
            } else {
                Glide.with(mContext).load(mProfile.getRequested_by().getProfilePic()).into(profileImageView);
            }
        } else {
            Glide.with(mContext).load(mProfile.getFriend_image()).into(profileImageView);

            nameAgeTxt.setText(mProfile.getFriend_name());
        }
    }

    @SwipeOut
    private void onSwipedOut() {
        Log.d("EVENT", "onSwipedOut");
        mSwipeView.addView(this);
    }

    @SwipeCancelState
    private void onSwipeCancelState() {
        Log.d("EVENT", "onSwipeCancelState");
    }

    @SwipeIn
    private void onSwipeIn() {
        Log.d("EVENT", "onSwipedIn");
    }

    @SwipeInState
    private void onSwipeInState() {
        Log.d("EVENT", "onSwipeInState");
    }

    @SwipeOutState
    private void onSwipeOutState() {
        Log.d("EVENT", "onSwipeOutState");
    }


}
