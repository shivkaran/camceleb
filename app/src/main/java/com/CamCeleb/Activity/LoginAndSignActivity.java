package com.CamCeleb.Activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.CamCeleb.APIs.APIManager;
import com.CamCeleb.CelebSide.Fragment.CelebLoginFragment;
import com.CamCeleb.Fragments.LoginFragment;
import com.CamCeleb.Helper.APIService;
import com.CamCeleb.Insta.Urls;
import com.CamCeleb.POJOClass.AuthToken;
import com.CamCeleb.POJOClass.MasterModel;
import com.CamCeleb.POJOClass.Result;
import com.CamCeleb.POJOClass.ResultHandler;
import com.CamCeleb.R;
import com.CamCeleb.SplashActivity;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginAndSignActivity extends AppCompatActivity {

    TabLayout tabLayout;
    ViewPager viewPager;

    AuthToken auth_token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_and_sign);
        getSupportActionBar().hide();
        auth_token = new AuthToken();

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                String deviceToken = instanceIdResult.getToken();

                Log.d("LogGetdata", deviceToken);
                InsertDevice(deviceToken);  //why parse retoken here?
            }
        });

        tabLayout = findViewById(R.id.tablayout);
        viewPager = findViewById(R.id.viewpagers);


        new Handler().post(new Runnable() {
            @Override
            public void run() {

                tabLayout.setupWithViewPager(viewPager);


                viewPager.setAdapter(new FourthAdapter(getSupportFragmentManager()));
            }
        });
    }
    private void InsertDevice(String deviceToken) {


        if (auth_token.getAuthToken() == "") {

            String tokenids = deviceToken;

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Urls.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            //Defining retrofit api service
            APIService service = retrofit.create(APIService.class);

            Call<com.CamCeleb.POJOClass.Result> call = service.createUser(
                    tokenids,
                    "A"
            );
            Log.d("MainData", String.valueOf(tokenids));
            call.enqueue(new Callback<Result>() {

                @Override
                public void onResponse(Call<com.CamCeleb.POJOClass.Result> call, Response<Result> response) {
                    // progressDialog.dismiss();
                    Log.d("about_us", response.body().getData().toString());

                    int code = response.body().getCode();
                    String e = response.body().getError().toString();
                    JsonObject data = response.body().getData();

                    if (code == 1) {

                        JSONObject hashMap = null;
                        try {
                            hashMap = new JSONObject(data.get("token").toString());


                            String _token = "";
                            String _type = "";

                            try {
                                _token = hashMap.get("token").toString();
                                _type = hashMap.get("type").toString();

                            } catch (JSONException ex) {
                                ex.printStackTrace();
                            }

                            AuthToken t = new AuthToken();// Save Token From POJO CLASS
                            t.setToken(_token);
                            t.setType(_type);
                            t.saveAuthToken();

                            LoadMasterData(new ResultHandler<MasterModel>() {
                                @Override
                                public void onSuccess(MasterModel data) {
                                    Log.d("about_us", data.getAboutus());
                                    Log.d("terms", data.getTerms());

                                }

                                @Override
                                public void onFailure(String e) {

                                    // Toast.makeText(LoginAndSignActivity.this, e, Toast.LENGTH_LONG).show();


                                }
                            });


                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }
                        // LoadMasterData();
                    } else {
                        // Toast.makeText(LoginAndSignActivity.this, "Data Not Found", Toast.LENGTH_LONG).show();

                    }
                }

                @Override
                public void onFailure(Call<com.CamCeleb.POJOClass.Result> call, Throwable t) {
                    //progressDialog.dismiss();
                    // Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();

                }
            });
        } else {

            LoadMasterData(new ResultHandler<MasterModel>() {
                @Override
                public void onSuccess(MasterModel data) {
                    Log.d("about_us_123", data.getAboutus());
                    Log.d("terms", data.getTerms());

                }

                @Override
                public void onFailure(String e) {


                }
            });

        }

    }

    private void LoadMasterData(final ResultHandler<MasterModel> userResultHandler) {

        Retrofit retrofit = new APIManager().getService();

        final APIService client = retrofit.create(APIService.class);
        final Call<Result> calltargetResponse = client.getMasterData();


        calltargetResponse.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                Result UserResponse = response.body();

//                int code = response.body().getCode();
//                String[] e = response.body().getError();
                JsonObject data = response.body().getData();


                JSONObject jsonObject = null;
                JSONArray jsonElements = null;
                JSONObject cmsObject = null;

                try {

                    jsonObject = new JSONObject(data.toString());
                    cmsObject = jsonObject.getJSONObject("cms_pages");


                    jsonElements = jsonObject.getJSONArray("categories");


                    String _Faqs = "";
                    String _Terms = "";
                    String _Privacy = "";
                    String _about_us = "";


                    try {

                        _Faqs = cmsObject.get("faqs").toString();
                        _Terms = cmsObject.get("terms").toString();
                        _Privacy = cmsObject.get("privacy").toString();

                        _about_us = cmsObject.has("_about_us") ? jsonObject.getString("_about_us") : "";

//                        _about_us = cmsObject.get("_about_us").toString();


                    } catch (JSONException ex) {
                        ex.printStackTrace();
                        // Log.d("error", ex.getMessage());
                    }

                    MasterModel t = new MasterModel();

                    t.SaveSession();


                    SharedPreferences.Editor ed = SplashActivity.preferences.edit();
                    ed.putString("faqs",_Faqs); //
                    ed.putString("terms",_Terms); //
                    ed.putString("privacy",_Privacy); //
                    ed.putString("_about_us",_about_us); //
                    ed.putString("categorylist",jsonElements.toString()); //
                    ed.commit();


                    t.setFaqs(_Faqs);
                    t.setTerms(_Terms);
                    t.setPrivacy(_Privacy);
                    t.setAboutus(_about_us);
                    t.setCategoryfields(jsonElements);

                    //  Log.d("Onedata", cmsObject.toString());

                    userResultHandler.onSuccess(t);

                } catch (Exception ex) {

                }


            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {

                // Log.d("FAilApi",t.getMessage());
            }
        });
    }
    private class FourthAdapter extends FragmentPagerAdapter {
        public FourthAdapter(FragmentManager supportFragmentManager) {
            super(supportFragmentManager);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:

                    return new LoginFragment();
                case 1:
                    return new CelebLoginFragment();
            }

            return null;
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:

                    return "I AM FAN";
                case 1:
                    return "I AM CELEB";
            }


            return super.getPageTitle(position);
        }

        @Override
        public int getCount() {
            return 2;
        }
    }


    public void onBackPressed(){
        super.onBackPressed();
        finish();
    }
}
