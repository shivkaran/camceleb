package com.CamCeleb.Activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.CamCeleb.CelebSide.Fragment.CelebRegister;
import com.CamCeleb.Fragments.FanRegister;
import com.CamCeleb.R;
import com.google.android.material.tabs.TabLayout;

import java.util.List;

public class RegisterActivity extends AppCompatActivity {

    TabLayout tabLayout;
    ViewPager viewPager;

    String usertype = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        getSupportActionBar().hide();

        if (getIntent().getStringExtra("usertype") != null)
        {
            usertype = getIntent().getStringExtra("usertype");
        }


        tabLayout = findViewById(R.id.register_tablayout);
        viewPager = findViewById(R.id.register_viewpagers);

        new Handler().post(new Runnable() {
            @Override
            public void run() {

                tabLayout.setupWithViewPager(viewPager);


                viewPager.setAdapter(new FiveAdapter(getSupportFragmentManager()));

                if (usertype.equalsIgnoreCase("fan")) {
                    tabLayout.getTabAt(0).select();
                }else if (usertype.equalsIgnoreCase("celeb")){
                    tabLayout.getTabAt(1).select();
                }

            }
        });

       /* if (usertype.equalsIgnoreCase("fan")) {
            tabLayout.getTabAt(0).select();
        }else {
            tabLayout.getTabAt(1).select();

        }*/
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        int selected  =  tabLayout.getSelectedTabPosition();

        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {

                if (selected == 0) {
                    if (fragment instanceof FanRegister) {
                        fragment.onActivityResult(requestCode, resultCode, data);
                    }
                }else {
                    if (fragment instanceof CelebRegister) {
                        fragment.onActivityResult(requestCode, resultCode, data);
                    }
                }
            }
        }

    }

    private class FiveAdapter extends FragmentPagerAdapter {
        public FiveAdapter(FragmentManager supportFragmentManager) {
            super(supportFragmentManager);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:

                    return new FanRegister();
                case 1:
                    return new CelebRegister();
            }

            return null;
        }




        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:

                    return "I AM FAN";
                case 1:
                    return "I AM CELEB";
            }


            return super.getPageTitle(position);
        }


        @Override
        public int getCount() {
            return 2;
        }
    }
}

