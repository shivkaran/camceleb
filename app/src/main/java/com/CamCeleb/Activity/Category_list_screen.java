package com.CamCeleb.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.CamCeleb.Adapters.ActorSeeAll;
import com.CamCeleb.POJOClass.DashBorsData;
import com.CamCeleb.R;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.GsonBuilder;

public class Category_list_screen extends AppCompatActivity {


    DashBorsData dashBorsData;

    TextView txt_title_name;

    RecyclerView list_of_calegry;

    ImageView commentsbackbtn;
    ShimmerFrameLayout parentShimmerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_list_screen);
        getSupportActionBar().hide();



        parentShimmerLayout = findViewById(R.id.parentShimmerLayout);

        parentShimmerLayout.startShimmerAnimation();


        Intent intent = getIntent();


        if (getIntent() != null && getIntent().hasExtra("list_of_actors")) {
            dashBorsData = new GsonBuilder().create().fromJson(getIntent().getStringExtra("list_of_actors"), DashBorsData.class);
        }

        txt_title_name = findViewById(R.id.txt_title_name);
        list_of_calegry = findViewById(R.id.list_of_calegry);
        commentsbackbtn = findViewById(R.id.commentsbackbtn);

        txt_title_name.setText(dashBorsData.getName());


        ActorSeeAll adapter1 = new ActorSeeAll(this, dashBorsData.getData());

        //setting adapter to recyclerview
        list_of_calegry.setAdapter(adapter1);
        list_of_calegry.setLayoutManager(new GridLayoutManager(this, 2));

        parentShimmerLayout.stopShimmerAnimation();
        parentShimmerLayout.setVisibility(View.GONE);

        commentsbackbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


    }
}
