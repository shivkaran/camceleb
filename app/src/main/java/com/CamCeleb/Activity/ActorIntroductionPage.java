package com.CamCeleb.Activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.CamCeleb.APIs.DashBords;
import com.CamCeleb.Adapters.IntroActorAdapter;
import com.CamCeleb.POJOClass.ActorIntro;
import com.CamCeleb.POJOClass.Celebdetails.Celeb_Data_response;
import com.CamCeleb.POJOClass.Celebdetails.GsonResponse_celebDetails;
import com.CamCeleb.POJOClass.ResultHandler;
import com.CamCeleb.R;
import com.CamCeleb.UserSide.RequestActivityFan;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.LoopingMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultAllocator;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import java.util.List;

import io.supercharge.shimmerlayout.ShimmerLayout;

public class ActorIntroductionPage extends AppCompatActivity implements View.OnClickListener, Player.EventListener {

    RecyclerView recyclerView;
    List<ActorIntro> actorIntros;
    ImageView imageView, profile_image;
    TextView request,txt_name_of_testimonils;
    TextView txt_user_name, txt_category, txt_about_me,txt_price;
    private static final int CAMERA_REQUEST_CODE_VEDIO = 101;

    String celeb_id = "";
//    VideoView simpleVideoView;

    PlayerView videoFullScreenPlayer;
    SimpleExoPlayer player;
//    ShimmerLayout shimmerLayout;

    String str_price,str_name,profile_image_celeb;
    ImageView image_back;

    ShimmerFrameLayout parentShimmerLayout;

    String Video_url="";
    boolean aBoolean_vlume = true;

    RelativeLayout ll_main_layout;
    LinearLayout ll_request_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actor_introduction_page);


        parentShimmerLayout = findViewById(R.id.parentShimmerLayout);
        ll_main_layout = findViewById(R.id.ll_main_layout);

        parentShimmerLayout.startShimmerAnimation();



        Intent intent = getIntent();

        if (intent.getStringExtra("id") != null) {
            celeb_id = intent.getStringExtra("id");
        }

//        shimmerLayout = findViewById(R.id.shimmer_layout);
        txt_user_name = findViewById(R.id.txt_user_name);
        txt_about_me = findViewById(R.id.txt_about_me);
        txt_category = findViewById(R.id.txt_category);
//        shimmerLayout.startShimmerAnimation();

        profile_image = findViewById(R.id.profile_image);
        imageView = findViewById(R.id.capturevideo);
        request = findViewById(R.id.actor_intro_request);
        videoFullScreenPlayer = findViewById(R.id.videoFullScreenPlayer);
        txt_price = findViewById(R.id.txt_price);
        image_back = findViewById(R.id.image_back);
        txt_name_of_testimonils = findViewById(R.id.txt_name_of_testimonils);
//        simpleVideoView = (VideoView) findViewById(R.id.videoview);

        recyclerView = findViewById(R.id.actor_intro_recycler);
        ll_request_layout = findViewById(R.id.ll_request_layout);


        imageView.setOnClickListener(this);
        request.setOnClickListener(this);

        imageView.setVisibility(View.GONE);

        getCelebritydetail();

        if (player == null) {
            // 1. Create a default TrackSelector
            int MIN_BUFFER_DURATION = 90;
            //Max Video you want to buffer during PlayBack
            int MAX_BUFFER_DURATION = 90;
            //Min Video you want to buffer before start Playing it
            int MIN_PLAYBACK_START_BUFFER = 90;
            //Min video You want to buffer when user resumes video
            int MIN_PLAYBACK_RESUME_BUFFER = 5;
/*
            LoadControl loadControl = new DefaultLoadControl(
                    new DefaultAllocator(true, 16),
                    MIN_BUFFER_DURATION,
                    MAX_BUFFER_DURATION,
                    MIN_PLAYBACK_START_BUFFER,
                    MIN_PLAYBACK_RESUME_BUFFER, -1, true);*/

            LoadControl loadControl = new DefaultLoadControl();

            BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
            TrackSelection.Factory videoTrackSelectionFactory =
                    new AdaptiveTrackSelection.Factory(bandwidthMeter);
            TrackSelector trackSelector =
                    new DefaultTrackSelector(videoTrackSelectionFactory);
            // 2. Create the player
            player = ExoPlayerFactory.newSimpleInstance(new DefaultRenderersFactory(this), trackSelector, loadControl);

            videoFullScreenPlayer.setPlayer(player);


        }

        videoFullScreenPlayer.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (aBoolean_vlume) {
                    player.setVolume(0f);
                    aBoolean_vlume = false;
                }else {
                    player.setVolume(100f);
                    aBoolean_vlume = true;
                }
                return false;
            }
        });



        image_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    private void getCelebritydetail() {

        DashBords dashBords = new DashBords();


        dashBords.celebritydetail(celeb_id, new ResultHandler<GsonResponse_celebDetails>() {
            @Override
            public void onSuccess(GsonResponse_celebDetails data) {

                if (data.getSuccess() == 1) {

                    InitView(data.getData());

                } else {

                }

            }

            @Override
            public void onFailure(String exx) {

            }


        });

    }

    private void InitView(Celeb_Data_response data) {

//        shimmerLayout.stopShimmerAnimation();
        ll_main_layout.setVisibility(View.VISIBLE);
        parentShimmerLayout.stopShimmerAnimation();
        parentShimmerLayout.setVisibility(View.GONE);


        request.setText("Request");
        Video_url = data.getIntroVideo();
        buildMediaSource(Uri.parse(data.getIntroVideo()));

        if (data.getProfilePic().isEmpty())
        {
            profile_image.setImageDrawable(getResources().getDrawable(R.mipmap.profile_pic));
        }else {
            Picasso.with(ActorIntroductionPage.this).load(data.getProfilePic()).into(profile_image);
        }
        txt_user_name.setText(data.getFullName().trim());
        txt_about_me.setText(data.getAboutMe().trim());
        txt_category.setText(data.getCategories().trim());
        txt_price.setText("₹ "+data.getBookingPrice().trim());

        str_price = data.getBookingPrice().trim();
        str_name = data.getFullName().trim();
        profile_image_celeb = data.getProfilePic().trim();


        txt_name_of_testimonils.setText("Testimonials("+ data.getTestimonials().size()+")");
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(ActorIntroductionPage.this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);


        IntroActorAdapter adapter = new IntroActorAdapter(ActorIntroductionPage.this, data.getTestimonials());

        //setting adapter to recyclerview
        recyclerView.setAdapter(adapter);


        if (data.getIsOnline().equalsIgnoreCase("1"))
        {
            ll_request_layout.setBackground(getResources().getDrawable(R.drawable.introbtn));
            request.setText("Request");
            request.setTextColor(getResources().getColor(R.color.white));
            txt_price.setTextColor(getResources().getColor(R.color.white));
            request.setEnabled(true);


        }else {
            request.setText("Unavailable");
            request.setEnabled(false);
            request.setTextColor(getResources().getColor(R.color.gry));
            txt_price.setTextColor(getResources().getColor(R.color.gry));
            ll_request_layout.setBackground(getResources().getDrawable(R.drawable.lightgrybtn));
        }


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        player.stop();
    }

    private void buildMediaSource(Uri mUri) {
        // Measures bandwidth during playback. Can be null if not required.
        DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        // Produces DataSource instances through which media data is loaded.
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(this,
                Util.getUserAgent(this, getString(R.string.app_name)), bandwidthMeter);
        // This is the MediaSource representing the media to be played.
        MediaSource videoSource = new ExtractorMediaSource.Factory(dataSourceFactory).createMediaSource(mUri);
                // Prepare the player with the source.
        player.setRepeatMode(Player.REPEAT_MODE_ONE);
        player.prepare(videoSource);

        player.setPlayWhenReady(true);
        player.addListener(ActorIntroductionPage.this);


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.capturevideo:

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                    requestMultiplePermissions();

                } else {

                    Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                    if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {
                        startActivityForResult(takeVideoIntent, CAMERA_REQUEST_CODE_VEDIO);
                    }
                }

                break;
            case R.id.actor_intro_request:

                Intent intent = new Intent(ActorIntroductionPage.this, RequestActivityFan.class);
                intent.putExtra("celeb_id",celeb_id);
                intent.putExtra("celeb_name",str_name);
                intent.putExtra("profile_image_celeb",profile_image_celeb);
                intent.putExtra("price",str_price);
                startActivity(intent);
                break;


            default:
                break;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void requestMultiplePermissions() {

        if (ContextCompat.checkSelfPermission(ActorIntroductionPage.this, Manifest.permission.READ_EXTERNAL_STORAGE) + ContextCompat.checkSelfPermission(ActorIntroductionPage.this, Manifest.permission.CAMERA) + ContextCompat.checkSelfPermission(ActorIntroductionPage.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(ActorIntroductionPage.this, Manifest.permission.READ_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(ActorIntroductionPage.this, Manifest.permission.CAMERA) || ActivityCompat.shouldShowRequestPermissionRationale(ActorIntroductionPage.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                Snackbar.make(ActorIntroductionPage.this.findViewById(android.R.id.content),
                        "Please Grant Permissions to upload Video",
                        Snackbar.LENGTH_INDEFINITE).setAction("Enable",
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, CAMERA_REQUEST_CODE_VEDIO);
                            }
                        }).show();
            } else {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, CAMERA_REQUEST_CODE_VEDIO);
            }
        } else {
            Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
            if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {
                startActivityForResult(takeVideoIntent, CAMERA_REQUEST_CODE_VEDIO);
            }
        }
    }

    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        switch(playbackState) {
            case ExoPlayer.STATE_BUFFERING:
                break;
            case ExoPlayer.STATE_ENDED:
                player.setRepeatMode(Player.REPEAT_MODE_ONE);
                player.seekTo(0);
                player.setPlayWhenReady(true);
                break;
            case ExoPlayer.STATE_IDLE:
                break;
            case ExoPlayer.STATE_READY:
                break;
            default:
                break;
        }
    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {
        player.setRepeatMode(repeatMode);
    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

    }

    @Override
    public void onPositionDiscontinuity(int reason) {

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onSeekProcessed() {

    }

    @Override
    protected void onResume() {
        super.onResume();
        getCelebritydetail();
    }

    @Override
    protected void onPause() {
        super.onPause();
        player.stop();
        if (player != null) {
            player.setPlayWhenReady(false);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        player.stop();
        if (player != null) {
            player.setPlayWhenReady(false);
        }
    }
}
