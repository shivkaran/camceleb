package com.CamCeleb.Activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.CamCeleb.APIs.Authorization;
import com.CamCeleb.APIs.DashBords;
import com.CamCeleb.CelebSide.Request_Details_screen;
import com.CamCeleb.POJOClass.Comment.Gson_response_comment;
import com.CamCeleb.POJOClass.User;
import com.CamCeleb.SplashActivity;
import com.CamCeleb.UserSide.CommentsActivity;
import com.CamCeleb.POJOClass.Result;
import com.CamCeleb.POJOClass.ResultHandler;
import com.CamCeleb.R;
import com.CamCeleb.UserSide.User_Setting;
import com.facebook.login.LoginManager;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultAllocator;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import io.supercharge.shimmerlayout.ShimmerLayout;

public class CelebVideoPlayIntro extends AppCompatActivity implements Player.EventListener{

    ImageView imageView, comment;

    String str_video_url, str_like_count, str_comment_count, str_id,str_name,str_profile_image,is_my_video="",is_liked="";
    PlayerView videoplayintro;
    SimpleExoPlayer player;
    ShimmerLayout shimmerLayout;

    TextView intro_comments,intro_likes,intro_username;
    ImageView intro_userimg;

    LinearLayout ll_share,ll_like,ll_comment,ll_feedback;

    boolean flag = true;
    ImageView btn_paly_push;

    public static SharedPreferences preferences;

    String user_type = "";

    ProgressBar  progressbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_celeb_video_play_intro);
        getSupportActionBar().hide();


        preferences = getSharedPreferences(getPackageName() + "_preferences", MODE_PRIVATE);
        String  session = preferences.getString("SessionData", "");
        Gson gson = new Gson();
        User user = gson.fromJson(session, User.class);
        user_type = String.valueOf(user.getUserType());



        Intent intent = getIntent();

        if (intent.getStringExtra("str_video_url") != null) {
            str_video_url = intent.getStringExtra("str_video_url");
        }

        if (intent.getStringExtra("str_like_count") != null) {
            str_like_count = intent.getStringExtra("str_like_count");
        }

        if (intent.getStringExtra("str_comment_count") != null) {
            str_comment_count = intent.getStringExtra("str_comment_count");
        }

        if (intent.getStringExtra("str_id") != null) {
            str_id = intent.getStringExtra("str_id");
        }
        if (intent.getStringExtra("str_name") != null) {
            str_name = intent.getStringExtra("str_name");

        } if (intent.getStringExtra("str_profile_image") != null) {
            str_profile_image = intent.getStringExtra("str_profile_image");
        }if (intent.getStringExtra("is_my_video") != null) {
            is_my_video = intent.getStringExtra("is_my_video");
        }if (intent.getStringExtra("is_liked") != null) {
            is_liked = intent.getStringExtra("is_liked");
        }


        imageView = findViewById(R.id.closevideo);
        comment = findViewById(R.id.addcomment);
        intro_likes = findViewById(R.id.intro_likes);
        intro_comments = findViewById(R.id.intro_comments);
        intro_userimg = findViewById(R.id.intro_userimg);
        intro_username = findViewById(R.id.intro_username);
        btn_paly_push = findViewById(R.id.btn_paly_push);
        progressbar = findViewById(R.id.progressbar);


        ll_share = findViewById(R.id.ll_share);
        ll_like = findViewById(R.id.ll_like);
        ll_comment = findViewById(R.id.ll_comment);
        ll_feedback = findViewById(R.id.ll_feedback);

        videoplayintro = findViewById(R.id.videoplayintro);

        intro_likes.setText(str_like_count);
        intro_comments.setText(str_comment_count);
        intro_username.setText(str_name);


        progressbar.setVisibility(View.VISIBLE);


        if (is_my_video.equalsIgnoreCase("1"))
        {
            ll_feedback.setVisibility(View.VISIBLE);

        }else {
            ll_feedback.setVisibility(View.GONE);
        }


        if (str_profile_image.isEmpty())
        {
            intro_userimg.setImageDrawable(getResources().getDrawable(R.mipmap.profile_pic));
        }else {
            Picasso.with(this).load(str_profile_image).into(intro_userimg);
        }
        if (player == null) {


            LoadControl loadControl = new DefaultLoadControl();

            BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
            TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
            TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);
            // 2. Create the player

            player = ExoPlayerFactory.newSimpleInstance(new DefaultRenderersFactory(this), trackSelector, loadControl);
//            player.getPlaybackLooper();

            videoplayintro.setPlayer(player);
        }

        buildMediaSource(Uri.parse(str_video_url));
        GetCommentList();


        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        ll_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (user_type.equals("2"))
                {

                }else {
                    Intent intent = new Intent(CelebVideoPlayIntro.this, CommentsActivity.class);
                    intent.putExtra("video_id", str_id);
                    startActivity(intent);
                }
            }
        });

        ll_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (user_type.equals("2"))
                {

                }else {

                    ll_like.setEnabled(false);
                    SetLikeUnlike(str_id,is_liked);
                }

            }
        });

        ll_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String subject = "Chekout this awsome video on CamCeleb";

                String body = str_video_url+"  "+ subject;
                
                Intent txtIntent = new Intent(android.content.Intent.ACTION_SEND);
                txtIntent .setType("text/plain");
                txtIntent .putExtra(android.content.Intent.EXTRA_TEXT, body);
                startActivity(Intent.createChooser(txtIntent ,"Share"));
            }
        });

        ll_feedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowFeedbackDialog();
            }
        });

      videoplayintro.setOnTouchListener(new View.OnTouchListener() {
          @Override
          public boolean onTouch(View v, MotionEvent event) {
              if (flag) {
                  pausePlayer();
//                    player.setPlayWhenReady(false);
                  flag = false;
                  btn_paly_push.setVisibility(View.VISIBLE);
              }else {
                  playPlayer();
//                    player.setPlayWhenReady(true);
                  flag=true;
                  btn_paly_push.setVisibility(View.GONE);
              }
              return false;
          }
      });

    }

    private void pausePlayer() {
        if (player != null) {
            player.setPlayWhenReady(false);
        }
    }

    private void playPlayer() {
        if (player != null) {
            player.setPlayWhenReady(true);
        }
    }



    private void ShowFeedbackDialog() {

        final Dialog dialog = new Dialog(CelebVideoPlayIntro.this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.feedback_layout);

        final RatingBar ratingBar = dialog.findViewById(R.id.rating_bar);
        final EditText edt_feedback = dialog.findViewById(R.id.edt_feedback);
        CardView dialogButton =  dialog.findViewById(R.id.submit);
        CardView cancel =  dialog.findViewById(R.id.cancel);

        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String str_ratign = String.valueOf(ratingBar.getRating());
                String str_feedback =edt_feedback.getText().toString();

                if (str_ratign.equals("0.0"))
                {
                    // Toast.makeText(CelebVideoPlayIntro.this, "Please rate this video", Toast.LENGTH_SHORT).show();
                }else if (str_feedback.equals(""))
                {
                    edt_feedback.setError("Please add feedback");

                }else {
                    SaveFeedbackdata(str_ratign,str_feedback);
                    dialog.dismiss();
                }



            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();


    }

    private void SaveFeedbackdata(String str_ratign, String str_feedback) {
        DashBords dashBords = new DashBords();
        dashBords.SaveFeedback(str_id,str_ratign,str_feedback, new ResultHandler<Result>() {


            @Override
            public void onSuccess(Result data) {
                if (data.getCode() == 1) {
                    ll_feedback.setVisibility(View.GONE);
                    ShowsucessfullDaillog("Feedback added successfully");

                }else {
                    ShowsucessfullDaillog("Feedback not add please try again!");
                }
            }

            @Override
            public void onFailure(String exx) {
                ShowsucessfullDaillog("Feedback not add please try again!");
            }
        });
    }

    private void ShowsucessfullDaillog(String feedback_add_successfully_done) {

     /*   AlertDialog.Builder builder = new AlertDialog.Builder(CelebVideoPlayIntro.this,R.style.MyDialogTheme);
        builder.setTitle("CamCeleb");
        builder.setMessage(feedback_add_successfully_done);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
            }
        });

        builder.show();*/
        final Dialog dialog = new Dialog(this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custom_alert_dailog);

        TextView text = (TextView) dialog.findViewById(R.id.txt_message);
        text.setText(feedback_add_successfully_done);

        TextView dialogButton =  dialog.findViewById(R.id.submit);

        dialogButton.setText("Ok");


        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

            }
        });


        dialog.show();

    }

    private void SetLikeUnlike(String str_video_id, final String liked) {
        DashBords dashBords = new DashBords();
        dashBords.LikeUnlike(str_video_id, new ResultHandler<Result>() {

            @Override
            public void onSuccess(Result data) {

                if (data.getCode() == 1) {
                    ll_like.setEnabled(true);
                    int count_like = Integer.parseInt(str_like_count);

                    if (liked.equalsIgnoreCase("true")) {
                        is_liked = "false";
                        count_like = count_like - 1;

                        str_like_count = String.valueOf(count_like);
                    }else {
                        count_like = count_like + 1;
                        is_liked = "true";
                        str_like_count = String.valueOf(count_like);

                    }

                    intro_likes.setText(String.valueOf(count_like));
                }
            }

            @Override
            public void onFailure(String exx) {

            }
        });
    }

    private void buildMediaSource(Uri mUri) {

        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(this,
                Util.getUserAgent(this, getString(R.string.app_name)), null);
        // This is the MediaSource representing the media to be played.
        MediaSource videoSource = new ExtractorMediaSource.Factory(dataSourceFactory).createMediaSource(mUri);
        // Prepare the player with the source.
        player.prepare(videoSource);
        player.setPlayWhenReady(true);
        player.setRepeatMode(Player.REPEAT_MODE_ONE);
        player.addListener(CelebVideoPlayIntro.this);
//        player.getPlaybackLooper();


//


    }

    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

        if (playbackState == Player.STATE_READY)
        {
            progressbar.setVisibility(View.GONE);
        }
        if (playbackState == Player.STATE_ENDED) {
            buildMediaSource(Uri.parse(str_video_url));
        }
    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

    }

    @Override
    public void onPositionDiscontinuity(int reason) {

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onSeekProcessed() {

    }

    @Override
    public void onPause() {
        super.onPause();
        player.stop();
        if (player != null) {
            player.setPlayWhenReady(false);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        player.stop();
        if (player != null) {
            player.setPlayWhenReady(false);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (player != null) {
            buildMediaSource(Uri.parse(str_video_url));
            GetCommentList();
        }
    }


    private void GetCommentList() {

        DashBords dashBords = new DashBords();

        System.out.println("video id "+  str_id);

        dashBords.CommentList("0","100",str_id, new ResultHandler<Gson_response_comment>() {
            @Override
            public void onSuccess(Gson_response_comment data) {

                if (data.getSuccess() == 1) {

                    str_comment_count = String.valueOf(data.getData().getList().size());
                    intro_comments.setText(str_comment_count);

                } else {

                }

            }

            @Override
            public void onFailure(String exx) {

            }


        });

    }

}
