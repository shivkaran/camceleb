package com.CamCeleb.Activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.CamCeleb.APIs.DashBords;
import com.CamCeleb.APIs.Helper;
import com.CamCeleb.POJOClass.RequestVideo.Gson_Response_Requestvideo;
import com.CamCeleb.POJOClass.RequestVideo.Response_request;
import com.CamCeleb.POJOClass.ResultHandler;
import com.CamCeleb.PaymentPaytm.PaymentdoneActivity;
import com.CamCeleb.R;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class RequestForCelebActivity extends AppCompatActivity {

    Button paybtnsubmit;

    String celeb_id = "";
    String celeb_name = "";
    String price = "";

    String friend_name = "", phone_number = "", email = "", instructions = "", booking_for = "", is_private = "", str_image_path = "";

    TextView txt_title_rquest, txt_celeb_name, txt_user_name, txt_date_exp, txt_bio;
    int finalamount = 0;
    String type="";
    TextView txt_terms_condition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_for_celeb);
        getSupportActionBar().hide();


        if (getIntent().getStringExtra("celeb_id") != null) {
            celeb_id = getIntent().getStringExtra("celeb_id");
        }
        if (getIntent().getStringExtra("celeb_name") != null) {
            celeb_name = getIntent().getStringExtra("celeb_name");
        }
        if (getIntent().getStringExtra("price") != null) {
            price = getIntent().getStringExtra("price");
        }

        if (getIntent().getStringExtra("friend_name") != null) {
            friend_name = getIntent().getStringExtra("friend_name");
        }
        if (getIntent().getStringExtra("phone_number") != null) {
            phone_number = getIntent().getStringExtra("phone_number");
        }
        if (getIntent().getStringExtra("email") != null) {
            email = getIntent().getStringExtra("email");
        }
        if (getIntent().getStringExtra("instructions") != null) {
            instructions = getIntent().getStringExtra("instructions");
        }
        if (getIntent().getStringExtra("booking_for") != null) {
            booking_for = getIntent().getStringExtra("booking_for");
        }
        if (getIntent().getStringExtra("is_private") != null) {
            is_private = getIntent().getStringExtra("is_private");
        }
        if (getIntent().getStringExtra("str_image_path") != null) {
            str_image_path = getIntent().getStringExtra("str_image_path");
        }
        if (getIntent().getStringExtra("type") != null) {
            type = getIntent().getStringExtra("type");
        }


        Calendar cal = GregorianCalendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DAY_OF_YEAR, 7);
        Date sevendate = cal.getTime();
        SimpleDateFormat sdf1 = new SimpleDateFormat("dd MMM yyyy");
        final String date = sdf1.format(sevendate);

        paybtnsubmit = findViewById(R.id.paybtnsubmit);
        txt_title_rquest = findViewById(R.id.txt_title_rquest);
        txt_celeb_name = findViewById(R.id.txt_celeb_name);
        txt_user_name = findViewById(R.id.txt_user_name);
        txt_date_exp = findViewById(R.id.txt_date_exp);
        txt_bio = findViewById(R.id.txt_bio);
        txt_terms_condition = findViewById(R.id.txt_terms_condition);


        txt_celeb_name.setText(celeb_name);
        txt_bio.setText(instructions);
        txt_user_name.setText(instructions);
        txt_user_name.setText(friend_name);

        txt_date_exp.setText(date);

        txt_title_rquest.setText("Request to " + celeb_name);

        txt_title_rquest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        finalamount = Integer.parseInt(price);

        double GST_amount = (finalamount * 18) / 100;

        finalamount = (int) Math.round(finalamount + GST_amount);


        paybtnsubmit.setText("Pay  ₹  " + finalamount);

        txt_terms_condition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.camceleb.com/refund-policy"));
                startActivity(browserIntent);
            }
        });


        paybtnsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                DashBords dashBords = new DashBords();

                if (booking_for.equals("M")) {

                    dashBords.RequestvideoWithoutimage(celeb_id, "", phone_number, email, instructions, booking_for, is_private, new ResultHandler<Gson_Response_Requestvideo>() {
                        @Override
                        public void onSuccess(Gson_Response_Requestvideo data) {

                            if (data.getSuccess() == 1) {
                                String order_id = String.valueOf(data.getData().getOrderId());

                                Response_request dashBorsData = data.getData();

                                Intent i = new Intent(RequestForCelebActivity.this, PaymentdoneActivity.class);
                                i.putExtra("list_of_payment", new Gson().toJson(dashBorsData));
                                i.putExtra("amount", String.valueOf(finalamount));
                                startActivity(i);

                            }

                        }

                        @Override
                        public void onFailure(String exx) {

                        }
                    });

                } else {
                    if (str_image_path.equals(""))
                    {
                        dashBords.RequestvideoWithoutimage(celeb_id, friend_name, phone_number, email, instructions, booking_for, is_private, new ResultHandler<Gson_Response_Requestvideo>() {
                            @Override
                            public void onSuccess(Gson_Response_Requestvideo data) {

                                if (data.getSuccess() == 1) {
                                    String order_id = String.valueOf(data.getData().getOrderId());

                                    Response_request dashBorsData = data.getData();

                                    Intent i = new Intent(RequestForCelebActivity.this, PaymentdoneActivity.class);
                                    i.putExtra("list_of_payment", new Gson().toJson(dashBorsData));
                                    i.putExtra("amount", String.valueOf(finalamount));
                                    startActivity(i);

                                }

                            }

                            @Override
                            public void onFailure(String exx) {

                            }
                        });

                    }else {
                        dashBords.Requestvideo(Helper.createRequestBody(celeb_id), Helper.createRequestBody(friend_name), Helper.createRequestBody(phone_number), Helper.createRequestBody(email), Helper.createRequestBody(instructions), Helper.createRequestBody(booking_for), Helper.createRequestBody(is_private), Uri.parse(str_image_path), type, new ResultHandler<Gson_Response_Requestvideo>() {
                            @Override
                            public void onSuccess(Gson_Response_Requestvideo data) {
                                if (data.getSuccess() == 1) {
                                    String order_id = String.valueOf(data.getData().getOrderId());

                                    Response_request dashBorsData = data.getData();

                                    Intent i = new Intent(RequestForCelebActivity.this, PaymentdoneActivity.class);
                                    i.putExtra("list_of_payment", new Gson().toJson(dashBorsData));
                                    i.putExtra("amount", String.valueOf(finalamount));
                                    startActivity(i);

                                }
                            }

                            @Override
                            public void onFailure(String exx) {

                            }
                        });
                    }
                }
            }
        });
    }
}
