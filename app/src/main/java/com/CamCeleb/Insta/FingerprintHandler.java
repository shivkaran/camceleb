package com.CamCeleb.Insta;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.hardware.fingerprint.FingerprintManager;
import android.os.CancellationSignal;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.CamCeleb.APIs.Authorization;
import com.CamCeleb.POJOClass.Login_model.Gson_response_login_data;
import com.CamCeleb.POJOClass.ResultHandler;
import com.CamCeleb.R;
import com.CamCeleb.UserSide.OTPVerificationActivity;
import com.CamCeleb.UserSide.UserHomeActivity;

import static android.content.Context.MODE_PRIVATE;

public class FingerprintHandler extends FingerprintManager.AuthenticationCallback {

    // You should use the CancellationSignal method whenever your app can no longer process user input, for example when your app goes
    // into the background. If you don’t use this method, then other apps will be unable to access the touch sensor, including the lockscreen!//

    CancellationSignal cancellationSignal;
    Context context;

    private SharedPreferences loginPreferences;
    private SharedPreferences.Editor loginPrefsEditor;

    public FingerprintHandler(Context mContext) {

        context = mContext;
    }

    //Implement the startAuth method, which is responsible for starting the fingerprint authentication process//

    public void startAuth(FingerprintManager manager, FingerprintManager.CryptoObject
            cryptoObject) {

        cancellationSignal = new CancellationSignal();
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        manager.authenticate(cryptoObject, cancellationSignal, 0, this, null);
    }

    @Override
    //onAuthenticationError is called when a fatal error has occurred. It provides the error code and error message as its parameters//

    public void onAuthenticationError(int errMsgId, CharSequence errString) {

        //I’m going to display the results of fingerprint authentication as a series of toasts.
        //Here, I’m creating the message that’ll be displayed if an error occurs//

        // Toast.makeText(context, "Authentication error\n" + errString, Toast.LENGTH_LONG).show();
        ShowDailog("Authentication error\n" + errString);
    }

    @Override

    //onAuthenticationFailed is called when the fingerprint doesn’t match with any of the fingerprints registered on the device//

    public void onAuthenticationFailed() {
        // Toast.makeText(context, "Authentication failed", Toast.LENGTH_LONG).show();

        ShowDailog("Authentication failed");

    }

    @Override

    //onAuthenticationHelp is called when a non-fatal error has occurred. This method provides additional information about the error,
    //so to provide the user with as much feedback as possible I’m incorporating this information into my toast//
    public void onAuthenticationHelp(int helpMsgId, CharSequence helpString) {
        // Toast.makeText(context, "Authentication help\n" + helpString, Toast.LENGTH_LONG).show();
    }

    @Override

    //onAuthenticationSucceeded is called when a fingerprint has been successfully matched to one of the fingerprints stored on the user’s device//
    public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {

        loginPreferences = context.getSharedPreferences("loginPrefs", MODE_PRIVATE);
        loginPrefsEditor = loginPreferences.edit();


        String user_name = loginPreferences.getString("username", "");
        String password = loginPreferences.getString("password", "");


        checkLoginData(user_name, password);

     /*   context.startActivity(new Intent(context, UserHomeActivity.class));
        ((AppCompatActivity) context).finish();
        // Toast.makeText(context, "Success!", Toast.LENGTH_LONG).show();*/

    }

    private void checkLoginData(String user_name, String password) {

        final ProgressDialog progressDialog = ProgressDialog.show(context,null,"please wait",true,false);
        Authorization authorization = new Authorization();
        authorization.LoginData(user_name, password, "F", new ResultHandler<Gson_response_login_data>() {

            @Override
            public void onSuccess(Gson_response_login_data data) {

                progressDialog.dismiss();
                if (data.getSuccess() == 1) {


                    if (data.getData().getIdVerification().equals("noverify")) {

                        data.getData().SaveSession();
                        // Toast.makeText(context, " First Verifly Your Account", Toast.LENGTH_SHORT).show();

                        Intent intent = new Intent(context, OTPVerificationActivity.class);
                        context.startActivity(intent);

                    } else {

                        data.getData().SaveSession();

                        context.startActivity(new Intent(context, UserHomeActivity.class));
                        ((AppCompatActivity) context).finish();
                    }
                } else {


                }
            }

            @Override
            public void onFailure(String exx) {
                Log.d("msg", exx);
                progressDialog.dismiss();

            }
        });
    }
    private void ShowDailog(String login_failed_please_try_again) {

        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dailog_layout);

        TextView text = (TextView) dialog.findViewById(R.id.txt_message);
        text.setText(login_failed_please_try_again);

        Button dialogButton = (Button) dialog.findViewById(R.id.submit);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }
}

