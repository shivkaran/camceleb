package com.CamCeleb;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.gohn.parallaxviewpager.ParallaxViewPager;

class SampleAdapter extends PagerAdapter {

    Context context;
    String title;
    LayoutInflater inflater;


    int[] images = new int[]{R.mipmap.slide1, R.mipmap.slide2, R.mipmap.slide3};
    String[] texts = new String[]{
            "Get connected with your star through personalized video message",
            "Connecting fans with their beloved celebrities",
            "We offer the moments that last forever.",

    };

    public SampleAdapter(LayoutInflater layoutInflater, String s) {

        this.inflater = layoutInflater;
        this.title = s;
    }

    @Override
    public int getCount() {
        //return Integer.MAX_VALUE;
        return 3;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(final ViewGroup container, final int position) {

        View v = inflater.inflate(R.layout.layout_sample, null);


        ImageView image = (ImageView) v.findViewById(R.id.image);
        TextView text = (TextView) v.findViewById(R.id.text);



        image.setImageResource(images[position % 5]);
        text.setText(texts[position % 5]);

        ViewPager vp = (ViewPager) container;
        vp.addView(v, 0);
        vp.removeView(v);




        container.addView(v);
        return v;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
        ParallaxViewPager vp = (ParallaxViewPager) container;
        View view = (View) object;
        vp.removeView(view);
    }


}