package com.CamCeleb.APIs;

import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import androidx.loader.content.CursorLoader;

import com.CamCeleb.Helper.APIService;
import com.CamCeleb.Helper.ProgressRequestBody;
import com.CamCeleb.MainActivity;
import com.CamCeleb.POJOClass.Celeb_Home_Request.Gson_Response_Celeb_Request_list;
import com.CamCeleb.POJOClass.Celeb_dashoad.Gson_response_celeb_dashbord;
import com.CamCeleb.POJOClass.Celeb_profile_details.Gson_Response_Celeb_profile_details;
import com.CamCeleb.POJOClass.ChecksumGenrate.GsonResponse_checksume;
import com.CamCeleb.POJOClass.Login_model.Gson_response_login_data;
import com.CamCeleb.POJOClass.Request_Details.Gson_Response_Requestdetail;
import com.CamCeleb.POJOClass.Result;
import com.CamCeleb.POJOClass.ResultHandler;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.facebook.FacebookSdk.getApplicationContext;

public class Celeb_DashBords extends APIManager {

    public void CelebDash(String start, String limit,String status, final ResultHandler<Gson_Response_Celeb_Request_list> objectResultHandler) {

        Retrofit retrofit = getService();
        APIService apiService = retrofit.create(APIService.class);
        Call<Gson_Response_Celeb_Request_list> call = apiService.CelebRequest(start, limit,status);

        Log.d("onertyu", call.toString());

        call.enqueue(new Callback<Gson_Response_Celeb_Request_list>() {
            @Override
            public void onResponse(Call<Gson_Response_Celeb_Request_list> call, Response<Gson_Response_Celeb_Request_list> response) {

                objectResultHandler.onSuccess(response.body());

            }

            @Override
            public void onFailure(Call<Gson_Response_Celeb_Request_list> call, Throwable t) {

                System.out.println(call.toString());
            }
        });
    }


    public void Dashboard(String start, String limit, final ResultHandler<Gson_response_celeb_dashbord> gson_response_favourite_dataResultHandler) {

        Retrofit retrofit = getService();
        APIService apiService = retrofit.create(APIService.class);
        Call<Gson_response_celeb_dashbord> call = apiService.CelebDashBord(start, limit);


        call.enqueue(new Callback<Gson_response_celeb_dashbord>() {
            @Override
            public void onResponse(Call<Gson_response_celeb_dashbord> call, Response<Gson_response_celeb_dashbord> response) {


                gson_response_favourite_dataResultHandler.onSuccess(response.body());

            }

            @Override
            public void onFailure(Call<Gson_response_celeb_dashbord> call, Throwable t) {

                System.out.println(call.toString());

            }
        });
    }

    public void UpdateProfile(String is_online, String type, final ResultHandler<Gson_response_login_data> gson_response_login_dataResultHandler) {

        Retrofit retrofit = getService();
        APIService apiService = retrofit.create(APIService.class);
        Call<Gson_response_login_data> call;
        if (type.equalsIgnoreCase("price")) {
            call = apiService.UpdateProfileCelebPrice(is_online);
        } else {
            call = apiService.UpdateProfileCeleb(is_online);
        }
        Log.d("onertyu", call.toString());

        call.enqueue(new Callback<Gson_response_login_data>() {
            @Override
            public void onResponse(Call<Gson_response_login_data> call, Response<Gson_response_login_data> response) {
                if (response.isSuccessful()) {
                    if (response.body().getSuccess() == 1) {
                        try {

                            Gson_response_login_data gson_response_comment = response.body();

                            gson_response_login_dataResultHandler.onSuccess(gson_response_comment);

                        } catch (Exception e1) {
                            Log.d("APIErr", e1.getLocalizedMessage());


                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<Gson_response_login_data> call, Throwable t) {


            }
        });


    }


    public void AcceptReject(String request_id, String action, final ResultHandler<Result> resultResultHandler) {
        Retrofit retrofit = getService();
        APIService apiService = retrofit.create(APIService.class);
        Call<Result> call = apiService.AcceptReject(request_id, action);

        Log.d("onertyu", call.toString());

        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                if (response.isSuccessful()) {
                    if (response.body().getCode() == 1) {
                        try {

                            Result gson_response_comment = response.body();

                            resultResultHandler.onSuccess(gson_response_comment);

                        } catch (Exception e1) {
                            Log.d("APIErr", e1.getLocalizedMessage());


                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {

                resultResultHandler.onFailure(call.toString());
            }
        });

    }


    public void celebritydetail(String user_id,String start,String limit, final ResultHandler<Gson_Response_Celeb_profile_details> gsonResponse_celebDetails) {
        Retrofit retrofit = getService();
        APIService apiService = retrofit.create(APIService.class);
        Call<Gson_Response_Celeb_profile_details> call = apiService.CelebrityProfile(user_id,start,limit);

        Log.d("onertyu", call.toString());

        call.enqueue(new Callback<Gson_Response_Celeb_profile_details>() {
            @Override
            public void onResponse(Call<Gson_Response_Celeb_profile_details> call, Response<Gson_Response_Celeb_profile_details> response) {
                if (response.isSuccessful()) {
                    if (response.body().getSuccess() == 1) {
                        try {

                            Gson_Response_Celeb_profile_details celeb_data_response = response.body();

                            gsonResponse_celebDetails.onSuccess(celeb_data_response);

                        } catch (Exception e1) {
                            Log.d("APIErr", e1.getLocalizedMessage());


                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<Gson_Response_Celeb_profile_details> call, Throwable t) {
                gsonResponse_celebDetails.onFailure(call.toString());
            }
        });

    }

    public void UpdateBankDetails(String owner_type, String str_banckname, String str_branch_name, String str_account_number, String str_ifsc_code, String str_beneficiary_name, final ResultHandler<Gson_response_login_data> gson_response_login_dataResultHandler) {

        Retrofit retrofit = getService();
        APIService apiService = retrofit.create(APIService.class);
        Call<Gson_response_login_data> call = apiService.UpdateBankDetails(owner_type, str_banckname, str_branch_name, str_account_number, str_ifsc_code, str_beneficiary_name);

        Log.d("onertyu", call.toString());

        call.enqueue(new Callback<Gson_response_login_data>() {
            @Override
            public void onResponse(Call<Gson_response_login_data> call, Response<Gson_response_login_data> response) {
                if (response.isSuccessful()) {
                    if (response.body().getSuccess() == 1) {
                        try {

                            Gson_response_login_data gson_response_comment = response.body();

                            gson_response_login_dataResultHandler.onSuccess(gson_response_comment);

                        } catch (Exception e1) {
                            Log.d("APIErr", e1.getLocalizedMessage());


                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<Gson_response_login_data> call, Throwable t) {

                gson_response_login_dataResultHandler.onFailure(call.toString());
            }
        });

    }

    public void UpdateProfile_withImage(RequestBody full_name, RequestBody email, RequestBody social_platform_type, RequestBody social_page_name, RequestBody about_me, RequestBody categories_id, RequestBody phone_number, Uri profileUri, final ResultHandler<Gson_response_login_data> gson_response_login_dataResultHandler) {
        Retrofit retrofit = getService();
        final APIService client = retrofit.create(APIService.class);

        File file = new File(profileUri.getPath());

        MultipartBody.Part requestFile = Helper.createMutilPart(file, "profile_pic");

        //  Log.d("file", requestFile.toString());
        Call<Gson_response_login_data> call = client.UpdateCelebProfile_image(
                full_name,
                social_platform_type,
                social_page_name,
                about_me,
                categories_id,
                phone_number,
                requestFile);

        Log.d("REQUEST", call.request().body().toString());

        call.enqueue(new Callback<Gson_response_login_data>() {
            @Override
            public void onResponse(Call<Gson_response_login_data> call, Response<Gson_response_login_data> response) {
                if (response.isSuccessful()) {
                    if (response.body().getSuccess() == 1) {
                        try {

                            Gson_response_login_data gson_response_comment = response.body();

                            gson_response_login_dataResultHandler.onSuccess(gson_response_comment);

                        } catch (Exception e1) {
                            Log.d("APIErr", e1.getLocalizedMessage());


                        }
                    }
                }

            }

            @Override
            public void onFailure(Call<Gson_response_login_data> call, Throwable t) {
                gson_response_login_dataResultHandler.onFailure(call.toString());
            }
        });


    }

    private String getRealPathFromURI(Uri profile_pic) {
        String[] proj = {MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(getApplicationContext(), profile_pic, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }


    public void RequestDetails(Context context, String request_id, final ResultHandler<Gson_Response_Requestdetail> gson_response_requestdetailResultHandler) {
        final ProgressDialog progressDialog = ProgressDialog.show(context, null, "Please wait", true, false);

        Retrofit retrofit = getService();
        APIService apiService = retrofit.create(APIService.class);
        Call<Gson_Response_Requestdetail> call = apiService.RequestDetails(request_id);

        Log.d("onertyu", call.toString());

        call.enqueue(new Callback<Gson_Response_Requestdetail>() {
            @Override
            public void onResponse(Call<Gson_Response_Requestdetail> call, Response<Gson_Response_Requestdetail> response) {
                if (response.isSuccessful()) {

                    progressDialog.dismiss();
                    if (response.body().getSuccess() == 1) {
                        try {

                            Gson_Response_Requestdetail celeb_data_response = response.body();

                            System.out.println(celeb_data_response.getData().toString());


                            gson_response_requestdetailResultHandler.onSuccess(celeb_data_response);

                        } catch (Exception e1) {
                            Log.d("APIErr", e1.getLocalizedMessage());


                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<Gson_Response_Requestdetail> call, Throwable t) {

                progressDialog.dismiss();
                System.out.println(call.toString());

                gson_response_requestdetailResultHandler.onFailure(call.toString());

            }
        });

    }

    public void requestactionVideo(final Context context, final RequestBody request_id, final RequestBody action, File image_url_video, Uri video_thumb, final ResultHandler<Result> resultResultHandler) {

        final ProgressDialog progressDialog11 = ProgressDialog.show(context, null, "Please wait", true, false);


        Retrofit retrofit = getService();
        final APIService apiService = retrofit.create(APIService.class);
        File file = image_url_video;
        File file2 = new File(getRealPathFromURI(video_thumb));


        final ProgressDialog progressdialog = new ProgressDialog(context);

        progressdialog.setIndeterminate(false);

        progressdialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);

        progressdialog.setCancelable(false);
        progressdialog.setTitle("Uploading video...");

        progressdialog.setMax(100);

        progressdialog.show();


        ProgressRequestBody fileBody = new ProgressRequestBody(file, new ProgressRequestBody.UploadCallbacks() {
            @Override
            public void onProgressUpdate(int percentage) {
                progressdialog.setProgress(percentage);
            }

            @Override
            public void onError() {
                Toast.makeText(context, "Uploaded Failed!", Toast.LENGTH_SHORT).show();
                progressdialog.dismiss();
            }

            @Override
            public void onFinish() {
                progressdialog.dismiss();


            }

            @Override
            public void uploadStart() {
                Toast.makeText(context, "Uploaded start!", Toast.LENGTH_SHORT).show();

            }
        });




        MultipartBody.Part requestFile = MultipartBody.Part.createFormData("video", file.getName(), fileBody);
        MultipartBody.Part requestFile2 = Helper.createMutilPart(file2, "video_thumb");


        Call<Result> call = apiService.UpdateCelebVideo(
                request_id,
                action,
                requestFile,
                requestFile2);

        Log.d("onertyu", call.toString());

        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {

                System.out.println(response.toString());
                System.out.println(call.toString());


                progressDialog11.dismiss();
                Result gson_response_comment = response.body();

                resultResultHandler.onSuccess(gson_response_comment);


            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                progressdialog.dismiss();
                progressDialog11.dismiss();
                resultResultHandler.onFailure(call.toString());

                System.out.println(t.toString());
                System.out.println(call.toString());

                // Toast.makeText(context, "Video upload Failed Try again!", Toast.LENGTH_SHORT).show();
                //      // Toast.makeText(context, call.toString(), Toast.LENGTH_SHORT).show();
            }
        });







//
//        MultipartBody.Part requestFile = Helper.createMutilPart(file, "video");
//



    }

    public void UpdateProfileAllFeild(String full_name, String email, String phone_number, String social_platform_type, String social_page_name, String about_me, String categories_id, final ResultHandler<Gson_response_login_data> gson_response_login_dataResultHandler) {
        Retrofit retrofit = getService();
        APIService apiService = retrofit.create(APIService.class);
        Call<Gson_response_login_data> call = apiService.UpdateProfileCelebAllDetails(full_name, phone_number, social_platform_type, social_page_name, about_me, categories_id);

        Log.d("onertyu", call.toString());

        call.enqueue(new Callback<Gson_response_login_data>() {
            @Override
            public void onResponse(Call<Gson_response_login_data> call, Response<Gson_response_login_data> response) {
                if (response.isSuccessful()) {
                    if (response.body().getSuccess() == 1) {
                        try {

                            Gson_response_login_data gson_response_comment = response.body();

                            gson_response_login_dataResultHandler.onSuccess(gson_response_comment);

                        } catch (Exception e1) {
                            Log.d("APIErr", e1.getLocalizedMessage());


                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<Gson_response_login_data> call, Throwable t) {


            }
        });

    }

    public void generatechecksum(String txn_amount, String order_id, final ResultHandler<GsonResponse_checksume> resultResultHandler) {
        Retrofit retrofit = getService();
        APIService apiService = retrofit.create(APIService.class);
        Call<GsonResponse_checksume> call = apiService.Gereratechecksum(txn_amount, order_id);

        Log.d("onertyu", call.toString());

        call.enqueue(new Callback<GsonResponse_checksume>() {
            @Override
            public void onResponse(Call<GsonResponse_checksume> call, Response<GsonResponse_checksume> response) {
                if (response.isSuccessful()) {
                    if (response.body().getSuccess() == 1) {
                        try {

                            GsonResponse_checksume gson_response_comment = response.body();

                            resultResultHandler.onSuccess(gson_response_comment);

                        } catch (Exception e1) {
                            Log.d("APIErr", e1.getLocalizedMessage());


                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<GsonResponse_checksume> call, Throwable t) {


            }
        });
    }

    public void UpdateProfile_introvideo(final Context context, File introvideo, Uri video_thumb, final ResultHandler<Gson_response_login_data> resultResultHandler) {

        final ProgressDialog progressDialog11 = ProgressDialog.show(context, null, "Please wait", true, false);

        Retrofit retrofit = getService();
        APIService apiService = retrofit.create(APIService.class);
        File file = introvideo;
        File file2 = new File(getRealPathFromURI(video_thumb));


        final ProgressDialog progressdialog = new ProgressDialog(context);

        progressdialog.setIndeterminate(false);

        progressdialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);

        progressdialog.setCancelable(false);
        progressdialog.setTitle("Uploading video...");

        progressdialog.setMax(100);

        progressdialog.show();


//

        ProgressRequestBody fileBody = new ProgressRequestBody(file, new ProgressRequestBody.UploadCallbacks() {
            @Override
            public void onProgressUpdate(int percentage) {
                progressdialog.setProgress(percentage);
            }

            @Override
            public void onError() {
                Toast.makeText(context, "Uploaded Failed!", Toast.LENGTH_SHORT).show();
                progressdialog.dismiss();
            }

            @Override
            public void onFinish() {
                progressdialog.dismiss();
            }

            @Override
            public void uploadStart() {

            }
        });




       /* MultipartBody.Part requestFile = Helper.createMutilPart(file, "intro_video");
       */


        MultipartBody.Part requestFile = MultipartBody.Part.createFormData("intro_video", file.getName(), fileBody);
        MultipartBody.Part requestFile2 = Helper.createMutilPart(file2, "video_thumb");

    //    MultipartBody.Part requestFile2 = MultipartBody.Part.createFormData("video_thumb", file2.getName(), fileBody);


        //  Log.d("file", requestFile.toString());
        Call<Gson_response_login_data> call = apiService.UpdateProfile_introvideo(requestFile, requestFile2);

        Log.d("onertyu", call.toString());

        call.enqueue(new Callback<Gson_response_login_data>() {
            @Override
            public void onResponse(Call<Gson_response_login_data> call, Response<Gson_response_login_data> response) {


                progressDialog11.dismiss();
                Gson_response_login_data gson_response_comment = response.body();
                resultResultHandler.onSuccess(gson_response_comment);

            }

            @Override
            public void onFailure(Call<Gson_response_login_data> call, Throwable t) {

                progressdialog.dismiss();
                progressDialog11.dismiss();
                resultResultHandler.onFailure(call.toString());
                //       progressDialog.dismiss();
                // Toast.makeText(context, "Video upload Failed Try again!", Toast.LENGTH_SHORT).show();
            }
        });

    }
}
