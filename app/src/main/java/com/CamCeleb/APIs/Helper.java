package com.CamCeleb.APIs;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class Helper {

    public static RequestBody createRequestBody(String request) {
        return RequestBody.create(MediaType.parse("text/plain"), request);
    }

    public  static MultipartBody.Part createMutilPart(File multipart,String key){
        RequestBody requestBody=RequestBody.create(MediaType.parse("multipart/form-data"), multipart);
        return MultipartBody.Part.createFormData(key,multipart.getName(),requestBody);
    }
}
