package com.CamCeleb.APIs;

import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

import androidx.loader.content.CursorLoader;

import com.CamCeleb.Helper.APIService;
import com.CamCeleb.Helper.ProgressRequestBody;
import com.CamCeleb.MainActivity;
import com.CamCeleb.POJOClass.Carddata;
import com.CamCeleb.POJOClass.Celebdetails.GsonResponse_celebDetails;
import com.CamCeleb.POJOClass.Comment.Comment_send.Gson_response_Comment_send;
import com.CamCeleb.POJOClass.Comment.Gson_response_comment;
import com.CamCeleb.POJOClass.DashboardList;
import com.CamCeleb.POJOClass.Favourite.Gson_Response_Favourite_data;
import com.CamCeleb.POJOClass.Login_model.Gson_response_login_data;
import com.CamCeleb.POJOClass.Notification.Gson_response_notification;
import com.CamCeleb.POJOClass.RequestList.Gson_response_requestlist;
import com.CamCeleb.POJOClass.RequestVideo.Gson_Response_Requestvideo;
import com.CamCeleb.POJOClass.Result;
import com.CamCeleb.POJOClass.ResultHandler;
import com.CamCeleb.POJOClass.SearchData.Gson_Response_search_data;
import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;

import java.io.File;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static com.facebook.FacebookSdk.getApplicationContext;


public class DashBords extends APIManager {

    public void DashBord(final String celeb_id,String start,String limit, final ResultHandler<DashboardList> resultHandler) {

        Log.d("API INVOLKED", "With Value" + celeb_id);

        Retrofit retrofit = getService();
        APIService apiService = retrofit.create(APIService.class);
        Call<Result> call = apiService.UserDashbord("", start, limit);


        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {

                int code = response.body().getCode();
                String e = response.body().getError().toString();
                JsonObject data = response.body().getData();



                /*Log.d("getTokenresponseone", String.valueOf(code));
                Log.d("getTokenresponsecodeone", String.valueOf(e));
                Log.d("getTokenresponserrorone", data.toString());*/


                Gson gson = new Gson();

                try {
                    DashboardList list = gson.fromJson(data.toString(), DashboardList.class);

//                    Log.d("res len", "= " + list.getData().length);

                    resultHandler.onSuccess(list);

                } catch (JsonIOException el) {
                    Log.d("res err", el.getLocalizedMessage());
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {

            }
        });
    }




    public void celebritydetail(String celeb_id, final ResultHandler<GsonResponse_celebDetails> gsonResponse_celebDetails) {

        Retrofit retrofit = getService();
        APIService apiService = retrofit.create(APIService.class);
        Call<GsonResponse_celebDetails> call = apiService.Celebritydetail(celeb_id);

        Log.d("onertyu", call.toString());

        call.enqueue(new Callback<GsonResponse_celebDetails>() {
            @Override
            public void onResponse(Call<GsonResponse_celebDetails> call, Response<GsonResponse_celebDetails> response) {
                if (response.isSuccessful()) {
                    if (response.body().getSuccess() == 1) {
                        try {

                            GsonResponse_celebDetails celeb_data_response = response.body();

                            gsonResponse_celebDetails.onSuccess(celeb_data_response);

                        } catch (Exception e1) {
                            Log.d("APIErr", e1.getLocalizedMessage());


                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<GsonResponse_celebDetails> call, Throwable t) {

            }
        });

    }


    public void CommentList(String start,String limit,String video_id, final ResultHandler<Gson_response_comment> commentResultHandler) {

        Retrofit retrofit = getService();
        APIService apiService = retrofit.create(APIService.class);
        Call<Gson_response_comment> call = apiService.CommentList(start,limit,video_id);

        Log.d("onertyu", call.toString());

        call.enqueue(new Callback<Gson_response_comment>() {
            @Override
            public void onResponse(Call<Gson_response_comment> call, Response<Gson_response_comment> response) {
                if (response.isSuccessful()) {
                    if (response.body().getSuccess() == 1) {
                        try {

                            Gson_response_comment gson_response_comment = response.body();

                            commentResultHandler.onSuccess(gson_response_comment);

                        } catch (Exception e1) {
                            Log.d("APIErr", e1.getLocalizedMessage());


                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<Gson_response_comment> call, Throwable t) {


            }
        });
    }


    public void CommentSend(String video_id, String comment, final ResultHandler<Gson_response_Comment_send> gson_response_comment_sendResultHandler) {
        Retrofit retrofit = getService();
        APIService apiService = retrofit.create(APIService.class);
        Call<Gson_response_Comment_send> call = apiService.CommentSend(video_id, comment);

        Log.d("onertyu", call.toString());

        call.enqueue(new Callback<Gson_response_Comment_send>() {
            @Override
            public void onResponse(Call<Gson_response_Comment_send> call, Response<Gson_response_Comment_send> response) {
                if (response.isSuccessful()) {
                    if (response.body().getSuccess() == 1) {
                        try {

                            Gson_response_Comment_send gson_response_comment = response.body();

                            gson_response_comment_sendResultHandler.onSuccess(gson_response_comment);

                        } catch (Exception e1) {
                            Log.d("APIErr", e1.getLocalizedMessage());


                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<Gson_response_Comment_send> call, Throwable t) {


            }
        });
    }

    public void LikeUnlike(String video_id, final ResultHandler<Result> resultResultHandler) {

        Retrofit retrofit = getService();
        APIService apiService = retrofit.create(APIService.class);
        Call<Result> call = apiService.LikeUnlike(video_id);

        Log.d("onertyu", call.toString());

        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                if (response.isSuccessful()) {
                    if (response.body().getCode() == 1) {
                        try {

                            Result gson_response_comment = response.body();

                            resultResultHandler.onSuccess(gson_response_comment);

                        } catch (Exception e1) {
                            Log.d("APIErr", e1.getLocalizedMessage());


                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {


            }
        });


    }

    public void addremovefavourite(String str_video_id, final ResultHandler<Result> resultResultHandler) {
        Retrofit retrofit = getService();
        APIService apiService = retrofit.create(APIService.class);
        Call<Result> call = apiService.addremovefavourite(str_video_id);

        Log.d("onertyu", call.toString());

        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                if (response.isSuccessful()) {
                    if (response.body().getCode() == 1) {
                        try {

                            Result gson_response_comment = response.body();

                            resultResultHandler.onSuccess(gson_response_comment);

                        } catch (Exception e1) {
                            Log.d("APIErr", e1.getLocalizedMessage());


                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {


            }
        });

    }

    public void RequestvideoWithoutimage(String celeb_id, String friend_name, String phone_number, String email, String instructions, String booking_for, String is_private, final ResultHandler<Gson_Response_Requestvideo> gson_response_requestvideoResultHandler) {

        Retrofit retrofit = getService();
        APIService apiService = retrofit.create(APIService.class);
        Call<Gson_Response_Requestvideo> call = apiService.requestvideowithout_image(celeb_id, friend_name, phone_number, email, instructions, booking_for, is_private);

        Log.d("onertyu", call.toString());

        call.enqueue(new Callback<Gson_Response_Requestvideo>() {
            @Override
            public void onResponse(Call<Gson_Response_Requestvideo> call, Response<Gson_Response_Requestvideo> response) {
                if (response.isSuccessful()) {
                    if (response.body().getSuccess() == 1) {
                        try {

                            Gson_Response_Requestvideo gson_response_comment = response.body();

                            gson_response_requestvideoResultHandler.onSuccess(gson_response_comment);

                        } catch (Exception e1) {
                            Log.d("APIErr", e1.getLocalizedMessage());


                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<Gson_Response_Requestvideo> call, Throwable t) {

            }
        });


    }

    private String getRealPathFromURI(Uri profile_pic) {
        String[] proj = {MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(getApplicationContext(), profile_pic, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }

    public void Requestvideo(RequestBody celeb_id, RequestBody friend_name, RequestBody phone_number, RequestBody email, RequestBody instructions, RequestBody booking_for, RequestBody is_private, Uri str_image_path, String type, final ResultHandler<Gson_Response_Requestvideo> gson_response_requestvideoResultHandler) {

        Retrofit retrofit = getService();
        final APIService client = retrofit.create(APIService.class);


        File file = new File(str_image_path.getPath());


        MultipartBody.Part requestFile = Helper.createMutilPart(file, "friend_image");

        //  Log.d("file", requestFile.toString());

        Call<Gson_Response_Requestvideo> call = client.requestvideo(
                celeb_id,
                friend_name,
                phone_number,
                email,
                instructions,
                booking_for,
                is_private,
                requestFile);

        Log.d("REQUEST", call.request().body().toString());

        call.enqueue(new Callback<Gson_Response_Requestvideo>() {
            @Override
            public void onResponse(Call<Gson_Response_Requestvideo> call, Response<Gson_Response_Requestvideo> response) {
                if (response.isSuccessful()) {
                    if (response.body().getSuccess() == 1) {
                        try {

                            Gson_Response_Requestvideo gson_response_comment = response.body();

                            gson_response_requestvideoResultHandler.onSuccess(gson_response_comment);

                        } catch (Exception e1) {
                            Log.d("APIErr", e1.getLocalizedMessage());


                        }
                    }
                }

            }

            @Override
            public void onFailure(Call<Gson_Response_Requestvideo> call, Throwable t) {

            }
        });

    }

    public void Notifiactionlist(String start, String limit, final ResultHandler<Gson_response_notification> gson_response_notificationResultHandler) {


        Retrofit retrofit = getService();
        APIService apiService = retrofit.create(APIService.class);
        Call<Gson_response_notification> call = apiService.NotificationList(start, limit);


        call.enqueue(new Callback<Gson_response_notification>() {
            @Override
            public void onResponse(Call<Gson_response_notification> call, Response<Gson_response_notification> response) {


                gson_response_notificationResultHandler.onSuccess(response.body());

            }

            @Override
            public void onFailure(Call<Gson_response_notification> call, Throwable t) {
                System.out.println("error " + call.toString());
                System.out.println("error " + t.toString());
            }
        });

    }


    public void DashBordSearch(final String start, String limit, String search_text, final ResultHandler<Gson_Response_search_data> resultHandler) {


        Retrofit retrofit = getService();
        APIService apiService = retrofit.create(APIService.class);
        Call<Gson_Response_search_data> call = apiService.SearchData(search_text, start, limit);


        call.enqueue(new Callback<Gson_Response_search_data>() {
            @Override
            public void onResponse(Call<Gson_Response_search_data> call, Response<Gson_Response_search_data> response) {


                resultHandler.onSuccess(response.body());

            }

            @Override
            public void onFailure(Call<Gson_Response_search_data> call, Throwable t) {

            }
        });
    }


    public void RequestList(String start, String limit, final ResultHandler<Gson_response_requestlist> resultHandler) {

        Retrofit retrofit = getService();
        APIService apiService = retrofit.create(APIService.class);
        Call<Gson_response_requestlist> call = apiService.RequestList(start, limit);


        call.enqueue(new Callback<Gson_response_requestlist>() {
            @Override
            public void onResponse(Call<Gson_response_requestlist> call, Response<Gson_response_requestlist> response) {


                resultHandler.onSuccess(response.body());

            }

            @Override
            public void onFailure(Call<Gson_response_requestlist> call, Throwable t) {

            }
        });


    }

    public void FavouriteList(final ResultHandler<Gson_Response_Favourite_data> gson_response_favourite_dataResultHandler) {

        Retrofit retrofit = getService();
        APIService apiService = retrofit.create(APIService.class);
        Call<Gson_Response_Favourite_data> call = apiService.FavouriteList();


        call.enqueue(new Callback<Gson_Response_Favourite_data>() {
            @Override
            public void onResponse(Call<Gson_Response_Favourite_data> call, Response<Gson_Response_Favourite_data> response) {


                gson_response_favourite_dataResultHandler.onSuccess(response.body());

            }

            @Override
            public void onFailure(Call<Gson_Response_Favourite_data> call, Throwable t) {
                System.out.println(call.toString());
            }
        });
    }

    public void CancelRequest(Context context,String request_id, final ResultHandler<Result> resultResultHandler) {

        final ProgressDialog progressDialog = ProgressDialog.show(context, null, "please wait", true, false);

        Retrofit retrofit = getService();
        APIService apiService = retrofit.create(APIService.class);
        Call<Result> call = apiService.CancelRequest(request_id);

        Log.d("onertyu", call.toString());

        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                if (response.isSuccessful()) {
                    progressDialog.dismiss();
                    if (response.body().getCode() == 1) {

                        Result gson_response_comment = response.body();

                        resultResultHandler.onSuccess(gson_response_comment);


                    }
                } else {
                    progressDialog.dismiss();
                    Result gson_response_comment = response.body();
                    resultResultHandler.onSuccess(gson_response_comment);
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                progressDialog.dismiss();

            }
        });


    }

    public void UpdateProfile_withImage(Context context,RequestBody full_name, RequestBody phone_number, Uri profileUri, final ResultHandler<Gson_response_login_data> gson_response_login_dataResultHandler) {

        Retrofit retrofit = getService();
        final APIService client = retrofit.create(APIService.class);
        File file = new File(profileUri.getPath());


//        File file = new File(getRealPathFromURI(profileUri));
      /*

        final ProgressDialog  progressdialog = new ProgressDialog(context);

        progressdialog.setIndeterminate(false);

        progressdialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        progressdialog.setCancelable(true);

        progressdialog.setMax(100);

        progressdialog.show();


//

        ProgressRequestBody fileBody = new ProgressRequestBody(file, new ProgressRequestBody.UploadCallbacks() {
            @Override
            public void onProgressUpdate(int percentage) {
                progressdialog.setProgress(percentage);
            }

            @Override
            public void onError() {

            }

            @Override
            public void onFinish() {
                progressdialog.dismiss();
            }

            @Override
            public void uploadStart() {

            }
        });
        MultipartBody.Part requestFile = MultipartBody.Part.createFormData("profile_pic", file.getName(), fileBody);
*/
        MultipartBody.Part requestFile = Helper.createMutilPart(file, "profile_pic");

        //  Log.d("file", requestFile.toString());
        Call<Gson_response_login_data> call = client.UpdateProfile_image(
                full_name,
                phone_number,
                requestFile);

        Log.d("REQUEST", call.request().body().toString());

        call.enqueue(new Callback<Gson_response_login_data>() {
            @Override
            public void onResponse(Call<Gson_response_login_data> call, Response<Gson_response_login_data> response) {
                if (response.isSuccessful()) {
                    if (response.body().getSuccess() == 1) {
                        try {

                            Gson_response_login_data gson_response_comment = response.body();

                            gson_response_login_dataResultHandler.onSuccess(gson_response_comment);

                        } catch (Exception e1) {
                            Log.d("APIErr", e1.getLocalizedMessage());


                        }
                    }
                }

            }

            @Override
            public void onFailure(Call<Gson_response_login_data> call, Throwable t) {
                gson_response_login_dataResultHandler.onFailure(call.toString());
            }
        });


    }

    public void UpdateProfile(String full_name, String phone_number, final ResultHandler<Gson_response_login_data> gson_response_login_dataResultHandler) {

        Retrofit retrofit = getService();
        APIService apiService = retrofit.create(APIService.class);
        Call<Gson_response_login_data> call = apiService.UpdateProfile(full_name, phone_number);

        Log.d("onertyu", call.toString());

        call.enqueue(new Callback<Gson_response_login_data>() {
            @Override
            public void onResponse(Call<Gson_response_login_data> call, Response<Gson_response_login_data> response) {
                if (response.isSuccessful()) {
                    if (response.body().getSuccess() == 1) {
                        try {

                            Gson_response_login_data gson_response_comment = response.body();

                            gson_response_login_dataResultHandler.onSuccess(gson_response_comment);

                        } catch (Exception e1) {
                            Log.d("APIErr", e1.getLocalizedMessage());


                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<Gson_response_login_data> call, Throwable t) {
                gson_response_login_dataResultHandler.onFailure(call.toString());

            }
        });


    }



    public void PaymentResponse(String order_id,String transection_id, String status, final ResultHandler<Result> resultResultHandler) {
        Retrofit retrofit = getService();
        APIService apiService = retrofit.create(APIService.class);
        Call<Result> call = apiService.Paymentresponse(order_id,transection_id,"", status);

        Log.d("onertyu", call.toString());

        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                if (response.isSuccessful()) {
                    if (response.body().getCode() == 1) {

                        Result gson_response_comment = response.body();

                        resultResultHandler.onSuccess(gson_response_comment);
                    }
                } else {
                    Result gson_response_comment = response.body();
                    resultResultHandler.onSuccess(gson_response_comment);
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {


            }
        });


    }

    public void RequestvideoWithoutimageUpdate(Context context, String request_id, String friend_name, String phone_number, String email, String instructions, String booking_for, String is_private, final ResultHandler<Gson_Response_Requestvideo> gson_response_requestvideoResultHandler) {
        final ProgressDialog progressDialog = ProgressDialog.show(context, null, "please wait", true, false);

        Retrofit retrofit = getService();
        APIService apiService = retrofit.create(APIService.class);
        Call<Gson_Response_Requestvideo> call = apiService.requestvideowithout_image_updaterequest(request_id, friend_name, phone_number, email, instructions, booking_for, is_private);

        Log.d("onertyu", call.toString());

        call.enqueue(new Callback<Gson_Response_Requestvideo>() {
            @Override
            public void onResponse(Call<Gson_Response_Requestvideo> call, Response<Gson_Response_Requestvideo> response) {
                if (response.isSuccessful()) {
                    if (response.body().getSuccess() == 1) {
                        progressDialog.dismiss();
                        try {

                            Gson_Response_Requestvideo gson_response_comment = response.body();

                            gson_response_requestvideoResultHandler.onSuccess(gson_response_comment);

                        } catch (Exception e1) {
                            Log.d("APIErr", e1.getLocalizedMessage());
                            progressDialog.dismiss();

                        }
                    } else {
                        progressDialog.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(Call<Gson_Response_Requestvideo> call, Throwable t) {
                progressDialog.dismiss();
            }
        });

    }

    public void Requestvideoupdate(Context context,RequestBody request_id, RequestBody friend_name, RequestBody phone_number, RequestBody email, RequestBody instructions, RequestBody booking_for, RequestBody is_private, Uri str_image_path, String type, final ResultHandler<Gson_Response_Requestvideo> gson_response_requestvideoResultHandler) {
        final ProgressDialog progressDialog = ProgressDialog.show(context, null, "please wait", true, false);

        Retrofit retrofit = getService();
        final APIService client = retrofit.create(APIService.class);


        File file = new File(str_image_path.getPath());


        MultipartBody.Part requestFile = Helper.createMutilPart(file, "friend_image");

        //  Log.d("file", requestFile.toString());

        Call<Gson_Response_Requestvideo> call = client.requestvideoUpdate(
                request_id,
                friend_name,
                phone_number,
                email,
                instructions,
                booking_for,
                is_private,
                requestFile);

        Log.d("REQUEST", call.request().body().toString());

        call.enqueue(new Callback<Gson_Response_Requestvideo>() {
            @Override
            public void onResponse(Call<Gson_Response_Requestvideo> call, Response<Gson_Response_Requestvideo> response) {
                if (response.isSuccessful()) {
                    if (response.body().getSuccess() == 1) {
                        progressDialog.dismiss();

                        try {

                            Gson_Response_Requestvideo gson_response_comment = response.body();

                            gson_response_requestvideoResultHandler.onSuccess(gson_response_comment);

                        } catch (Exception e1) {
                            Log.d("APIErr", e1.getLocalizedMessage());


                        }
                    }else {
                        progressDialog.dismiss();

                    }
                }

            }

            @Override
            public void onFailure(Call<Gson_Response_Requestvideo> call, Throwable t) {
                progressDialog.dismiss();

            }
        });

    }

    public void SaveFeedback(String str_id, String str_ratign, String str_feedback, final ResultHandler<Result> resultResultHandler) {

        Retrofit retrofit = getService();
        APIService apiService = retrofit.create(APIService.class);
        Call<Result> call = apiService.SaveFeedback(str_id,str_ratign,str_feedback);

        Log.d("onertyu", call.toString());

        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                if (response.isSuccessful()) {
                    if (response.body().getCode() == 1) {
                        try {

                            Result gson_response_comment = response.body();

                            resultResultHandler.onSuccess(gson_response_comment);

                        } catch (Exception e1) {
                            Log.d("APIErr", e1.getLocalizedMessage());


                        }
                    }else {

                        Result gson_response_comment = response.body();

                        resultResultHandler.onSuccess(gson_response_comment);
                    }
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {


            }
        });


    }

    public void flagunflag(Integer id, final ResultHandler<Gson_response_comment> commentResultHandler) {
        Retrofit retrofit = getService();
        APIService apiService = retrofit.create(APIService.class);
        Call<Gson_response_comment> call = apiService.flagunflag(String.valueOf(id));

        Log.d("onertyu", call.toString());

        call.enqueue(new Callback<Gson_response_comment>() {
            @Override
            public void onResponse(Call<Gson_response_comment> call, Response<Gson_response_comment> response) {
                if (response.isSuccessful()) {
                    if (response.body().getSuccess() == 1) {
                        try {

                            Gson_response_comment gson_response_comment = response.body();

                            commentResultHandler.onSuccess(gson_response_comment);

                        } catch (Exception e1) {
                            Log.d("APIErr", e1.getLocalizedMessage());


                        }
                    }else {

                        Gson_response_comment gson_response_comment = response.body();

                        commentResultHandler.onSuccess(gson_response_comment);
                    }
                }
            }

            @Override
            public void onFailure(Call<Gson_response_comment> call, Throwable t) {


            }
        });

    }
}