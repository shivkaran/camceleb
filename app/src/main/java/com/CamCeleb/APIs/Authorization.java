package com.CamCeleb.APIs;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import androidx.loader.content.CursorLoader;

import com.CamCeleb.Helper.APIService;
import com.CamCeleb.POJOClass.Login_model.Gson_response_login_data;
import com.CamCeleb.POJOClass.OTPVerification.Gson_Response_OTP_Resend;
import com.CamCeleb.POJOClass.Result;
import com.CamCeleb.POJOClass.ResultHandler;
import com.CamCeleb.R;
import com.google.gson.JsonObject;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static com.facebook.FacebookSdk.getApplicationContext;

public class Authorization extends APIManager {


    public void userSignUp(final Context context, final RequestBody user_type, final RequestBody full_names, final RequestBody email, final RequestBody password, final RequestBody phone_number,
                           RequestBody booking_price, RequestBody social_page_names, RequestBody social_platform_types, final RequestBody followers_count, RequestBody categories,
                           RequestBody about_me, final Uri profile_pic, final ResultHandler<Gson_response_login_data> resultHandler) {
        final ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.show();

        Retrofit retrofit = getService();
        final APIService client = retrofit.create(APIService.class);


        File file = new File(profile_pic.getPath());


        MultipartBody.Part requestFile = Helper.createMutilPart(file, "profile_pic");

        //  Log.d("file", requestFile.toString());

        Call<Gson_response_login_data> call = client.RegisterUser(user_type,
                full_names,
                email,
                password,
                phone_number,
                booking_price,
                social_platform_types,
                social_page_names,
                followers_count,
                categories,
                about_me,
                requestFile);

        Log.d("REQUEST", call.request().body().toString());
        //calling the api
        call.enqueue(new Callback<Gson_response_login_data>() {
            @Override
            public void onResponse(Call<Gson_response_login_data> call, Response<Gson_response_login_data> response) {

                progressDialog.dismiss();


                resultHandler.onSuccess(response.body());

            }

            @Override
            public void onFailure(Call<Gson_response_login_data> call, Throwable t) {
                progressDialog.dismiss();
                Log.e("Errrrrr", t.getMessage());
                ShowDailog(context, "Something went wrong try again!");
            }
        });


    }

    private void ShowDailog(Context context, String message) {


        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dailog_layout);

        TextView text = (TextView) dialog.findViewById(R.id.txt_message);
        text.setText(message.toString());

        Button dialogButton = (Button) dialog.findViewById(R.id.submit);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

        dialog.show();


    }


    private String getRealPathFromURI(Uri profile_pic) {
        String[] proj = {MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(getApplicationContext(), profile_pic, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }


    public void Forgotpassword(String emailid, final ResultHandler<Gson_Response_OTP_Resend> resultHandler) {


        Retrofit retrofit = new APIManager().getService();
        final APIService client = retrofit.create(APIService.class);
        Call<Gson_Response_OTP_Resend> call = client.Forgotpassword(emailid);

        call.enqueue(new Callback<Gson_Response_OTP_Resend>() {
            @Override
            public void onResponse(Call<Gson_Response_OTP_Resend> call, Response<Gson_Response_OTP_Resend> response) {


                // Toast.makeText(getApplicationContext(), response.body().toString(), Toast.LENGTH_LONG).show();

         /*  Log.d("getTokenresponseone", String.valueOf(code));
             Log.d("getTokenresponsecodeone", String.valueOf(e));
             Log.d("getTokenresponserrorone", String.valueOf(data));*/

                resultHandler.onSuccess(response.body());

            }

            @Override
            public void onFailure(Call<Gson_Response_OTP_Resend> call, Throwable t) {

                // Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

    }


    public void ResendOtp(String email, String otp, final ResultHandler<Gson_Response_OTP_Resend> userResultHandler) {
        Retrofit retrofit = new APIManager().getService();
        final APIService client = retrofit.create(APIService.class);
        Call<Gson_Response_OTP_Resend> call = client.ResendOtp(email, otp);

        //calling the api
        call.enqueue(new Callback<Gson_Response_OTP_Resend>() {
            @Override
            public void onResponse(Call<Gson_Response_OTP_Resend> call, Response<Gson_Response_OTP_Resend> response) {

                userResultHandler.onSuccess(response.body());


            }

            @Override
            public void onFailure(Call<Gson_Response_OTP_Resend> call, Throwable t) {

                userResultHandler.onFailure(call.toString());
                // Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

    }


    public void VerificationOTP(String email, String otp, final ResultHandler<Gson_Response_OTP_Resend> userResultHandler) {
        Retrofit retrofit = new APIManager().getService();
        final APIService client = retrofit.create(APIService.class);
        Call<Gson_Response_OTP_Resend> call = client.VerificationOtp(email, otp);

        //calling the api
        call.enqueue(new Callback<Gson_Response_OTP_Resend>() {
            @Override
            public void onResponse(Call<Gson_Response_OTP_Resend> call, Response<Gson_Response_OTP_Resend> response) {

                userResultHandler.onSuccess(response.body());


            }

            @Override
            public void onFailure(Call<Gson_Response_OTP_Resend> call, Throwable t) {

                userResultHandler.onFailure(call.toString());

                // Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    public void Changepassword(String old_password, String new_password, final ResultHandler<Result> handler) {

        Retrofit retrofit = new APIManager().getService();
        final APIService apiService = retrofit.create(APIService.class);
        Call<Result> call = apiService.Changepassword(old_password, new_password);

        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                int code = response.body().getCode();
                String e = response.body().getError().toString();
                JsonObject data = response.body().getData();


                handler.onSuccess(response.body());
                       /* Log.d("getTokenresponseone", String.valueOf(code));
                        Log.d("getTokenresponsecodeone", String.valueOf(e));
                        Log.d("getTokenresponserrorone", String.valueOf(data));*/
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                // Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }


    public void SocialLogindata(final String social_type, final String is_provided_by_user, final String email, final String social_id, String full_name, String Phonenumvber, final ResultHandler<Gson_response_login_data> handler) {

        Retrofit retrofit = new APIManager().getService();
        final APIService apiService = retrofit.create(APIService.class);
        Call<Gson_response_login_data> call = apiService.SocialLogin(social_type, is_provided_by_user, email, social_id, full_name, Phonenumvber);

        call.enqueue(new Callback<Gson_response_login_data>() {
            @Override
            public void onResponse(Call<Gson_response_login_data> call, Response<Gson_response_login_data> response) {

                handler.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<Gson_response_login_data> call, Throwable t) {
                // Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                handler.onFailure(t.getMessage());
            }
        });
    }


    public void LoginData(String emailid, String passworddata, String usertype, final ResultHandler<Gson_response_login_data> handler) {

        Retrofit retrofit = new APIManager().getService();
        APIService apiService = retrofit.create(APIService.class);

        Call<Gson_response_login_data> call = apiService.UserLogin(emailid, passworddata, usertype);

        call.enqueue(new Callback<Gson_response_login_data>() {
            @Override
            public void onResponse(Call<Gson_response_login_data> call, Response<Gson_response_login_data> response) {

                handler.onSuccess(response.body());

            }

            @Override
            public void onFailure(Call<Gson_response_login_data> call, Throwable t) {
                handler.onFailure(call.toString());

            }
        });


    }


    public void Logout(Context context, final ResultHandler<JsonObject> userResultHandler) {
        final ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.show();
        Retrofit retrofit = new APIManager().getService();

        final APIService client = retrofit.create(APIService.class);
        final Call<Result> calls = client.Logout();

        calls.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {

                int code = response.body().getCode();
                String e = response.body().getError().toString();
                JsonObject data = response.body().getData();
                progressDialog.dismiss();
                userResultHandler.onSuccess(data);
                // Toast.makeText(getApplicationContext(), "Logout Successfully", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });


    }


    public void CelebVideoAccepct(RequestBody request_id, RequestBody action, final Uri video, final Uri video_thumb, final ResultHandler<JsonObject> objectResultHandler) {

        Retrofit retrofit = new APIManager().getService();
        APIService apiService = retrofit.create(APIService.class);

        Bitmap thumb = ThumbnailUtils.createVideoThumbnail(video.getPath(),
                MediaStore.Images.Thumbnails.MINI_KIND);


        Log.d("Thumb", video_thumb.toString());

        /*MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        retriever.setDataSource("video_thumb", new HashMap<String, String>());
        Bitmap image = retriever.getFrameAtTime(2000000, MediaMetadataRetriever.OPTION_CLOSEST_SYNC);*/

//        Log.d("v", video_thumb.toString());

        File file = new File(getRealPathFromURI1(video));
        File file1 = new File(getApplicationContext().getCacheDir(), video_thumb.getPath());

        MultipartBody.Part requestFile = Helper.createMutilPart(file, "video");
        MultipartBody.Part part = Helper.createMutilPart(file, "video_thumb");


        RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file1);
        MultipartBody.Part body = MultipartBody.Part.createFormData("upload", file.getName(), reqFile);

        //Log.d("file", requestFile.toString());

        Call<Result> call = apiService.VideoAccepct(request_id, action, requestFile, body);


        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                // Toast.makeText(getApplicationContext(), response.body().toString(), Toast.LENGTH_LONG).show();
                int code = response.body().getCode();
                String e = response.body().getError().toString();
                JsonObject data = response.body().getData();


                Log.d("getTokenresponseone", String.valueOf(code));
                Log.d("getTokenresponsecodeone", e.toString());
                Log.d("getTokenresponserrorone", data.toString());


                if (code == 1) {
                    // Toast.makeText(getApplicationContext(), response.body().getData().toString(), Toast.LENGTH_SHORT).show();


                    objectResultHandler.onSuccess(data);


                } else {
//                    String e = response.body().getError().toString();

                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {

            }
        });

    }

    private String getRealPathFromURI1(Uri video) {
        String[] proj = {MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(getApplicationContext(), video, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }


}
