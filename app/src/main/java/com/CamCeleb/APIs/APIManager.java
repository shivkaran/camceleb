package com.CamCeleb.APIs;

import android.util.Log;

import com.CamCeleb.Insta.Urls;
import com.CamCeleb.POJOClass.AuthToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIManager {

    AuthToken auth_token;

    public Retrofit getService() {

        auth_token = new AuthToken();


        Log.d("TOKENVALUE", auth_token.getAuthToken());

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        httpClient.connectTimeout(100, TimeUnit.MINUTES);
        httpClient.readTimeout(100, TimeUnit.MINUTES);
        httpClient.writeTimeout(100,TimeUnit.MINUTES);
        httpClient.addInterceptor(new Interceptor() {
                                      @Override
                                      public Response intercept(Interceptor.Chain chain) throws IOException {
                                          Request original = chain.request();

                                          Request request = original.newBuilder()
                                                  .header("Authorization", auth_token.getAuthToken())
                                                  .method(original.method(), original.body())
                                                  .build();

                                          return chain.proceed(request);
                                      }
                                  });


                OkHttpClient client = httpClient.build();




        return new Retrofit.Builder()
                .baseUrl(Urls.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)

                .build();
    }
}





//        OkHttpClient c=new OkHttpClient();
//       OkHttpClient.Builder builder=new OkHttpClient.Builder();
// OkHttpClient c = new OkHttpClient();
//   OkHttpClient.Builder clientBuilder = c.newBuilder();

//Create a new Interceptor.
     /*   Interceptor headerAuthorizationInterceptor = new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                okhttp3.Request request = chain.request();
                Headers headers = request.headers().newBuilder().add("Authorization", auth_token.getAuthToken()).build();
                request = request.newBuilder().headers(headers).build();
                return chain.proceed(request);
            }
        };
        //Add the interceptor to the client builder.
        clientBuilder.addInterceptor(headerAuthorizationInterceptor);*/







//        builder.addInterceptor(new Interceptor() {
//            @Override
//            public Response intercept(Chain chain) throws IOException {
//                Request request=chain.request();
//
//                Request.Builder builder1=request.newBuilder().header("Authorization", auth_token.getAuthToken());
//                Request request1=builder1.build();
//
//
//                return chain.proceed(request1);
//            }
//        });
//
//
//
//        return new Retrofit.Builder()
//                .baseUrl(Urls.BASE_URL)
//                .client(c)
//                .addConverterFactory(GsonConverterFactory.create())
//                .build();