package com.CamCeleb;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.PixelFormat;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.CamCeleb.APIs.APIManager;
import com.CamCeleb.Activity.LoginAndSignActivity;
import com.CamCeleb.CelebSide.CelebHomeActivity;
import com.CamCeleb.CelebSide.Request_Details_screen;
import com.CamCeleb.Helper.APIService;
import com.CamCeleb.Insta.Urls;
import com.CamCeleb.POJOClass.AuthToken;
import com.CamCeleb.POJOClass.MasterModel;
import com.CamCeleb.POJOClass.Result;
import com.CamCeleb.POJOClass.ResultHandler;
import com.CamCeleb.POJOClass.User;
import com.CamCeleb.UserSide.UserHomeActivity;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.Manifest.permission.WAKE_LOCK;


public class SplashActivity extends AppCompatActivity {

    String Tokentype;
    AuthToken auth_token;
    String str;
    String deviceToken;
    MasterModel masterModel;
    public static final String Device_id = "deviceId";
    public static SharedPreferences preferences;

    ImageView img_icon;
    String request_id = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getSupportActionBar().hide();

     /*   getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON |
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON | PixelFormat.TRANSLUCENT);
*/

        preferences = getSharedPreferences(getPackageName() + "_preferences", MODE_PRIVATE);

        auth_token = new AuthToken();

        img_icon = findViewById(R.id.img_icon);



       /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(this,
                WAKE_LOCK)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{WAKE_LOCK},
                    100);


        }

*/

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                deviceToken = instanceIdResult.getToken();

                Log.d("LogGetdata", deviceToken);
//                InsertDevice(deviceToken);
            }
        });


        String session = preferences.getString("SessionData", "");
        Log.d("EMAILNAME", session);


        if (session == "") {

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    InsertDevice(deviceToken);
                }
            }, 2000);


        } else {
            Gson gson = new Gson();
            User user = gson.fromJson(session, User.class);   //get data from response

            if (user.getUserType() == 1) {

                if (getIntent().getStringExtra("request_id") != null) {

                    request_id = getIntent().getStringExtra("request_id");
                    Intent intent = new Intent(SplashActivity.this, Request_Details_screen.class);
                    intent.putExtra("request_id", request_id);
                    startActivity(intent);
                    finish();

                } else {

                    if (user.getIdVerification().equals("noverify")) {

                        Intent intent = new Intent(SplashActivity.this, LoginAndSignActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        Intent intent = new Intent(SplashActivity.this, UserHomeActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }

            } else {
                String Admin_verification = user.getAdmin_verification();
                if (getIntent().getStringExtra("request_id") != null) {
                    request_id = getIntent().getStringExtra("request_id");
                    Intent intent = new Intent(SplashActivity.this, Request_Details_screen.class);
                    intent.putExtra("request_id", request_id);
                    startActivity(intent);
                    finish();
                } else {
                    if (user.getIdVerification().equals("noverify")) {
                        Intent intent = new Intent(SplashActivity.this, LoginAndSignActivity.class);
                        startActivity(intent);
                        finish();
                    } else if (Admin_verification.equalsIgnoreCase("N")) {
                        Intent intent = new Intent(SplashActivity.this, LoginAndSignActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        Intent intent = new Intent(SplashActivity.this, CelebHomeActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }
            }
        }



/*
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                String session = preferences.getString("SessionData", "");
                Log.d("EMAILNAME", session);


                if (session == "") {
//                    Intent intent = new Intent(SplashActivity.this, MainActivity.class);
//                    startActivity(intent);

                } else {
                    Gson gson = new Gson();
                    User user = gson.fromJson(session, User.class);   //get data from response

                    if (user.getUserType() == 1) {

                        if (user.getIdVerification().equals("noverify")) {
                            Intent intent = new Intent(SplashActivity.this, LoginAndSignActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            Intent intent = new Intent(SplashActivity.this, UserHomeActivity.class);
                            startActivity(intent);
                            finish();
                        }

                    } else {
                        if (user.getIdVerification().equals("noverify")) {
                            Intent intent = new Intent(SplashActivity.this, LoginAndSignActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            Intent intent = new Intent(SplashActivity.this, CelebHomeActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    }
                }


            }
        }, 2000);*/


    }

    private void InsertDevice(String deviceToken) {


        if (auth_token.getAuthToken() == "") {

            String tokenids = deviceToken;

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Urls.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            //Defining retrofit api service
            APIService service = retrofit.create(APIService.class);

            Call<com.CamCeleb.POJOClass.Result> call = service.createUser(
                    tokenids,
                    "A"
            );
            Log.d("MainData", String.valueOf(tokenids));
            call.enqueue(new Callback<com.CamCeleb.POJOClass.Result>() {

                @Override
                public void onResponse(Call<com.CamCeleb.POJOClass.Result> call, Response<com.CamCeleb.POJOClass.Result> response) {
                    // progressDialog.dismiss();
                    Log.d("about_us", response.body().getData().toString());

                    int code = response.body().getCode();
                    String e = response.body().getError().toString();
                    JsonObject data = response.body().getData();

                    if (code == 1) {

                        JSONObject hashMap = null;
                        try {
                            hashMap = new JSONObject(data.get("token").toString());


                            String _token = "";
                            String _type = "";

                            try {
                                _token = hashMap.get("token").toString();
                                _type = hashMap.get("type").toString();

                            } catch (JSONException ex) {
                                ex.printStackTrace();
                            }

                            AuthToken t = new AuthToken();// Save Token From POJO CLASS
                            t.setToken(_token);
                            t.setType(_type);
                            t.saveAuthToken();

                            LoadMasterData(new ResultHandler<MasterModel>() {
                                @Override
                                public void onSuccess(MasterModel data) {
                                    Log.d("about_us", data.getAboutus());
                                    Log.d("terms", data.getTerms());
                                    Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                                    startActivity(intent);
                                    finish();
                                }

                                @Override
                                public void onFailure(String e) {

                                    ShowDailog(e.toString());


                                }
                            });


                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }
                        // LoadMasterData();

                    } else {
                        ShowDailog(e.toString());

                    }
                }

                @Override
                public void onFailure(Call<com.CamCeleb.POJOClass.Result> call, Throwable t) {
                    //progressDialog.dismiss();
                    ShowDailog("Something went wrong please try again!");

                }
            });
        } else {

            LoadMasterData(new ResultHandler<MasterModel>() {
                @Override
                public void onSuccess(MasterModel data) {
                    Log.d("about_us_123", data.getAboutus());
                    Log.d("terms", data.getTerms());
                    Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }

                @Override
                public void onFailure(String e) {
                    ShowDailog(e.toString());

                }
            });

        }

    }

    private void ShowDailog(String login_failed_please_try_again) {

        final Dialog dialog = new Dialog(SplashActivity.this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dailog_layout);

        TextView text = (TextView) dialog.findViewById(R.id.txt_message);
        text.setText(login_failed_please_try_again);

        Button dialogButton = (Button) dialog.findViewById(R.id.submit);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }


    private void LoadMasterData(final ResultHandler<MasterModel> userResultHandler) {

        Retrofit retrofit = new APIManager().getService();

        final APIService client = retrofit.create(APIService.class);
        final Call<Result> calltargetResponse = client.getMasterData();


        calltargetResponse.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                Result UserResponse = response.body();

//                int code = response.body().getCode();
//                String[] e = response.body().getError();
                JsonObject data = response.body().getData();


                JSONObject jsonObject = null;
                JSONArray jsonElements = null;
                JSONObject cmsObject = null;

                try {

                    jsonObject = new JSONObject(data.toString());
                    cmsObject = jsonObject.getJSONObject("cms_pages");


                    jsonElements = jsonObject.getJSONArray("categories");


                    String _Faqs = "";
                    String _Terms = "";
                    String _Privacy = "";
                    String _about_us = "";


                    try {

                        _Faqs = cmsObject.get("faqs").toString();
                        _Terms = cmsObject.get("terms").toString();
                        _Privacy = cmsObject.get("privacy").toString();

                        _about_us = cmsObject.has("_about_us") ? jsonObject.getString("_about_us") : "";

//                        _about_us = cmsObject.get("_about_us").toString();


                    } catch (JSONException ex) {
                        ex.printStackTrace();
                        // Log.d("error", ex.getMessage());
                    }

                    MasterModel t = new MasterModel();

                    t.SaveSession();


                    SharedPreferences.Editor ed = SplashActivity.preferences.edit();
                    ed.putString("faqs", _Faqs); //
                    ed.putString("terms", _Terms); //
                    ed.putString("privacy", _Privacy); //
                    ed.putString("_about_us", _about_us); //
                    ed.putString("categorylist", jsonElements.toString()); //
                    ed.commit();


                    t.setFaqs(_Faqs);
                    t.setTerms(_Terms);
                    t.setPrivacy(_Privacy);
                    t.setAboutus(_about_us);
                    t.setCategoryfields(jsonElements);

                    //  Log.d("Onedata", cmsObject.toString());

                    userResultHandler.onSuccess(t);

                } catch (Exception ex) {

                }


            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {

                ShowDailog("Something went wrong please try again!");

            }
        });
    }
}
