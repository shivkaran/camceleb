package com.CamCeleb.Firebase_notification;

import android.annotation.SuppressLint;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.CamCeleb.Helper.BadgeUtils;
import com.CamCeleb.R;
import com.CamCeleb.SplashActivity;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    String request_id = "";
    String type = "";

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

//        sendRegistrationToServer(refreshedToken);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {


        System.out.println("Response notifiaction " + remoteMessage.getData());


        String badge = remoteMessage.getData().get("badge");
        String video_id = remoteMessage.getData().get("video_id");
        type = remoteMessage.getData().get("type");
        request_id = remoteMessage.getData().get("request_id");
        String message = remoteMessage.getData().get("message");


        Log.d(TAG, "Refreshed token: " + request_id);





     /*   try {
            JSONObject jsonObject = new JSONObject(badge);

            String str_badge = jsonObject.getString("badge");


        } catch (JSONException e) {
            e.printStackTrace();
        }
*/

        if (remoteMessage.getNotification() != null) {

            sendNotification(remoteMessage.getData().get("message"), type);
        }


        System.out.println(message);
        if (remoteMessage.getData().size() > 0) {
            message = remoteMessage.getData().get("message");

            sendNotification(message, type);

        }

        //Displaying a notiffication with the message

        Intent in = new Intent("pushnotifiaction");
        Bundle extras = new Bundle();
        extras.putString("request_id", request_id);
        extras.putString("type", type);
        extras.putString("badge", badge);
        in.putExtras(extras);
        sendBroadcast(in);

    }


    private void sendNotification(String message, String type) {


        String CHANNEL_ID = "my_channel_01";
        CharSequence name = "my_channel";
        String Description = "This is my channel";

        int NOTIFICATION_ID = 234;

        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);

        if (!powerManager.isInteractive()) { // if screen is not already on, turn it on (get wake_lock for 10 seconds)
            @SuppressLint("InvalidWakeLockTag") PowerManager.WakeLock wl = powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.ON_AFTER_RELEASE, "MH24_SCREENLOCK");
            wl.acquire(10000);
            @SuppressLint("InvalidWakeLockTag") PowerManager.WakeLock wl_cpu = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MH24_SCREENLOCK");
            wl_cpu.acquire(10000);
        }


        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {


            AudioAttributes att = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                    .build();

            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            mChannel.setDescription(message);
            //       mChannel.setSound(Uri.parse("android.resource://" + getApplication().getPackageName() + "/" + R.raw.user),att);
            mChannel.enableLights(true);
            mChannel.setLightColor(Color.RED);
            mChannel.enableVibration(true);
            mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            mChannel.setShowBadge(true);
            mChannel.setLockscreenVisibility(NOTIFICATION_ID);
            mChannel.setName(capitaliseName(type));
            mChannel.setImportance(importance);
            mChannel.setShowBadge(true);

            if (notificationManager != null) {

                notificationManager.createNotificationChannel(mChannel);
            }

        }

        Intent resultIntent = new Intent(this, SplashActivity.class);
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        resultIntent.putExtra("request_id", request_id);
        resultIntent.putExtra("type", type);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(SplashActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);


        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentText(message)
                .setContentTitle(capitaliseName(type))
                .setSmallIcon(R.mipmap.splash_icon)
                .setContentIntent(resultPendingIntent)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL)
                .setNumber(1)
                .setDefaults(NotificationCompat.DEFAULT_ALL)
                .setPriority(NotificationCompat.PRIORITY_HIGH) // or NotificationCompat.PRIORITY_MAX
                //   .setSound(Uri.parse("android.resource://" + getApplication().getPackageName() + "/" + R.raw.user))
                .setAutoCancel(true);


        int unOpenCount = 0;
        unOpenCount = unOpenCount + 1;

//        AuthToken.savePreferenceLong("NOTICOUNT",unOpenCount,this);

        SharedPreferences.Editor ed =  getSharedPreferences(getPackageName() + "_preferences", MODE_PRIVATE).edit();
        ed.putString("NOTICOUNT", String.valueOf(unOpenCount));
        ed.commit();
        BadgeUtils.setBadge(MyFirebaseMessagingService.this, (int) unOpenCount);


        if (notificationManager != null) {

            notificationManager.notify(NOTIFICATION_ID, builder.build());
        }
    }


    public static String capitaliseName(String name) {
        String collect[] = name.split(" ");
        String returnName = "";
        for (int i = 0; i < collect.length; i++) {
            collect[i] = collect[i].trim().toLowerCase();
            if (collect[i].isEmpty() == false) {
                returnName = returnName + collect[i].substring(0, 1).toUpperCase() + collect[i].substring(1) + " ";
            }
        }
        returnName = returnName.replace("_", " ");

        return returnName.trim();
    }
}
